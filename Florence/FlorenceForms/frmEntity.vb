' ***********************************************************************
' Assembly         : Florence
' Author           : Nicholas Pennington
' Created          : 11-28-2012
'
' Last Modified By : acullig
' Last Modified On : 03-01-2013
' ***********************************************************************
' <copyright file="frmEntity.vb" company="">
'     Copyright (c) . All rights reserved.
' </copyright>
' <summary></summary>
' ***********************************************************************
Imports System.Data.SqlClient
Imports RenaissanceGlobals
Imports RenaissanceGlobals.Globals
Imports RenaissanceDataClass

''' <summary>
''' Class frmEntity
''' </summary>
Public Class frmEntity

  Inherits System.Windows.Forms.Form
  Implements StandardFlorenceForm

#Region " Windows Form Designer generated code "

    ''' <summary>
    ''' Prevents a default instance of the <see cref="frmEntity"/> class from being created.
    ''' </summary>
  Private Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
    ''' <summary>
    ''' Disposes of the resources (other than memory) used by the <see cref="T:System.Windows.Forms.Form" />.
    ''' </summary>
    ''' <param name="disposing">true to release both managed and unmanaged resources; false to release only unmanaged resources.</param>
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
    ''' <summary>
    ''' The components
    ''' </summary>
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
    ''' <summary>
    ''' The edit audit ID
    ''' </summary>
  Friend WithEvents editAuditID As System.Windows.Forms.TextBox
    ''' <summary>
    ''' The edit_ entity name
    ''' </summary>
  Friend WithEvents edit_EntityName As System.Windows.Forms.TextBox
    ''' <summary>
    ''' The BTN nav first
    ''' </summary>
  Friend WithEvents btnNavFirst As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN nav prev
    ''' </summary>
  Friend WithEvents btnNavPrev As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN nav next
    ''' </summary>
  Friend WithEvents btnNavNext As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN last
    ''' </summary>
  Friend WithEvents btnLast As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN add
    ''' </summary>
  Friend WithEvents btnAdd As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN delete
    ''' </summary>
  Friend WithEvents btnDelete As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN cancel
    ''' </summary>
  Friend WithEvents btnCancel As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN save
    ''' </summary>
  Friend WithEvents btnSave As System.Windows.Forms.Button
    ''' <summary>
    ''' The combo_ select entity ID
    ''' </summary>
  Friend WithEvents Combo_SelectEntityID As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The panel1
    ''' </summary>
  Friend WithEvents Panel1 As System.Windows.Forms.Panel
    ''' <summary>
    ''' The label1
    ''' </summary>
  Friend WithEvents Label1 As System.Windows.Forms.Label
    ''' <summary>
    ''' The group box1
    ''' </summary>
  Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    ''' <summary>
    ''' The root menu
    ''' </summary>
  Friend WithEvents RootMenu As System.Windows.Forms.MenuStrip
    ''' <summary>
    ''' The combo_ item group ID
    ''' </summary>
  Friend WithEvents Combo_ItemGroupID As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The LBL_ group ID
    ''' </summary>
  Friend WithEvents lbl_GroupID As System.Windows.Forms.Label
    ''' <summary>
    ''' The LBL_ entity name
    ''' </summary>
  Friend WithEvents lbl_EntityName As System.Windows.Forms.Label
    ''' <summary>
    ''' The LBL instrument ID
    ''' </summary>
  Friend WithEvents LblInstrumentID As System.Windows.Forms.Label
    ''' <summary>
    ''' The combo_ instrument ID
    ''' </summary>
  Friend WithEvents Combo_InstrumentID As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The label2
    ''' </summary>
  Friend WithEvents Label2 As System.Windows.Forms.Label
    ''' <summary>
    ''' The text_ admin contact
    ''' </summary>
  Friend WithEvents Text_AdminContact As System.Windows.Forms.TextBox
    ''' <summary>
    ''' The label3
    ''' </summary>
  Friend WithEvents Label3 As System.Windows.Forms.Label
    ''' <summary>
    ''' The text_ admin phone
    ''' </summary>
  Friend WithEvents Text_AdminPhone As System.Windows.Forms.TextBox
    ''' <summary>
    ''' The label4
    ''' </summary>
  Friend WithEvents Label4 As System.Windows.Forms.Label
    ''' <summary>
    ''' The text_ admin E mail
    ''' </summary>
  Friend WithEvents Text_AdminEMail As System.Windows.Forms.TextBox
    ''' <summary>
    ''' The label5
    ''' </summary>
  Friend WithEvents Label5 As System.Windows.Forms.Label
    ''' <summary>
    ''' The text_ admin fax
    ''' </summary>
  Friend WithEvents Text_AdminFax As System.Windows.Forms.TextBox
    ''' <summary>
    ''' The LBL_ admin email salutation
    ''' </summary>
  Friend WithEvents lbl_AdminEmailSalutation As System.Windows.Forms.Label
    ''' <summary>
    ''' The text_ admin email salutation
    ''' </summary>
	Friend WithEvents Text_AdminEmailSalutation As System.Windows.Forms.TextBox
    ''' <summary>
    ''' The label6
    ''' </summary>
	Friend WithEvents Label6 As System.Windows.Forms.Label
    ''' <summary>
    ''' The combo_ select group
    ''' </summary>
	Friend WithEvents Combo_SelectGroup As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The check_ is inactive
    ''' </summary>
	Friend WithEvents Check_IsInactive As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The combo_ DD status
    ''' </summary>
	Friend WithEvents Combo_DDStatus As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The panel_ DD status
    ''' </summary>
	Friend WithEvents Panel_DDStatus As System.Windows.Forms.Panel
    ''' <summary>
    ''' The label7
    ''' </summary>
	Friend WithEvents Label7 As System.Windows.Forms.Label
    ''' <summary>
    ''' The radio_ D d_ approved
    ''' </summary>
	Friend WithEvents Radio_DD_Approved As System.Windows.Forms.RadioButton
    ''' <summary>
    ''' The radio_ D d_ denied
    ''' </summary>
	Friend WithEvents Radio_DD_Denied As System.Windows.Forms.RadioButton
    ''' <summary>
    ''' The radio_ D d_ in progress
    ''' </summary>
	Friend WithEvents Radio_DD_InProgress As System.Windows.Forms.RadioButton
    ''' <summary>
    ''' The label8
    ''' </summary>
	Friend WithEvents Label8 As System.Windows.Forms.Label
    ''' <summary>
    ''' The BTN close
    ''' </summary>
	Friend WithEvents btnClose As System.Windows.Forms.Button
    ''' <summary>
    ''' Initializes the component.
    ''' </summary>
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Me.editAuditID = New System.Windows.Forms.TextBox
		Me.edit_EntityName = New System.Windows.Forms.TextBox
		Me.btnNavFirst = New System.Windows.Forms.Button
		Me.btnNavPrev = New System.Windows.Forms.Button
		Me.btnNavNext = New System.Windows.Forms.Button
		Me.btnLast = New System.Windows.Forms.Button
		Me.btnAdd = New System.Windows.Forms.Button
		Me.btnDelete = New System.Windows.Forms.Button
		Me.btnCancel = New System.Windows.Forms.Button
		Me.btnSave = New System.Windows.Forms.Button
		Me.Combo_SelectEntityID = New System.Windows.Forms.ComboBox
		Me.Panel1 = New System.Windows.Forms.Panel
		Me.Label1 = New System.Windows.Forms.Label
		Me.GroupBox1 = New System.Windows.Forms.GroupBox
		Me.btnClose = New System.Windows.Forms.Button
		Me.RootMenu = New System.Windows.Forms.MenuStrip
		Me.Combo_ItemGroupID = New System.Windows.Forms.ComboBox
		Me.lbl_GroupID = New System.Windows.Forms.Label
		Me.lbl_EntityName = New System.Windows.Forms.Label
		Me.LblInstrumentID = New System.Windows.Forms.Label
		Me.Combo_InstrumentID = New System.Windows.Forms.ComboBox
		Me.Label2 = New System.Windows.Forms.Label
		Me.Text_AdminContact = New System.Windows.Forms.TextBox
		Me.Label3 = New System.Windows.Forms.Label
		Me.Text_AdminPhone = New System.Windows.Forms.TextBox
		Me.Label4 = New System.Windows.Forms.Label
		Me.Text_AdminEMail = New System.Windows.Forms.TextBox
		Me.Label5 = New System.Windows.Forms.Label
		Me.Text_AdminFax = New System.Windows.Forms.TextBox
		Me.lbl_AdminEmailSalutation = New System.Windows.Forms.Label
		Me.Text_AdminEmailSalutation = New System.Windows.Forms.TextBox
		Me.Label6 = New System.Windows.Forms.Label
		Me.Combo_SelectGroup = New System.Windows.Forms.ComboBox
		Me.Check_IsInactive = New System.Windows.Forms.CheckBox
		Me.Combo_DDStatus = New System.Windows.Forms.ComboBox
		Me.Panel_DDStatus = New System.Windows.Forms.Panel
		Me.Radio_DD_Approved = New System.Windows.Forms.RadioButton
		Me.Radio_DD_Denied = New System.Windows.Forms.RadioButton
		Me.Radio_DD_InProgress = New System.Windows.Forms.RadioButton
		Me.Label8 = New System.Windows.Forms.Label
		Me.Label7 = New System.Windows.Forms.Label
		Me.Panel1.SuspendLayout()
		Me.Panel_DDStatus.SuspendLayout()
		Me.SuspendLayout()
		'
		'editAuditID
		'
		Me.editAuditID.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.editAuditID.Enabled = False
		Me.editAuditID.Location = New System.Drawing.Point(389, 60)
		Me.editAuditID.Name = "editAuditID"
		Me.editAuditID.Size = New System.Drawing.Size(50, 20)
		Me.editAuditID.TabIndex = 2
		'
		'edit_EntityName
		'
		Me.edit_EntityName.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.edit_EntityName.Location = New System.Drawing.Point(121, 103)
		Me.edit_EntityName.MaxLength = 100
		Me.edit_EntityName.Name = "edit_EntityName"
		Me.edit_EntityName.Size = New System.Drawing.Size(320, 20)
		Me.edit_EntityName.TabIndex = 3
		'
		'btnNavFirst
		'
		Me.btnNavFirst.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.btnNavFirst.Location = New System.Drawing.Point(8, 8)
		Me.btnNavFirst.Name = "btnNavFirst"
		Me.btnNavFirst.Size = New System.Drawing.Size(40, 28)
		Me.btnNavFirst.TabIndex = 0
		Me.btnNavFirst.Text = "<<"
		'
		'btnNavPrev
		'
		Me.btnNavPrev.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.btnNavPrev.Location = New System.Drawing.Point(50, 8)
		Me.btnNavPrev.Name = "btnNavPrev"
		Me.btnNavPrev.Size = New System.Drawing.Size(35, 28)
		Me.btnNavPrev.TabIndex = 1
		Me.btnNavPrev.Text = "<"
		'
		'btnNavNext
		'
		Me.btnNavNext.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.btnNavNext.Location = New System.Drawing.Point(88, 8)
		Me.btnNavNext.Name = "btnNavNext"
		Me.btnNavNext.Size = New System.Drawing.Size(35, 28)
		Me.btnNavNext.TabIndex = 2
		Me.btnNavNext.Text = ">"
		'
		'btnLast
		'
		Me.btnLast.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.btnLast.Location = New System.Drawing.Point(124, 8)
		Me.btnLast.Name = "btnLast"
		Me.btnLast.Size = New System.Drawing.Size(40, 28)
		Me.btnLast.TabIndex = 3
		Me.btnLast.Text = ">>"
		'
		'btnAdd
		'
		Me.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.btnAdd.Location = New System.Drawing.Point(176, 8)
		Me.btnAdd.Name = "btnAdd"
		Me.btnAdd.Size = New System.Drawing.Size(75, 28)
		Me.btnAdd.TabIndex = 4
		Me.btnAdd.Text = "&New"
		'
		'btnDelete
		'
		Me.btnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
		Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.btnDelete.Location = New System.Drawing.Point(154, 508)
		Me.btnDelete.Name = "btnDelete"
		Me.btnDelete.Size = New System.Drawing.Size(75, 28)
		Me.btnDelete.TabIndex = 15
		Me.btnDelete.Text = "&Delete"
		'
		'btnCancel
		'
		Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
		Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
		Me.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.btnCancel.Location = New System.Drawing.Point(238, 508)
		Me.btnCancel.Name = "btnCancel"
		Me.btnCancel.Size = New System.Drawing.Size(75, 28)
		Me.btnCancel.TabIndex = 16
		Me.btnCancel.Text = "&Cancel"
		'
		'btnSave
		'
		Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
		Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.btnSave.Location = New System.Drawing.Point(66, 508)
		Me.btnSave.Name = "btnSave"
		Me.btnSave.Size = New System.Drawing.Size(75, 28)
		Me.btnSave.TabIndex = 14
		Me.btnSave.Text = "&Save"
		'
		'Combo_SelectEntityID
		'
		Me.Combo_SelectEntityID.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Combo_SelectEntityID.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Combo_SelectEntityID.Location = New System.Drawing.Point(121, 60)
		Me.Combo_SelectEntityID.Name = "Combo_SelectEntityID"
		Me.Combo_SelectEntityID.Size = New System.Drawing.Size(262, 21)
		Me.Combo_SelectEntityID.TabIndex = 1
		'
		'Panel1
		'
		Me.Panel1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
		Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Panel1.Controls.Add(Me.btnNavFirst)
		Me.Panel1.Controls.Add(Me.btnNavPrev)
		Me.Panel1.Controls.Add(Me.btnNavNext)
		Me.Panel1.Controls.Add(Me.btnLast)
		Me.Panel1.Controls.Add(Me.btnAdd)
		Me.Panel1.Location = New System.Drawing.Point(102, 452)
		Me.Panel1.Name = "Panel1"
		Me.Panel1.Size = New System.Drawing.Size(260, 48)
		Me.Panel1.TabIndex = 13
		'
		'Label1
		'
		Me.Label1.Location = New System.Drawing.Point(6, 60)
		Me.Label1.Name = "Label1"
		Me.Label1.Size = New System.Drawing.Size(100, 23)
		Me.Label1.TabIndex = 21
		Me.Label1.Text = "Select"
		'
		'GroupBox1
		'
		Me.GroupBox1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.GroupBox1.Location = New System.Drawing.Point(5, 86)
		Me.GroupBox1.Name = "GroupBox1"
		Me.GroupBox1.Size = New System.Drawing.Size(436, 10)
		Me.GroupBox1.TabIndex = 77
		Me.GroupBox1.TabStop = False
		'
		'btnClose
		'
		Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
		Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.btnClose.Location = New System.Drawing.Point(322, 508)
		Me.btnClose.Name = "btnClose"
		Me.btnClose.Size = New System.Drawing.Size(75, 28)
		Me.btnClose.TabIndex = 17
		Me.btnClose.Text = "&Close"
		'
		'RootMenu
		'
		Me.RootMenu.AllowMerge = False
		Me.RootMenu.Location = New System.Drawing.Point(0, 0)
		Me.RootMenu.Name = "RootMenu"
		Me.RootMenu.Size = New System.Drawing.Size(452, 24)
		Me.RootMenu.TabIndex = 18
		Me.RootMenu.Text = "MenuStrip1"
		'
		'Combo_ItemGroupID
		'
		Me.Combo_ItemGroupID.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Combo_ItemGroupID.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Combo_ItemGroupID.FormattingEnabled = True
		Me.Combo_ItemGroupID.Location = New System.Drawing.Point(121, 129)
		Me.Combo_ItemGroupID.Name = "Combo_ItemGroupID"
		Me.Combo_ItemGroupID.Size = New System.Drawing.Size(320, 21)
		Me.Combo_ItemGroupID.TabIndex = 4
		'
		'lbl_GroupID
		'
		Me.lbl_GroupID.AutoSize = True
		Me.lbl_GroupID.Location = New System.Drawing.Point(6, 132)
		Me.lbl_GroupID.Name = "lbl_GroupID"
		Me.lbl_GroupID.Size = New System.Drawing.Size(108, 13)
		Me.lbl_GroupID.TabIndex = 80
		Me.lbl_GroupID.Text = "Due Dilligence Group"
		'
		'lbl_EntityName
		'
		Me.lbl_EntityName.Location = New System.Drawing.Point(6, 106)
		Me.lbl_EntityName.Name = "lbl_EntityName"
		Me.lbl_EntityName.Size = New System.Drawing.Size(111, 17)
		Me.lbl_EntityName.TabIndex = 86
		Me.lbl_EntityName.Text = "Description"
		'
		'LblInstrumentID
		'
		Me.LblInstrumentID.Location = New System.Drawing.Point(5, 159)
		Me.LblInstrumentID.Name = "LblInstrumentID"
		Me.LblInstrumentID.Size = New System.Drawing.Size(111, 17)
		Me.LblInstrumentID.TabIndex = 88
		Me.LblInstrumentID.Text = "Instrument"
		'
		'Combo_InstrumentID
		'
		Me.Combo_InstrumentID.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Combo_InstrumentID.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Combo_InstrumentID.FormattingEnabled = True
		Me.Combo_InstrumentID.Location = New System.Drawing.Point(121, 156)
		Me.Combo_InstrumentID.Name = "Combo_InstrumentID"
		Me.Combo_InstrumentID.Size = New System.Drawing.Size(320, 21)
		Me.Combo_InstrumentID.TabIndex = 5
		'
		'Label2
		'
		Me.Label2.Location = New System.Drawing.Point(6, 186)
		Me.Label2.Name = "Label2"
		Me.Label2.Size = New System.Drawing.Size(111, 17)
		Me.Label2.TabIndex = 90
		Me.Label2.Text = "Admin Contact"
		'
		'Text_AdminContact
		'
		Me.Text_AdminContact.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Text_AdminContact.Location = New System.Drawing.Point(121, 183)
		Me.Text_AdminContact.MaxLength = 100
		Me.Text_AdminContact.Name = "Text_AdminContact"
		Me.Text_AdminContact.Size = New System.Drawing.Size(320, 20)
		Me.Text_AdminContact.TabIndex = 6
		'
		'Label3
		'
		Me.Label3.Location = New System.Drawing.Point(6, 238)
		Me.Label3.Name = "Label3"
		Me.Label3.Size = New System.Drawing.Size(111, 17)
		Me.Label3.TabIndex = 92
		Me.Label3.Text = "Phone Number"
		'
		'Text_AdminPhone
		'
		Me.Text_AdminPhone.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Text_AdminPhone.Location = New System.Drawing.Point(121, 235)
		Me.Text_AdminPhone.MaxLength = 50
		Me.Text_AdminPhone.Name = "Text_AdminPhone"
		Me.Text_AdminPhone.Size = New System.Drawing.Size(320, 20)
		Me.Text_AdminPhone.TabIndex = 8
		'
		'Label4
		'
		Me.Label4.Location = New System.Drawing.Point(6, 264)
		Me.Label4.Name = "Label4"
		Me.Label4.Size = New System.Drawing.Size(111, 17)
		Me.Label4.TabIndex = 94
		Me.Label4.Text = "EMail Address"
		'
		'Text_AdminEMail
		'
		Me.Text_AdminEMail.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Text_AdminEMail.Location = New System.Drawing.Point(121, 261)
		Me.Text_AdminEMail.MaxLength = 100
		Me.Text_AdminEMail.Name = "Text_AdminEMail"
		Me.Text_AdminEMail.Size = New System.Drawing.Size(320, 20)
		Me.Text_AdminEMail.TabIndex = 9
		'
		'Label5
		'
		Me.Label5.Location = New System.Drawing.Point(6, 290)
		Me.Label5.Name = "Label5"
		Me.Label5.Size = New System.Drawing.Size(111, 17)
		Me.Label5.TabIndex = 96
		Me.Label5.Text = "Fax number"
		'
		'Text_AdminFax
		'
		Me.Text_AdminFax.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Text_AdminFax.Location = New System.Drawing.Point(121, 287)
		Me.Text_AdminFax.MaxLength = 50
		Me.Text_AdminFax.Name = "Text_AdminFax"
		Me.Text_AdminFax.Size = New System.Drawing.Size(320, 20)
		Me.Text_AdminFax.TabIndex = 10
		'
		'lbl_AdminEmailSalutation
		'
		Me.lbl_AdminEmailSalutation.Location = New System.Drawing.Point(6, 212)
		Me.lbl_AdminEmailSalutation.Name = "lbl_AdminEmailSalutation"
		Me.lbl_AdminEmailSalutation.Size = New System.Drawing.Size(111, 17)
		Me.lbl_AdminEmailSalutation.TabIndex = 98
		Me.lbl_AdminEmailSalutation.Text = "EMail Salutation"
		'
		'Text_AdminEmailSalutation
		'
		Me.Text_AdminEmailSalutation.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Text_AdminEmailSalutation.Location = New System.Drawing.Point(121, 209)
		Me.Text_AdminEmailSalutation.MaxLength = 100
		Me.Text_AdminEmailSalutation.Name = "Text_AdminEmailSalutation"
		Me.Text_AdminEmailSalutation.Size = New System.Drawing.Size(320, 20)
		Me.Text_AdminEmailSalutation.TabIndex = 7
		'
		'Label6
		'
		Me.Label6.AutoSize = True
		Me.Label6.Location = New System.Drawing.Point(6, 36)
		Me.Label6.Name = "Label6"
		Me.Label6.Size = New System.Drawing.Size(69, 13)
		Me.Label6.TabIndex = 100
		Me.Label6.Text = "Select Group"
		'
		'Combo_SelectGroup
		'
		Me.Combo_SelectGroup.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Combo_SelectGroup.Location = New System.Drawing.Point(121, 33)
		Me.Combo_SelectGroup.Name = "Combo_SelectGroup"
		Me.Combo_SelectGroup.Size = New System.Drawing.Size(318, 21)
		Me.Combo_SelectGroup.TabIndex = 0
		'
		'Check_IsInactive
		'
		Me.Check_IsInactive.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
		Me.Check_IsInactive.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Check_IsInactive.Location = New System.Drawing.Point(9, 312)
		Me.Check_IsInactive.Name = "Check_IsInactive"
		Me.Check_IsInactive.Size = New System.Drawing.Size(125, 18)
		Me.Check_IsInactive.TabIndex = 11
		Me.Check_IsInactive.Text = "Inactive Entity"
		Me.Check_IsInactive.UseVisualStyleBackColor = True
		'
		'Combo_DDStatus
		'
		Me.Combo_DDStatus.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Combo_DDStatus.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Combo_DDStatus.FormattingEnabled = True
		Me.Combo_DDStatus.Location = New System.Drawing.Point(118, 68)
		Me.Combo_DDStatus.Name = "Combo_DDStatus"
		Me.Combo_DDStatus.Size = New System.Drawing.Size(318, 21)
		Me.Combo_DDStatus.TabIndex = 4
		'
		'Panel_DDStatus
		'
		Me.Panel_DDStatus.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Panel_DDStatus.Controls.Add(Me.Radio_DD_Approved)
		Me.Panel_DDStatus.Controls.Add(Me.Radio_DD_Denied)
		Me.Panel_DDStatus.Controls.Add(Me.Radio_DD_InProgress)
		Me.Panel_DDStatus.Controls.Add(Me.Label8)
		Me.Panel_DDStatus.Controls.Add(Me.Label7)
		Me.Panel_DDStatus.Controls.Add(Me.Combo_DDStatus)
		Me.Panel_DDStatus.Location = New System.Drawing.Point(2, 339)
		Me.Panel_DDStatus.Name = "Panel_DDStatus"
		Me.Panel_DDStatus.Size = New System.Drawing.Size(447, 98)
		Me.Panel_DDStatus.TabIndex = 12
		'
		'Radio_DD_Approved
		'
		Me.Radio_DD_Approved.AutoSize = True
		Me.Radio_DD_Approved.Location = New System.Drawing.Point(118, 44)
		Me.Radio_DD_Approved.Name = "Radio_DD_Approved"
		Me.Radio_DD_Approved.Size = New System.Drawing.Size(95, 17)
		Me.Radio_DD_Approved.TabIndex = 3
		Me.Radio_DD_Approved.TabStop = True
		Me.Radio_DD_Approved.Text = "D/D Approved"
		Me.Radio_DD_Approved.UseVisualStyleBackColor = True
		'
		'Radio_DD_Denied
		'
		Me.Radio_DD_Denied.AutoSize = True
		Me.Radio_DD_Denied.Location = New System.Drawing.Point(118, 24)
		Me.Radio_DD_Denied.Name = "Radio_DD_Denied"
		Me.Radio_DD_Denied.Size = New System.Drawing.Size(92, 17)
		Me.Radio_DD_Denied.TabIndex = 2
		Me.Radio_DD_Denied.TabStop = True
		Me.Radio_DD_Denied.Text = "D/D Rejected"
		Me.Radio_DD_Denied.UseVisualStyleBackColor = True
		'
		'Radio_DD_InProgress
		'
		Me.Radio_DD_InProgress.AutoSize = True
		Me.Radio_DD_InProgress.Location = New System.Drawing.Point(118, 4)
		Me.Radio_DD_InProgress.Name = "Radio_DD_InProgress"
		Me.Radio_DD_InProgress.Size = New System.Drawing.Size(102, 17)
		Me.Radio_DD_InProgress.TabIndex = 1
		Me.Radio_DD_InProgress.TabStop = True
		Me.Radio_DD_InProgress.Text = "D/D In Progress"
		Me.Radio_DD_InProgress.UseVisualStyleBackColor = True
		'
		'Label8
		'
		Me.Label8.Location = New System.Drawing.Point(3, 71)
		Me.Label8.Name = "Label8"
		Me.Label8.Size = New System.Drawing.Size(111, 17)
		Me.Label8.TabIndex = 5
		Me.Label8.Text = "Approval Note"
		'
		'Label7
		'
		Me.Label7.AutoSize = True
		Me.Label7.Location = New System.Drawing.Point(3, 4)
		Me.Label7.Name = "Label7"
		Me.Label7.Size = New System.Drawing.Size(115, 13)
		Me.Label7.TabIndex = 0
		Me.Label7.Text = "Due Dilligence Status :"
		'
		'frmEntity
		'
		Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
		Me.CancelButton = Me.btnCancel
		Me.ClientSize = New System.Drawing.Size(452, 546)
		Me.Controls.Add(Me.Panel_DDStatus)
		Me.Controls.Add(Me.Check_IsInactive)
		Me.Controls.Add(Me.Label6)
		Me.Controls.Add(Me.Combo_SelectGroup)
		Me.Controls.Add(Me.lbl_AdminEmailSalutation)
		Me.Controls.Add(Me.Text_AdminEmailSalutation)
		Me.Controls.Add(Me.Label5)
		Me.Controls.Add(Me.Text_AdminFax)
		Me.Controls.Add(Me.Label4)
		Me.Controls.Add(Me.Text_AdminEMail)
		Me.Controls.Add(Me.Label3)
		Me.Controls.Add(Me.Text_AdminPhone)
		Me.Controls.Add(Me.Label2)
		Me.Controls.Add(Me.Text_AdminContact)
		Me.Controls.Add(Me.LblInstrumentID)
		Me.Controls.Add(Me.Combo_InstrumentID)
		Me.Controls.Add(Me.lbl_EntityName)
		Me.Controls.Add(Me.Combo_ItemGroupID)
		Me.Controls.Add(Me.btnClose)
		Me.Controls.Add(Me.GroupBox1)
		Me.Controls.Add(Me.Label1)
		Me.Controls.Add(Me.Panel1)
		Me.Controls.Add(Me.Combo_SelectEntityID)
		Me.Controls.Add(Me.btnSave)
		Me.Controls.Add(Me.editAuditID)
		Me.Controls.Add(Me.edit_EntityName)
		Me.Controls.Add(Me.btnDelete)
		Me.Controls.Add(Me.btnCancel)
		Me.Controls.Add(Me.RootMenu)
		Me.Controls.Add(Me.lbl_GroupID)
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
		Me.MainMenuStrip = Me.RootMenu
		Me.MinimumSize = New System.Drawing.Size(458, 463)
		Me.Name = "frmEntity"
		Me.Text = "Add/Edit Due Dilligence Entities"
		Me.Panel1.ResumeLayout(False)
		Me.Panel_DDStatus.ResumeLayout(False)
		Me.Panel_DDStatus.PerformLayout()
		Me.ResumeLayout(False)
		Me.PerformLayout()

	End Sub

#End Region

#Region " Form Locals and Constants "


	' Form 'Parent', the Main Venice form.
	' Generally only accessed through the 'MainForm' property.
    ''' <summary>
    ''' The _ main form
    ''' </summary>
	Private WithEvents _MainForm As FlorenceMain

	' Form ToolTip
    ''' <summary>
    ''' The form tooltip
    ''' </summary>
	Private FormTooltip As New ToolTip()

	' Form Constants, specific to the table being updated.

    ''' <summary>
    ''' The ALWAY s_ CLOS e_ THI s_ FORM
    ''' </summary>
	Private ALWAYS_CLOSE_THIS_FORM As Boolean = False

	' Form Locals, initialised on 'New' defining what standard data items to use
    ''' <summary>
    ''' The THI s_ TABLENAME
    ''' </summary>
	Private THIS_TABLENAME As String
    ''' <summary>
    ''' The THI s_ ADAPTORNAME
    ''' </summary>
	Private THIS_ADAPTORNAME As String
    ''' <summary>
    ''' The THI s_ DATASETNAME
    ''' </summary>
	Private THIS_DATASETNAME As String


	' The standard ChangeID for this form. e.g. tblFlorenceEntity
    ''' <summary>
    ''' The THI s_ FOR m_ change ID
    ''' </summary>
	Private THIS_FORM_ChangeID As RenaissanceGlobals.RenaissanceChangeID

    ''' <summary>
    ''' The THI s_ FOR m_ selecting combo
    ''' </summary>
	Private THIS_FORM_SelectingCombo As ComboBox
    ''' <summary>
    ''' The THI s_ FOR m_ new move to control
    ''' </summary>
	Private THIS_FORM_NewMoveToControl As Control

	' Form Specific Order fields
    ''' <summary>
    ''' The THI s_ FOR m_ select by
    ''' </summary>
	Private THIS_FORM_SelectBy As String
    ''' <summary>
    ''' The THI s_ FOR m_ order by
    ''' </summary>
	Private THIS_FORM_OrderBy As String

    ''' <summary>
    ''' The THI s_ FOR m_ value member
    ''' </summary>
	Private THIS_FORM_ValueMember As String

	' Form specific Permissioning variables
    ''' <summary>
    ''' The THI s_ FOR m_ permission area
    ''' </summary>
	Private THIS_FORM_PermissionArea As String
    ''' <summary>
    ''' The THI s_ FOR m_ permission type
    ''' </summary>
	Private THIS_FORM_PermissionType As RenaissanceGlobals.PermissionFeatureType

	' Form specific Form type 
    ''' <summary>
    ''' The THI s_ FOR m_ form ID
    ''' </summary>
	Private THIS_FORM_FormID As FlorenceFormID

	' Data Structures

    ''' <summary>
    ''' My dataset
    ''' </summary>
	Private myDataset As RenaissanceDataClass.DSFlorenceEntity		 ' Form Specific !!!!
    ''' <summary>
    ''' My table
    ''' </summary>
	Private myTable As RenaissanceDataClass.DSFlorenceEntity.tblFlorenceEntityDataTable
    ''' <summary>
    ''' My connection
    ''' </summary>
	Private myConnection As SqlConnection
    ''' <summary>
    ''' My adaptor
    ''' </summary>
	Private myAdaptor As SqlDataAdapter

    ''' <summary>
    ''' The this standard dataset
    ''' </summary>
	Private ThisStandardDataset As RenaissanceGlobals.StandardDataset


	' Active Element.

    ''' <summary>
    ''' The sorted rows
    ''' </summary>
	Private SortedRows() As DataRow
    ''' <summary>
    ''' The select by sorted rows
    ''' </summary>
	Private SelectBySortedRows() As DataRow
    ''' <summary>
    ''' The this data row
    ''' </summary>
	Private thisDataRow As RenaissanceDataClass.DSFlorenceEntity.tblFlorenceEntityRow		 ' Form Specific !!!!
    ''' <summary>
    ''' The this audit ID
    ''' </summary>
	Private thisAuditID As Integer
    ''' <summary>
    ''' The this position
    ''' </summary>
	Private thisPosition As Integer
    ''' <summary>
    ''' The _ is over cancel button
    ''' </summary>
	Private _IsOverCancelButton As Boolean
    ''' <summary>
    ''' The _ in use
    ''' </summary>
	Private _InUse As Boolean

	' Form Status Flags

    ''' <summary>
    ''' The form is valid
    ''' </summary>
	Private FormIsValid As Boolean
    ''' <summary>
    ''' The form changed
    ''' </summary>
	Private FormChanged As Boolean
    ''' <summary>
    ''' The _ form open failed
    ''' </summary>
	Private _FormOpenFailed As Boolean
    ''' <summary>
    ''' The in paint
    ''' </summary>
	Private InPaint As Boolean
    ''' <summary>
    ''' The add new record
    ''' </summary>
	Private AddNewRecord As Boolean

	' User Permission Flags

    ''' <summary>
    ''' The has read permission
    ''' </summary>
	Private HasReadPermission As Boolean
    ''' <summary>
    ''' The has update permission
    ''' </summary>
	Private HasUpdatePermission As Boolean
    ''' <summary>
    ''' The has insert permission
    ''' </summary>
	Private HasInsertPermission As Boolean
    ''' <summary>
    ''' The has delete permission
    ''' </summary>
	Private HasDeletePermission As Boolean

#End Region

#Region " Form 'Properties' "

    ''' <summary>
    ''' Gets the main form.
    ''' </summary>
    ''' <value>The main form.</value>
	Public ReadOnly Property MainForm() As FlorenceMain Implements Globals.StandardFlorenceForm.MainForm
		' Public property to return handle to the 'Main' Venice form, where in reside most of the 
		' data structures and many common utilities.
		Get
			Return _MainForm
		End Get
	End Property

    ''' <summary>
    ''' Gets or sets a value indicating whether this instance is over cancel button.
    ''' </summary>
    ''' <value><c>true</c> if this instance is over cancel button; otherwise, <c>false</c>.</value>
	Public Property IsOverCancelButton() As Boolean Implements Globals.StandardFlorenceForm.IsOverCancelButton
		' Public property maintaining a value indicating if the cursor is over the 'Cancel'
		' Button on this form.
		' This property is specifically designed for use by the field formating Event functions
		' In order that they do not impose format restrictions if the user is about to click the 
		' 'Cancel' button.
		'
		Get
			Return _IsOverCancelButton
		End Get
		Set(ByVal Value As Boolean)
			_IsOverCancelButton = Value
		End Set
	End Property

    ''' <summary>
    ''' Gets a value indicating whether this instance is in paint.
    ''' </summary>
    ''' <value><c>true</c> if this instance is in paint; otherwise, <c>false</c>.</value>
	Public ReadOnly Property IsInPaint() As Boolean Implements Globals.StandardFlorenceForm.IsInPaint
		Get
			Return InPaint
		End Get
	End Property

    ''' <summary>
    ''' Gets a value indicating whether [in use].
    ''' </summary>
    ''' <value><c>true</c> if [in use]; otherwise, <c>false</c>.</value>
	Public ReadOnly Property InUse() As Boolean Implements Globals.StandardFlorenceForm.InUse
		Get
			Return _InUse
		End Get
	End Property

    ''' <summary>
    ''' Gets a value indicating whether [form open failed].
    ''' </summary>
    ''' <value><c>true</c> if [form open failed]; otherwise, <c>false</c>.</value>
	Public ReadOnly Property FormOpenFailed() As Boolean Implements Globals.StandardFlorenceForm.FormOpenFailed
		Get
			Return _FormOpenFailed
		End Get
	End Property

#End Region

    ''' <summary>
    ''' Initializes a new instance of the <see cref="frmEntity"/> class.
    ''' </summary>
    ''' <param name="pMainForm">The p main form.</param>
	Public Sub New(ByVal pMainForm As FlorenceMain)
		' *************************************************************
		' Custom 'New'. 
		' Passes in the reference to the parent form.
		' 
		' Establishes form specific variables.
		' Establishes Form specific Data connection / data structures.
		'
		' *************************************************************

		Me.New()

		_MainForm = pMainForm
		AddHandler _MainForm.FlorenceAutoUpdate, AddressOf Me.AutoUpdate

		_FormOpenFailed = False
		_InUse = True

		' ******************************************************
		' Form Specific Settings :
		' ******************************************************

		THIS_FORM_SelectingCombo = Me.Combo_SelectEntityID
		THIS_FORM_NewMoveToControl = Me.edit_EntityName

		' Default Select and Order fields.

		THIS_FORM_SelectBy = "EntityName"
		THIS_FORM_OrderBy = "EntityName"

		THIS_FORM_ValueMember = "EntityID"

		' Form Permissioning :-

		THIS_FORM_PermissionArea = Me.Name
		THIS_FORM_PermissionType = RenaissanceGlobals.PermissionFeatureType.TypeForm

		' 'This' form ID

		THIS_FORM_FormID = FlorenceFormID.frmEntity

		' This form's dataset type.

		ThisStandardDataset = RenaissanceStandardDatasets.tblFlorenceEntity	 ' This Defines the Form Data !!! 

		' Format Event Handlers for form controls

		AddHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler
		AddHandler edit_EntityName.LostFocus, AddressOf MainForm.Text_NotNull

		' Form Control Changed events
		AddHandler THIS_FORM_SelectingCombo.SelectedIndexChanged, AddressOf Combo_SelectComboChanged
		AddHandler THIS_FORM_SelectingCombo.KeyUp, AddressOf MainForm.ComboSelectAsYouType

		AddHandler edit_EntityName.TextChanged, AddressOf Me.FormControlChanged
		AddHandler Text_AdminContact.TextChanged, AddressOf Me.FormControlChanged
		AddHandler Text_AdminEmailSalutation.TextChanged, AddressOf Me.FormControlChanged
		AddHandler Text_AdminEMail.TextChanged, AddressOf Me.FormControlChanged
		AddHandler Text_AdminFax.TextChanged, AddressOf Me.FormControlChanged
		AddHandler Text_AdminPhone.TextChanged, AddressOf Me.FormControlChanged

		AddHandler Combo_ItemGroupID.SelectedValueChanged, AddressOf Me.FormControlChanged
		AddHandler Combo_ItemGroupID.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
		AddHandler Combo_ItemGroupID.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
		AddHandler Combo_ItemGroupID.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
		AddHandler Combo_ItemGroupID.KeyUp, AddressOf MainForm.ComboSelectAsYouType_NoMatch

		AddHandler Combo_InstrumentID.SelectedValueChanged, AddressOf Me.FormControlChanged
		AddHandler Combo_InstrumentID.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
		AddHandler Combo_InstrumentID.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
		AddHandler Combo_InstrumentID.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
		AddHandler Combo_InstrumentID.KeyUp, AddressOf MainForm.ComboSelectAsYouType_NoMatch

		AddHandler Check_IsInactive.CheckedChanged, AddressOf Me.FormControlChanged

		AddHandler Combo_DDStatus.SelectedValueChanged, AddressOf Me.FormControlChanged
		AddHandler Combo_DDStatus.KeyUp, AddressOf MainForm.ComboSelectAsYouType_NoMatch
		AddHandler Combo_DDStatus.KeyUp, AddressOf FormControlChanged

		AddHandler Radio_DD_Approved.CheckedChanged, AddressOf Me.FormControlChanged
		AddHandler Radio_DD_Denied.CheckedChanged, AddressOf Me.FormControlChanged
		AddHandler Radio_DD_InProgress.CheckedChanged, AddressOf Me.FormControlChanged

		' Set up the ToolTip
		MainForm.SetFormToolTip(Me, FormTooltip)

		' ******************************************************
		' End Form Specific.
		' ******************************************************

		' Data object names standard to this Form type.

		THIS_TABLENAME = ThisStandardDataset.TableName
		THIS_ADAPTORNAME = ThisStandardDataset.Adaptorname
		THIS_DATASETNAME = ThisStandardDataset.DatasetName

		THIS_FORM_ChangeID = ThisStandardDataset.ChangeID

		' Establish / Retrieve data objects for this form.

		myConnection = MainForm.MainDataHandler.Get_Connection(FLORENCE_CONNECTION)
		myAdaptor = MainForm.MainDataHandler.Get_Adaptor(THIS_ADAPTORNAME, FLORENCE_CONNECTION, THIS_TABLENAME)
		myDataset = MainForm.Load_Table(ThisStandardDataset, False)
		myTable = myDataset.Tables(0)

		'Me.Controls.Add(MainForm.BuildStandardFormMenu(Me.RootMenu, myDataset.Tables(0), AddressOf SelectMenuEvent, AddressOf OrderMenuEvent, AddressOf AuditReportMenuEvent))
		MainForm.BuildStandardFormMenu(Me.RootMenu, myDataset.Tables(0), AddressOf SelectMenuEvent, AddressOf OrderMenuEvent, AddressOf AuditReportMenuEvent)
		Me.RootMenu.PerformLayout()

		Try
			InPaint = True

			Call SetGroupCombo()
			Call SetInstrumentCombo()
			Call SetDDStatusCombo()
		Catch ex As Exception
		Finally
			InPaint = False
		End Try

	End Sub

#Region " This Form Event handlers : FormLoad / FormClose "

	' Form Initialisation code.
	'
    ''' <summary>
    ''' Resets the form.
    ''' </summary>
	Public Sub ResetForm() Implements StandardFlorenceForm.ResetForm
		THIS_FORM_SelectBy = "EntityName"
		THIS_FORM_OrderBy = "EntityName"

		Call Form_Load(Me, New System.EventArgs)
	End Sub

    ''' <summary>
    ''' Closes the form.
    ''' </summary>
	Public Sub CloseForm() Implements StandardFlorenceForm.CloseForm
		ALWAYS_CLOSE_THIS_FORM = True
		Me.Close()
	End Sub


    ''' <summary>
    ''' Handles the Load event of the Form control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

		Me.MainForm.LogError(Me.Name, LOG_LEVELS.Audit, "", "Form Loaded", "", False)
		_FormOpenFailed = False
		_InUse = True

		' Initialise Data structures. Connection, Adaptor and Dataset.

		If Not (MainForm Is Nothing) Then
			FormIsValid = True
		Else
			MessageBox.Show("'Main' form is not set. Closing form", "no Main Form", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
			FormIsValid = False
			_FormOpenFailed = True
			Exit Sub
		End If

		If (myConnection Is Nothing) Then
			Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Database connection is not established. Closing form.", "", True)

			FormIsValid = False
			_FormOpenFailed = True
			Exit Sub
		End If

		If (myAdaptor Is Nothing) Then
			Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Data Adaptor is not established. Closing form.", "", True)

			FormIsValid = False
			_FormOpenFailed = True
			Exit Sub
		End If

		If (myDataset Is Nothing) Then
			Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Form Dataset is not established. Closing form.", "", True)

			FormIsValid = False
			_FormOpenFailed = True
			Exit Sub
		End If

		' Initialse form

		InPaint = True
		IsOverCancelButton = False

		' Check User permissions
		Call CheckPermissions()
		If (HasReadPermission = False) Then
			Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User does not have Read permission for this Form..", "", True)

			FormIsValid = False
			_FormOpenFailed = True
			Exit Sub
		End If

		' Build Sorted data list from which this form operates
		Call SetSortedRows()

		' Display initial record.

		thisPosition = 0
		If THIS_FORM_SelectingCombo.Items.Count > 0 Then
			Me.THIS_FORM_SelectingCombo.SelectedIndex = 0
			thisDataRow = SortedRows(thisPosition)
			Call GetFormData(thisDataRow)
		Else
			Me.THIS_FORM_SelectingCombo.SelectedIndex = (-1)
			Call GetFormData(Nothing)
		End If

		InPaint = False


	End Sub

    ''' <summary>
    ''' Handles the Closing event of the frmEntity control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.ComponentModel.CancelEventArgs"/> instance containing the event data.</param>
	Private Sub frmEntity_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
		Dim HideForm As Boolean

		' Hide or Close this form ?
		' All depends on how many of this form type are Open or in Cache...

		_InUse = False

		If (ALWAYS_CLOSE_THIS_FORM = True) Or (FormIsValid = False) Then
			HideForm = False
		Else
			If (FormChanged = True) Then
				Call SetFormData()
			End If

			HideForm = True
			If MainForm.FlorenceForms.CountOf(THIS_FORM_FormID) > MainForm.EntryForm_CacheCount Then
				HideForm = False
			End If
		End If

		If HideForm = True Then
			MainForm.HideInFormsCollection(Me)
			Me.Hide()	' NPP Fix

			e.Cancel = True
		Else
			Try
				MainForm.RemoveFromFormsCollection(Me)
				RemoveHandler _MainForm.FlorenceAutoUpdate, AddressOf Me.AutoUpdate

				RemoveHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler
				RemoveHandler edit_EntityName.LostFocus, AddressOf MainForm.Text_NotNull

				' Form Control Changed events
				RemoveHandler THIS_FORM_SelectingCombo.SelectedIndexChanged, AddressOf Combo_SelectComboChanged
				RemoveHandler THIS_FORM_SelectingCombo.KeyUp, AddressOf MainForm.ComboSelectAsYouType

				RemoveHandler edit_EntityName.TextChanged, AddressOf Me.FormControlChanged
				RemoveHandler Text_AdminContact.TextChanged, AddressOf Me.FormControlChanged
				RemoveHandler Text_AdminEmailSalutation.TextChanged, AddressOf Me.FormControlChanged

				RemoveHandler Text_AdminEMail.TextChanged, AddressOf Me.FormControlChanged
				RemoveHandler Text_AdminFax.TextChanged, AddressOf Me.FormControlChanged
				RemoveHandler Text_AdminPhone.TextChanged, AddressOf Me.FormControlChanged

				RemoveHandler Combo_ItemGroupID.SelectedValueChanged, AddressOf Me.FormControlChanged
				RemoveHandler Combo_ItemGroupID.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
				RemoveHandler Combo_ItemGroupID.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
				RemoveHandler Combo_ItemGroupID.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
				RemoveHandler Combo_ItemGroupID.KeyUp, AddressOf MainForm.ComboSelectAsYouType_NoMatch

				RemoveHandler Combo_InstrumentID.SelectedValueChanged, AddressOf Me.FormControlChanged
				RemoveHandler Combo_InstrumentID.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
				RemoveHandler Combo_InstrumentID.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
				RemoveHandler Combo_InstrumentID.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
				RemoveHandler Combo_InstrumentID.KeyUp, AddressOf MainForm.ComboSelectAsYouType_NoMatch

				RemoveHandler Check_IsInactive.CheckedChanged, AddressOf Me.FormControlChanged

				RemoveHandler Combo_DDStatus.SelectedValueChanged, AddressOf Me.FormControlChanged
				RemoveHandler Combo_DDStatus.KeyUp, AddressOf MainForm.ComboSelectAsYouType_NoMatch
				RemoveHandler Combo_DDStatus.KeyUp, AddressOf FormControlChanged

				RemoveHandler Radio_DD_Approved.CheckedChanged, AddressOf Me.FormControlChanged
				RemoveHandler Radio_DD_Denied.CheckedChanged, AddressOf Me.FormControlChanged
				RemoveHandler Radio_DD_InProgress.CheckedChanged, AddressOf Me.FormControlChanged

			Catch ex As Exception
			End Try
		End If

	End Sub


#End Region

	' Routine to handle changes / updates to tables by this and other windows.
	' If this, or any other, form posts a change to a table, then it will invoke an update event 
	' detailing what tables have been altered.
	' Each Form will receive this event through this 'AutoUpdate' routine whice is configured to handle
	' the 'VeniceAutoUpdate' event of the main Venice form.
	' Each form may them react as appropriate to changes in any table that might impact it.
	'
    ''' <summary>
    ''' Autoes the update.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="RenaissanceGlobals.RenaissanceUpdateEventArgs"/> instance containing the event data.</param>
	Private Sub AutoUpdate(ByVal sender As System.Object, ByVal e As RenaissanceGlobals.RenaissanceUpdateEventArgs)
		Dim OrgInPaint As Boolean
		Dim KnowledgeDateChanged As Boolean
		Dim SetButtonStatus_Flag As Boolean
		Dim RefreshForm As Boolean = False

		If (Me.IsDisposed) OrElse (Me.Disposing) OrElse (Me.InUse = False) Then Exit Sub

		OrgInPaint = InPaint
		InPaint = True
		KnowledgeDateChanged = False
		SetButtonStatus_Flag = False

		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True) Or (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.Connection) = True) Then
			KnowledgeDateChanged = True
			RefreshForm = True
		End If

		' ****************************************************************
		' Check for changes relevant to this form
		' ****************************************************************

		' Changes to the tblInstrument table :-
		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblInstrument) = True) Or KnowledgeDateChanged Then

			' Re-Set combo.
			Call SetInstrumentCombo()
		End If

		' tblFlorenceGroup
		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblFlorenceGroup) = True) Or KnowledgeDateChanged Then

			' Re-Set combo.
			Call SetGroupCombo()
		End If

		' Changes to the tblFlorenceEntity table :-
		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblFlorenceEntity) = True) Or KnowledgeDateChanged Then

			' Re-Set combo.
			Call SetDDStatusCombo()
		End If


		' Changes to the KnowledgeDate :-
		If e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True Then
			SetButtonStatus_Flag = True
		End If

		' Changes to the tblUserPermissions table :-
		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblUserPermissions) = True) Or KnowledgeDateChanged Then

			' Check ongoing permissions.

			Call CheckPermissions()
			If (HasReadPermission = False) Then
				Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User has lost Read permission for this Form.", "", True)

				FormIsValid = False
				Me.Close()
				Exit Sub
			End If

			SetButtonStatus_Flag = True

		End If



		' ****************************************************************
		' Changes to the Main FORM table :-
		' ****************************************************************

		If (e.TableChanged(THIS_FORM_ChangeID) = True) Or KnowledgeDateChanged Then
			RefreshForm = True

			' Re-Set Controls etc.
			Call SetSortedRows()

			' Move again to the correct item
			thisPosition = Get_Position(thisAuditID)

			' Validate current position.
			If thisPosition >= 0 And thisPosition < Me.SortedRows.GetLength(0) Then
				thisDataRow = Me.SortedRows(thisPosition)
			Else
				If (Me.SortedRows.GetLength(0) > 0) And (AddNewRecord = False) Then
					thisPosition = 0
					thisDataRow = Me.SortedRows(thisPosition)
					FormChanged = False
				Else
					thisDataRow = Nothing
				End If
			End If

			' Set SelectingCombo Index.
			If (Me.THIS_FORM_SelectingCombo.Items.Count <= 0) Then
				Try
					Me.THIS_FORM_SelectingCombo.SelectedIndex = -1
				Catch ex As Exception
				End Try
			ElseIf (thisPosition < 0) Then
				Try
					MainForm.ClearComboSelection(THIS_FORM_SelectingCombo)
				Catch ex As Exception
				End Try
			Else
				Try
					Me.THIS_FORM_SelectingCombo.SelectedIndex = thisPosition
				Catch ex As Exception
				End Try
			End If

		End If

		' ****************************************************************
		' Repaint if not currently in Edit Mode
		'
		' ****************************************************************

		InPaint = OrgInPaint

		' Retrieve form data if appropriate, i.e. this data item is not already being edited.
		If (RefreshForm = True) AndAlso (FormChanged = False) AndAlso (AddNewRecord = False) Then
			GetFormData(thisDataRow) ' Includes a call to 'SetButtonStatus()'
		Else
			If SetButtonStatus_Flag Then
				Call SetButtonStatus()
			End If
		End If

	End Sub

#Region " Workhorse functions : SetSortedRows / CheckPermission / ControlsChanged / Menu Events "

	' Build Sorted list from the Source dataset and update the Select Combo
    ''' <summary>
    ''' Sets the sorted rows.
    ''' </summary>
	Private Sub SetSortedRows()

		Dim OrgInPaint As Boolean

		Dim thisrow As DataRow
		Dim thisDrowDownWidth As Integer
		Dim SizingBitmap As Bitmap
		Dim SizingGraphics As Graphics
		Dim SelectString As String = "RN >= 0"

		' Form Specific Selection Combo :-
		If (THIS_FORM_SelectingCombo Is Nothing) Then Exit Sub

		' Code is pretty Generic from here on...

		' Set paint local so that changes to the selection combo do not trigger form updates.

		OrgInPaint = InPaint
		InPaint = True

		If (Me.Combo_SelectGroup.SelectedIndex > 0) AndAlso (IsNumeric(Combo_SelectGroup.SelectedValue)) Then
			SelectString = "ItemGroupID=" & CInt(Combo_SelectGroup.SelectedValue).ToString
		End If

		' Get selected Row sets, or exit if no data is present.
		If myDataset Is Nothing Then
			ReDim SortedRows(0)
			ReDim SelectBySortedRows(0)
		Else
			SortedRows = myTable.Select(SelectString, THIS_FORM_OrderBy)
			SelectBySortedRows = myTable.Select(SelectString, THIS_FORM_SelectBy)
		End If

		' Set Combo data source
		THIS_FORM_SelectingCombo.DataSource = SortedRows
		THIS_FORM_SelectingCombo.DisplayMember = THIS_FORM_SelectBy
		THIS_FORM_SelectingCombo.ValueMember = THIS_FORM_ValueMember

		' Ensure that the Selection Combo's DrowDownWidth is wide enough to take the required strings

		thisDrowDownWidth = THIS_FORM_SelectingCombo.Width
		For Each thisrow In SortedRows

			' Compute the string dimensions in the given font
			SizingBitmap = New Bitmap(1, 1, System.Drawing.Imaging.PixelFormat.Format32bppArgb)
			SizingGraphics = Graphics.FromImage(SizingBitmap)
			Dim stringSize As SizeF = SizingGraphics.MeasureString(thisrow(THIS_FORM_SelectBy).ToString, THIS_FORM_SelectingCombo.Font)
			If (stringSize.Width) > thisDrowDownWidth Then
				thisDrowDownWidth = CInt(stringSize.Width)
			End If
		Next

		THIS_FORM_SelectingCombo.DropDownWidth = thisDrowDownWidth

		InPaint = OrgInPaint
	End Sub


	' Check User permissions
    ''' <summary>
    ''' Checks the permissions.
    ''' </summary>
	Private Sub CheckPermissions()

		Dim Permissions As Integer

		Permissions = MainForm.CheckPermissions(THIS_FORM_PermissionArea, THIS_FORM_PermissionType)

		HasReadPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermRead) > 0)
		HasUpdatePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermUpdate) > 0)
		HasInsertPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermInsert) > 0)
		HasDeletePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermDelete) > 0)

	End Sub

	' Flag changes to form controls, Event Associations made in 'New' routine.
    ''' <summary>
    ''' Forms the control changed.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub FormControlChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

		If InPaint = False Then
			If (Me.HasUpdatePermission) Or (Me.HasInsertPermission And Me.AddNewRecord) Then
				FormChanged = True
				Me.btnSave.Enabled = True
				Me.btnCancel.Enabled = True
			End If
		End If

	End Sub

    ''' <summary>
    ''' Selects the menu event.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
	Public Sub SelectMenuEvent(ByVal sender As Object, ByVal e As EventArgs)
		' Event handler for the Select Menu
		' Event association is made during the dynamic menu creation

		THIS_FORM_SelectBy = CType(sender, System.Windows.Forms.ToolStripMenuItem).Text

		Call SetSortedRows()

	End Sub

    ''' <summary>
    ''' Orders the menu event.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
	Public Sub OrderMenuEvent(ByVal sender As Object, ByVal e As EventArgs)
		' Event handler for the Order Menu
		' Event association is made during the dynamic menu creation

		THIS_FORM_OrderBy = CType(sender, System.Windows.Forms.ToolStripMenuItem).Text

		Call SetSortedRows()

	End Sub


    ''' <summary>
    ''' Audits the report menu event.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
	Public Sub AuditReportMenuEvent(ByVal sender As Object, ByVal e As EventArgs)
		' Event handler for the Audit Report Menu
		' Event association is made during the dynamic menu creation

		Select Case CInt(CType(sender, System.Windows.Forms.ToolStripMenuItem).Tag)
			Case 0 ' This Record
				If (Me.AddNewRecord = False) AndAlso (thisAuditID >= 0) Then
					Run_AuditReport_ThisRecord(MainForm, ThisStandardDataset, thisAuditID)
				End If

			Case 1 ' All Records
				Run_AuditReport_ALLRecords(MainForm, ThisStandardDataset)

		End Select

	End Sub


#End Region

#Region " Set Form Combos (Form Specific Code) "

    ''' <summary>
    ''' Sets the DD status combo.
    ''' </summary>
	Private Sub SetDDStatusCombo()

		Call MainForm.SetTblGenericCombo( _
		Me.Combo_DDStatus, _
		RenaissanceStandardDatasets.tblFlorenceEntity, _
		"DDStatusComment", _
		"DDStatusComment", _
		"", True, True, False)	 ' 

	End Sub

    ''' <summary>
    ''' Sets the group combo.
    ''' </summary>
	Private Sub SetGroupCombo()

		Call MainForm.SetTblGenericCombo( _
		Me.Combo_ItemGroupID, _
		RenaissanceStandardDatasets.tblFlorenceGroup, _
		"ItemGroupDescription", _
		"ItemGroup", _
		"", False, True, False)		' 

		Call MainForm.SetTblGenericCombo( _
		Me.Combo_SelectGroup, _
		RenaissanceStandardDatasets.tblFlorenceGroup, _
		"ItemGroupDescription", _
		"ItemGroup", _
		"", False, True, True, 0, "All Groups")		' 

	End Sub

    ''' <summary>
    ''' Sets the instrument combo.
    ''' </summary>
	Private Sub SetInstrumentCombo()

		Call MainForm.SetTblGenericCombo( _
		Me.Combo_InstrumentID, _
		RenaissanceStandardDatasets.tblInstrument, _
		"InstrumentDescription", _
		"InstrumentID", _
		"(InstrumentType=" & CInt(RenaissanceGlobals.InstrumentTypes.Fund).ToString & ") AND (InstrumentParent=InstrumentID)", False, True, True, 0)	 ' 

	End Sub

#End Region

#Region " Get & Set Form Data / SetButton / ValidateForm / btnCancel Events (Form Specific Code) "

    ''' <summary>
    ''' Gets the form data.
    ''' </summary>
    ''' <param name="ThisRow">The this row.</param>
	Private Sub GetFormData(ByRef ThisRow As RenaissanceDataClass.DSFlorenceEntity.tblFlorenceEntityRow)
		' Routine to populate form Controls given a data row.
		' Form is cleared for invalid Datarows or Invalid Form.

		Dim OrgInpaint As Boolean

		' Set 'Paint' flag to prevent event actions caused by changing the value of form fields.
		OrgInpaint = InPaint
		InPaint = True


		If (ThisRow Is Nothing) Or (FormIsValid = False) Or (Me.InUse = False) Then
			' Bad / New Datarow - Clear Form.

			thisAuditID = (-1)

			Me.editAuditID.Text = ""
			Me.edit_EntityName.Text = ""

			If Me.Combo_ItemGroupID.Items.Count > 0 Then
				Combo_ItemGroupID.SelectedIndex = 0
			End If

			If Me.Combo_InstrumentID.Items.Count > 0 Then
				Combo_InstrumentID.SelectedIndex = 0
			End If

			Me.Text_AdminContact.Text = ""
			Me.Text_AdminEmailSalutation.Text = ""
			Me.Text_AdminEMail.Text = ""
			Me.Text_AdminFax.Text = ""
			Me.Text_AdminPhone.Text = ""
			Me.Check_IsInactive.Checked = False
			Me.Radio_DD_InProgress.Checked = True
			If Me.Combo_DDStatus.Items.Count > 0 Then
				Me.Combo_DDStatus.SelectedIndex = 0
			End If

			If AddNewRecord = True Then
				Me.btnCancel.Enabled = True
				Me.THIS_FORM_SelectingCombo.Enabled = False
			Else
				Me.btnCancel.Enabled = False
				Me.THIS_FORM_SelectingCombo.Enabled = True
			End If

		Else

			' Populate Form with given data.
			Try
				thisAuditID = thisDataRow.AuditID

				Me.editAuditID.Text = thisDataRow.AuditID.ToString

				Me.edit_EntityName.Text = thisDataRow.EntityName

				If Me.Combo_ItemGroupID.Items.Count > 0 Then
					Combo_ItemGroupID.SelectedValue = thisDataRow.ItemGroupID
				End If

				If Me.Combo_InstrumentID.Items.Count > 0 Then
					Combo_InstrumentID.SelectedValue = thisDataRow.InstrumentID
				End If

				Me.Text_AdminContact.Text = thisDataRow.AdminContact
				Me.Text_AdminEmailSalutation.Text = thisDataRow.AdminEmailSalutation
				Me.Text_AdminEMail.Text = thisDataRow.AdminEMail
				Me.Text_AdminFax.Text = thisDataRow.AdminFax
				Me.Text_AdminPhone.Text = thisDataRow.AdminPhone

				Me.Check_IsInactive.Checked = thisDataRow.EntityIsInactive

				If (thisDataRow.dDStatus = FlorenceDueDilligenceStatusIDs.Approved) Then
					Me.Radio_DD_Approved.Checked = True
				ElseIf (thisDataRow.dDStatus = FlorenceDueDilligenceStatusIDs.Declined) Then
					Me.Radio_DD_Denied.Checked = True
				Else
					Me.Radio_DD_InProgress.Checked = True
				End If

				Me.Combo_DDStatus.SelectedValue = thisDataRow.dDStatusComment

				AddNewRecord = False
				MainForm.SetComboSelectionLengths(Me)

				Me.btnCancel.Enabled = False
				Me.THIS_FORM_SelectingCombo.Enabled = True

			Catch ex As Exception

				Call MainForm.LogError(Me.Name & ", GetFormData", 0, ex.Message, "Error Showing Data", ex.StackTrace, True)
				Call GetFormData(Nothing)

			End Try

		End If

		' Allow Field events to trigger before 'InPaint' Is re-set. 
		' (Should) Prevent Validation errors during Form Draw.
		Application.DoEvents()

		' Restore 'Paint' flag.
		InPaint = OrgInpaint
		FormChanged = False
		Me.btnSave.Enabled = False

		' As it says on the can....
		Call SetButtonStatus()

	End Sub

    ''' <summary>
    ''' Sets the form data.
    ''' </summary>
    ''' <param name="pConfirm">if set to <c>true</c> [p confirm].</param>
    ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
	Private Function SetFormData(Optional ByVal pConfirm As Boolean = True) As Boolean
		' *************************************************************
		'
		' *************************************************************
		Dim ErrMessage As String
		Dim ErrFlag As Boolean
		Dim ErrStack As String
		Dim ProtectedItem As Boolean = False

		ErrMessage = ""
		ErrStack = ""

		' *************************************************************
		' Appropriate Save permission :-
		' *************************************************************

		If (Me.HasUpdatePermission = False) And ((Me.HasInsertPermission = False) Or (Me.AddNewRecord = False)) Then
			MainForm.LogError(Me.Name & ", SetFormData()", LOG_LEVELS.Warning, "", "You do not have permission to save this record.", "", True)
			Call btnCancel_Click(Me, New System.EventArgs)
			Exit Function
		End If

		' *************************************************************
		' If Save button is disabled then should not be able to save, exit silently.
		' *************************************************************
		If Me.btnSave.Enabled = False Then
			Call btnCancel_Click(Me, New System.EventArgs)
			Exit Function
		End If

		' *************************************************************
		' KnowledgeDate OK :-
		' *************************************************************

		If (MainForm.Main_Knowledgedate > KNOWLEDGEDATE_NOW) Then
			MainForm.LogError(Me.Name & ", SetFormData()", LOG_LEVELS.Warning, "", "Changes to Venice are not allowed when the Knowledgedate is not set to `NOW`", "", True)
			Call btnCancel_Click(Me, New System.EventArgs)
			Exit Function
		End If

		' Confirm Save, if required.

		If (pConfirm = True) Then
			If MessageBox.Show("Save Changes ?", "Save Changes ?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.No Then
				Return True
				Exit Function
			End If
		End If

		' *************************************************************
		' Procedure to Save the current form information.
		' *************************************************************

		Dim StatusString As String
		Dim LogString As String
		Dim UpdateRows(0) As DataRow
		Dim Position As Integer

		If (FormChanged = False) Or (FormIsValid = False) Then
			Return False
			Exit Function
		End If

		' Validation
		StatusString = ""
		If ValidateForm(StatusString) = False Then
			MainForm.LogError(Me.Name & ", SetFormData()", LOG_LEVELS.Warning, "", "Validation Error." & vbCrLf & StatusString, "", True)
			Return False
			Exit Function
		End If

		' Check current position in the table.
		Position = Get_Position(thisAuditID)

		' Allow for new or missing ID.
		If Position < 0 Then
			If (AddNewRecord = True) OrElse (SortedRows.Length <= 0) Then
				thisDataRow = myTable.NewRow
				LogString = "Add : "
				AddNewRecord = True
			Else
				Return False
				Exit Function
			End If
		Else
			' Row found OK.
			LogString = "Edit : AuditID = " & thisAuditID.ToString

			thisDataRow = Me.SortedRows(Position)

		End If

		' Set 'Paint' flag.
		InPaint = True

		' *************************************************************
		' Lock the Data Table, to prevent update conflicts.
		' *************************************************************
		SyncLock myTable

			' Initiate Edit,
			thisDataRow.BeginEdit()

			' Set Data Values

			thisDataRow.EntityName = edit_EntityName.Text
			LogString &= ", EntityName = " & edit_EntityName.Text

			thisDataRow.EntityIsInactive = Check_IsInactive.Checked
			LogString &= ", IsInactive = " & Check_IsInactive.Checked.ToString

			thisDataRow.ItemGroupID = CInt(Me.Combo_ItemGroupID.SelectedValue)
			LogString &= ", GroupID = " & CInt(Me.Combo_ItemGroupID.SelectedValue).ToString

			If (Combo_InstrumentID.SelectedIndex >= 0) Then
				thisDataRow.InstrumentID = CInt(Me.Combo_InstrumentID.SelectedValue)
			Else
				thisDataRow.InstrumentID = 0
			End If
			LogString &= ", Instrument = " & thisDataRow.InstrumentID.ToString

			thisDataRow.AdminContact = Me.Text_AdminContact.Text
			LogString &= ", AdminContact = " & Text_AdminContact.Text

			thisDataRow.AdminEmailSalutation = Me.Text_AdminEmailSalutation.Text
			LogString &= ", AdminEmailSalutation = " & Text_AdminEmailSalutation.Text

			thisDataRow.AdminPhone = Me.Text_AdminPhone.Text
			LogString &= ", AdminPhone = " & Text_AdminPhone.Text

			thisDataRow.AdminFax = Me.Text_AdminFax.Text
			LogString &= ", AdminFax = " & Text_AdminFax.Text

			thisDataRow.AdminEmail = Me.Text_AdminEMail.Text
			LogString &= ", AdminEmail = " & Text_AdminEMail.Text

			If Radio_DD_Approved.Checked Then
				thisDataRow.DDStatus = RenaissanceGlobals.FlorenceDueDilligenceStatusIDs.Approved
				LogString &= ", DDStatus = Approved"
			ElseIf Radio_DD_Denied.Checked Then
				thisDataRow.DDStatus = RenaissanceGlobals.FlorenceDueDilligenceStatusIDs.Declined
				LogString &= ", DDStatus = Declined"
			Else
				thisDataRow.DDStatus = RenaissanceGlobals.FlorenceDueDilligenceStatusIDs.InProgress
				LogString &= ", DDStatus = In Progress"
			End If

			thisDataRow.DDStatusComment = Combo_DDStatus.Text
			LogString &= ", DDStatusComment = " & Combo_DDStatus.Text

			thisDataRow.EndEdit()
			InPaint = False

			' Add and Update DataRow. 

			ErrFlag = False

			If AddNewRecord = True Then
				Try
					myTable.Rows.Add(thisDataRow)
				Catch ex As Exception
					ErrFlag = True
					ErrMessage = ex.Message
					ErrStack = ex.StackTrace
				End Try
			End If

			UpdateRows(0) = thisDataRow

			' Post Additions / Updates to the underlying table :-
			Dim temp As Integer
			Try
				If (ErrFlag = False) Then
					myAdaptor.InsertCommand.Parameters("@Knowledgedate").Value = MainForm.Main_Knowledgedate
					myAdaptor.UpdateCommand.Parameters("@Knowledgedate").Value = MainForm.Main_Knowledgedate

					temp = MainForm.AdaptorUpdate(Me.Name, myAdaptor, UpdateRows)
				End If

			Catch ex As Exception

				ErrMessage = ex.Message
				ErrFlag = True
				ErrStack = ex.StackTrace

			End Try

			thisAuditID = thisDataRow.AuditID


		End SyncLock

		' *************************************************************
		' Error report. Not done in the Try-Catch as that would serve to persist the SyncLock.
		' *************************************************************

		If (ErrFlag = True) Then
			Call MainForm.LogError(Me.Name & ", SetFormData", 0, ErrMessage, "Error Saving Data", ErrStack, True)
		End If

		' Finish off

		AddNewRecord = False
		FormChanged = False

		Me.THIS_FORM_SelectingCombo.Enabled = True

		' Propagate changes

		Call MainForm.Main_RaiseEvent(New RenaissanceGlobals.RenaissanceUpdateEventArgs(THIS_FORM_ChangeID))

	End Function

    ''' <summary>
    ''' Sets the button status.
    ''' </summary>
	Private Sub SetButtonStatus()
		' Sets the status of the form controlls appropriate to the current users 
		'permissions and the 'Changed' or 'New' status of the form.

		' No Read permission :-

		If Me.HasReadPermission = False Then
			MainForm.LogError(Me.Name & ", SetButtonStatus()", 0, "", "You do not have Read permission for this Form", "", True)
			Me.Close()
			Exit Sub
		End If

		' Has Insert Permission.
		If Me.HasInsertPermission Then
			Me.btnAdd.Enabled = True
		Else
			Me.btnAdd.Enabled = False
		End If

		' Has Delete permission. 
		If (Me.HasDeletePermission) And (Me.AddNewRecord = False) And (thisPosition >= 0) Then
			Me.btnDelete.Enabled = True
		Else
			Me.btnDelete.Enabled = False
		End If

		If (Me.MainForm.Main_Knowledgedate <= KNOWLEDGEDATE_NOW) And _
		 ((Me.HasUpdatePermission) Or (Me.HasInsertPermission And Me.AddNewRecord)) Then

			Me.edit_EntityName.Enabled = True
			Combo_ItemGroupID.Enabled = True
			Combo_InstrumentID.Enabled = True
			Text_AdminContact.Enabled = True
			Text_AdminEmailSalutation.Enabled = True
			Text_AdminEMail.Enabled = True
			Text_AdminFax.Enabled = True
			Text_AdminPhone.Enabled = True
			Panel_DDStatus.Enabled = True

		Else

			Me.edit_EntityName.Enabled = False
			Combo_ItemGroupID.Enabled = False
			Combo_InstrumentID.Enabled = False
			Text_AdminContact.Enabled = False
			Text_AdminEmailSalutation.Enabled = False
			Text_AdminEMail.Enabled = False
			Text_AdminFax.Enabled = False
			Text_AdminPhone.Enabled = False
			Panel_DDStatus.Enabled = False

		End If

	End Sub

    ''' <summary>
    ''' Validates the form.
    ''' </summary>
    ''' <param name="pReturnString">The p return string.</param>
    ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
	Private Function ValidateForm(ByRef pReturnString As String) As Boolean
		' Form Validation code.
		' 
		' This code should be the final arbiter of what is allowed. no assumptions regarding 
		' prior validation should be made.
		' 
		' This Code is called by the SetFormData routine before position changes.
		'
		Dim RVal As Boolean

		RVal = True
		pReturnString = ""

		' Validate 

		If Me.edit_EntityName.Text.Length <= 0 Then
			pReturnString = "Due Dilligence Entity must not be left blank."
			RVal = False
		End If

		If Me.Combo_ItemGroupID.SelectedIndex < 0 Then
			pReturnString = "Due Dilligence Group must not be left blank."
			RVal = False
		End If

		Return RVal

	End Function

    ''' <summary>
    ''' Handles the MouseEnter event of the btnCancel control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnCancel_MouseEnter(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.MouseEnter
		' Simple Events for the 'Cancel' Button to maintain the status of the 'IsOverCancelButton' flag
		Me.IsOverCancelButton = True
	End Sub

    ''' <summary>
    ''' Handles the MouseLeave event of the btnCancel control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnCancel_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.MouseLeave
		' Simple Events for the 'Cancel' Button to maintain the status of the 'IsOverCancelButton' flag
		Me.IsOverCancelButton = False
	End Sub

    ''' <summary>
    ''' Handles the SelectedIndexChanged event of the Combo_SelectGroup control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Combo_SelectGroup_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_SelectGroup.SelectedIndexChanged
		' ********************************************************************************************
		' This form allows the user to display only those Entities from a given group.
		' This combo controls this.
		'
		' ********************************************************************************************

		If InPaint Then
			Exit Sub
		End If

		If (FormChanged = True) Then
			Call SetFormData()
		End If

		Call SetSortedRows()

		thisPosition = 0
		If thisPosition >= THIS_FORM_SelectingCombo.Items.Count Then
			thisPosition = (THIS_FORM_SelectingCombo.Items.Count - 1)
		End If

		THIS_FORM_SelectingCombo.SelectedIndex = thisPosition

		If (thisPosition >= 0) Then
			thisDataRow = Me.SortedRows(thisPosition)
		Else
			thisDataRow = Nothing
		End If

		Call GetFormData(thisDataRow)
	End Sub

#End Region

#Region " Navigation Code / GetPosition() (Generic Code) "

    ''' <summary>
    ''' Handles the SelectComboChanged event of the Combo control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Combo_SelectComboChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
		' Selection Combo. SelectedItem changed.
		'

		' Don't react to changes made in paint routines etc.
		If InPaint = True Then Exit Sub

		If (FormChanged = True) Then
			Call SetFormData()
		End If

		' Find the correct data row, then show it...
		thisPosition = THIS_FORM_SelectingCombo.SelectedIndex

		If (thisPosition >= 0) Then
			thisDataRow = Me.SortedRows(thisPosition)
		Else
			thisDataRow = Nothing
		End If

		Call GetFormData(thisDataRow)

	End Sub


    ''' <summary>
    ''' Handles the Click event of the btnNavPrev control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnNavPrev_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNavPrev.Click
		' 'Previous' Button.

		If (FormChanged = True) Then
			Call SetFormData()
		End If

		If thisPosition > 0 Then thisPosition -= 1
		If thisPosition >= THIS_FORM_SelectingCombo.Items.Count Then
			thisPosition = (THIS_FORM_SelectingCombo.Items.Count - 1)
		End If

		THIS_FORM_SelectingCombo.SelectedIndex = thisPosition

		Exit Sub

		If myTable.Rows.Count > thisPosition Then
			thisDataRow = myTable.Rows(thisPosition)
			Call GetFormData(thisDataRow)
		Else
			Call GetFormData(Nothing)
		End If

	End Sub

    ''' <summary>
    ''' Handles the Click event of the btnNavNext control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnNavNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNavNext.Click
		' 'Next' Button.

		If (FormChanged = True) Then
			Call SetFormData()
		End If

		If thisPosition < (myTable.Rows.Count - 1) Then thisPosition += 1
		If thisPosition >= THIS_FORM_SelectingCombo.Items.Count Then
			thisPosition = (THIS_FORM_SelectingCombo.Items.Count - 1)
		End If

		THIS_FORM_SelectingCombo.SelectedIndex = thisPosition

		Exit Sub

		If myTable.Rows.Count > thisPosition Then
			thisDataRow = myTable.Rows(thisPosition)
			Call GetFormData(thisDataRow)
		Else
			Call GetFormData(Nothing)
		End If

	End Sub

    ''' <summary>
    ''' Handles the Click event of the btnNavFirst control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnNavFirst_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNavFirst.Click
		' 'First' Button.

		If (FormChanged = True) Then
			Call SetFormData()
		End If

		thisPosition = 0
		If thisPosition >= THIS_FORM_SelectingCombo.Items.Count Then
			thisPosition = (THIS_FORM_SelectingCombo.Items.Count - 1)
		End If

		THIS_FORM_SelectingCombo.SelectedIndex = thisPosition

	End Sub

    ''' <summary>
    ''' Handles the Click event of the btnLast control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnLast_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLast.Click
		' 'Last' Button.

		If (FormChanged = True) Then
			Call SetFormData()
		End If

		thisPosition = THIS_FORM_SelectingCombo.Items.Count - 1

		THIS_FORM_SelectingCombo.SelectedIndex = thisPosition

	End Sub

    ''' <summary>
    ''' Get_s the position.
    ''' </summary>
    ''' <param name="pAuditID">The p audit ID.</param>
    ''' <returns>System.Int32.</returns>
	Private Function Get_Position(ByVal pAuditID As Integer) As Integer
		' Returns the position in the current 'SortedRows' array of the entry corresponding to the given 
		' AudidID.

		Dim MinIndex As Integer
		Dim MaxIndex As Integer
		Dim CurrentMin As Integer
		Dim CurrentMax As Integer
		Dim searchDataRow As DataRow

		Try
			' SortedRows exists ?

			If SortedRows Is Nothing Then
				Return (-1)
				Exit Function
			End If

			' Return (-1) if there are no rows in the 'SortedRows'

			If (SortedRows.GetLength(0) <= 0) Then
				Return (-1)
				Exit Function
			End If


			' Use a modified Search moving outwards from the last returned value to locate the required value.
			' Reflecting the fact that for the most part One looks for closely located records.

			MinIndex = 0
			MaxIndex = SortedRows.GetLength(0) - 1


			' Check First and Last records (Incase 'First' or 'Last' was pressed).

			searchDataRow = SortedRows(MinIndex)

			If (searchDataRow.IsNull("AuditID") = False) AndAlso (CInt(CInt(searchDataRow("AuditID"))) = pAuditID) Then
				Return MinIndex
				Exit Function
			End If

			searchDataRow = SortedRows(MaxIndex)
			If (searchDataRow.IsNull("AuditID") = False) AndAlso (CInt(CInt(searchDataRow("AuditID"))) = pAuditID) Then
				Return MaxIndex
				Exit Function
			End If

			' now search outwards from the last returned value.

			If (Me.thisPosition >= MinIndex) And (Me.thisPosition <= MaxIndex) Then
				CurrentMin = thisPosition
				CurrentMax = CurrentMin + 1
			Else
				CurrentMin = CInt((MinIndex + MaxIndex) / 2)
				CurrentMax = CurrentMin + 1
			End If

			While (CurrentMin >= MinIndex) Or (CurrentMax <= MaxIndex)
				If (CurrentMin >= MinIndex) Then
					searchDataRow = SortedRows(CurrentMin)

					If (searchDataRow.IsNull("AuditID") = False) AndAlso (CInt(CInt(searchDataRow("AuditID"))) = pAuditID) Then
						Return CurrentMin
						Exit Function
					End If

					CurrentMin -= 1
				End If

				If (CurrentMax <= MaxIndex) Then
					searchDataRow = SortedRows(CurrentMax)

					If (searchDataRow.IsNull("AuditID") = False) AndAlso (CInt(CInt(searchDataRow("AuditID"))) = pAuditID) Then
						Return CurrentMax
						Exit Function
					End If

					CurrentMax += 1
				End If

			End While

			Return (-1)
			Exit Function

		Catch ex As Exception
			MainForm.LogError(Me.Name & ", Get_Position()", LOG_LEVELS.Warning, ex.Message, "", ex.StackTrace, True)
			Return (-1)
			Exit Function
		End Try

	End Function

#End Region

#Region " Buttons : Add / Delete / Cancel / Save and Close (Generic Code) "

    ''' <summary>
    ''' Handles the Click event of the btnCancel control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
		' Cancel Changes, redisplay form.

		FormChanged = False
		AddNewRecord = False

		Me.THIS_FORM_SelectingCombo.Enabled = True

		If (thisPosition >= 0) And (thisPosition < Me.SortedRows.GetLength(0)) Then
			thisDataRow = Me.SortedRows(thisPosition)
			Call GetFormData(thisDataRow)
		Else
			Call btnNavFirst_Click(Me, New System.EventArgs)
		End If

	End Sub

    ''' <summary>
    ''' Handles the Click event of the btnSave control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
		' Save Changes, if any, without prompting.

		If (FormChanged = True) Then
			Call SetFormData(False)
		End If

	End Sub

    ''' <summary>
    ''' Handles the Click event of the btnDelete control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
		Dim Position As Integer
		Dim NextAuditID As Integer

		If (AddNewRecord = True) Then
			Call btnCancel_Click(Me, New System.EventArgs)
			Exit Sub
		End If

		' No Appropriate Save permission :-

		If (Me.HasDeletePermission = False) Then
			MainForm.LogError(Me.Name & ", btnDelete()", LOG_LEVELS.Warning, "", "You do not have permission to Delete this record.", "", True)
			Call btnCancel_Click(Me, New System.EventArgs)
			Exit Sub
		End If

		' *************************************************************
		' KnowledgeDate OK :-
		' *************************************************************

		If (MainForm.Main_Knowledgedate > KNOWLEDGEDATE_NOW) Then
			MainForm.LogError(Me.Name & ", btnDelete()", LOG_LEVELS.Warning, "", "Changes to Venice are not allowed when the Knowledgedate is not set to `NOW`", "", True)
			Call btnCancel_Click(Me, New System.EventArgs)
			Exit Sub
		End If

		' *************************************************************
		' Confirm :-
		' *************************************************************
		If MsgBox("Are you Sure ?", MsgBoxStyle.OkCancel, "Confirm Delete") = MsgBoxResult.Cancel Then
			Exit Sub
		End If

		' Check Data position.

		Position = Get_Position(thisAuditID)

		If Position < 0 Then Exit Sub

		' Check Referential Integrity 
		If CheckReferentialIntegrity(_MainForm, Me.SortedRows(Position)) = False Then
			MainForm.LogError(Me.Name & ", btnDelete()", LOG_LEVELS.Warning, "", "Deleting this record would voilate referential integrity.", "", True)
			Call btnCancel_Click(Me, New System.EventArgs)
			Exit Sub
		End If

		' Resolve row to show after deleting this one.

		NextAuditID = (-1)
		If (Position + 1) < Me.SortedRows.GetLength(0) Then
			NextAuditID = CInt(Me.SortedRows(Position + 1)("AuditID"))
		ElseIf (Position - 1) >= 0 Then
			NextAuditID = CInt(Me.SortedRows(Position - 1)("AuditID"))
		Else
			NextAuditID = (-1)
		End If

		' Delete this row

		Try
			Me.SortedRows(Position).Delete()

			MainForm.AdaptorUpdate(Me.Name, myAdaptor, myTable)

		Catch ex As Exception

			Call MainForm.LogError(Me.Name & ", Delete()", LOG_LEVELS.Error, ex.Message, "Error Deleting Record.", ex.StackTrace, True)
		End Try

		' Tidy Up.

		FormChanged = False

		thisAuditID = NextAuditID
		Call MainForm.Main_RaiseEvent(New RenaissanceGlobals.RenaissanceUpdateEventArgs(THIS_FORM_ChangeID))

	End Sub

    ''' <summary>
    ''' Handles the Click event of the btnAdd control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
		' Prepare form to Add a new record.

		If (FormChanged = True) Then
			Call SetFormData()
		End If

		thisPosition = -1
		InPaint = True
		MainForm.ClearComboSelection(THIS_FORM_SelectingCombo)
		InPaint = False

		GetFormData(Nothing)
		AddNewRecord = True
		Me.btnCancel.Enabled = True
		Me.THIS_FORM_SelectingCombo.Enabled = False
		If (Me.Combo_SelectGroup.SelectedIndex > 0) AndAlso (IsNumeric(Combo_SelectGroup.SelectedValue)) Then
			Me.Combo_ItemGroupID.SelectedValue = Combo_SelectGroup.SelectedValue
		End If

		Call SetButtonStatus()

		THIS_FORM_NewMoveToControl.Focus()

	End Sub

    ''' <summary>
    ''' Handles the Click event of the btnClose control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
		' Close Form

		If (FormChanged = True) Then
			Call SetFormData()
		End If

		Me.Close()

	End Sub

#End Region

#Region " Bug hunting "

  ' AddHandler myAdaptor.RowUpdating, AddressOf OnRowUpdating
  ' AddHandler myAdaptor.RowUpdated, AddressOf OnRowUpdated
  ' AddHandler myAdaptor.FillError, AddressOf OnRowFillError

    ''' <summary>
    ''' Called when [row updating].
    ''' </summary>
    ''' <param name="Sender">The sender.</param>
    ''' <param name="e">The <see cref="SqlRowUpdatingEventArgs"/> instance containing the event data.</param>
  Protected Shared Sub OnRowUpdating(ByVal Sender As Object, ByVal e As SqlRowUpdatingEventArgs)
    Dim a As Integer

    If Not e.Command Is Nothing Then
      a = 1
    End If

    If Not e.Errors Is Nothing Then
      a = 2
    End If
  End Sub

    ''' <summary>
    ''' Called when [row updated].
    ''' </summary>
    ''' <param name="Sender">The sender.</param>
    ''' <param name="e">The <see cref="SqlRowUpdatedEventArgs"/> instance containing the event data.</param>
  Protected Shared Sub OnRowUpdated(ByVal Sender As Object, ByVal e As SqlRowUpdatedEventArgs)
    Dim a As Integer

    If Not e.Command Is Nothing Then
      a = 1
    End If

    If Not e.Errors Is Nothing Then
      a = 2
    End If
  End Sub

    ''' <summary>
    ''' Called when [row fill error].
    ''' </summary>
    ''' <param name="Sender">The sender.</param>
    ''' <param name="e">The <see cref="FillErrorEventArgs"/> instance containing the event data.</param>
  Protected Shared Sub OnRowFillError(ByVal Sender As Object, ByVal e As FillErrorEventArgs)
    Dim a As Integer

    a = 1
  End Sub

#End Region



End Class
