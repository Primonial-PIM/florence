' ***********************************************************************
' Assembly         : Florence
' Author           : Nicholas Pennington
' Created          : 11-28-2012
'
' Last Modified By : acullig
' Last Modified On : 11-05-2012
' ***********************************************************************
' <copyright file="frmViewReport.vb" company="">
'     Copyright (c) . All rights reserved.
' </copyright>
' <summary></summary>
' ***********************************************************************
''' <summary>
''' Class frmViewReport
''' </summary>
Public Class frmViewReport
  Inherits System.Windows.Forms.Form
  Implements StandardFlorenceForm

#Region " Windows Form Designer generated code "

    ''' <summary>
    ''' Prevents a default instance of the <see cref="frmViewReport"/> class from being created.
    ''' </summary>
  Private Sub New()
	MyBase.New()

	'This call is required by the Windows Form Designer.
	InitializeComponent()

	'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
    ''' <summary>
    ''' Disposes of the resources (other than memory) used by the <see cref="T:System.Windows.Forms.Form" />.
    ''' </summary>
    ''' <param name="disposing">true to release both managed and unmanaged resources; false to release only unmanaged resources.</param>
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
	If disposing Then
	  If Not (components Is Nothing) Then
		components.Dispose()
	  End If
	End If
	MyBase.Dispose(disposing)
  End Sub
    ''' <summary>
    ''' The report preview
    ''' </summary>
  Friend WithEvents ReportPreview As C1.Win.C1Preview.C1PrintPreviewControl

  'Required by the Windows Form Designer
    ''' <summary>
    ''' The components
    ''' </summary>
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
    ''' <summary>
    ''' Initializes the component.
    ''' </summary>
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.ReportPreview = New C1.Win.C1Preview.C1PrintPreviewControl
    CType(Me.ReportPreview.PreviewPane, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.ReportPreview.SuspendLayout()
    Me.SuspendLayout()
    '
    'ReportPreview
    '
    Me.ReportPreview.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.ReportPreview.Location = New System.Drawing.Point(1, 1)
    Me.ReportPreview.Name = "ReportPreview"
    '
    'ReportPreview.PreviewPane
    '
    Me.ReportPreview.PreviewPane.ExportOptions.Content = New C1.Win.C1Preview.ExporterOptions(-1) {}
    Me.ReportPreview.PreviewPane.IntegrateExternalTools = True
    Me.ReportPreview.PreviewPane.TabIndex = 0
    Me.ReportPreview.Size = New System.Drawing.Size(885, 728)
    Me.ReportPreview.TabIndex = 0
    '
    'frmViewReport
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
    Me.ClientSize = New System.Drawing.Size(888, 729)
    Me.Controls.Add(Me.ReportPreview)
    Me.Name = "frmViewReport"
    Me.Text = "Florence Report"
    CType(Me.ReportPreview.PreviewPane, System.ComponentModel.ISupportInitialize).EndInit()
    Me.ReportPreview.ResumeLayout(False)
    Me.ReportPreview.PerformLayout()
    Me.ResumeLayout(False)

  End Sub

#End Region

#Region " Local Variable Declaration"

    ''' <summary>
    ''' The _ main form
    ''' </summary>
  Private _MainForm As FlorenceMain
    ''' <summary>
    ''' The _ form open failed
    ''' </summary>
  Private _FormOpenFailed As Boolean

  ' Form ToolTip
    ''' <summary>
    ''' The form tooltip
    ''' </summary>
  Private FormTooltip As New ToolTip()

#End Region

#Region " Form Properties"

    ''' <summary>
    ''' Gets or sets a value indicating whether this instance is over cancel button.
    ''' </summary>
    ''' <value><c>true</c> if this instance is over cancel button; otherwise, <c>false</c>.</value>
  Public Property IsOverCancelButton() As Boolean Implements Globals.StandardFlorenceForm.IsOverCancelButton
    Get
      Return False
    End Get
    Set(ByVal Value As Boolean)

    End Set
  End Property

    ''' <summary>
    ''' Closes the form.
    ''' </summary>
  Public Sub CloseForm() Implements Globals.StandardFlorenceForm.CloseForm
    Me.Close()
  End Sub

    ''' <summary>
    ''' Gets a value indicating whether this instance is in paint.
    ''' </summary>
    ''' <value><c>true</c> if this instance is in paint; otherwise, <c>false</c>.</value>
  Public ReadOnly Property IsInPaint() As Boolean Implements Globals.StandardFlorenceForm.IsInPaint
    Get
      Return False
    End Get
  End Property

    ''' <summary>
    ''' Gets a value indicating whether [in use].
    ''' </summary>
    ''' <value><c>true</c> if [in use]; otherwise, <c>false</c>.</value>
  Public ReadOnly Property InUse() As Boolean Implements Globals.StandardFlorenceForm.InUse
    Get
      Return True
    End Get
  End Property

    ''' <summary>
    ''' Gets a value indicating whether [form open failed].
    ''' </summary>
    ''' <value><c>true</c> if [form open failed]; otherwise, <c>false</c>.</value>
  Public ReadOnly Property FormOpenFailed() As Boolean Implements Globals.StandardFlorenceForm.FormOpenFailed
    Get
      Return _FormOpenFailed
    End Get
  End Property

    ''' <summary>
    ''' Gets the main form.
    ''' </summary>
    ''' <value>The main form.</value>
  Public ReadOnly Property MainForm() As FlorenceMain Implements Globals.StandardFlorenceForm.MainForm
    Get
      Return _MainForm
    End Get
  End Property

    ''' <summary>
    ''' Resets the form.
    ''' </summary>
  Public Sub ResetForm() Implements Globals.StandardFlorenceForm.ResetForm
    ReportPreview.Document = Nothing
  End Sub

    ''' <summary>
    ''' Gets or sets the report document.
    ''' </summary>
    ''' <value>The report document.</value>
  Public Property ReportDocument() As System.Drawing.Printing.PrintDocument
    Get
      Return ReportPreview.Document
    End Get
    Set(ByVal Value As System.Drawing.Printing.PrintDocument)
      ReportPreview.Document = Value
    End Set
  End Property

#End Region

    ''' <summary>
    ''' Initializes a new instance of the <see cref="frmViewReport"/> class.
    ''' </summary>
    ''' <param name="pMainForm">The p main form.</param>
  Public Sub New(ByRef pMainForm As FlorenceMain)
    Me.New()

    _MainForm = pMainForm
    _FormOpenFailed = False

    ' Set up the ToolTip
    MainForm.SetFormToolTip(Me, FormTooltip)

  End Sub


    ''' <summary>
    ''' Handles the Closing event of the frmViewReport control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.ComponentModel.CancelEventArgs"/> instance containing the event data.</param>
  Private Sub frmViewReport_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing

    Try
			If (ReportPreview.PreviewPane.Busy) Then
				e.Cancel = True
				Exit Sub
			End If

			MainForm.RemoveFromFormsCollection(Me)
    Catch ex As Exception
    End Try

  End Sub

End Class
