' ***********************************************************************
' Assembly         : Florence
' Author           : Nicholas Pennington
' Created          : 11-28-2012
'
' Last Modified By : acullig
' Last Modified On : 11-05-2012
' ***********************************************************************
' <copyright file="frmItems.vb" company="">
'     Copyright (c) . All rights reserved.
' </copyright>
' <summary></summary>
' ***********************************************************************
Imports System.Data.SqlClient
Imports RenaissanceGlobals
Imports RenaissanceGlobals.Globals

' @KeyDetail

''' <summary>
''' Class frmItems
''' </summary>
Public Class frmItems

  Inherits System.Windows.Forms.Form
  Implements StandardFlorenceForm

#Region " Windows Form Designer generated code "

    ''' <summary>
    ''' Prevents a default instance of the <see cref="frmItems"/> class from being created.
    ''' </summary>
  Private Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
    ''' <summary>
    ''' Disposes of the resources (other than memory) used by the <see cref="T:System.Windows.Forms.Form" />.
    ''' </summary>
    ''' <param name="disposing">true to release both managed and unmanaged resources; false to release only unmanaged resources.</param>
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
    ''' <summary>
    ''' The components
    ''' </summary>
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
    ''' <summary>
    ''' The LBL item description
    ''' </summary>
  Friend WithEvents lblItemDescription As System.Windows.Forms.Label
    ''' <summary>
    ''' The edit audit ID
    ''' </summary>
  Friend WithEvents editAuditID As System.Windows.Forms.TextBox
    ''' <summary>
    ''' The edit_ item description
    ''' </summary>
  Friend WithEvents edit_ItemDescription As System.Windows.Forms.TextBox
    ''' <summary>
    ''' The BTN nav first
    ''' </summary>
  Friend WithEvents btnNavFirst As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN nav prev
    ''' </summary>
  Friend WithEvents btnNavPrev As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN nav next
    ''' </summary>
  Friend WithEvents btnNavNext As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN last
    ''' </summary>
  Friend WithEvents btnLast As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN add
    ''' </summary>
  Friend WithEvents btnAdd As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN delete
    ''' </summary>
  Friend WithEvents btnDelete As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN cancel
    ''' </summary>
  Friend WithEvents btnCancel As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN save
    ''' </summary>
  Friend WithEvents btnSave As System.Windows.Forms.Button
    ''' <summary>
    ''' The combo_ select item ID
    ''' </summary>
  Friend WithEvents Combo_SelectItemID As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The panel1
    ''' </summary>
  Friend WithEvents Panel1 As System.Windows.Forms.Panel
    ''' <summary>
    ''' The label1
    ''' </summary>
  Friend WithEvents Label1 As System.Windows.Forms.Label
    ''' <summary>
    ''' The group box1
    ''' </summary>
  Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    ''' <summary>
    ''' The root menu
    ''' </summary>
  Friend WithEvents RootMenu As System.Windows.Forms.MenuStrip
    ''' <summary>
    ''' The combo_ item category
    ''' </summary>
  Friend WithEvents Combo_ItemCategory As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The LBL_ category
    ''' </summary>
  Friend WithEvents lbl_Category As System.Windows.Forms.Label
    ''' <summary>
    ''' The LBL_ update period
    ''' </summary>
  Friend WithEvents lbl_UpdatePeriod As System.Windows.Forms.Label
    ''' <summary>
    ''' The combo_ update period
    ''' </summary>
  Friend WithEvents Combo_UpdatePeriod As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The text_ item message
    ''' </summary>
  Friend WithEvents Text_ItemMessage As System.Windows.Forms.TextBox
    ''' <summary>
    ''' The LBL_ item message
    ''' </summary>
  Friend WithEvents lbl_ItemMessage As System.Windows.Forms.Label
    ''' <summary>
    ''' The LBL_ item group
    ''' </summary>
  Friend WithEvents lbl_ItemGroup As System.Windows.Forms.Label
    ''' <summary>
    ''' The combo_ item group
    ''' </summary>
  Friend WithEvents Combo_ItemGroup As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The check_ item discontinued
    ''' </summary>
  Friend WithEvents Check_ItemDiscontinued As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The check_ key detail
    ''' </summary>
  Friend WithEvents Check_KeyDetail As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The check_ is limit item
    ''' </summary>
  Friend WithEvents Check_IsLimitItem As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The panel_ limit details
    ''' </summary>
  Friend WithEvents Panel_LimitDetails As System.Windows.Forms.Panel
    ''' <summary>
    ''' The label2
    ''' </summary>
  Friend WithEvents Label2 As System.Windows.Forms.Label
    ''' <summary>
    ''' The combo_ limit type
    ''' </summary>
  Friend WithEvents Combo_LimitType As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The label_ second limit
    ''' </summary>
  Friend WithEvents Label_SecondLimit As System.Windows.Forms.Label
    ''' <summary>
    ''' The label_ first limit
    ''' </summary>
  Friend WithEvents Label_FirstLimit As System.Windows.Forms.Label
    ''' <summary>
    ''' The numeric_ limit to
    ''' </summary>
  Friend WithEvents Numeric_LimitTo As RenaissanceControls.NumericTextBox
    ''' <summary>
    ''' The numeric_ limit from
    ''' </summary>
  Friend WithEvents Numeric_LimitFrom As RenaissanceControls.NumericTextBox
    ''' <summary>
    ''' The check_ is data item
    ''' </summary>
  Friend WithEvents Check_IsDataItem As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The combo_ data item type
    ''' </summary>
  Friend WithEvents Combo_DataItemType As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The label3
    ''' </summary>
  Friend WithEvents Label3 As System.Windows.Forms.Label
    ''' <summary>
    ''' The check_ limit is percent
    ''' </summary>
	Friend WithEvents Check_LimitIsPercent As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The numeric_ limit threshold
    ''' </summary>
	Friend WithEvents Numeric_LimitThreshold As RenaissanceControls.NumericTextBox
    ''' <summary>
    ''' The label4
    ''' </summary>
	Friend WithEvents Label4 As System.Windows.Forms.Label
    ''' <summary>
    ''' The label5
    ''' </summary>
	Friend WithEvents Label5 As System.Windows.Forms.Label
    ''' <summary>
    ''' The combo_ select group
    ''' </summary>
	Friend WithEvents Combo_SelectGroup As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The check_ boolean limit
    ''' </summary>
  Friend WithEvents Check_BooleanLimit As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The label6
    ''' </summary>
  Friend WithEvents Label6 As System.Windows.Forms.Label
    ''' <summary>
    ''' The combo_ data update period
    ''' </summary>
  Friend WithEvents Combo_DataUpdatePeriod As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The check_ update status on data update
    ''' </summary>
  Friend WithEvents Check_UpdateStatusOnDataUpdate As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The BTN close
    ''' </summary>
  Friend WithEvents btnClose As System.Windows.Forms.Button
    ''' <summary>
    ''' Initializes the component.
    ''' </summary>
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.lblItemDescription = New System.Windows.Forms.Label
        Me.editAuditID = New System.Windows.Forms.TextBox
        Me.edit_ItemDescription = New System.Windows.Forms.TextBox
        Me.btnNavFirst = New System.Windows.Forms.Button
        Me.btnNavPrev = New System.Windows.Forms.Button
        Me.btnNavNext = New System.Windows.Forms.Button
        Me.btnLast = New System.Windows.Forms.Button
        Me.btnAdd = New System.Windows.Forms.Button
        Me.btnDelete = New System.Windows.Forms.Button
        Me.btnCancel = New System.Windows.Forms.Button
        Me.btnSave = New System.Windows.Forms.Button
        Me.Combo_SelectItemID = New System.Windows.Forms.ComboBox
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.Label1 = New System.Windows.Forms.Label
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.btnClose = New System.Windows.Forms.Button
        Me.RootMenu = New System.Windows.Forms.MenuStrip
        Me.Combo_ItemCategory = New System.Windows.Forms.ComboBox
        Me.lbl_Category = New System.Windows.Forms.Label
        Me.lbl_UpdatePeriod = New System.Windows.Forms.Label
        Me.Combo_UpdatePeriod = New System.Windows.Forms.ComboBox
        Me.Text_ItemMessage = New System.Windows.Forms.TextBox
        Me.lbl_ItemMessage = New System.Windows.Forms.Label
        Me.lbl_ItemGroup = New System.Windows.Forms.Label
        Me.Combo_ItemGroup = New System.Windows.Forms.ComboBox
        Me.Check_ItemDiscontinued = New System.Windows.Forms.CheckBox
        Me.Check_KeyDetail = New System.Windows.Forms.CheckBox
        Me.Check_IsLimitItem = New System.Windows.Forms.CheckBox
        Me.Panel_LimitDetails = New System.Windows.Forms.Panel
        Me.Check_BooleanLimit = New System.Windows.Forms.CheckBox
        Me.Numeric_LimitThreshold = New RenaissanceControls.NumericTextBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.Check_LimitIsPercent = New System.Windows.Forms.CheckBox
        Me.Numeric_LimitTo = New RenaissanceControls.NumericTextBox
        Me.Numeric_LimitFrom = New RenaissanceControls.NumericTextBox
        Me.Label_SecondLimit = New System.Windows.Forms.Label
        Me.Label_FirstLimit = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Combo_LimitType = New System.Windows.Forms.ComboBox
        Me.Check_IsDataItem = New System.Windows.Forms.CheckBox
        Me.Combo_DataItemType = New System.Windows.Forms.ComboBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.Combo_SelectGroup = New System.Windows.Forms.ComboBox
        Me.Label6 = New System.Windows.Forms.Label
        Me.Combo_DataUpdatePeriod = New System.Windows.Forms.ComboBox
        Me.Check_UpdateStatusOnDataUpdate = New System.Windows.Forms.CheckBox
        Me.Panel1.SuspendLayout()
        Me.Panel_LimitDetails.SuspendLayout()
        Me.SuspendLayout()
        '
        'lblItemDescription
        '
        Me.lblItemDescription.AutoSize = True
        Me.lblItemDescription.Location = New System.Drawing.Point(3, 153)
        Me.lblItemDescription.Name = "lblItemDescription"
        Me.lblItemDescription.Size = New System.Drawing.Size(83, 13)
        Me.lblItemDescription.TabIndex = 39
        Me.lblItemDescription.Text = "Item Description"
        '
        'editAuditID
        '
        Me.editAuditID.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.editAuditID.Enabled = False
        Me.editAuditID.Location = New System.Drawing.Point(308, 58)
        Me.editAuditID.Name = "editAuditID"
        Me.editAuditID.Size = New System.Drawing.Size(64, 20)
        Me.editAuditID.TabIndex = 2
        '
        'edit_ItemDescription
        '
        Me.edit_ItemDescription.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.edit_ItemDescription.Location = New System.Drawing.Point(120, 150)
        Me.edit_ItemDescription.Name = "edit_ItemDescription"
        Me.edit_ItemDescription.Size = New System.Drawing.Size(256, 20)
        Me.edit_ItemDescription.TabIndex = 5
        '
        'btnNavFirst
        '
        Me.btnNavFirst.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.btnNavFirst.Location = New System.Drawing.Point(8, 8)
        Me.btnNavFirst.Name = "btnNavFirst"
        Me.btnNavFirst.Size = New System.Drawing.Size(40, 28)
        Me.btnNavFirst.TabIndex = 0
        Me.btnNavFirst.Text = "<<"
        '
        'btnNavPrev
        '
        Me.btnNavPrev.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.btnNavPrev.Location = New System.Drawing.Point(50, 8)
        Me.btnNavPrev.Name = "btnNavPrev"
        Me.btnNavPrev.Size = New System.Drawing.Size(35, 28)
        Me.btnNavPrev.TabIndex = 1
        Me.btnNavPrev.Text = "<"
        '
        'btnNavNext
        '
        Me.btnNavNext.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.btnNavNext.Location = New System.Drawing.Point(88, 8)
        Me.btnNavNext.Name = "btnNavNext"
        Me.btnNavNext.Size = New System.Drawing.Size(35, 28)
        Me.btnNavNext.TabIndex = 2
        Me.btnNavNext.Text = ">"
        '
        'btnLast
        '
        Me.btnLast.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.btnLast.Location = New System.Drawing.Point(124, 8)
        Me.btnLast.Name = "btnLast"
        Me.btnLast.Size = New System.Drawing.Size(40, 28)
        Me.btnLast.TabIndex = 3
        Me.btnLast.Text = ">>"
        '
        'btnAdd
        '
        Me.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.btnAdd.Location = New System.Drawing.Point(176, 8)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.Size = New System.Drawing.Size(75, 28)
        Me.btnAdd.TabIndex = 4
        Me.btnAdd.Text = "&New"
        '
        'btnDelete
        '
        Me.btnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.btnDelete.Location = New System.Drawing.Point(115, 550)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.Size = New System.Drawing.Size(75, 28)
        Me.btnDelete.TabIndex = 19
        Me.btnDelete.Text = "&Delete"
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.btnCancel.Location = New System.Drawing.Point(199, 550)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(75, 28)
        Me.btnCancel.TabIndex = 20
        Me.btnCancel.Text = "&Cancel"
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.btnSave.Location = New System.Drawing.Point(27, 550)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(75, 28)
        Me.btnSave.TabIndex = 18
        Me.btnSave.Text = "&Save"
        '
        'Combo_SelectItemID
        '
        Me.Combo_SelectItemID.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Combo_SelectItemID.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.Combo_SelectItemID.Location = New System.Drawing.Point(120, 58)
        Me.Combo_SelectItemID.Name = "Combo_SelectItemID"
        Me.Combo_SelectItemID.Size = New System.Drawing.Size(182, 21)
        Me.Combo_SelectItemID.TabIndex = 1
        '
        'Panel1
        '
        Me.Panel1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.btnNavFirst)
        Me.Panel1.Controls.Add(Me.btnNavPrev)
        Me.Panel1.Controls.Add(Me.btnNavNext)
        Me.Panel1.Controls.Add(Me.btnLast)
        Me.Panel1.Controls.Add(Me.btnAdd)
        Me.Panel1.Location = New System.Drawing.Point(63, 494)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(260, 48)
        Me.Panel1.TabIndex = 17
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(4, 58)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(100, 23)
        Me.Label1.TabIndex = 23
        Me.Label1.Text = "Select"
        '
        'GroupBox1
        '
        Me.GroupBox1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox1.Location = New System.Drawing.Point(8, 86)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(364, 4)
        Me.GroupBox1.TabIndex = 77
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "GroupBox1"
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.btnClose.Location = New System.Drawing.Point(283, 550)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(75, 28)
        Me.btnClose.TabIndex = 21
        Me.btnClose.Text = "&Close"
        '
        'RootMenu
        '
        Me.RootMenu.Location = New System.Drawing.Point(0, 0)
        Me.RootMenu.Name = "RootMenu"
        Me.RootMenu.Size = New System.Drawing.Size(384, 24)
        Me.RootMenu.TabIndex = 22
        Me.RootMenu.Text = " "
        '
        'Combo_ItemCategory
        '
        Me.Combo_ItemCategory.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Combo_ItemCategory.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.Combo_ItemCategory.FormattingEnabled = True
        Me.Combo_ItemCategory.Location = New System.Drawing.Point(120, 123)
        Me.Combo_ItemCategory.Name = "Combo_ItemCategory"
        Me.Combo_ItemCategory.Size = New System.Drawing.Size(256, 21)
        Me.Combo_ItemCategory.TabIndex = 4
        '
        'lbl_Category
        '
        Me.lbl_Category.AutoSize = True
        Me.lbl_Category.Location = New System.Drawing.Point(3, 126)
        Me.lbl_Category.Name = "lbl_Category"
        Me.lbl_Category.Size = New System.Drawing.Size(49, 13)
        Me.lbl_Category.TabIndex = 80
        Me.lbl_Category.Text = "Category"
        '
        'lbl_UpdatePeriod
        '
        Me.lbl_UpdatePeriod.AutoSize = True
        Me.lbl_UpdatePeriod.Location = New System.Drawing.Point(3, 179)
        Me.lbl_UpdatePeriod.Name = "lbl_UpdatePeriod"
        Me.lbl_UpdatePeriod.Size = New System.Drawing.Size(108, 13)
        Me.lbl_UpdatePeriod.TabIndex = 82
        Me.lbl_UpdatePeriod.Text = "Status Update Period"
        '
        'Combo_UpdatePeriod
        '
        Me.Combo_UpdatePeriod.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Combo_UpdatePeriod.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.Combo_UpdatePeriod.FormattingEnabled = True
        Me.Combo_UpdatePeriod.Location = New System.Drawing.Point(120, 176)
        Me.Combo_UpdatePeriod.Name = "Combo_UpdatePeriod"
        Me.Combo_UpdatePeriod.Size = New System.Drawing.Size(256, 21)
        Me.Combo_UpdatePeriod.TabIndex = 6
        '
        'Text_ItemMessage
        '
        Me.Text_ItemMessage.AcceptsReturn = True
        Me.Text_ItemMessage.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Text_ItemMessage.Location = New System.Drawing.Point(119, 204)
        Me.Text_ItemMessage.MaxLength = 500
        Me.Text_ItemMessage.Multiline = True
        Me.Text_ItemMessage.Name = "Text_ItemMessage"
        Me.Text_ItemMessage.Size = New System.Drawing.Size(256, 37)
        Me.Text_ItemMessage.TabIndex = 7
        '
        'lbl_ItemMessage
        '
        Me.lbl_ItemMessage.AutoSize = True
        Me.lbl_ItemMessage.Location = New System.Drawing.Point(3, 204)
        Me.lbl_ItemMessage.Name = "lbl_ItemMessage"
        Me.lbl_ItemMessage.Size = New System.Drawing.Size(73, 13)
        Me.lbl_ItemMessage.TabIndex = 84
        Me.lbl_ItemMessage.Text = "Item Message"
        '
        'lbl_ItemGroup
        '
        Me.lbl_ItemGroup.AutoSize = True
        Me.lbl_ItemGroup.Location = New System.Drawing.Point(3, 99)
        Me.lbl_ItemGroup.Name = "lbl_ItemGroup"
        Me.lbl_ItemGroup.Size = New System.Drawing.Size(59, 13)
        Me.lbl_ItemGroup.TabIndex = 86
        Me.lbl_ItemGroup.Text = "Item Group"
        '
        'Combo_ItemGroup
        '
        Me.Combo_ItemGroup.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Combo_ItemGroup.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.Combo_ItemGroup.FormattingEnabled = True
        Me.Combo_ItemGroup.Location = New System.Drawing.Point(120, 96)
        Me.Combo_ItemGroup.Name = "Combo_ItemGroup"
        Me.Combo_ItemGroup.Size = New System.Drawing.Size(256, 21)
        Me.Combo_ItemGroup.TabIndex = 3
        '
        'Check_ItemDiscontinued
        '
        Me.Check_ItemDiscontinued.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Check_ItemDiscontinued.AutoSize = True
        Me.Check_ItemDiscontinued.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Check_ItemDiscontinued.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.Check_ItemDiscontinued.Location = New System.Drawing.Point(157, 465)
        Me.Check_ItemDiscontinued.Name = "Check_ItemDiscontinued"
        Me.Check_ItemDiscontinued.Size = New System.Drawing.Size(117, 18)
        Me.Check_ItemDiscontinued.TabIndex = 16
        Me.Check_ItemDiscontinued.Text = "Item Discontinued"
        Me.Check_ItemDiscontinued.UseVisualStyleBackColor = True
        '
        'Check_KeyDetail
        '
        Me.Check_KeyDetail.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Check_KeyDetail.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Check_KeyDetail.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.Check_KeyDetail.Location = New System.Drawing.Point(6, 465)
        Me.Check_KeyDetail.Name = "Check_KeyDetail"
        Me.Check_KeyDetail.Size = New System.Drawing.Size(125, 18)
        Me.Check_KeyDetail.TabIndex = 15
        Me.Check_KeyDetail.Text = "Key Detail"
        Me.Check_KeyDetail.UseVisualStyleBackColor = True
        '
        'Check_IsLimitItem
        '
        Me.Check_IsLimitItem.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Check_IsLimitItem.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Check_IsLimitItem.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.Check_IsLimitItem.Location = New System.Drawing.Point(7, 324)
        Me.Check_IsLimitItem.Name = "Check_IsLimitItem"
        Me.Check_IsLimitItem.Size = New System.Drawing.Size(125, 18)
        Me.Check_IsLimitItem.TabIndex = 13
        Me.Check_IsLimitItem.Text = "Limit Item"
        Me.Check_IsLimitItem.UseVisualStyleBackColor = True
        '
        'Panel_LimitDetails
        '
        Me.Panel_LimitDetails.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel_LimitDetails.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Panel_LimitDetails.Controls.Add(Me.Check_BooleanLimit)
        Me.Panel_LimitDetails.Controls.Add(Me.Numeric_LimitThreshold)
        Me.Panel_LimitDetails.Controls.Add(Me.Label4)
        Me.Panel_LimitDetails.Controls.Add(Me.Check_LimitIsPercent)
        Me.Panel_LimitDetails.Controls.Add(Me.Numeric_LimitTo)
        Me.Panel_LimitDetails.Controls.Add(Me.Numeric_LimitFrom)
        Me.Panel_LimitDetails.Controls.Add(Me.Label_SecondLimit)
        Me.Panel_LimitDetails.Controls.Add(Me.Label_FirstLimit)
        Me.Panel_LimitDetails.Controls.Add(Me.Label2)
        Me.Panel_LimitDetails.Controls.Add(Me.Combo_LimitType)
        Me.Panel_LimitDetails.Location = New System.Drawing.Point(6, 351)
        Me.Panel_LimitDetails.Name = "Panel_LimitDetails"
        Me.Panel_LimitDetails.Size = New System.Drawing.Size(372, 108)
        Me.Panel_LimitDetails.TabIndex = 14
        '
        'Check_BooleanLimit
        '
        Me.Check_BooleanLimit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Check_BooleanLimit.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Check_BooleanLimit.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.Check_BooleanLimit.Location = New System.Drawing.Point(6, 29)
        Me.Check_BooleanLimit.Name = "Check_BooleanLimit"
        Me.Check_BooleanLimit.Size = New System.Drawing.Size(109, 17)
        Me.Check_BooleanLimit.TabIndex = 96
        Me.Check_BooleanLimit.Text = "Limit"
        Me.Check_BooleanLimit.UseVisualStyleBackColor = True
        Me.Check_BooleanLimit.Visible = False
        '
        'Numeric_LimitThreshold
        '
        Me.Numeric_LimitThreshold.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Numeric_LimitThreshold.Location = New System.Drawing.Point(103, 79)
        Me.Numeric_LimitThreshold.Name = "Numeric_LimitThreshold"
        Me.Numeric_LimitThreshold.RenaissanceTag = Nothing
        Me.Numeric_LimitThreshold.Size = New System.Drawing.Size(261, 20)
        Me.Numeric_LimitThreshold.TabIndex = 4
        Me.Numeric_LimitThreshold.Text = "0.00%"
        Me.Numeric_LimitThreshold.TextFormat = "#,##0.00%"
        Me.Numeric_LimitThreshold.Value = 0
        '
        'Label4
        '
        Me.Label4.Location = New System.Drawing.Point(3, 82)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(94, 18)
        Me.Label4.TabIndex = 95
        Me.Label4.Text = "Pass Threshold"
        '
        'Check_LimitIsPercent
        '
        Me.Check_LimitIsPercent.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Check_LimitIsPercent.CheckAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.Check_LimitIsPercent.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.Check_LimitIsPercent.Location = New System.Drawing.Point(336, 4)
        Me.Check_LimitIsPercent.Name = "Check_LimitIsPercent"
        Me.Check_LimitIsPercent.Size = New System.Drawing.Size(28, 18)
        Me.Check_LimitIsPercent.TabIndex = 1
        Me.Check_LimitIsPercent.Text = "%"
        Me.Check_LimitIsPercent.UseVisualStyleBackColor = True
        '
        'Numeric_LimitTo
        '
        Me.Numeric_LimitTo.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Numeric_LimitTo.Location = New System.Drawing.Point(103, 53)
        Me.Numeric_LimitTo.Name = "Numeric_LimitTo"
        Me.Numeric_LimitTo.RenaissanceTag = Nothing
        Me.Numeric_LimitTo.Size = New System.Drawing.Size(261, 20)
        Me.Numeric_LimitTo.TabIndex = 3
        Me.Numeric_LimitTo.Text = "0.00"
        Me.Numeric_LimitTo.TextFormat = "#,##0.00"
        Me.Numeric_LimitTo.Value = 0
        '
        'Numeric_LimitFrom
        '
        Me.Numeric_LimitFrom.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Numeric_LimitFrom.Location = New System.Drawing.Point(103, 28)
        Me.Numeric_LimitFrom.Name = "Numeric_LimitFrom"
        Me.Numeric_LimitFrom.RenaissanceTag = Nothing
        Me.Numeric_LimitFrom.Size = New System.Drawing.Size(261, 20)
        Me.Numeric_LimitFrom.TabIndex = 2
        Me.Numeric_LimitFrom.Text = "0.00"
        Me.Numeric_LimitFrom.TextFormat = "#,##0.00"
        Me.Numeric_LimitFrom.Value = 0
        '
        'Label_SecondLimit
        '
        Me.Label_SecondLimit.Location = New System.Drawing.Point(3, 56)
        Me.Label_SecondLimit.Name = "Label_SecondLimit"
        Me.Label_SecondLimit.Size = New System.Drawing.Size(84, 18)
        Me.Label_SecondLimit.TabIndex = 92
        Me.Label_SecondLimit.Text = "Second Limit"
        '
        'Label_FirstLimit
        '
        Me.Label_FirstLimit.Location = New System.Drawing.Point(3, 31)
        Me.Label_FirstLimit.Name = "Label_FirstLimit"
        Me.Label_FirstLimit.Size = New System.Drawing.Size(84, 18)
        Me.Label_FirstLimit.TabIndex = 91
        Me.Label_FirstLimit.Text = "First Limit"
        '
        'Label2
        '
        Me.Label2.Location = New System.Drawing.Point(3, 5)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(84, 20)
        Me.Label2.TabIndex = 90
        Me.Label2.Text = "Limit Type"
        '
        'Combo_LimitType
        '
        Me.Combo_LimitType.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Combo_LimitType.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.Combo_LimitType.FormattingEnabled = True
        Me.Combo_LimitType.Location = New System.Drawing.Point(103, 2)
        Me.Combo_LimitType.Name = "Combo_LimitType"
        Me.Combo_LimitType.Size = New System.Drawing.Size(230, 21)
        Me.Combo_LimitType.TabIndex = 0
        '
        'Check_IsDataItem
        '
        Me.Check_IsDataItem.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Check_IsDataItem.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Check_IsDataItem.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.Check_IsDataItem.Location = New System.Drawing.Point(7, 248)
        Me.Check_IsDataItem.Name = "Check_IsDataItem"
        Me.Check_IsDataItem.Size = New System.Drawing.Size(125, 18)
        Me.Check_IsDataItem.TabIndex = 9
        Me.Check_IsDataItem.Text = "Data Item"
        Me.Check_IsDataItem.UseVisualStyleBackColor = True
        '
        'Combo_DataItemType
        '
        Me.Combo_DataItemType.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Combo_DataItemType.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.Combo_DataItemType.Location = New System.Drawing.Point(199, 247)
        Me.Combo_DataItemType.Name = "Combo_DataItemType"
        Me.Combo_DataItemType.Size = New System.Drawing.Size(176, 21)
        Me.Combo_DataItemType.TabIndex = 10
        '
        'Label3
        '
        Me.Label3.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label3.Location = New System.Drawing.Point(138, 250)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(56, 19)
        Me.Label3.TabIndex = 87
        Me.Label3.Text = "Type"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(3, 31)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(69, 13)
        Me.Label5.TabIndex = 89
        Me.Label5.Text = "Select Group"
        '
        'Combo_SelectGroup
        '
        Me.Combo_SelectGroup.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Combo_SelectGroup.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.Combo_SelectGroup.Location = New System.Drawing.Point(119, 31)
        Me.Combo_SelectGroup.Name = "Combo_SelectGroup"
        Me.Combo_SelectGroup.Size = New System.Drawing.Size(253, 21)
        Me.Combo_SelectGroup.TabIndex = 0
        '
        'Label6
        '
        Me.Label6.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(4, 276)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(101, 13)
        Me.Label6.TabIndex = 91
        Me.Label6.Text = "Data Update Period"
        '
        'Combo_DataUpdatePeriod
        '
        Me.Combo_DataUpdatePeriod.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Combo_DataUpdatePeriod.Enabled = False
        Me.Combo_DataUpdatePeriod.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.Combo_DataUpdatePeriod.FormattingEnabled = True
        Me.Combo_DataUpdatePeriod.Location = New System.Drawing.Point(119, 273)
        Me.Combo_DataUpdatePeriod.Name = "Combo_DataUpdatePeriod"
        Me.Combo_DataUpdatePeriod.Size = New System.Drawing.Size(256, 21)
        Me.Combo_DataUpdatePeriod.TabIndex = 11
        '
        'Check_UpdateStatusOnDataUpdate
        '
        Me.Check_UpdateStatusOnDataUpdate.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Check_UpdateStatusOnDataUpdate.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.Check_UpdateStatusOnDataUpdate.Location = New System.Drawing.Point(119, 300)
        Me.Check_UpdateStatusOnDataUpdate.Name = "Check_UpdateStatusOnDataUpdate"
        Me.Check_UpdateStatusOnDataUpdate.Size = New System.Drawing.Size(249, 18)
        Me.Check_UpdateStatusOnDataUpdate.TabIndex = 12
        Me.Check_UpdateStatusOnDataUpdate.Text = "Update Item Status when Data is Updated"
        Me.Check_UpdateStatusOnDataUpdate.UseVisualStyleBackColor = True
        '
        'frmItems
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(384, 585)
        Me.Controls.Add(Me.Check_UpdateStatusOnDataUpdate)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Combo_DataUpdatePeriod)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Combo_SelectGroup)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Combo_DataItemType)
        Me.Controls.Add(Me.Check_IsDataItem)
        Me.Controls.Add(Me.Panel_LimitDetails)
        Me.Controls.Add(Me.Check_IsLimitItem)
        Me.Controls.Add(Me.Check_KeyDetail)
        Me.Controls.Add(Me.Check_ItemDiscontinued)
        Me.Controls.Add(Me.lbl_ItemGroup)
        Me.Controls.Add(Me.Combo_ItemGroup)
        Me.Controls.Add(Me.Text_ItemMessage)
        Me.Controls.Add(Me.lbl_ItemMessage)
        Me.Controls.Add(Me.lbl_UpdatePeriod)
        Me.Controls.Add(Me.Combo_UpdatePeriod)
        Me.Controls.Add(Me.lbl_Category)
        Me.Controls.Add(Me.Combo_ItemCategory)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Combo_SelectItemID)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.editAuditID)
        Me.Controls.Add(Me.edit_ItemDescription)
        Me.Controls.Add(Me.lblItemDescription)
        Me.Controls.Add(Me.btnDelete)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.RootMenu)
        Me.MainMenuStrip = Me.RootMenu
        Me.MinimumSize = New System.Drawing.Size(400, 600)
        Me.Name = "frmItems"
        Me.Text = "Add/Edit Due Dilligence Items"
        Me.Panel1.ResumeLayout(False)
        Me.Panel_LimitDetails.ResumeLayout(False)
        Me.Panel_LimitDetails.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

#End Region

#Region " Form Locals and Constants "


	' Form 'Parent', the Main Venice form.
	' Generally only accessed through the 'MainForm' property.
    ''' <summary>
    ''' The _ main form
    ''' </summary>
	Private WithEvents _MainForm As FlorenceMain

	' Form ToolTip
    ''' <summary>
    ''' The form tooltip
    ''' </summary>
	Private FormTooltip As New ToolTip()

	' Form Constants, specific to the table being updated.

    ''' <summary>
    ''' The ALWAY s_ CLOS e_ THI s_ FORM
    ''' </summary>
	Private ALWAYS_CLOSE_THIS_FORM As Boolean = False

	' Form Locals, initialised on 'New' defining what standard data items to use
    ''' <summary>
    ''' The THI s_ TABLENAME
    ''' </summary>
	Private THIS_TABLENAME As String
    ''' <summary>
    ''' The THI s_ ADAPTORNAME
    ''' </summary>
	Private THIS_ADAPTORNAME As String
    ''' <summary>
    ''' The THI s_ DATASETNAME
    ''' </summary>
	Private THIS_DATASETNAME As String


	' The standard ChangeID for this form. e.g. tblFlorenceItems
    ''' <summary>
    ''' The THI s_ FOR m_ change ID
    ''' </summary>
	Private THIS_FORM_ChangeID As RenaissanceGlobals.RenaissanceChangeID

    ''' <summary>
    ''' The THI s_ FOR m_ selecting combo
    ''' </summary>
	Private THIS_FORM_SelectingCombo As ComboBox
    ''' <summary>
    ''' The THI s_ FOR m_ new move to control
    ''' </summary>
	Private THIS_FORM_NewMoveToControl As Control

	' Form Specific Order fields
    ''' <summary>
    ''' The THI s_ FOR m_ select by
    ''' </summary>
	Private THIS_FORM_SelectBy As String
    ''' <summary>
    ''' The THI s_ FOR m_ order by
    ''' </summary>
	Private THIS_FORM_OrderBy As String

    ''' <summary>
    ''' The THI s_ FOR m_ value member
    ''' </summary>
	Private THIS_FORM_ValueMember As String

	' Form specific Permissioning variables
    ''' <summary>
    ''' The THI s_ FOR m_ permission area
    ''' </summary>
	Private THIS_FORM_PermissionArea As String
    ''' <summary>
    ''' The THI s_ FOR m_ permission type
    ''' </summary>
	Private THIS_FORM_PermissionType As RenaissanceGlobals.PermissionFeatureType

	' Form specific Form type 
    ''' <summary>
    ''' The THI s_ FOR m_ form ID
    ''' </summary>
	Private THIS_FORM_FormID As FlorenceFormID

	' Data Structures

    ''' <summary>
    ''' My dataset
    ''' </summary>
	Private myDataset As RenaissanceDataClass.DSFlorenceItems		 ' Form Specific !!!!
    ''' <summary>
    ''' My table
    ''' </summary>
	Private myTable As RenaissanceDataClass.DSFlorenceItems.tblFlorenceItemsDataTable
    ''' <summary>
    ''' My connection
    ''' </summary>
	Private myConnection As SqlConnection
    ''' <summary>
    ''' My adaptor
    ''' </summary>
	Private myAdaptor As SqlDataAdapter

    ''' <summary>
    ''' The this standard dataset
    ''' </summary>
	Private ThisStandardDataset As RenaissanceGlobals.StandardDataset


	' Active Element.

    ''' <summary>
    ''' The sorted rows
    ''' </summary>
	Private SortedRows() As DataRow
    ''' <summary>
    ''' The select by sorted rows
    ''' </summary>
	Private SelectBySortedRows() As DataRow
    ''' <summary>
    ''' The this data row
    ''' </summary>
	Private thisDataRow As RenaissanceDataClass.DSFlorenceItems.tblFlorenceItemsRow		' Form Specific !!!!
    ''' <summary>
    ''' The this audit ID
    ''' </summary>
	Private thisAuditID As Integer
    ''' <summary>
    ''' The this position
    ''' </summary>
	Private thisPosition As Integer
    ''' <summary>
    ''' The _ is over cancel button
    ''' </summary>
	Private _IsOverCancelButton As Boolean
    ''' <summary>
    ''' The _ in use
    ''' </summary>
	Private _InUse As Boolean

	' Form Status Flags

    ''' <summary>
    ''' The form is valid
    ''' </summary>
	Private FormIsValid As Boolean
    ''' <summary>
    ''' The form changed
    ''' </summary>
	Private FormChanged As Boolean
    ''' <summary>
    ''' The _ form open failed
    ''' </summary>
	Private _FormOpenFailed As Boolean
    ''' <summary>
    ''' The in paint
    ''' </summary>
	Private InPaint As Boolean
    ''' <summary>
    ''' The add new record
    ''' </summary>
	Private AddNewRecord As Boolean

	' User Permission Flags

    ''' <summary>
    ''' The has read permission
    ''' </summary>
	Private HasReadPermission As Boolean
    ''' <summary>
    ''' The has update permission
    ''' </summary>
	Private HasUpdatePermission As Boolean
    ''' <summary>
    ''' The has insert permission
    ''' </summary>
	Private HasInsertPermission As Boolean
    ''' <summary>
    ''' The has delete permission
    ''' </summary>
	Private HasDeletePermission As Boolean

#End Region

#Region " Form 'Properties' "

    ''' <summary>
    ''' Gets the main form.
    ''' </summary>
    ''' <value>The main form.</value>
	Public ReadOnly Property MainForm() As FlorenceMain Implements Globals.StandardFlorenceForm.MainForm
		' Public property to return handle to the 'Main' Venice form, where in reside most of the 
		' data structures and many common utilities.
		Get
			Return _MainForm
		End Get
	End Property

    ''' <summary>
    ''' Gets or sets a value indicating whether this instance is over cancel button.
    ''' </summary>
    ''' <value><c>true</c> if this instance is over cancel button; otherwise, <c>false</c>.</value>
	Public Property IsOverCancelButton() As Boolean Implements Globals.StandardFlorenceForm.IsOverCancelButton
		' Public property maintaining a value indicating if the cursor is over the 'Cancel'
		' Button on this form.
		' This property is specifically designed for use by the field formating Event functions
		' In order that they do not impose format restrictions if the user is about to click the 
		' 'Cancel' button.
		'
		Get
			Return _IsOverCancelButton
		End Get
		Set(ByVal Value As Boolean)
			_IsOverCancelButton = Value
		End Set
	End Property

    ''' <summary>
    ''' Gets a value indicating whether this instance is in paint.
    ''' </summary>
    ''' <value><c>true</c> if this instance is in paint; otherwise, <c>false</c>.</value>
	Public ReadOnly Property IsInPaint() As Boolean Implements Globals.StandardFlorenceForm.IsInPaint
		Get
			Return InPaint
		End Get
	End Property

    ''' <summary>
    ''' Gets a value indicating whether [in use].
    ''' </summary>
    ''' <value><c>true</c> if [in use]; otherwise, <c>false</c>.</value>
	Public ReadOnly Property InUse() As Boolean Implements Globals.StandardFlorenceForm.InUse
		Get
			Return _InUse
		End Get
	End Property

    ''' <summary>
    ''' Gets a value indicating whether [form open failed].
    ''' </summary>
    ''' <value><c>true</c> if [form open failed]; otherwise, <c>false</c>.</value>
	Public ReadOnly Property FormOpenFailed() As Boolean Implements Globals.StandardFlorenceForm.FormOpenFailed
		Get
			Return _FormOpenFailed
		End Get
	End Property

#End Region

    ''' <summary>
    ''' Initializes a new instance of the <see cref="frmItems"/> class.
    ''' </summary>
    ''' <param name="pMainForm">The p main form.</param>
	Public Sub New(ByVal pMainForm As FlorenceMain)
		' *************************************************************
		' Custom 'New'. 
		' Passes in the reference to the parent form.
		' 
		' Establishes form specific variables.
		' Establishes Form specific Data connection / data structures.
		'
		' *************************************************************

		Me.New()

		_MainForm = pMainForm
		AddHandler _MainForm.FlorenceAutoUpdate, AddressOf Me.AutoUpdate

		_FormOpenFailed = False
		_InUse = True

		' ******************************************************
		' Form Specific Settings :
		' ******************************************************

		THIS_FORM_SelectingCombo = Me.Combo_SelectItemID
		THIS_FORM_NewMoveToControl = Me.Combo_ItemGroup

		' Default Select and Order fields.

		THIS_FORM_SelectBy = "ItemDescription"
		THIS_FORM_OrderBy = "ItemDescription"

		THIS_FORM_ValueMember = "ItemID"

		' Form Permissioning :-

		THIS_FORM_PermissionArea = Me.Name
		THIS_FORM_PermissionType = RenaissanceGlobals.PermissionFeatureType.TypeForm

		' 'This' form ID

		THIS_FORM_FormID = FlorenceFormID.frmItems

		' This form's dataset type.

		ThisStandardDataset = RenaissanceStandardDatasets.tblFlorenceItems	' This Defines the Form Data !!! 

		' Format Event Handlers for form controls

		AddHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler
		AddHandler edit_ItemDescription.LostFocus, AddressOf MainForm.Text_NotNull

		' Form Control Changed events
		AddHandler THIS_FORM_SelectingCombo.SelectedIndexChanged, AddressOf Combo_SelectComboChanged
		AddHandler THIS_FORM_SelectingCombo.KeyUp, AddressOf MainForm.ComboSelectAsYouType

		AddHandler edit_ItemDescription.TextChanged, AddressOf Me.FormControlChanged
		AddHandler Text_ItemMessage.TextChanged, AddressOf Me.FormControlChanged
		AddHandler Check_ItemDiscontinued.CheckStateChanged, AddressOf Me.FormControlChanged
		AddHandler Check_KeyDetail.CheckStateChanged, AddressOf Me.FormControlChanged
		AddHandler Check_IsDataItem.CheckStateChanged, AddressOf Me.FormControlChanged
		AddHandler Check_IsLimitItem.CheckStateChanged, AddressOf Me.FormControlChanged
		AddHandler Check_BooleanLimit.CheckStateChanged, AddressOf Me.FormControlChanged
		AddHandler Check_LimitIsPercent.CheckStateChanged, AddressOf Me.FormControlChanged
		AddHandler Check_UpdateStatusOnDataUpdate.CheckStateChanged, AddressOf Me.FormControlChanged

		AddHandler Numeric_LimitFrom.ValueChanged, AddressOf Me.FormControlChanged
		AddHandler Numeric_LimitTo.ValueChanged, AddressOf Me.FormControlChanged
		AddHandler Numeric_LimitThreshold.ValueChanged, AddressOf Me.FormControlChanged

		AddHandler Combo_ItemGroup.SelectedValueChanged, AddressOf Me.FormControlChanged
		AddHandler Combo_ItemGroup.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
		AddHandler Combo_ItemGroup.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
		AddHandler Combo_ItemGroup.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
		AddHandler Combo_ItemGroup.KeyUp, AddressOf MainForm.ComboSelectAsYouType

		AddHandler Combo_ItemCategory.SelectedValueChanged, AddressOf Me.FormControlChanged
		AddHandler Combo_ItemCategory.KeyUp, AddressOf MainForm.ComboSelectAsYouType_NoMatch

		AddHandler Combo_UpdatePeriod.SelectedValueChanged, AddressOf Me.FormControlChanged
		AddHandler Combo_UpdatePeriod.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
		AddHandler Combo_UpdatePeriod.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
		AddHandler Combo_UpdatePeriod.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
		AddHandler Combo_UpdatePeriod.KeyUp, AddressOf MainForm.ComboSelectAsYouType

		AddHandler Combo_DataUpdatePeriod.SelectedValueChanged, AddressOf Me.FormControlChanged
		AddHandler Combo_DataUpdatePeriod.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
		AddHandler Combo_DataUpdatePeriod.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
		AddHandler Combo_DataUpdatePeriod.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
		AddHandler Combo_DataUpdatePeriod.KeyUp, AddressOf MainForm.ComboSelectAsYouType

		AddHandler Combo_LimitType.SelectedValueChanged, AddressOf Me.FormControlChanged
		AddHandler Combo_LimitType.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
		AddHandler Combo_LimitType.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
		AddHandler Combo_LimitType.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
		AddHandler Combo_LimitType.KeyUp, AddressOf MainForm.ComboSelectAsYouType

		AddHandler Combo_DataItemType.SelectedValueChanged, AddressOf Me.FormControlChanged
		AddHandler Combo_DataItemType.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
		AddHandler Combo_DataItemType.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
		AddHandler Combo_DataItemType.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
		AddHandler Combo_DataItemType.KeyUp, AddressOf MainForm.ComboSelectAsYouType

		' Set up the ToolTip
		MainForm.SetFormToolTip(Me, FormTooltip)

		' ******************************************************
		' End Form Specific.
		' ******************************************************

		' Data object names standard to this Form type.

		THIS_TABLENAME = ThisStandardDataset.TableName
		THIS_ADAPTORNAME = ThisStandardDataset.Adaptorname
		THIS_DATASETNAME = ThisStandardDataset.DatasetName

		THIS_FORM_ChangeID = ThisStandardDataset.ChangeID

		' Establish / Retrieve data objects for this form.

		myConnection = MainForm.MainDataHandler.Get_Connection(FLORENCE_CONNECTION)
		myAdaptor = MainForm.MainDataHandler.Get_Adaptor(THIS_ADAPTORNAME, FLORENCE_CONNECTION, THIS_TABLENAME)
		myDataset = MainForm.Load_Table(ThisStandardDataset, False)
		myTable = myDataset.Tables(0)

		MainForm.BuildStandardFormMenu(Me.RootMenu, myDataset.Tables(0), AddressOf SelectMenuEvent, AddressOf OrderMenuEvent, AddressOf AuditReportMenuEvent)
		Me.RootMenu.PerformLayout()

		Try
			InPaint = True

			Call SetItemGroupCombo()
			Call SetItemCategoryCombo()
			Call SetUpdatePeriodCombo()
			Call SetDataItemTypeCombo()
			Call SetLimitTypeCombo()
		Catch ex As Exception
		Finally
			InPaint = False
		End Try

	End Sub

#Region " This Form Event handlers : FormLoad / FormClose "

	' Form Initialisation code.
	'
    ''' <summary>
    ''' Resets the form.
    ''' </summary>
	Public Sub ResetForm() Implements StandardFlorenceForm.ResetForm
		THIS_FORM_SelectBy = "ItemDescription"
		THIS_FORM_OrderBy = "ItemDescription"

		Call Form_Load(Me, New System.EventArgs)
	End Sub

    ''' <summary>
    ''' Closes the form.
    ''' </summary>
	Public Sub CloseForm() Implements StandardFlorenceForm.CloseForm
		ALWAYS_CLOSE_THIS_FORM = True
		Me.Close()
	End Sub


    ''' <summary>
    ''' Handles the Load event of the Form control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

		Me.MainForm.LogError(Me.Name, LOG_LEVELS.Audit, "", "Form Loaded", "", False)
		_FormOpenFailed = False
		_InUse = True

		' Initialise Data structures. Connection, Adaptor and Dataset.

		If Not (MainForm Is Nothing) Then
			FormIsValid = True
		Else
			MessageBox.Show("'Main' form is not set. Closing form", "no Main Form", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
			FormIsValid = False
			_FormOpenFailed = True
			Exit Sub
		End If

		If (myConnection Is Nothing) Then
			Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Database connection is not established. Closing form.", "", True)

			FormIsValid = False
			_FormOpenFailed = True
			Exit Sub
		End If

		If (myAdaptor Is Nothing) Then
			Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Data Adaptor is not established. Closing form.", "", True)

			FormIsValid = False
			_FormOpenFailed = True
			Exit Sub
		End If

		If (myDataset Is Nothing) Then
			Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Form Dataset is not established. Closing form.", "", True)

			FormIsValid = False
			_FormOpenFailed = True
			Exit Sub
		End If

		' Initialse form

		InPaint = True
		IsOverCancelButton = False

		' Check User permissions
		Call CheckPermissions()
		If (HasReadPermission = False) Then
			Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User does not have Read permission for this Form..", "", True)

			FormIsValid = False
			_FormOpenFailed = True
			Exit Sub
		End If

		' Build Sorted data list from which this form operates
		Call SetSortedRows()

		' Display initial record.

		thisPosition = 0
		If THIS_FORM_SelectingCombo.Items.Count > 0 Then
			Me.THIS_FORM_SelectingCombo.SelectedIndex = 0
			thisDataRow = SortedRows(thisPosition)
			Call GetFormData(thisDataRow)
		Else
			Me.THIS_FORM_SelectingCombo.SelectedIndex = (-1)
			Call GetFormData(Nothing)
		End If

		InPaint = False

		If (Combo_SelectGroup.SelectedIndex > 0) Then
			Combo_SelectGroup.SelectedIndex = 0
		End If

	End Sub

    ''' <summary>
    ''' Handles the Closing event of the frmItems control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.ComponentModel.CancelEventArgs"/> instance containing the event data.</param>
	Private Sub frmItems_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
		Dim HideForm As Boolean

		' Hide or Close this form ?
		' All depends on how many of this form type are Open or in Cache...

		_InUse = False

		If (ALWAYS_CLOSE_THIS_FORM = True) Or (FormIsValid = False) Then
			HideForm = False
		Else
			If (FormChanged = True) Then
				Call SetFormData()
			End If

			HideForm = True
			If MainForm.FlorenceForms.CountOf(THIS_FORM_FormID) > MainForm.EntryForm_CacheCount Then
				HideForm = False
			End If
		End If

		If HideForm = True Then
			MainForm.HideInFormsCollection(Me)
			Me.Hide()	' NPP Fix

			e.Cancel = True
		Else
			Try
				MainForm.RemoveFromFormsCollection(Me)
				RemoveHandler _MainForm.FlorenceAutoUpdate, AddressOf Me.AutoUpdate

				RemoveHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler
				RemoveHandler edit_ItemDescription.LostFocus, AddressOf MainForm.Text_NotNull

				' Form Control Changed events
				RemoveHandler THIS_FORM_SelectingCombo.SelectedIndexChanged, AddressOf Combo_SelectComboChanged
				RemoveHandler THIS_FORM_SelectingCombo.KeyUp, AddressOf MainForm.ComboSelectAsYouType

				RemoveHandler edit_ItemDescription.TextChanged, AddressOf Me.FormControlChanged
				RemoveHandler Text_ItemMessage.TextChanged, AddressOf Me.FormControlChanged
				RemoveHandler Check_ItemDiscontinued.CheckStateChanged, AddressOf Me.FormControlChanged
				RemoveHandler Check_KeyDetail.CheckStateChanged, AddressOf Me.FormControlChanged
				RemoveHandler Check_IsDataItem.CheckStateChanged, AddressOf Me.FormControlChanged
				RemoveHandler Check_IsLimitItem.CheckStateChanged, AddressOf Me.FormControlChanged
				RemoveHandler Check_BooleanLimit.CheckStateChanged, AddressOf Me.FormControlChanged
				RemoveHandler Check_LimitIsPercent.CheckStateChanged, AddressOf Me.FormControlChanged
				RemoveHandler Check_UpdateStatusOnDataUpdate.CheckStateChanged, AddressOf Me.FormControlChanged
				RemoveHandler Numeric_LimitFrom.ValueChanged, AddressOf Me.FormControlChanged
				RemoveHandler Numeric_LimitTo.ValueChanged, AddressOf Me.FormControlChanged
				RemoveHandler Numeric_LimitThreshold.ValueChanged, AddressOf Me.FormControlChanged

				RemoveHandler Combo_ItemGroup.SelectedValueChanged, AddressOf Me.FormControlChanged
				RemoveHandler Combo_ItemGroup.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
				RemoveHandler Combo_ItemGroup.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
				RemoveHandler Combo_ItemGroup.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
				RemoveHandler Combo_ItemGroup.KeyUp, AddressOf MainForm.ComboSelectAsYouType

				RemoveHandler Combo_ItemCategory.SelectedValueChanged, AddressOf Me.FormControlChanged
				RemoveHandler Combo_ItemCategory.KeyUp, AddressOf MainForm.ComboSelectAsYouType_NoMatch

				RemoveHandler Combo_UpdatePeriod.SelectedValueChanged, AddressOf Me.FormControlChanged
				RemoveHandler Combo_UpdatePeriod.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
				RemoveHandler Combo_UpdatePeriod.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
				RemoveHandler Combo_UpdatePeriod.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
				RemoveHandler Combo_UpdatePeriod.KeyUp, AddressOf MainForm.ComboSelectAsYouType

				RemoveHandler Combo_DataUpdatePeriod.SelectedValueChanged, AddressOf Me.FormControlChanged
				RemoveHandler Combo_DataUpdatePeriod.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
				RemoveHandler Combo_DataUpdatePeriod.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
				RemoveHandler Combo_DataUpdatePeriod.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
				RemoveHandler Combo_DataUpdatePeriod.KeyUp, AddressOf MainForm.ComboSelectAsYouType

				RemoveHandler Combo_LimitType.SelectedValueChanged, AddressOf Me.FormControlChanged
				RemoveHandler Combo_LimitType.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
				RemoveHandler Combo_LimitType.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
				RemoveHandler Combo_LimitType.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
				RemoveHandler Combo_LimitType.KeyUp, AddressOf MainForm.ComboSelectAsYouType

				RemoveHandler Combo_DataItemType.SelectedValueChanged, AddressOf Me.FormControlChanged
				RemoveHandler Combo_DataItemType.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
				RemoveHandler Combo_DataItemType.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
				RemoveHandler Combo_DataItemType.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
				RemoveHandler Combo_DataItemType.KeyUp, AddressOf MainForm.ComboSelectAsYouType

			Catch ex As Exception
			End Try
		End If

	End Sub


#End Region

	' Routine to handle changes / updates to tables by this and other windows.
	' If this, or any other, form posts a change to a table, then it will invoke an update event 
	' detailing what tables have been altered.
	' Each Form will receive this event through this 'AutoUpdate' routine whice is configured to handle
	' the 'VeniceAutoUpdate' event of the main Venice form.
	' Each form may them react as appropriate to changes in any table that might impact it.
	'
    ''' <summary>
    ''' Autoes the update.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="RenaissanceGlobals.RenaissanceUpdateEventArgs"/> instance containing the event data.</param>
	Private Sub AutoUpdate(ByVal sender As System.Object, ByVal e As RenaissanceGlobals.RenaissanceUpdateEventArgs)
		Dim OrgInPaint As Boolean
		Dim KnowledgeDateChanged As Boolean
		Dim SetButtonStatus_Flag As Boolean
		Dim RefreshForm As Boolean = False

		If (Me.IsDisposed) OrElse (Me.Disposing) OrElse (Me.InUse = False) Then Exit Sub

		OrgInPaint = InPaint
		InPaint = True
		KnowledgeDateChanged = False
		SetButtonStatus_Flag = False

		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True) Or (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.Connection) = True) Then
			KnowledgeDateChanged = True
			RefreshForm = True
		End If

		' ****************************************************************
		' Check for changes relevant to this form
		' ****************************************************************

		' Changes to the tblFlorenceItems table :-
		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblFlorenceItems) = True) Or KnowledgeDateChanged Then

			' Re-Set combo.
			Call SetItemCategoryCombo()
		End If

		' SetItemGroupCombo
		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblFlorenceGroup) = True) Or KnowledgeDateChanged Then

			' Re-Set combo.
			Call SetItemGroupCombo()
		End If

		' Changes to the tblDealingPeriod table :-
		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblDealingPeriod) = True) Or KnowledgeDateChanged Then

			' Re-Set combo.
			Call SetUpdatePeriodCombo()
		End If

		' Changes to the KnowledgeDate :-
		If e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True Then
			SetButtonStatus_Flag = True
		End If

		' Changes to the tblUserPermissions table :-
		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblUserPermissions) = True) Or KnowledgeDateChanged Then

			' Check ongoing permissions.

			Call CheckPermissions()
			If (HasReadPermission = False) Then
				Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User has lost Read permission for this Form.", "", True)

				FormIsValid = False
				Me.Close()
				Exit Sub
			End If

			SetButtonStatus_Flag = True

		End If



		' ****************************************************************
		' Changes to the Main FORM table :-
		' ****************************************************************

		If (e.TableChanged(THIS_FORM_ChangeID) = True) Or KnowledgeDateChanged Then
			RefreshForm = True

			' Re-Set Controls etc.
			Call SetSortedRows()

			' Move again to the correct item
			thisPosition = Get_Position(thisAuditID)

			' Validate current position.
			If thisPosition >= 0 And thisPosition < Me.SortedRows.GetLength(0) Then
				thisDataRow = Me.SortedRows(thisPosition)
			Else
				If (Me.SortedRows.GetLength(0) > 0) And (AddNewRecord = False) Then
					thisPosition = 0
					thisDataRow = Me.SortedRows(thisPosition)
					FormChanged = False
				Else
					thisDataRow = Nothing
				End If
			End If

			' Set SelectingCombo Index.
			If (Me.THIS_FORM_SelectingCombo.Items.Count <= 0) Then
				Try
					Me.THIS_FORM_SelectingCombo.SelectedIndex = -1
				Catch ex As Exception
				End Try
			ElseIf (thisPosition < 0) Then
				Try
					MainForm.ClearComboSelection(THIS_FORM_SelectingCombo)
				Catch ex As Exception
				End Try
			Else
				Try
					Me.THIS_FORM_SelectingCombo.SelectedIndex = thisPosition
				Catch ex As Exception
				End Try
			End If

		End If

		' ****************************************************************
		' Repaint if not currently in Edit Mode
		'
		' ****************************************************************

		InPaint = OrgInPaint

		' Retrieve form data if appropriate, i.e. this data item is not already being edited.
		If (RefreshForm = True) AndAlso (FormChanged = False) AndAlso (AddNewRecord = False) Then
			GetFormData(thisDataRow) ' Includes a call to 'SetButtonStatus()'
		Else
			If SetButtonStatus_Flag Then
				Call SetButtonStatus()
			End If
		End If

	End Sub

#Region " Workhorse functions : SetSortedRows / CheckPermission / ControlsChanged / Menu Events "

	' Build Sorted list from the Source dataset and update the Select Combo
    ''' <summary>
    ''' Sets the sorted rows.
    ''' </summary>
	Private Sub SetSortedRows()

		Dim OrgInPaint As Boolean

		Dim thisrow As DataRow
		Dim thisDrowDownWidth As Integer
		Dim SizingBitmap As Bitmap
		Dim SizingGraphics As Graphics
		Dim SelectString As String = "RN >= 0"


		' Form Specific Selection Combo :-
		If (THIS_FORM_SelectingCombo Is Nothing) Then Exit Sub

		' Code is pretty Generic from here on...

		' Set paint local so that changes to the selection combo do not trigger form updates.

		OrgInPaint = InPaint
		InPaint = True

		If (Me.Combo_SelectGroup.SelectedIndex > 0) AndAlso (IsNumeric(Combo_SelectGroup.SelectedValue)) Then
			SelectString = "ItemGroup=" & CInt(Combo_SelectGroup.SelectedValue).ToString
		End If

		' Get selected Row sets, or exit if no data is present.
		If myDataset Is Nothing Then
			ReDim SortedRows(0)
			ReDim SelectBySortedRows(0)
		Else
			SortedRows = myTable.Select(SelectString, THIS_FORM_OrderBy)
			SelectBySortedRows = myTable.Select(SelectString, THIS_FORM_SelectBy)
		End If

		' Set Combo data source
		THIS_FORM_SelectingCombo.DataSource = SortedRows
		THIS_FORM_SelectingCombo.DisplayMember = THIS_FORM_SelectBy
		THIS_FORM_SelectingCombo.ValueMember = THIS_FORM_ValueMember

		' Ensure that the Selection Combo's DrowDownWidth is wide enough to take the required strings

		thisDrowDownWidth = THIS_FORM_SelectingCombo.Width
		For Each thisrow In SortedRows

			' Compute the string dimensions in the given font
			SizingBitmap = New Bitmap(1, 1, System.Drawing.Imaging.PixelFormat.Format32bppArgb)
			SizingGraphics = Graphics.FromImage(SizingBitmap)
			Dim stringSize As SizeF = SizingGraphics.MeasureString(thisrow(THIS_FORM_SelectBy).ToString, THIS_FORM_SelectingCombo.Font)
			If (stringSize.Width) > thisDrowDownWidth Then
				thisDrowDownWidth = CInt(stringSize.Width)
			End If
		Next

		THIS_FORM_SelectingCombo.DropDownWidth = thisDrowDownWidth

		InPaint = OrgInPaint
	End Sub


	' Check User permissions
    ''' <summary>
    ''' Checks the permissions.
    ''' </summary>
	Private Sub CheckPermissions()

		Dim Permissions As Integer

		Permissions = MainForm.CheckPermissions(THIS_FORM_PermissionArea, THIS_FORM_PermissionType)

		HasReadPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermRead) > 0)
		HasUpdatePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermUpdate) > 0)
		HasInsertPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermInsert) > 0)
		HasDeletePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermDelete) > 0)

	End Sub

	' Flag changes to form controls, Event Associations made in 'New' routine.
    ''' <summary>
    ''' Forms the control changed.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub FormControlChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

		If InPaint = False Then
			If (Me.HasUpdatePermission) Or (Me.HasInsertPermission And Me.AddNewRecord) Then
				FormChanged = True
				Me.btnSave.Enabled = True
				Me.btnCancel.Enabled = True
			End If
		End If

	End Sub

    ''' <summary>
    ''' Selects the menu event.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
	Public Sub SelectMenuEvent(ByVal sender As Object, ByVal e As EventArgs)
		' Event handler for the Select Menu
		' Event association is made during the dynamic menu creation

		THIS_FORM_SelectBy = CType(sender, System.Windows.Forms.ToolStripMenuItem).Text

		Call SetSortedRows()

	End Sub

    ''' <summary>
    ''' Orders the menu event.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
	Public Sub OrderMenuEvent(ByVal sender As Object, ByVal e As EventArgs)
		' Event handler for the Order Menu
		' Event association is made during the dynamic menu creation

		THIS_FORM_OrderBy = CType(sender, System.Windows.Forms.ToolStripMenuItem).Text

		Call SetSortedRows()

	End Sub


    ''' <summary>
    ''' Audits the report menu event.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
	Public Sub AuditReportMenuEvent(ByVal sender As Object, ByVal e As EventArgs)
		' Event handler for the Audit Report Menu
		' Event association is made during the dynamic menu creation

		Select Case CInt(CType(sender, System.Windows.Forms.ToolStripMenuItem).Tag)
			Case 0 ' This Record
				If (Me.AddNewRecord = False) AndAlso (thisAuditID >= 0) Then
					Run_AuditReport_ThisRecord(MainForm, ThisStandardDataset, thisAuditID)
				End If

			Case 1 ' All Records
				Run_AuditReport_ALLRecords(MainForm, ThisStandardDataset)

		End Select

	End Sub


#End Region

#Region " Set Form Combos (Form Specific Code) "

    ''' <summary>
    ''' Sets the item group combo.
    ''' </summary>
	Private Sub SetItemGroupCombo()

		Call MainForm.SetTblGenericCombo( _
		Me.Combo_ItemGroup, _
		RenaissanceStandardDatasets.tblFlorenceGroup, _
		"ItemGroupDescription", _
		"ItemGroup", _
		"", False, True, False)		' 

		Call MainForm.SetTblGenericCombo( _
		Me.Combo_SelectGroup, _
		RenaissanceStandardDatasets.tblFlorenceGroup, _
		"ItemGroupDescription", _
		"ItemGroup", _
		"", False, True, True, 0, "All Groups")		' 

	End Sub

    ''' <summary>
    ''' Sets the item category combo.
    ''' </summary>
	Private Sub SetItemCategoryCombo()

		Call MainForm.SetTblGenericCombo( _
		Me.Combo_ItemCategory, _
		RenaissanceStandardDatasets.tblFlorenceItems, _
		"Category", _
		"Category", _
		"", True, True, False)	 ' 

	End Sub

    ''' <summary>
    ''' Sets the update period combo.
    ''' </summary>
	Private Sub SetUpdatePeriodCombo()

		Call MainForm.SetTblGenericCombo( _
	 Me.Combo_UpdatePeriod, _
	 GetType(RenaissanceGlobals.DealingPeriod), True, "No Update", 0)		' 

		Call MainForm.SetTblGenericCombo( _
		 Me.Combo_DataUpdatePeriod, _
		 GetType(RenaissanceGlobals.DealingPeriod), True, "No Update", 0)		' 

	End Sub

    ''' <summary>
    ''' Sets the limit type combo.
    ''' </summary>
	Private Sub SetLimitTypeCombo()

		Call MainForm.SetTblGenericCombo( _
		 Me.Combo_LimitType, _
		 GetType(RenaissanceGlobals.FlorenceLimitType), False)

	End Sub
	'
    ''' <summary>
    ''' Sets the data item type combo.
    ''' </summary>
	Private Sub SetDataItemTypeCombo()

		Call MainForm.SetTblGenericCombo( _
		 Me.Combo_DataItemType, _
		 GetType(RenaissanceGlobals.RenaissanceDataType), False)

	End Sub

#End Region

#Region " Get & Set Form Data / SetButton / ValidateForm / btnCancel Events (Form Specific Code) "

    ''' <summary>
    ''' Gets the form data.
    ''' </summary>
    ''' <param name="ThisRow">The this row.</param>
	Private Sub GetFormData(ByRef ThisRow As RenaissanceDataClass.DSFlorenceItems.tblFlorenceItemsRow)
		' Routine to populate form Controls given a data row.
		' Form is cleared for invalid Datarows or Invalid Form.

		Dim OrgInpaint As Boolean

		' Set 'Paint' flag to prevent event actions caused by changing the value of form fields.
		OrgInpaint = InPaint
		InPaint = True


		If (ThisRow Is Nothing) Or (FormIsValid = False) Or (Me.InUse = False) Then
			' Bad / New Datarow - Clear Form.

			thisAuditID = (-1)

			Me.editAuditID.Text = ""

			If Me.Combo_ItemGroup.Items.Count > 0 Then
				Combo_ItemGroup.SelectedIndex = 0
			End If

			If Me.Combo_ItemCategory.Items.Count > 0 Then
				Combo_ItemCategory.SelectedIndex = 0
			End If

			Me.edit_ItemDescription.Text = ""

			If Me.Combo_UpdatePeriod.Items.Count > 0 Then
				Combo_UpdatePeriod.SelectedIndex = 0
			End If

			Me.Text_ItemMessage.Text = ""
			Check_ItemDiscontinued.Checked = False
			Check_KeyDetail.Checked = False

			Me.Check_IsDataItem.Checked = False
			Call Check_IsDataItem_CheckedChanged(Nothing, Nothing)
			If Me.Combo_DataItemType.Items.Count > 0 Then
				Combo_DataItemType.SelectedIndex = 0
			End If
			If Me.Combo_DataUpdatePeriod.Items.Count > 0 Then
				Combo_DataUpdatePeriod.SelectedIndex = 0
			End If
			Check_UpdateStatusOnDataUpdate.Checked = False

			Me.Check_IsLimitItem.Checked = False
			Call Check_IsLimitItem_CheckedChanged(Nothing, Nothing)
			If Combo_LimitType.Items.Count > 0 Then
				Me.Combo_LimitType.SelectedIndex = 0
			End If
			Check_LimitIsPercent.Checked = False

			Me.Numeric_LimitFrom.Value = 0
			Me.Numeric_LimitTo.Value = 0
			Me.Numeric_LimitThreshold.Value = 0
			Me.Check_BooleanLimit.Checked = False

			If AddNewRecord = True Then
				Me.btnCancel.Enabled = True
				Me.THIS_FORM_SelectingCombo.Enabled = False
			Else
				Me.btnCancel.Enabled = False
				Me.THIS_FORM_SelectingCombo.Enabled = True
			End If

		Else

			' Populate Form with given data.
			Try
				thisAuditID = thisDataRow.AuditID

				Me.editAuditID.Text = thisDataRow.AuditID.ToString

				If Me.Combo_ItemGroup.Items.Count > 0 Then
					Combo_ItemGroup.SelectedValue = thisDataRow.ItemGroup
				End If

				If Me.Combo_ItemCategory.Items.Count > 0 Then
					Combo_ItemCategory.SelectedValue = thisDataRow.Category
				End If

				Me.edit_ItemDescription.Text = thisDataRow.ItemDescription

				If Me.Combo_UpdatePeriod.Items.Count > 0 Then
					Combo_UpdatePeriod.SelectedValue = CType(thisDataRow.UpdatePeriod, RenaissanceGlobals.DealingPeriod)
				End If

				Me.Text_ItemMessage.Text = thisDataRow.ItemMessage

				If thisDataRow.ItemDiscontinued = 0 Then
					Check_ItemDiscontinued.Checked = False
				Else
					Check_ItemDiscontinued.Checked = True
				End If

				If thisDataRow.KeyDetail = 0 Then
					Check_KeyDetail.Checked = False
				Else
					Check_KeyDetail.Checked = True
				End If

				If (thisDataRow.IsDataItem = 0) Then
					Me.Check_IsDataItem.Checked = False
					If Me.Combo_DataItemType.Items.Count > 0 Then
						Combo_DataItemType.SelectedIndex = 0
					End If
					If Me.Combo_DataUpdatePeriod.Items.Count > 0 Then
						Combo_DataUpdatePeriod.SelectedIndex = 0
					End If
				Else
					Me.Check_IsDataItem.Checked = True
					Combo_DataItemType.SelectedValue = thisDataRow.DataItemType
					Combo_DataUpdatePeriod.SelectedValue = thisDataRow.DataUpdatePeriod
				End If
				Check_UpdateStatusOnDataUpdate.Checked = thisDataRow.UpdateStatusOnDataUpdate

				Call Check_IsDataItem_CheckedChanged(Nothing, Nothing)

				If (thisDataRow.IsLimitItem = 0) Then
					Me.Check_IsLimitItem.Checked = False
					If Combo_LimitType.Items.Count > 0 Then
						Me.Combo_LimitType.SelectedValue = 0
					End If
				Else
					Me.Check_IsLimitItem.Checked = True
					If Combo_LimitType.Items.Count > 0 Then
						Me.Combo_LimitType.SelectedValue = thisDataRow.LimitType
					End If
				End If
				Call Check_IsLimitItem_CheckedChanged(Nothing, Nothing)
				Check_LimitIsPercent.Checked = thisDataRow.LimitIsPercent

				Me.Numeric_LimitFrom.Value = CDbl(thisDataRow.FromLimit)
				Me.Numeric_LimitTo.Value = CDbl(thisDataRow.ToLimit)
				Me.Numeric_LimitThreshold.Value = CDbl(thisDataRow.LimitThreshold)
				Me.Check_BooleanLimit.Checked = CBool(thisDataRow.FromLimit)

				AddNewRecord = False
				MainForm.SetComboSelectionLengths(Me)

				Me.btnCancel.Enabled = False
				Me.THIS_FORM_SelectingCombo.Enabled = True

			Catch ex As Exception

				Call MainForm.LogError(Me.Name & ", GetFormData", 0, ex.Message, "Error Showing Data", ex.StackTrace, True)
				Call GetFormData(Nothing)

			End Try

		End If

		' Allow Field events to trigger before 'InPaint' Is re-set. 
		' (Should) Prevent Validation errors during Form Draw.
		Application.DoEvents()

		' Restore 'Paint' flag.
		InPaint = OrgInpaint
		FormChanged = False
		Me.btnSave.Enabled = False

		' As it says on the can....
		Call SetButtonStatus()

	End Sub

    ''' <summary>
    ''' Sets the form data.
    ''' </summary>
    ''' <param name="pConfirm">if set to <c>true</c> [p confirm].</param>
    ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
	Private Function SetFormData(Optional ByVal pConfirm As Boolean = True) As Boolean
		' *************************************************************
		'
		' *************************************************************
		Dim ErrMessage As String
		Dim ErrFlag As Boolean
		Dim ErrStack As String
		Dim ProtectedItem As Boolean = False

		ErrMessage = ""
		ErrStack = ""

		' *************************************************************
		' Appropriate Save permission :-
		' *************************************************************

		If (Me.HasUpdatePermission = False) And ((Me.HasInsertPermission = False) Or (Me.AddNewRecord = False)) Then
			MainForm.LogError(Me.Name & ", SetFormData()", LOG_LEVELS.Warning, "", "You do not have permission to save this record.", "", True)
			Call btnCancel_Click(Me, New System.EventArgs)
			Exit Function
		End If

		' *************************************************************
		' If Save button is disabled then should not be able to save, exit silently.
		' *************************************************************
		If Me.btnSave.Enabled = False Then
			Call btnCancel_Click(Me, New System.EventArgs)
			Exit Function
		End If

		' *************************************************************
		' KnowledgeDate OK :-
		' *************************************************************

		If (MainForm.Main_Knowledgedate > KNOWLEDGEDATE_NOW) Then
			MainForm.LogError(Me.Name & ", SetFormData()", LOG_LEVELS.Warning, "", "Changes to Venice are not allowed when the Knowledgedate is not set to `NOW`", "", True)
			Call btnCancel_Click(Me, New System.EventArgs)
			Exit Function
		End If

		' Confirm Save, if required.

		If (pConfirm = True) Then
			If MessageBox.Show("Save Changes ?", "Save Changes ?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.No Then
				Return True
				Exit Function
			End If
		End If

		' *************************************************************
		' Procedure to Save the current form information.
		' *************************************************************

		Dim StatusString As String
		Dim LogString As String
		Dim UpdateRows(0) As DataRow
		Dim Position As Integer

		If (FormChanged = False) Or (FormIsValid = False) Then
			Return False
			Exit Function
		End If

		' Validation
		StatusString = ""
		If ValidateForm(StatusString) = False Then
			MainForm.LogError(Me.Name & ", SetFormData()", LOG_LEVELS.Warning, "", "Validation Error." & vbCrLf & StatusString, "", True)
			Return False
			Exit Function
		End If

		' Check current position in the table.
		Position = Get_Position(thisAuditID)

		' Allow for new or missing ID.
		If Position < 0 Then
			If (AddNewRecord = True) OrElse (SortedRows.Length <= 0) Then
				thisDataRow = myTable.NewRow
				LogString = "Add : "
				AddNewRecord = True
			Else
				Return False
				Exit Function
			End If
		Else
			' Row found OK.
			LogString = "Edit : AuditID = " & thisAuditID.ToString

			thisDataRow = Me.SortedRows(Position)

		End If

		' Set 'Paint' flag.
		InPaint = True

		' *************************************************************
		' Lock the Data Table, to prevent update conflicts.
		' *************************************************************
		SyncLock myTable

			' Initiate Edit,
			thisDataRow.BeginEdit()

			' Set Data Values

			thisDataRow.ItemGroup = CInt(Combo_ItemGroup.SelectedValue)
			LogString &= ", ItemGroup = " & Combo_ItemGroup.Text

			thisDataRow.Category = Me.Combo_ItemCategory.Text
			LogString &= ", Category = " & Combo_ItemCategory.Text

			thisDataRow.ItemDescription = edit_ItemDescription.Text
			LogString &= ", ItemDescription = " & thisDataRow.ItemDescription

			thisDataRow.UpdatePeriod = CInt(Combo_UpdatePeriod.SelectedValue)
			LogString &= ", Period = " & Combo_UpdatePeriod.SelectedText

			thisDataRow.ItemMessage = Text_ItemMessage.Text
			LogString &= ", Message = " & Text_ItemMessage.Text

			' Check_ItemDiscontinued
			If (Check_ItemDiscontinued.Checked = True) Then
				thisDataRow.ItemDiscontinued = (-1)
			Else
				thisDataRow.ItemDiscontinued = 0
			End If

			If (Check_KeyDetail.Checked = True) Then
				thisDataRow.KeyDetail = (-1)
			Else
				thisDataRow.KeyDetail = 0
			End If

			LogString &= ", Key Detail = " & Check_KeyDetail.Checked.ToString
			LogString &= ", Discontinued = " & Check_ItemDiscontinued.Checked.ToString

			If (Check_IsDataItem.Checked = True) Then
				thisDataRow.IsDataItem = (-1)
				If (Combo_DataItemType.SelectedIndex >= 0) And (IsNumeric(Combo_DataItemType.SelectedValue)) Then
					thisDataRow.DataItemType = CInt(Combo_DataItemType.SelectedValue)
				Else
					thisDataRow.DataItemType = 0
				End If
				If (Combo_DataUpdatePeriod.SelectedIndex >= 0) And (IsNumeric(Combo_DataUpdatePeriod.SelectedValue)) Then
					thisDataRow.DataUpdatePeriod = CInt(Combo_DataUpdatePeriod.SelectedValue)
				Else
					thisDataRow.DataUpdatePeriod = 0
				End If
			Else
				thisDataRow.IsDataItem = 0
				thisDataRow.DataItemType = 0
				thisDataRow.DataUpdatePeriod = 0
			End If

			' To Do !!!!!!!!!!!!!
			thisDataRow.UpdateStatusOnDataUpdate = Check_UpdateStatusOnDataUpdate.Checked

			If (Check_IsLimitItem.Checked = True) Then
				thisDataRow.IsLimitItem = (-1)
			Else
				thisDataRow.IsLimitItem = 0
			End If

			thisDataRow.LimitType = CInt(RenaissanceGlobals.FlorenceLimitType.Between)
			If (Combo_LimitType.SelectedIndex >= 0) AndAlso (IsNumeric(Combo_LimitType.SelectedValue)) Then
				thisDataRow.LimitType = CInt(Combo_LimitType.SelectedValue)
			End If

			thisDataRow.LimitIsPercent = Me.Check_LimitIsPercent.Checked

			thisDataRow.FromLimit = 0
			thisDataRow.ToLimit = 0
			Try
				Select Case CInt(thisDataRow.LimitType)
					Case RenaissanceGlobals.FlorenceLimitType.Between
						thisDataRow.FromLimit = CDbl(Me.Numeric_LimitFrom.Value)
						thisDataRow.ToLimit = CDbl(Me.Numeric_LimitTo.Value)

					Case RenaissanceGlobals.FlorenceLimitType.Outside
						thisDataRow.FromLimit = CDbl(Me.Numeric_LimitFrom.Value)
						thisDataRow.ToLimit = CDbl(Me.Numeric_LimitTo.Value)

					Case RenaissanceGlobals.FlorenceLimitType.LessThan
						thisDataRow.ToLimit = CDbl(Me.Numeric_LimitTo.Value)

					Case RenaissanceGlobals.FlorenceLimitType.GreaterThan
						thisDataRow.FromLimit = CDbl(Me.Numeric_LimitFrom.Value)

					Case RenaissanceGlobals.FlorenceLimitType.Boolean
						thisDataRow.FromLimit = CDbl(Me.Check_BooleanLimit.Checked)
				End Select

			Catch ex As Exception
			End Try
			thisDataRow.LimitThreshold = Me.Numeric_LimitThreshold.Value

			If Combo_LimitType.Items.Count > 0 Then
				Me.Combo_LimitType.SelectedValue = thisDataRow.LimitType
			End If

			Me.Numeric_LimitFrom.Value = CDbl(thisDataRow.FromLimit)
			Me.Numeric_LimitTo.Value = CDbl(thisDataRow.ToLimit)

			thisDataRow.EndEdit()
			InPaint = False

			' Add and Update DataRow. 

			ErrFlag = False

			If AddNewRecord = True Then
				Try
					myTable.Rows.Add(thisDataRow)
				Catch ex As Exception
					ErrFlag = True
					ErrMessage = ex.Message
					ErrStack = ex.StackTrace
				End Try
			End If

			UpdateRows(0) = thisDataRow

			' Post Additions / Updates to the underlying table :-
			Dim temp As Integer
			Try
				If (ErrFlag = False) Then
					myAdaptor.InsertCommand.Parameters("@Knowledgedate").Value = MainForm.Main_Knowledgedate
					myAdaptor.UpdateCommand.Parameters("@Knowledgedate").Value = MainForm.Main_Knowledgedate

					temp = MainForm.AdaptorUpdate(Me.Name, myAdaptor, UpdateRows)
				End If

			Catch ex As Exception

				ErrMessage = ex.Message
				ErrFlag = True
				ErrStack = ex.StackTrace

			End Try

			thisAuditID = thisDataRow.AuditID


		End SyncLock

		' *************************************************************
		' Error report. Not done in the Try-Catch as that would serve to persist the SyncLock.
		' *************************************************************

		If (ErrFlag = True) Then
			Call MainForm.LogError(Me.Name & ", SetFormData", 0, ErrMessage, "Error Saving Data", ErrStack, True)
		End If

		' Finish off

		AddNewRecord = False
		FormChanged = False

		Me.THIS_FORM_SelectingCombo.Enabled = True

		' Propagate changes

		Call MainForm.Main_RaiseEvent(New RenaissanceGlobals.RenaissanceUpdateEventArgs(THIS_FORM_ChangeID))

	End Function

    ''' <summary>
    ''' Sets the button status.
    ''' </summary>
	Private Sub SetButtonStatus()
		' Sets the status of the form controlls appropriate to the current users 
		'permissions and the 'Changed' or 'New' status of the form.

		' No Read permission :-

		If Me.HasReadPermission = False Then
			MainForm.LogError(Me.Name & ", SetButtonStatus()", 0, "", "You do not have Read permission for this Form", "", True)
			Me.Close()
			Exit Sub
		End If

		' Has Insert Permission.
		If Me.HasInsertPermission Then
			Me.btnAdd.Enabled = True
		Else
			Me.btnAdd.Enabled = False
		End If

		' Has Delete permission. 
		If (Me.HasDeletePermission) And (Me.AddNewRecord = False) And (thisPosition >= 0) Then
			Me.btnDelete.Enabled = True
		Else
			Me.btnDelete.Enabled = False
		End If

		If (Me.MainForm.Main_Knowledgedate <= KNOWLEDGEDATE_NOW) And _
		 ((Me.HasUpdatePermission) Or (Me.HasInsertPermission And Me.AddNewRecord)) Then

			Combo_ItemGroup.Enabled = True
			Combo_ItemCategory.Enabled = True
			edit_ItemDescription.Enabled = True
			Combo_UpdatePeriod.Enabled = True
			Text_ItemMessage.Enabled = True

			Check_IsDataItem.Enabled = True
			If (Check_IsDataItem.Checked) Then
				Combo_DataItemType.Enabled = True
				Combo_DataUpdatePeriod.Enabled = True
				Check_UpdateStatusOnDataUpdate.Enabled = True
			Else
				Combo_DataItemType.Enabled = False
				Combo_DataUpdatePeriod.Enabled = False
				Check_UpdateStatusOnDataUpdate.Enabled = False
			End If

			Check_ItemDiscontinued.Enabled = True
			Check_KeyDetail.Enabled = True
		Else

			Combo_ItemGroup.Enabled = False
			Combo_ItemCategory.Enabled = False
			edit_ItemDescription.Enabled = False
			Combo_UpdatePeriod.Enabled = False
			Text_ItemMessage.Enabled = False
			Check_IsDataItem.Enabled = False
			Combo_DataItemType.Enabled = False
			Combo_DataUpdatePeriod.Enabled = False
			Check_UpdateStatusOnDataUpdate.Enabled = False

			Check_ItemDiscontinued.Enabled = False
			Check_KeyDetail.Enabled = False
		End If

	End Sub

    ''' <summary>
    ''' Validates the form.
    ''' </summary>
    ''' <param name="pReturnString">The p return string.</param>
    ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
	Private Function ValidateForm(ByRef pReturnString As String) As Boolean
		' Form Validation code.
		' 
		' This code should be the final arbiter of what is allowed. no assumptions regarding 
		' prior validation should be made.
		' 
		' This Code is called by the SetFormData routine before position changes.
		'
		Dim RVal As Boolean

		RVal = True
		pReturnString = ""

		' Validate 

		If Me.Combo_ItemGroup.SelectedIndex < 0 Then
			pReturnString = "Item Group must not be left blank."
			RVal = False
		End If

		If Me.edit_ItemDescription.Text.Length <= 0 Then
			pReturnString = "Item Description must not be left blank."
			RVal = False
		End If

		If Me.Combo_UpdatePeriod.SelectedIndex < 0 Then
			pReturnString = "Update Period must not be left blank."
			RVal = False
		End If

		If Me.Text_ItemMessage.Text.Length <= 0 Then
			pReturnString = "Item message must not be left blank."
			RVal = False
		End If

		Dim DataItemIsBoolean As Boolean = False
		Dim LimitItemIsBoolean As Boolean = False

		If Check_IsDataItem.Checked Then
			If (IsNumeric(Combo_DataItemType.SelectedValue) = False) OrElse (CType(Combo_DataItemType.SelectedValue, RenaissanceGlobals.RenaissanceDataType) = RenaissanceGlobals.RenaissanceDataType.None) Then
				pReturnString = "Data Item Type must be set."
				RVal = False
			ElseIf CType(Combo_DataItemType.SelectedValue, RenaissanceGlobals.RenaissanceDataType) = RenaissanceDataType.BooleanType Then
				DataItemIsBoolean = True
			End If
		End If

		If Me.Check_IsLimitItem.Checked Then
			If (Combo_LimitType.SelectedIndex < 0) OrElse (IsNumeric(Me.Combo_LimitType.SelectedValue) = False) Then
				pReturnString = "Limit Type must be set."
				RVal = False
			ElseIf CType(Combo_LimitType.SelectedValue, RenaissanceGlobals.FlorenceLimitType) = FlorenceLimitType.Boolean Then
				LimitItemIsBoolean = True
			End If

			If (LimitItemIsBoolean Or DataItemIsBoolean) AndAlso (Not (LimitItemIsBoolean = DataItemIsBoolean)) AndAlso (Check_IsLimitItem.Checked) Then
				pReturnString = "Limit Type must match data type if either is Boolean."
				RVal = False
			End If

		End If


		Return RVal

	End Function


    ''' <summary>
    ''' Handles the MouseEnter event of the btnCancel control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnCancel_MouseEnter(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.MouseEnter
		' Simple Events for the 'Cancel' Button to maintain the status of the 'IsOverCancelButton' flag
		Me.IsOverCancelButton = True
	End Sub

    ''' <summary>
    ''' Handles the MouseLeave event of the btnCancel control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnCancel_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.MouseLeave
		' Simple Events for the 'Cancel' Button to maintain the status of the 'IsOverCancelButton' flag
		Me.IsOverCancelButton = False
	End Sub

    ''' <summary>
    ''' Handles the CheckedChanged event of the Check_IsLimitItem control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Check_IsLimitItem_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Check_IsLimitItem.CheckedChanged
		' ********************************************************************************************
		' Enable the Limit Details panel is this Is a Limit Item.
		'
		' ********************************************************************************************

		If Check_IsLimitItem.Checked Then
			Me.Panel_LimitDetails.Enabled = True
			Me.Check_IsDataItem.Checked = True

			If (Combo_LimitType.SelectedIndex < 0) AndAlso (Combo_LimitType.Items.Count > 0) Then
				Combo_LimitType.SelectedIndex = 0
			End If

		Else
			Me.Panel_LimitDetails.Enabled = False
		End If
	End Sub

    ''' <summary>
    ''' Handles the SelectedIndexChanged event of the Combo_LimitType control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Combo_LimitType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_LimitType.SelectedIndexChanged
		' ********************************************************************************************
		' Alter Limit labels appropriate to the chosen Limit type.
		'
		' ********************************************************************************************

		If (Combo_LimitType.SelectedIndex >= 0) AndAlso (IsNumeric(Combo_LimitType.SelectedValue)) Then
			Select Case CInt(Combo_LimitType.SelectedValue)
				Case RenaissanceGlobals.FlorenceLimitType.Between
					Me.Label_FirstLimit.Text = "Minimum Limit"
					Me.Label_SecondLimit.Text = "Maximum Limit"
					Me.Numeric_LimitFrom.Visible = True
					Me.Numeric_LimitTo.Visible = True
					Me.Label_FirstLimit.Visible = True
					Me.Label_SecondLimit.Visible = True
					Me.Check_BooleanLimit.Visible = False
					Me.Check_LimitIsPercent.Enabled = True

				Case RenaissanceGlobals.FlorenceLimitType.Outside
					Me.Label_FirstLimit.Text = "Minimum Limit"
					Me.Label_SecondLimit.Text = "Maximum Limit"
					Me.Numeric_LimitFrom.Visible = True
					Me.Numeric_LimitTo.Visible = True
					Me.Label_FirstLimit.Visible = True
					Me.Label_SecondLimit.Visible = True
					Me.Check_BooleanLimit.Visible = False
					Me.Check_LimitIsPercent.Enabled = True

				Case RenaissanceGlobals.FlorenceLimitType.LessThan
					Me.Label_FirstLimit.Text = ""
					Me.Label_SecondLimit.Text = "Limit Value"
					Me.Numeric_LimitFrom.Visible = False
					Me.Numeric_LimitTo.Visible = True
					Me.Label_FirstLimit.Visible = True
					Me.Label_SecondLimit.Visible = True
					Me.Check_BooleanLimit.Visible = False
					Me.Check_LimitIsPercent.Enabled = True

				Case RenaissanceGlobals.FlorenceLimitType.GreaterThan
					Me.Label_FirstLimit.Text = "Limit Value"
					Me.Label_SecondLimit.Text = ""
					Me.Numeric_LimitFrom.Visible = True
					Me.Numeric_LimitTo.Visible = False
					Me.Label_FirstLimit.Visible = True
					Me.Label_SecondLimit.Visible = True
					Me.Check_BooleanLimit.Visible = False
					Me.Check_LimitIsPercent.Enabled = True

				Case RenaissanceGlobals.FlorenceLimitType.Boolean
					Me.Label_FirstLimit.Text = ""
					Me.Label_SecondLimit.Text = ""
					Me.Numeric_LimitFrom.Visible = False
					Me.Numeric_LimitTo.Visible = False
					Me.Label_FirstLimit.Visible = False
					Me.Label_SecondLimit.Visible = False
					Me.Check_BooleanLimit.Visible = True
					Me.Check_LimitIsPercent.Enabled = False
					Me.Check_LimitIsPercent.Checked = False

			End Select
		End If

	End Sub

    ''' <summary>
    ''' Handles the CheckedChanged event of the Check_IsDataItem control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Check_IsDataItem_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Check_IsDataItem.CheckedChanged
		' ********************************************************************************************
		' Enable DataItemType combo as necessary.
		' If this Item is a Limit Item, then DataItem must be enabled.
		'
		' ********************************************************************************************

		Try
			If (Check_IsDataItem.Checked) Then
				Me.Combo_DataItemType.Enabled = True
				Me.Combo_DataUpdatePeriod.Enabled = True
				Me.Check_UpdateStatusOnDataUpdate.Enabled = True

				If (Combo_DataUpdatePeriod.SelectedIndex <= 0) And (Combo_DataUpdatePeriod.SelectedIndex <> Me.Combo_UpdatePeriod.SelectedIndex) Then
					Combo_DataUpdatePeriod.SelectedIndex = Me.Combo_UpdatePeriod.SelectedIndex
				End If
			Else
				If (Check_IsLimitItem.Checked) Then
					Check_IsDataItem.Checked = True
				Else
					Me.Combo_DataItemType.Enabled = False
					Me.Combo_DataUpdatePeriod.Enabled = False
					Me.Check_UpdateStatusOnDataUpdate.Enabled = False
				End If
			End If
		Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Check_IsDataItem_CheckedChanged()", ex.StackTrace, True)
		End Try

	End Sub

    ''' <summary>
    ''' Handles the CheckedChanged event of the Check_LimitIsPercent control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Check_LimitIsPercent_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Check_LimitIsPercent.CheckedChanged
		' ********************************************************************************************
		' Alter the format of the Limit Value boxes if this is a percentage figure.
		'
		' ********************************************************************************************

		Try
			If Check_LimitIsPercent.Checked Then
				Me.Numeric_LimitFrom.TextFormat = "#,##0.00##%"
				Me.Numeric_LimitTo.TextFormat = "#,##0.00##%"
			Else
				Me.Numeric_LimitFrom.TextFormat = "#,##0.00##"
				Me.Numeric_LimitTo.TextFormat = "#,##0.00##"
			End If
		Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Check_LimitIsPercent_CheckedChanged()", ex.StackTrace, True)
		End Try
	End Sub

    ''' <summary>
    ''' Handles the CheckedChanged event of the Check_BooleanLimit control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Check_BooleanLimit_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Check_BooleanLimit.CheckedChanged
		' ********************************************************************************************
		'
		'
		' ********************************************************************************************

		Try
			If (Check_BooleanLimit.Checked) Then
				Check_BooleanLimit.Text = "Limit is 'True'"
			Else
				Check_BooleanLimit.Text = "Limit is 'False'"
			End If
		Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Check_BooleanLimit_CheckedChanged()", ex.StackTrace, True)
		End Try
	End Sub

    ''' <summary>
    ''' Handles the SelectedIndexChanged event of the Combo_SelectGroup control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Combo_SelectGroup_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_SelectGroup.SelectedIndexChanged
		' ********************************************************************************************
		' This form allows the user to display only those items from a given group.
		' This combo controls this.
		'
		' ********************************************************************************************

		If InPaint Then
			Exit Sub
		End If

		Try
			If (FormChanged = True) Then
				Call SetFormData()
			End If

			Call SetSortedRows()

			thisPosition = 0
			If thisPosition >= THIS_FORM_SelectingCombo.Items.Count Then
				thisPosition = (THIS_FORM_SelectingCombo.Items.Count - 1)
			End If

			THIS_FORM_SelectingCombo.SelectedIndex = thisPosition

			If (thisPosition >= 0) Then
				thisDataRow = Me.SortedRows(thisPosition)
			Else
				thisDataRow = Nothing
			End If
		Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Combo_SelectGroup_SelectedIndexChanged()", ex.StackTrace, True)
		End Try

		Call GetFormData(thisDataRow)

	End Sub

#End Region


#Region " Navigation Code / GetPosition() (Generic Code) "

    ''' <summary>
    ''' Handles the SelectComboChanged event of the Combo control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Combo_SelectComboChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    ' ********************************************************************************************
    ' Selection Combo. SelectedItem changed.
		'
    ' ********************************************************************************************

		' Don't react to changes made in paint routines etc.
		If InPaint = True Then Exit Sub

    Try
      If (FormChanged = True) Then
        Call SetFormData()
      End If

      ' Find the correct data row, then show it...
      thisPosition = THIS_FORM_SelectingCombo.SelectedIndex

      If (thisPosition >= 0) Then
        thisDataRow = Me.SortedRows(thisPosition)
      Else
        thisDataRow = Nothing
      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Combo_SelectComboChanged()", ex.StackTrace, True)
    End Try

    Try
      Call GetFormData(thisDataRow)
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Combo_SelectComboChanged()", ex.StackTrace, True)
    End Try

	End Sub


    ''' <summary>
    ''' Handles the Click event of the btnNavPrev control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnNavPrev_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNavPrev.Click
    ' ********************************************************************************************
    ' 'Previous' Button.
    ' ********************************************************************************************

		If (FormChanged = True) Then
			Call SetFormData()
		End If

    Try
      If thisPosition > 0 Then thisPosition -= 1
      If thisPosition >= THIS_FORM_SelectingCombo.Items.Count Then
        thisPosition = (THIS_FORM_SelectingCombo.Items.Count - 1)
      End If

      THIS_FORM_SelectingCombo.SelectedIndex = thisPosition

      Exit Sub

      If myTable.Rows.Count > thisPosition Then
        thisDataRow = myTable.Rows(thisPosition)
        Call GetFormData(thisDataRow)
      Else
        Call GetFormData(Nothing)
      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in btnNavPrev_Click()", ex.StackTrace, True)
    End Try

	End Sub

    ''' <summary>
    ''' Handles the Click event of the btnNavNext control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnNavNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNavNext.Click
    ' ********************************************************************************************
    ' 'Next' Button.
    ' ********************************************************************************************

    Try
      If (FormChanged = True) Then
        Call SetFormData()
      End If

      If thisPosition < (myTable.Rows.Count - 1) Then thisPosition += 1
      If thisPosition >= THIS_FORM_SelectingCombo.Items.Count Then
        thisPosition = (THIS_FORM_SelectingCombo.Items.Count - 1)
      End If

      THIS_FORM_SelectingCombo.SelectedIndex = thisPosition
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in btnNavNext_Click()", ex.StackTrace, True)
    End Try

	End Sub

    ''' <summary>
    ''' Handles the Click event of the btnNavFirst control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnNavFirst_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNavFirst.Click
    ' ********************************************************************************************
    ' 'First' Button.
    ' ********************************************************************************************

    Try
      If (FormChanged = True) Then
        Call SetFormData()
      End If

      thisPosition = 0
      If thisPosition >= THIS_FORM_SelectingCombo.Items.Count Then
        thisPosition = (THIS_FORM_SelectingCombo.Items.Count - 1)
      End If

      THIS_FORM_SelectingCombo.SelectedIndex = thisPosition
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in btnNavFirst_Click()", ex.StackTrace, True)
    End Try

	End Sub

    ''' <summary>
    ''' Handles the Click event of the btnLast control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnLast_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLast.Click
    ' ********************************************************************************************
    ' 'Last' Button.
    ' ********************************************************************************************

    Try
      If (FormChanged = True) Then
        Call SetFormData()
      End If

      thisPosition = THIS_FORM_SelectingCombo.Items.Count - 1

      THIS_FORM_SelectingCombo.SelectedIndex = thisPosition
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in btnLast_Click()", ex.StackTrace, True)
    End Try

	End Sub

    ''' <summary>
    ''' Get_s the position.
    ''' </summary>
    ''' <param name="pAuditID">The p audit ID.</param>
    ''' <returns>System.Int32.</returns>
	Private Function Get_Position(ByVal pAuditID As Integer) As Integer
    ' ********************************************************************************************
    ' Returns the position in the current 'SortedRows' array of the entry corresponding to the given 
		' AudidID.
    ' ********************************************************************************************

		Dim MinIndex As Integer
		Dim MaxIndex As Integer
		Dim CurrentMin As Integer
		Dim CurrentMax As Integer
		Dim searchDataRow As DataRow

		Try
			' SortedRows exists ?

			If SortedRows Is Nothing Then
				Return (-1)
				Exit Function
			End If

			' Return (-1) if there are no rows in the 'SortedRows'

			If (SortedRows.GetLength(0) <= 0) Then
				Return (-1)
				Exit Function
			End If


			' Use a modified Search moving outwards from the last returned value to locate the required value.
			' Reflecting the fact that for the most part One looks for closely located records.

			MinIndex = 0
			MaxIndex = SortedRows.GetLength(0) - 1


			' Check First and Last records (Incase 'First' or 'Last' was pressed).

			searchDataRow = SortedRows(MinIndex)

			If (searchDataRow.IsNull("AuditID") = False) AndAlso (CInt(CInt(searchDataRow("AuditID"))) = pAuditID) Then
				Return MinIndex
				Exit Function
			End If

			searchDataRow = SortedRows(MaxIndex)
			If (searchDataRow.IsNull("AuditID") = False) AndAlso (CInt(CInt(searchDataRow("AuditID"))) = pAuditID) Then
				Return MaxIndex
				Exit Function
			End If

			' now search outwards from the last returned value.

			If (Me.thisPosition >= MinIndex) And (Me.thisPosition <= MaxIndex) Then
				CurrentMin = thisPosition
				CurrentMax = CurrentMin + 1
			Else
				CurrentMin = CInt((MinIndex + MaxIndex) / 2)
				CurrentMax = CurrentMin + 1
			End If

			While (CurrentMin >= MinIndex) Or (CurrentMax <= MaxIndex)
				If (CurrentMin >= MinIndex) Then
					searchDataRow = SortedRows(CurrentMin)

					If (searchDataRow.IsNull("AuditID") = False) AndAlso (CInt(CInt(searchDataRow("AuditID"))) = pAuditID) Then
						Return CurrentMin
						Exit Function
					End If

					CurrentMin -= 1
				End If

				If (CurrentMax <= MaxIndex) Then
					searchDataRow = SortedRows(CurrentMax)

					If (searchDataRow.IsNull("AuditID") = False) AndAlso (CInt(CInt(searchDataRow("AuditID"))) = pAuditID) Then
						Return CurrentMax
						Exit Function
					End If

					CurrentMax += 1
				End If

			End While

			Return (-1)
			Exit Function

		Catch ex As Exception
			MainForm.LogError(Me.Name & ", Get_Position()", LOG_LEVELS.Warning, ex.Message, "", ex.StackTrace, True)
			Return (-1)
			Exit Function
		End Try

	End Function

#End Region

#Region " Buttons : Add / Delete / Cancel / Save and Close (Generic Code) "

    ''' <summary>
    ''' Handles the Click event of the btnCancel control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
		' Cancel Changes, redisplay form.

		FormChanged = False
		AddNewRecord = False

		Me.THIS_FORM_SelectingCombo.Enabled = True

		If (thisPosition >= 0) And (thisPosition < Me.SortedRows.GetLength(0)) Then
			thisDataRow = Me.SortedRows(thisPosition)
			Call GetFormData(thisDataRow)
		Else
			Call btnNavFirst_Click(Me, New System.EventArgs)
		End If

	End Sub

    ''' <summary>
    ''' Handles the Click event of the btnSave control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
		' Save Changes, if any, without prompting.

		If (FormChanged = True) Then
			Call SetFormData(False)
		End If

	End Sub

    ''' <summary>
    ''' Handles the Click event of the btnDelete control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
		Dim Position As Integer
		Dim NextAuditID As Integer

		If (AddNewRecord = True) Then
			Call btnCancel_Click(Me, New System.EventArgs)
			Exit Sub
		End If

		' No Appropriate Save permission :-

		If (Me.HasDeletePermission = False) Then
			MainForm.LogError(Me.Name & ", btnDelete()", LOG_LEVELS.Warning, "", "You do not have permission to Delete this record.", "", True)
			Call btnCancel_Click(Me, New System.EventArgs)
			Exit Sub
		End If

		' *************************************************************
		' KnowledgeDate OK :-
		' *************************************************************

		If (MainForm.Main_Knowledgedate > KNOWLEDGEDATE_NOW) Then
			MainForm.LogError(Me.Name & ", btnDelete()", LOG_LEVELS.Warning, "", "Changes to Venice are not allowed when the Knowledgedate is not set to `NOW`", "", True)
			Call btnCancel_Click(Me, New System.EventArgs)
			Exit Sub
		End If

		' *************************************************************
		' Confirm :-
		' *************************************************************
		If MsgBox("Are you Sure ?", MsgBoxStyle.OkCancel, "Confirm Delete") = MsgBoxResult.Cancel Then
			Exit Sub
		End If

		' Check Data position.

		Position = Get_Position(thisAuditID)

		If Position < 0 Then Exit Sub

		' Check Referential Integrity 
		If CheckReferentialIntegrity(_MainForm, Me.SortedRows(Position)) = False Then
			MainForm.LogError(Me.Name & ", btnDelete()", LOG_LEVELS.Warning, "", "Deleting this record would voilate referential integrity.", "", True)
			Call btnCancel_Click(Me, New System.EventArgs)
			Exit Sub
		End If

		' Resolve row to show after deleting this one.

		NextAuditID = (-1)
		If (Position + 1) < Me.SortedRows.GetLength(0) Then
			NextAuditID = CInt(Me.SortedRows(Position + 1)("AuditID"))
		ElseIf (Position - 1) >= 0 Then
			NextAuditID = CInt(Me.SortedRows(Position - 1)("AuditID"))
		Else
			NextAuditID = (-1)
		End If

		' Delete this row

		Try
			Me.SortedRows(Position).Delete()

			MainForm.AdaptorUpdate(Me.Name, myAdaptor, myTable)

		Catch ex As Exception

			Call MainForm.LogError(Me.Name & ", Delete()", LOG_LEVELS.Error, ex.Message, "Error Deleting Record.", ex.StackTrace, True)
		End Try

		' Tidy Up.

		FormChanged = False

		thisAuditID = NextAuditID
		Call MainForm.Main_RaiseEvent(New RenaissanceGlobals.RenaissanceUpdateEventArgs(THIS_FORM_ChangeID))

	End Sub

    ''' <summary>
    ''' Handles the Click event of the btnAdd control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
		' Prepare form to Add a new record.

		If (FormChanged = True) Then
			Call SetFormData()
		End If

		thisPosition = -1

		Try
			InPaint = True
			MainForm.ClearComboSelection(THIS_FORM_SelectingCombo)
		Catch ex As Exception
		Finally
			InPaint = False
		End Try

		GetFormData(Nothing)
		AddNewRecord = True
		Me.btnCancel.Enabled = True
		Me.THIS_FORM_SelectingCombo.Enabled = False
		If (Me.Combo_SelectGroup.SelectedIndex > 0) AndAlso (IsNumeric(Combo_SelectGroup.SelectedValue)) Then
			Me.Combo_ItemGroup.SelectedValue = Combo_SelectGroup.SelectedValue
		End If

		Call SetButtonStatus()

		THIS_FORM_NewMoveToControl.Focus()

	End Sub

    ''' <summary>
    ''' Handles the Click event of the btnClose control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
		' Close Form

		If (FormChanged = True) Then
			Call SetFormData()
		End If

		Me.Close()

	End Sub

#End Region

#Region " Bug hunting "

	' AddHandler myAdaptor.RowUpdating, AddressOf OnRowUpdating
	' AddHandler myAdaptor.RowUpdated, AddressOf OnRowUpdated
	' AddHandler myAdaptor.FillError, AddressOf OnRowFillError

    ''' <summary>
    ''' Called when [row updating].
    ''' </summary>
    ''' <param name="Sender">The sender.</param>
    ''' <param name="e">The <see cref="SqlRowUpdatingEventArgs"/> instance containing the event data.</param>
	Protected Shared Sub OnRowUpdating(ByVal Sender As Object, ByVal e As SqlRowUpdatingEventArgs)
		Dim a As Integer

		If Not e.Command Is Nothing Then
			a = 1
		End If

		If Not e.Errors Is Nothing Then
			a = 2
		End If
	End Sub

    ''' <summary>
    ''' Called when [row updated].
    ''' </summary>
    ''' <param name="Sender">The sender.</param>
    ''' <param name="e">The <see cref="SqlRowUpdatedEventArgs"/> instance containing the event data.</param>
	Protected Shared Sub OnRowUpdated(ByVal Sender As Object, ByVal e As SqlRowUpdatedEventArgs)
		Dim a As Integer

		If Not e.Command Is Nothing Then
			a = 1
		End If

		If Not e.Errors Is Nothing Then
			a = 2
		End If
	End Sub

    ''' <summary>
    ''' Called when [row fill error].
    ''' </summary>
    ''' <param name="Sender">The sender.</param>
    ''' <param name="e">The <see cref="FillErrorEventArgs"/> instance containing the event data.</param>
	Protected Shared Sub OnRowFillError(ByVal Sender As Object, ByVal e As FillErrorEventArgs)
		Dim a As Integer

		a = 1
	End Sub

#End Region



End Class
