' ***********************************************************************
' Assembly         : Florence
' Author           : Nicholas Pennington
' Created          : 11-28-2012
'
' Last Modified By : acullig
' Last Modified On : 11-05-2012
' ***********************************************************************
' <copyright file="frmUpdateDataItems.vb" company="">
'     Copyright (c) . All rights reserved.
' </copyright>
' <summary></summary>
' ***********************************************************************
Imports System.Data.SqlClient
Imports RenaissanceGlobals
Imports RenaissanceGlobals.Globals
Imports RenaissanceControls
Imports RenaissanceDataClass
Imports RenaissanceUtilities.DatePeriodFunctions

Imports C1.Win.C1FlexGrid

''' <summary>
''' Class frmUpdateDataItems
''' </summary>
Public Class frmUpdateDataItems

  Inherits System.Windows.Forms.Form
  Implements StandardFlorenceForm

#Region " Windows Form Designer generated code "

    ''' <summary>
    ''' Prevents a default instance of the <see cref="frmUpdateDataItems"/> class from being created.
    ''' </summary>
  Private Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
    ''' <summary>
    ''' Disposes of the resources (other than memory) used by the <see cref="T:System.Windows.Forms.Form" />.
    ''' </summary>
    ''' <param name="disposing">true to release both managed and unmanaged resources; false to release only unmanaged resources.</param>
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
    ''' <summary>
    ''' The components
    ''' </summary>
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
    ''' <summary>
    ''' The combo_ group
    ''' </summary>
  Friend WithEvents Combo_Group As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The label_ cpty is fund
    ''' </summary>
  Friend WithEvents label_CptyIsFund As System.Windows.Forms.Label
    ''' <summary>
    ''' The combo_ entity
    ''' </summary>
  Friend WithEvents Combo_Entity As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The label2
    ''' </summary>
  Friend WithEvents Label2 As System.Windows.Forms.Label
    ''' <summary>
    ''' The combo_ field select1
    ''' </summary>
  Friend WithEvents Combo_FieldSelect1 As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The combo_ select1_ operator
    ''' </summary>
  Friend WithEvents Combo_Select1_Operator As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The combo_ select1_ value
    ''' </summary>
  Friend WithEvents Combo_Select1_Value As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The combo_ select2_ value
    ''' </summary>
  Friend WithEvents Combo_Select2_Value As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The combo_ field select2
    ''' </summary>
  Friend WithEvents Combo_FieldSelect2 As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The combo_ select2_ operator
    ''' </summary>
  Friend WithEvents Combo_Select2_Operator As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The combo_ select3_ value
    ''' </summary>
  Friend WithEvents Combo_Select3_Value As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The combo_ field select3
    ''' </summary>
  Friend WithEvents Combo_FieldSelect3 As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The combo_ select3_ operator
    ''' </summary>
  Friend WithEvents Combo_Select3_Operator As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The combo_ and or_1
    ''' </summary>
  Friend WithEvents Combo_AndOr_1 As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The combo_ and or_2
    ''' </summary>
  Friend WithEvents Combo_AndOr_2 As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The check_ omit completed
    ''' </summary>
  Friend WithEvents Check_OmitCompleted As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The grid_ transactions
    ''' </summary>
  Friend WithEvents Grid_Transactions As C1.Win.C1FlexGrid.C1FlexGrid
    ''' <summary>
    ''' The check_ overdue only
    ''' </summary>
  Friend WithEvents Check_OverdueOnly As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The combo_ item
    ''' </summary>
  Friend WithEvents Combo_Item As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The label1
    ''' </summary>
  Friend WithEvents Label1 As System.Windows.Forms.Label
    ''' <summary>
    ''' The BTN save
    ''' </summary>
  Friend WithEvents btnSave As System.Windows.Forms.Button
    ''' <summary>
    ''' The root menu
    ''' </summary>
  Friend WithEvents RootMenu As System.Windows.Forms.MenuStrip
    ''' <summary>
    ''' The select status_ status strip
    ''' </summary>
  Friend WithEvents SelectStatus_StatusStrip As System.Windows.Forms.StatusStrip
    ''' <summary>
    ''' The status bar_ select transactions
    ''' </summary>
  Friend WithEvents StatusBar_SelectTransactions As System.Windows.Forms.ToolStripStatusLabel
    ''' <summary>
    ''' The select status_ progress bar
    ''' </summary>
  Friend WithEvents SelectStatus_ProgressBar As System.Windows.Forms.ToolStripProgressBar
    ''' <summary>
    ''' The split container1
    ''' </summary>
  Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
    ''' <summary>
    ''' The label3
    ''' </summary>
  Friend WithEvents Label3 As System.Windows.Forms.Label
    ''' <summary>
    ''' The D t_ reference date
    ''' </summary>
  Friend WithEvents DT_ReferenceDate As System.Windows.Forms.DateTimePicker
    ''' <summary>
    ''' The panel_ data entry
    ''' </summary>
  Friend WithEvents Panel_DataEntry As System.Windows.Forms.Panel
    ''' <summary>
    ''' The label_ data item_ date
    ''' </summary>
  Friend WithEvents Label_DataItem_Date As System.Windows.Forms.Label
    ''' <summary>
    ''' The label_ data item_ item
    ''' </summary>
  Friend WithEvents Label_DataItem_Item As System.Windows.Forms.Label
    ''' <summary>
    ''' The BTN cancel
    ''' </summary>
  Friend WithEvents btnCancel As System.Windows.Forms.Button
    ''' <summary>
    ''' Initializes the component.
    ''' </summary>
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmUpdateDataItems))
		Me.Combo_Group = New System.Windows.Forms.ComboBox
		Me.label_CptyIsFund = New System.Windows.Forms.Label
		Me.Combo_Entity = New System.Windows.Forms.ComboBox
		Me.Label2 = New System.Windows.Forms.Label
		Me.Combo_Select1_Operator = New System.Windows.Forms.ComboBox
		Me.Combo_FieldSelect1 = New System.Windows.Forms.ComboBox
		Me.Combo_Select1_Value = New System.Windows.Forms.ComboBox
		Me.Combo_Select2_Value = New System.Windows.Forms.ComboBox
		Me.Combo_FieldSelect2 = New System.Windows.Forms.ComboBox
		Me.Combo_Select2_Operator = New System.Windows.Forms.ComboBox
		Me.Combo_Select3_Value = New System.Windows.Forms.ComboBox
		Me.Combo_FieldSelect3 = New System.Windows.Forms.ComboBox
		Me.Combo_Select3_Operator = New System.Windows.Forms.ComboBox
		Me.Combo_AndOr_1 = New System.Windows.Forms.ComboBox
		Me.Combo_AndOr_2 = New System.Windows.Forms.ComboBox
		Me.Check_OmitCompleted = New System.Windows.Forms.CheckBox
		Me.Grid_Transactions = New C1.Win.C1FlexGrid.C1FlexGrid
		Me.Check_OverdueOnly = New System.Windows.Forms.CheckBox
		Me.Combo_Item = New System.Windows.Forms.ComboBox
		Me.Label1 = New System.Windows.Forms.Label
		Me.btnSave = New System.Windows.Forms.Button
		Me.btnCancel = New System.Windows.Forms.Button
		Me.RootMenu = New System.Windows.Forms.MenuStrip
		Me.SelectStatus_StatusStrip = New System.Windows.Forms.StatusStrip
		Me.SelectStatus_ProgressBar = New System.Windows.Forms.ToolStripProgressBar
		Me.StatusBar_SelectTransactions = New System.Windows.Forms.ToolStripStatusLabel
		Me.SplitContainer1 = New System.Windows.Forms.SplitContainer
		Me.Label_DataItem_Date = New System.Windows.Forms.Label
		Me.Label_DataItem_Item = New System.Windows.Forms.Label
		Me.Panel_DataEntry = New System.Windows.Forms.Panel
		Me.Label3 = New System.Windows.Forms.Label
		Me.DT_ReferenceDate = New System.Windows.Forms.DateTimePicker
		CType(Me.Grid_Transactions, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.SelectStatus_StatusStrip.SuspendLayout()
		Me.SplitContainer1.Panel1.SuspendLayout()
		Me.SplitContainer1.Panel2.SuspendLayout()
		Me.SplitContainer1.SuspendLayout()
		Me.SuspendLayout()
		'
		'Combo_Group
		'
		Me.Combo_Group.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Combo_Group.Location = New System.Drawing.Point(128, 29)
		Me.Combo_Group.Name = "Combo_Group"
		Me.Combo_Group.Size = New System.Drawing.Size(878, 21)
		Me.Combo_Group.TabIndex = 0
		'
		'label_CptyIsFund
		'
		Me.label_CptyIsFund.Location = New System.Drawing.Point(16, 33)
		Me.label_CptyIsFund.Name = "label_CptyIsFund"
		Me.label_CptyIsFund.Size = New System.Drawing.Size(104, 16)
		Me.label_CptyIsFund.TabIndex = 82
		Me.label_CptyIsFund.Text = "Group"
		'
		'Combo_Entity
		'
		Me.Combo_Entity.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Combo_Entity.Location = New System.Drawing.Point(128, 56)
		Me.Combo_Entity.Name = "Combo_Entity"
		Me.Combo_Entity.Size = New System.Drawing.Size(878, 21)
		Me.Combo_Entity.TabIndex = 1
		'
		'Label2
		'
		Me.Label2.Location = New System.Drawing.Point(16, 60)
		Me.Label2.Name = "Label2"
		Me.Label2.Size = New System.Drawing.Size(104, 16)
		Me.Label2.TabIndex = 84
		Me.Label2.Text = "Entity"
		'
		'Combo_Select1_Operator
		'
		Me.Combo_Select1_Operator.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
		Me.Combo_Select1_Operator.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Combo_Select1_Operator.Items.AddRange(New Object() {"", "=", "<>", "<", "<=", ">", ">=", "Like", "Not Like"})
		Me.Combo_Select1_Operator.Location = New System.Drawing.Point(272, 111)
		Me.Combo_Select1_Operator.Name = "Combo_Select1_Operator"
		Me.Combo_Select1_Operator.Size = New System.Drawing.Size(96, 21)
		Me.Combo_Select1_Operator.TabIndex = 4
		'
		'Combo_FieldSelect1
		'
		Me.Combo_FieldSelect1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
		Me.Combo_FieldSelect1.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Combo_FieldSelect1.Location = New System.Drawing.Point(128, 111)
		Me.Combo_FieldSelect1.Name = "Combo_FieldSelect1"
		Me.Combo_FieldSelect1.Size = New System.Drawing.Size(136, 21)
		Me.Combo_FieldSelect1.Sorted = True
		Me.Combo_FieldSelect1.TabIndex = 3
		'
		'Combo_Select1_Value
		'
		Me.Combo_Select1_Value.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Combo_Select1_Value.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Combo_Select1_Value.Location = New System.Drawing.Point(376, 111)
		Me.Combo_Select1_Value.Name = "Combo_Select1_Value"
		Me.Combo_Select1_Value.Size = New System.Drawing.Size(342, 21)
		Me.Combo_Select1_Value.TabIndex = 5
		'
		'Combo_Select2_Value
		'
		Me.Combo_Select2_Value.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Combo_Select2_Value.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Combo_Select2_Value.Location = New System.Drawing.Point(376, 137)
		Me.Combo_Select2_Value.Name = "Combo_Select2_Value"
		Me.Combo_Select2_Value.Size = New System.Drawing.Size(342, 21)
		Me.Combo_Select2_Value.TabIndex = 9
		'
		'Combo_FieldSelect2
		'
		Me.Combo_FieldSelect2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
		Me.Combo_FieldSelect2.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Combo_FieldSelect2.Location = New System.Drawing.Point(128, 137)
		Me.Combo_FieldSelect2.Name = "Combo_FieldSelect2"
		Me.Combo_FieldSelect2.Size = New System.Drawing.Size(136, 21)
		Me.Combo_FieldSelect2.Sorted = True
		Me.Combo_FieldSelect2.TabIndex = 7
		'
		'Combo_Select2_Operator
		'
		Me.Combo_Select2_Operator.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
		Me.Combo_Select2_Operator.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Combo_Select2_Operator.Items.AddRange(New Object() {"", "=", "<>", "<", "<=", ">", ">=", "Like", "Not Like"})
		Me.Combo_Select2_Operator.Location = New System.Drawing.Point(272, 137)
		Me.Combo_Select2_Operator.Name = "Combo_Select2_Operator"
		Me.Combo_Select2_Operator.Size = New System.Drawing.Size(96, 21)
		Me.Combo_Select2_Operator.TabIndex = 8
		'
		'Combo_Select3_Value
		'
		Me.Combo_Select3_Value.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Combo_Select3_Value.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Combo_Select3_Value.Location = New System.Drawing.Point(376, 163)
		Me.Combo_Select3_Value.Name = "Combo_Select3_Value"
		Me.Combo_Select3_Value.Size = New System.Drawing.Size(342, 21)
		Me.Combo_Select3_Value.TabIndex = 13
		'
		'Combo_FieldSelect3
		'
		Me.Combo_FieldSelect3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
		Me.Combo_FieldSelect3.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Combo_FieldSelect3.Location = New System.Drawing.Point(128, 163)
		Me.Combo_FieldSelect3.Name = "Combo_FieldSelect3"
		Me.Combo_FieldSelect3.Size = New System.Drawing.Size(136, 21)
		Me.Combo_FieldSelect3.Sorted = True
		Me.Combo_FieldSelect3.TabIndex = 11
		'
		'Combo_Select3_Operator
		'
		Me.Combo_Select3_Operator.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
		Me.Combo_Select3_Operator.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Combo_Select3_Operator.Items.AddRange(New Object() {"", "=", "<>", "<", "<=", ">", ">=", "Like", "Not Like"})
		Me.Combo_Select3_Operator.Location = New System.Drawing.Point(272, 163)
		Me.Combo_Select3_Operator.Name = "Combo_Select3_Operator"
		Me.Combo_Select3_Operator.Size = New System.Drawing.Size(96, 21)
		Me.Combo_Select3_Operator.TabIndex = 12
		'
		'Combo_AndOr_1
		'
		Me.Combo_AndOr_1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Combo_AndOr_1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
		Me.Combo_AndOr_1.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Combo_AndOr_1.Items.AddRange(New Object() {"AND", "OR"})
		Me.Combo_AndOr_1.Location = New System.Drawing.Point(730, 111)
		Me.Combo_AndOr_1.Name = "Combo_AndOr_1"
		Me.Combo_AndOr_1.Size = New System.Drawing.Size(96, 21)
		Me.Combo_AndOr_1.TabIndex = 6
		'
		'Combo_AndOr_2
		'
		Me.Combo_AndOr_2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Combo_AndOr_2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
		Me.Combo_AndOr_2.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Combo_AndOr_2.Items.AddRange(New Object() {"AND", "OR"})
		Me.Combo_AndOr_2.Location = New System.Drawing.Point(730, 137)
		Me.Combo_AndOr_2.Name = "Combo_AndOr_2"
		Me.Combo_AndOr_2.Size = New System.Drawing.Size(96, 21)
		Me.Combo_AndOr_2.TabIndex = 10
		'
		'Check_OmitCompleted
		'
		Me.Check_OmitCompleted.Location = New System.Drawing.Point(128, 187)
		Me.Check_OmitCompleted.Name = "Check_OmitCompleted"
		Me.Check_OmitCompleted.Size = New System.Drawing.Size(236, 16)
		Me.Check_OmitCompleted.TabIndex = 14
		Me.Check_OmitCompleted.Text = "Omit Completed"
		'
		'Grid_Transactions
		'
		Me.Grid_Transactions.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
								Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Grid_Transactions.ColumnInfo = resources.GetString("Grid_Transactions.ColumnInfo")
		Me.Grid_Transactions.Location = New System.Drawing.Point(1, 1)
		Me.Grid_Transactions.Name = "Grid_Transactions"
		Me.Grid_Transactions.Rows.DefaultSize = 17
		Me.Grid_Transactions.ShowThemedHeaders = C1.Win.C1FlexGrid.ShowThemedHeadersEnum.Columns
		Me.Grid_Transactions.Size = New System.Drawing.Size(378, 456)
		Me.Grid_Transactions.TabIndex = 0
		Me.Grid_Transactions.Tree.Column = 0
		'
		'Check_OverdueOnly
		'
		Me.Check_OverdueOnly.Location = New System.Drawing.Point(376, 187)
		Me.Check_OverdueOnly.Name = "Check_OverdueOnly"
		Me.Check_OverdueOnly.Size = New System.Drawing.Size(236, 16)
		Me.Check_OverdueOnly.TabIndex = 15
		Me.Check_OverdueOnly.Text = "Overdue Only"
		'
		'Combo_Item
		'
		Me.Combo_Item.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Combo_Item.Location = New System.Drawing.Point(128, 82)
		Me.Combo_Item.Name = "Combo_Item"
		Me.Combo_Item.Size = New System.Drawing.Size(878, 21)
		Me.Combo_Item.TabIndex = 2
		'
		'Label1
		'
		Me.Label1.Location = New System.Drawing.Point(16, 86)
		Me.Label1.Name = "Label1"
		Me.Label1.Size = New System.Drawing.Size(104, 16)
		Me.Label1.TabIndex = 108
		Me.Label1.Text = "Item"
		'
		'btnSave
		'
		Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
		Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.btnSave.Location = New System.Drawing.Point(113, 426)
		Me.btnSave.Name = "btnSave"
		Me.btnSave.Size = New System.Drawing.Size(127, 28)
		Me.btnSave.TabIndex = 2
		Me.btnSave.Text = "&Save"
		'
		'btnCancel
		'
		Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
		Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
		Me.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.btnCancel.Location = New System.Drawing.Point(266, 426)
		Me.btnCancel.Name = "btnCancel"
		Me.btnCancel.Size = New System.Drawing.Size(75, 28)
		Me.btnCancel.TabIndex = 3
		Me.btnCancel.Text = "&Cancel"
		'
		'RootMenu
		'
		Me.RootMenu.Location = New System.Drawing.Point(0, 0)
		Me.RootMenu.Name = "RootMenu"
		Me.RootMenu.Size = New System.Drawing.Size(1014, 24)
		Me.RootMenu.TabIndex = 17
		Me.RootMenu.Text = "RootMenu"
		'
		'SelectStatus_StatusStrip
		'
		Me.SelectStatus_StatusStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.SelectStatus_ProgressBar, Me.StatusBar_SelectTransactions})
		Me.SelectStatus_StatusStrip.Location = New System.Drawing.Point(0, 673)
		Me.SelectStatus_StatusStrip.Name = "SelectStatus_StatusStrip"
		Me.SelectStatus_StatusStrip.Size = New System.Drawing.Size(1014, 22)
		Me.SelectStatus_StatusStrip.TabIndex = 110
		Me.SelectStatus_StatusStrip.Text = "StatusStrip1"
		'
		'SelectStatus_ProgressBar
		'
		Me.SelectStatus_ProgressBar.Maximum = 20
		Me.SelectStatus_ProgressBar.Name = "SelectStatus_ProgressBar"
		Me.SelectStatus_ProgressBar.Size = New System.Drawing.Size(150, 17)
		Me.SelectStatus_ProgressBar.Step = 1
		Me.SelectStatus_ProgressBar.Visible = False
		'
		'StatusBar_SelectTransactions
		'
		Me.StatusBar_SelectTransactions.Name = "StatusBar_SelectTransactions"
		Me.StatusBar_SelectTransactions.Size = New System.Drawing.Size(111, 17)
		Me.StatusBar_SelectTransactions.Text = "ToolStripStatusLabel1"
		'
		'SplitContainer1
		'
		Me.SplitContainer1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
								Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.SplitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.SplitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1
		Me.SplitContainer1.Location = New System.Drawing.Point(0, 209)
		Me.SplitContainer1.MinimumSize = New System.Drawing.Size(700, 400)
		Me.SplitContainer1.Name = "SplitContainer1"
		'
		'SplitContainer1.Panel1
		'
		Me.SplitContainer1.Panel1.Controls.Add(Me.Grid_Transactions)
		Me.SplitContainer1.Panel1MinSize = 200
		'
		'SplitContainer1.Panel2
		'
		Me.SplitContainer1.Panel2.Controls.Add(Me.Label_DataItem_Date)
		Me.SplitContainer1.Panel2.Controls.Add(Me.Label_DataItem_Item)
		Me.SplitContainer1.Panel2.Controls.Add(Me.Panel_DataEntry)
		Me.SplitContainer1.Panel2.Controls.Add(Me.Label3)
		Me.SplitContainer1.Panel2.Controls.Add(Me.DT_ReferenceDate)
		Me.SplitContainer1.Panel2.Controls.Add(Me.btnCancel)
		Me.SplitContainer1.Panel2.Controls.Add(Me.btnSave)
		Me.SplitContainer1.Panel2MinSize = 300
		Me.SplitContainer1.Size = New System.Drawing.Size(1014, 461)
		Me.SplitContainer1.SplitterDistance = 384
		Me.SplitContainer1.TabIndex = 16
		'
		'Label_DataItem_Date
		'
		Me.Label_DataItem_Date.Location = New System.Drawing.Point(360, 35)
		Me.Label_DataItem_Date.Name = "Label_DataItem_Date"
		Me.Label_DataItem_Date.Size = New System.Drawing.Size(104, 16)
		Me.Label_DataItem_Date.TabIndex = 110
		Me.Label_DataItem_Date.Text = "Value Date"
		'
		'Label_DataItem_Item
		'
		Me.Label_DataItem_Item.Location = New System.Drawing.Point(237, 35)
		Me.Label_DataItem_Item.Name = "Label_DataItem_Item"
		Me.Label_DataItem_Item.Size = New System.Drawing.Size(104, 16)
		Me.Label_DataItem_Item.TabIndex = 109
		Me.Label_DataItem_Item.Text = "Item Value"
		'
		'Panel_DataEntry
		'
		Me.Panel_DataEntry.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
								Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Panel_DataEntry.AutoScroll = True
		Me.Panel_DataEntry.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.Panel_DataEntry.Location = New System.Drawing.Point(0, 54)
		Me.Panel_DataEntry.Name = "Panel_DataEntry"
		Me.Panel_DataEntry.Size = New System.Drawing.Size(622, 366)
		Me.Panel_DataEntry.TabIndex = 1
		'
		'Label3
		'
		Me.Label3.Location = New System.Drawing.Point(3, 9)
		Me.Label3.Name = "Label3"
		Me.Label3.Size = New System.Drawing.Size(104, 16)
		Me.Label3.TabIndex = 83
		Me.Label3.Text = "ReferenceDate"
		'
		'DT_ReferenceDate
		'
		Me.DT_ReferenceDate.Location = New System.Drawing.Point(113, 5)
		Me.DT_ReferenceDate.Name = "DT_ReferenceDate"
		Me.DT_ReferenceDate.Size = New System.Drawing.Size(255, 20)
		Me.DT_ReferenceDate.TabIndex = 0
		'
		'frmUpdateDataItems
		'
		Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
		Me.ClientSize = New System.Drawing.Size(1014, 695)
		Me.Controls.Add(Me.SplitContainer1)
		Me.Controls.Add(Me.SelectStatus_StatusStrip)
		Me.Controls.Add(Me.Combo_Item)
		Me.Controls.Add(Me.Label1)
		Me.Controls.Add(Me.Check_OverdueOnly)
		Me.Controls.Add(Me.Check_OmitCompleted)
		Me.Controls.Add(Me.Combo_AndOr_2)
		Me.Controls.Add(Me.Combo_AndOr_1)
		Me.Controls.Add(Me.Combo_Select3_Value)
		Me.Controls.Add(Me.Combo_FieldSelect3)
		Me.Controls.Add(Me.Combo_Select3_Operator)
		Me.Controls.Add(Me.Combo_Select2_Value)
		Me.Controls.Add(Me.Combo_FieldSelect2)
		Me.Controls.Add(Me.Combo_Select2_Operator)
		Me.Controls.Add(Me.Combo_Select1_Value)
		Me.Controls.Add(Me.Combo_FieldSelect1)
		Me.Controls.Add(Me.Combo_Select1_Operator)
		Me.Controls.Add(Me.Combo_Entity)
		Me.Controls.Add(Me.Label2)
		Me.Controls.Add(Me.Combo_Group)
		Me.Controls.Add(Me.label_CptyIsFund)
		Me.Controls.Add(Me.RootMenu)
		Me.MainMenuStrip = Me.RootMenu
		Me.MinimumSize = New System.Drawing.Size(800, 700)
		Me.Name = "frmUpdateDataItems"
		Me.Text = "Update Due Dilligence Data Items"
		CType(Me.Grid_Transactions, System.ComponentModel.ISupportInitialize).EndInit()
		Me.SelectStatus_StatusStrip.ResumeLayout(False)
		Me.SelectStatus_StatusStrip.PerformLayout()
		Me.SplitContainer1.Panel1.ResumeLayout(False)
		Me.SplitContainer1.Panel2.ResumeLayout(False)
		Me.SplitContainer1.ResumeLayout(False)
		Me.ResumeLayout(False)
		Me.PerformLayout()

	End Sub

#End Region

#Region " Form Locals and Constants "

  ' Form 'Parent', the Main Venice form.
  ' Generally only accessed through the 'MainForm' property.
    ''' <summary>
    ''' The _ main form
    ''' </summary>
  Private WithEvents _MainForm As FlorenceMain

  ' Form ToolTip
    ''' <summary>
    ''' The form tooltip
    ''' </summary>
  Private FormTooltip As New ToolTip()

  ' Form Constants, specific to the table being updated.

    ''' <summary>
    ''' The ALWAY s_ CLOS e_ THI s_ FORM
    ''' </summary>
  Private ALWAYS_CLOSE_THIS_FORM As Boolean = False

  ' Form Locals, initialised on 'New' defining what standard data items to use
    ''' <summary>
    ''' The THI s_ TABLENAME
    ''' </summary>
  Private THIS_TABLENAME As String
    ''' <summary>
    ''' The THI s_ ADAPTORNAME
    ''' </summary>
  Private THIS_ADAPTORNAME As String
    ''' <summary>
    ''' The THI s_ DATASETNAME
    ''' </summary>
  Private THIS_DATASETNAME As String


  ' The standard ChangeID for this form. e.g. tblFund
    ''' <summary>
    ''' The THI s_ FOR m_ change ID
    ''' </summary>
  Private THIS_FORM_ChangeID As RenaissanceGlobals.RenaissanceChangeID

  ' Form Specific Order fields
    ''' <summary>
    ''' The THI s_ FOR m_ order by
    ''' </summary>
  Private THIS_FORM_OrderBy As String

  ' Form specific Permissioning variables
    ''' <summary>
    ''' The THI s_ FOR m_ permission area
    ''' </summary>
  Private THIS_FORM_PermissionArea As String
    ''' <summary>
    ''' The THI s_ FOR m_ permission type
    ''' </summary>
  Private THIS_FORM_PermissionType As RenaissanceGlobals.PermissionFeatureType

  ' Form specific Form type 
    ''' <summary>
    ''' The THI s_ FOR m_ form ID
    ''' </summary>
  Private THIS_FORM_FormID As FlorenceFormID

  ' Data Structures

    ''' <summary>
    ''' My dataset
    ''' </summary>
  Private myDataset As DataSet
    ''' <summary>
    ''' My table
    ''' </summary>
  Private myTable As DataTable
    ''' <summary>
    ''' My connection
    ''' </summary>
  Private myConnection As SqlConnection
    ''' <summary>
    ''' My adaptor
    ''' </summary>
  Private myAdaptor As SqlDataAdapter
    ''' <summary>
    ''' My data view
    ''' </summary>
  Private myDataView As DataView

    ''' <summary>
    ''' The data value dataset
    ''' </summary>
  Private DataValueDataset As DataSet
    ''' <summary>
    ''' The data value table
    ''' </summary>
	Private DataValueTable As RenaissanceDataClass.DSFlorenceDataValues.tblFlorenceDataValuesDataTable
    ''' <summary>
    ''' The data value adaptor
    ''' </summary>
	Private DataValueAdaptor As SqlDataAdapter

    ''' <summary>
    ''' The this standard dataset
    ''' </summary>
	Private ThisStandardDataset As RenaissanceGlobals.StandardDataset

    ''' <summary>
    ''' The item status I ds DS
    ''' </summary>
	Dim ItemStatusIDsDS As RenaissanceDataClass.DSFlorenceStatusIDs = Nothing
    ''' <summary>
    ''' The item group DS
    ''' </summary>
	Dim ItemGroupDS As RenaissanceDataClass.DSFlorenceGroup = Nothing
    ''' <summary>
    ''' The item status DS
    ''' </summary>
	Dim ItemStatusDS As RenaissanceDataClass.DSFlorenceStatus = Nothing

    ''' <summary>
    ''' The data item changed
    ''' </summary>
  Private DataItemChanged As Boolean = False

  ' Control Arrays
    ''' <summary>
    ''' The data item control array
    ''' </summary>
  Private DataItemControlArray(-1) As Control
    ''' <summary>
    ''' The data item name label array
    ''' </summary>
  Private DataItemNameLabelArray(-1) As Label
    ''' <summary>
    ''' The data item period label array
    ''' </summary>
  Private DataItemPeriodLabelArray(-1) As Label
    ''' <summary>
    ''' The data item date array
    ''' </summary>
  Private DataItemDateArray(-1) As DateCombo
    ''' <summary>
    ''' The work item array
    ''' </summary>
  Private WorkItemArray(-1) As DSFlorenceWorkItemsData.tblFlorenceWorkItemsDataRow
    ''' <summary>
    ''' The data value array
    ''' </summary>
  Private DataValueArray(-1) As DSFlorenceDataValues.tblFlorenceDataValuesRow
    ''' <summary>
    ''' The data value changed array
    ''' </summary>
  Private DataValueChangedArray(-1) As Boolean
    ''' <summary>
    ''' The data last date array
    ''' </summary>
  Private DataLastDateArray(-1) As Date

  ' Active Element.

    ''' <summary>
    ''' The __ is over cancel button
    ''' </summary>
  Private __IsOverCancelButton As Boolean
    ''' <summary>
    ''' The _ in use
    ''' </summary>
  Private _InUse As Boolean

    ''' <summary>
    ''' The last selected group
    ''' </summary>
  Private LastSelectedGroup As Integer = (-1)
    ''' <summary>
    ''' The last selected entity
    ''' </summary>
  Private LastSelectedEntity As Integer = (-1)

  ' Form Status Flags

    ''' <summary>
    ''' The form is valid
    ''' </summary>
  Private FormIsValid As Boolean
    ''' <summary>
    ''' The in paint
    ''' </summary>
  Private InPaint As Boolean
    ''' <summary>
    ''' The _ form open failed
    ''' </summary>
  Private _FormOpenFailed As Boolean

  ' User Permission Flags

    ''' <summary>
    ''' The has read permission
    ''' </summary>
  Private HasReadPermission As Boolean
    ''' <summary>
    ''' The has update permission
    ''' </summary>
  Private HasUpdatePermission As Boolean
    ''' <summary>
    ''' The has insert permission
    ''' </summary>
  Private HasInsertPermission As Boolean
    ''' <summary>
    ''' The has delete permission
    ''' </summary>
  Private HasDeletePermission As Boolean

  ' Data Control Constants
    ''' <summary>
    ''' The data_ RO w_ HEIGHT
    ''' </summary>
  Private Const Data_ROW_HEIGHT As Integer = 23
    ''' <summary>
    ''' The data_ RO w_ TOP
    ''' </summary>
  Private Const Data_ROW_TOP As Integer = 3

    ''' <summary>
    ''' The data_ LABE l_ LEFT
    ''' </summary>
  Private Const Data_LABEL_LEFT As Integer = 3
    ''' <summary>
    ''' The data_ LABE l_ WIDTH
    ''' </summary>
	Private Const Data_LABEL_WIDTH As Integer = 325
    ''' <summary>
    ''' The data_ LABE l_ HEIGHT
    ''' </summary>
  Private Const Data_LABEL_HEIGHT As Integer = 20

    ''' <summary>
    ''' The data_ ITE m_ LEFT
    ''' </summary>
  Private Const Data_ITEM_LEFT As Integer = (Data_LABEL_LEFT + Data_LABEL_WIDTH + 10)
    ''' <summary>
    ''' The data_ ITE m_ WIDTH
    ''' </summary>
  Private Const Data_ITEM_WIDTH As Integer = 150
    ''' <summary>
    ''' The data_ ITE m_ HEIGHT
    ''' </summary>
  Private Const Data_ITEM_HEIGHT As Integer = 20

    ''' <summary>
    ''' The data_ DAT e_ LEFT
    ''' </summary>
  Private Const Data_DATE_LEFT As Integer = (Data_ITEM_LEFT + Data_ITEM_WIDTH + 10)
    ''' <summary>
    ''' The data_ DAT e_ WIDTH
    ''' </summary>
  Private Const Data_DATE_WIDTH As Integer = 100
    ''' <summary>
    ''' The data_ DAT e_ HEIGHT
    ''' </summary>
  Private Const Data_DATE_HEIGHT As Integer = 20

    ''' <summary>
    ''' The data_ period LABE l_ LEFT
    ''' </summary>
  Private Const Data_PeriodLABEL_LEFT As Integer = (Data_DATE_LEFT + Data_DATE_WIDTH + 10)
    ''' <summary>
    ''' The data_ period LABE l_ WIDTH
    ''' </summary>
  Private Const Data_PeriodLABEL_WIDTH As Integer = 50
    ''' <summary>
    ''' The data_ period LABE l_ HEIGHT
    ''' </summary>
  Private Const Data_PeriodLABEL_HEIGHT As Integer = 20


    ''' <summary>
    ''' Class GridColumns
    ''' </summary>
  Private Class GridColumns
        ''' <summary>
        ''' The first col
        ''' </summary>
    Public Shared FirstCol As Integer = 0
        ''' <summary>
        ''' The item status
        ''' </summary>
    Public Shared ItemStatus As Integer = 1
        ''' <summary>
        ''' The date completed
        ''' </summary>
    Public Shared DateCompleted As Integer = 2
        ''' <summary>
        ''' The overdue
        ''' </summary>
    Public Shared Overdue As Integer = 3
        ''' <summary>
        ''' The group ID
        ''' </summary>
    Public Shared GroupID As Integer = 4
        ''' <summary>
        ''' The entity ID
        ''' </summary>
    Public Shared EntityID As Integer = 5
        ''' <summary>
        ''' The is data item
        ''' </summary>
    Public Shared IsDataItem As Integer = 6
        ''' <summary>
        ''' The data item type
        ''' </summary>
    Public Shared DataItemType As Integer = 7
        ''' <summary>
        ''' The item ID
        ''' </summary>
    Public Shared ItemID As Integer = 8
        ''' <summary>
        ''' The status ID
        ''' </summary>
    Public Shared StatusID As Integer = 9
        ''' <summary>
        ''' The counter
        ''' </summary>
    Public Shared Counter As Integer = 10
  End Class

    ''' <summary>
    ''' Init_s the grid columns class.
    ''' </summary>
  Private Sub Init_GridColumnsClass()
    GridColumns.FirstCol = Me.Grid_Transactions.Cols("FirstCol").SafeIndex
    GridColumns.ItemStatus = Me.Grid_Transactions.Cols("ItemStatus").SafeIndex
    GridColumns.DateCompleted = Me.Grid_Transactions.Cols("DateCompleted").SafeIndex
    GridColumns.Overdue = Me.Grid_Transactions.Cols("Overdue").SafeIndex
    GridColumns.GroupID = Me.Grid_Transactions.Cols("GroupID").SafeIndex
    GridColumns.EntityID = Me.Grid_Transactions.Cols("EntityID").SafeIndex
    GridColumns.ItemID = Me.Grid_Transactions.Cols("ItemID").SafeIndex
    GridColumns.StatusID = Me.Grid_Transactions.Cols("StatusID").SafeIndex
    GridColumns.Counter = Me.Grid_Transactions.Cols("Counter").SafeIndex
    GridColumns.IsDataItem = Me.Grid_Transactions.Cols("IsDataItem").SafeIndex
    GridColumns.DataItemType = Me.Grid_Transactions.Cols("DataItemType").SafeIndex

  End Sub

#End Region

#Region " Form 'Properties' "

    ''' <summary>
    ''' Gets the main form.
    ''' </summary>
    ''' <value>The main form.</value>
  Public ReadOnly Property MainForm() As FlorenceMain Implements Globals.StandardFlorenceForm.MainForm
    ' Public property to return handle to the 'Main' Venice form, where in reside most of the 
    ' data structures and many common utilities.
    Get
      Return _MainForm
    End Get
  End Property

    ''' <summary>
    ''' Gets or sets a value indicating whether this instance is over cancel button.
    ''' </summary>
    ''' <value><c>true</c> if this instance is over cancel button; otherwise, <c>false</c>.</value>
  Public Property IsOverCancelButton() As Boolean Implements Globals.StandardFlorenceForm.IsOverCancelButton
    ' Public property maintaining a value indicating if the cursor is over the 'Cancel'
    ' Button on this form.
    ' This property is specifically designed for use by the field formating Event functions
    ' In order that they do not impose format restrictions if the user is about to click the 
    ' 'Cancel' button.
    '
    Get
      Return __IsOverCancelButton
    End Get
    Set(ByVal Value As Boolean)
      __IsOverCancelButton = Value
    End Set
  End Property

    ''' <summary>
    ''' Gets a value indicating whether this instance is in paint.
    ''' </summary>
    ''' <value><c>true</c> if this instance is in paint; otherwise, <c>false</c>.</value>
  Public ReadOnly Property IsInPaint() As Boolean Implements Globals.StandardFlorenceForm.IsInPaint
    Get
      Return InPaint
    End Get
  End Property

    ''' <summary>
    ''' Gets a value indicating whether [in use].
    ''' </summary>
    ''' <value><c>true</c> if [in use]; otherwise, <c>false</c>.</value>
  Public ReadOnly Property InUse() As Boolean Implements Globals.StandardFlorenceForm.InUse
    Get
      Return _InUse
    End Get
  End Property

    ''' <summary>
    ''' Gets a value indicating whether [form open failed].
    ''' </summary>
    ''' <value><c>true</c> if [form open failed]; otherwise, <c>false</c>.</value>
  Public ReadOnly Property FormOpenFailed() As Boolean Implements Globals.StandardFlorenceForm.FormOpenFailed
    Get
      Return _FormOpenFailed
    End Get
  End Property

#End Region

    ''' <summary>
    ''' Initializes a new instance of the <see cref="frmUpdateDataItems"/> class.
    ''' </summary>
    ''' <param name="pMainForm">The p main form.</param>
  Public Sub New(ByVal pMainForm As FlorenceMain)
    ' *************************************************************
    ' Custom 'New'. 
    ' Passes in the reference to the parent form.
    ' 
    ' Establishes form specific variables.
    ' Establishes Form specific Data connection / data structures.
    '
    ' *************************************************************

    Me.New()


    _MainForm = pMainForm
    AddHandler _MainForm.FlorenceAutoUpdate, AddressOf Me.AutoUpdate

    _FormOpenFailed = False
    _InUse = True

    ' Default Select and Order fields.

    THIS_FORM_OrderBy = "ItemGroupID, EntityName, ItemDescription"

    ' Form Permissioning :-

    THIS_FORM_PermissionArea = "frmUpdateDataItems"
    THIS_FORM_PermissionType = RenaissanceGlobals.PermissionFeatureType.TypeForm

    ' 'This' form ID

    THIS_FORM_FormID = FlorenceFormID.frmUpdateDataItems

    ' This form's dataset type.

    ThisStandardDataset = RenaissanceStandardDatasets.tblFlorenceWorkItemsData  ' This Defines the Form Data !!! 


    ' Data object names standard to this Form type.

    THIS_TABLENAME = ThisStandardDataset.TableName
    THIS_ADAPTORNAME = ThisStandardDataset.Adaptorname
    THIS_DATASETNAME = ThisStandardDataset.DatasetName

    THIS_FORM_ChangeID = ThisStandardDataset.ChangeID

    ' Establish / Retrieve data objects for this form.

    myConnection = MainForm.MainDataHandler.Get_Connection(FLORENCE_CONNECTION)
    myAdaptor = MainForm.MainDataHandler.Get_Adaptor(THIS_ADAPTORNAME, FLORENCE_CONNECTION, THIS_TABLENAME)
    myDataset = MainForm.Load_Table(ThisStandardDataset, False)
    myTable = myDataset.Tables(0)

    ' Get DataValues Tables
    DataValueDataset = MainForm.Load_Table(RenaissanceStandardDatasets.tblFlorenceDataValues)
    DataValueAdaptor = MainForm.MainDataHandler.Get_Adaptor(RenaissanceStandardDatasets.tblFlorenceDataValues.Adaptorname)
    DataValueTable = CType(DataValueDataset, RenaissanceDataClass.DSFlorenceDataValues).tblFlorenceDataValues

    ' Report
    SetReportMenu()
    RootMenu.PerformLayout()

    ' Grid :- Grid_Transactions

    Grid_Transactions.Rows.Count = 1
    Grid_Transactions.Cols.Fixed = 0
    '    Grid_Transactions.ExtendLastCol = True

    'styles
    Dim cs As CellStyle = Grid_Transactions.Styles.Normal
    cs.Border.Direction = BorderDirEnum.Vertical
    cs.WordWrap = False

    cs = Grid_Transactions.Styles.Add("Data", Grid_Transactions.Styles.Normal)
    cs.ForeColor = Color.Black

    cs = Grid_Transactions.Styles.Add("HighlightOpen", Grid_Transactions.Styles.Normal)
    cs.ForeColor = Color.Blue

    cs = Grid_Transactions.Styles.Add("HighlightOverdue", Grid_Transactions.Styles.Normal)
    'cs.BackColor = SystemColors.Info
    cs.ForeColor = Color.SeaGreen

    'outline tree
    Grid_Transactions.Tree.Column = 0
    Grid_Transactions.Tree.Style = TreeStyleFlags.Simple
    Grid_Transactions.AllowMerging = AllowMergingEnum.Nodes

    'other
    Grid_Transactions.AllowResizing = AllowResizingEnum.Columns
    Grid_Transactions.SelectionMode = SelectionModeEnum.Cell

    Call Init_GridColumnsClass()

    ' Establish initial DataView and Initialise Transactions Grig.

    Try
      myDataView = New DataView(myTable, "True", THIS_FORM_OrderBy, DataViewRowState.CurrentRows)
      myDataView.Sort = THIS_FORM_OrderBy
    Catch ex As Exception

    End Try

    ' Initialise Field select area

    Call BuildFieldSelectCombos()
    Me.Combo_AndOr_1.SelectedIndex = 0
    Me.Combo_AndOr_2.SelectedIndex = 0

    ' Form Control Changed events
    AddHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

    AddHandler Combo_Group.SelectedValueChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_Entity.SelectedValueChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_Item.SelectedValueChanged, AddressOf Me.FormControlChanged

    AddHandler Combo_Select1_Operator.SelectedValueChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_Select1_Value.SelectedValueChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_Select1_Value.KeyUp, AddressOf Me.Combo_FieldSelect_KeyUp
    AddHandler Combo_Select2_Operator.SelectedValueChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_Select2_Value.SelectedValueChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_Select2_Value.KeyUp, AddressOf Me.Combo_FieldSelect_KeyUp
    AddHandler Combo_Select3_Operator.SelectedValueChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_Select3_Value.SelectedValueChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_Select3_Value.KeyUp, AddressOf Me.Combo_FieldSelect_KeyUp

    AddHandler Combo_AndOr_1.SelectedValueChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_AndOr_2.SelectedValueChanged, AddressOf Me.FormControlChanged

    AddHandler Check_OmitCompleted.CheckedChanged, AddressOf Me.FormControlChanged

    AddHandler Combo_Group.KeyUp, AddressOf MainForm.ComboSelectAsYouType
    AddHandler Combo_Entity.KeyUp, AddressOf MainForm.ComboSelectAsYouType
    AddHandler Combo_Item.KeyUp, AddressOf MainForm.ComboSelectAsYouType

    AddHandler Combo_Group.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
    AddHandler Combo_Entity.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
    AddHandler Combo_Item.GotFocus, AddressOf MainForm.GenericCombo_GotFocus

    AddHandler Combo_Group.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
    AddHandler Combo_Entity.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
    AddHandler Combo_Item.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged

    AddHandler Combo_Group.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
    AddHandler Combo_Entity.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
    AddHandler Combo_Item.LostFocus, AddressOf MainForm.GenericCombo_LostFocus

    ' Set up the ToolTip
    MainForm.SetFormToolTip(Me, FormTooltip)

    ' Position Labels
    Label_DataItem_Item.Left = Data_ITEM_LEFT
    Label_DataItem_Date.Left = Data_DATE_LEFT

	End Sub

#Region " This Form Event handlers : FormLoad / FormClose "

	' Form Initialisation code.
	'
    ''' <summary>
    ''' Resets the form.
    ''' </summary>
	Public Sub ResetForm() Implements StandardFlorenceForm.ResetForm
		THIS_FORM_OrderBy = "ItemGroupID, EntityName, ItemDescription"

		Call Form_Load(Me, New System.EventArgs)
	End Sub

    ''' <summary>
    ''' Closes the form.
    ''' </summary>
	Public Sub CloseForm() Implements StandardFlorenceForm.CloseForm
		ALWAYS_CLOSE_THIS_FORM = True
		Me.Close()
	End Sub

    ''' <summary>
    ''' Handles the Load event of the Form control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

		Me.MainForm.LogError(Me.Name, LOG_LEVELS.Audit, "", "Form Loaded", "", False)
		_FormOpenFailed = False
		_InUse = True

		' Initialise Data structures. Connection, Adaptor and Dataset.

		If Not (MainForm Is Nothing) Then
			FormIsValid = True
		Else
			MessageBox.Show("'Main' form is not set. Closing form", "no Main Form", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
			FormIsValid = False
			_FormOpenFailed = True
			Exit Sub
		End If

		If (myConnection Is Nothing) Then
			Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Database connection is not established. Closing form.", "", True)

			FormIsValid = False
			_FormOpenFailed = True
			Exit Sub
		End If

		If (myAdaptor Is Nothing) Then
			Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Data Adaptor is not established. Closing form.", "", True)

			FormIsValid = False
			_FormOpenFailed = True
			Exit Sub
		End If

		If (myDataset Is Nothing) Then
			Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Form Dataset is not established. Closing form.", "", True)

			FormIsValid = False
			_FormOpenFailed = True
			Exit Sub
		End If


		' Check User permissions
		Try
			Call CheckPermissions()
			If (HasReadPermission = False) Then
				Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User does not have Read permission for this Form..", "", True)

				FormIsValid = False
				_FormOpenFailed = True

				Exit Sub
			End If
		Catch ex As Exception
			FormIsValid = False
			_FormOpenFailed = True

			Exit Sub
		End Try

		' Initialse form

		InPaint = True
		IsOverCancelButton = False

		' Initialise main select controls
		' Build Sorted data list from which this form operates

		Try
			Call SetGroupCombo()
			Call SetEntityCombo()
			Call SetItemCombo()

			DT_ReferenceDate.Value = GetBestGuessDataDate(DealingPeriod.Monthly, Now.Date)

			If Combo_Group.Items.Count > 0 Then
				Combo_Group.SelectedIndex = 0
			End If
			If Combo_Entity.Items.Count > 0 Then
				Combo_Entity.SelectedIndex = 0
			End If
			If Combo_Item.Items.Count > 0 Then
				Combo_Item.SelectedIndex = 0
			End If

			Me.Combo_FieldSelect1.SelectedIndex = 0
			Me.Combo_FieldSelect2.SelectedIndex = 0
			Me.Combo_FieldSelect3.SelectedIndex = 0

			Me.Check_OmitCompleted.Checked = False
			Me.Check_OverdueOnly.Checked = False

			Call SetSortedRows()
			If (Grid_Transactions.Rows.Count > 0) Then
				Me.Grid_Transactions.Row = 0
			End If

			Call MainForm.SetComboSelectionLengths(Me)
		Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error initialising Select Status Form.", ex.StackTrace, True)
		End Try

		InPaint = False

	End Sub

    ''' <summary>
    ''' Handles the Closing event of the frm control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.ComponentModel.CancelEventArgs"/> instance containing the event data.</param>
	Private Sub frm_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
		Dim HideForm As Boolean

		' Hide or Close this form ?
		' All depends on how many of this form type are Open or in Cache...

		_InUse = False

		If (ALWAYS_CLOSE_THIS_FORM = True) Or (FormIsValid = False) Then
			HideForm = False
		Else
			HideForm = True
			If MainForm.FlorenceForms.CountOf(THIS_FORM_FormID) > MainForm.EntryForm_CacheCount Then
				HideForm = False
			End If
		End If

		If HideForm = True Then
			MainForm.HideInFormsCollection(Me)
			Me.Hide()	' NPP Fix

			e.Cancel = True
		Else
			Try
				MainForm.RemoveFromFormsCollection(Me)
				RemoveHandler _MainForm.FlorenceAutoUpdate, AddressOf Me.AutoUpdate

				RemoveHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

				RemoveHandler Combo_Group.SelectedValueChanged, AddressOf Me.FormControlChanged
				RemoveHandler Combo_Entity.SelectedValueChanged, AddressOf Me.FormControlChanged
				RemoveHandler Combo_Item.SelectedValueChanged, AddressOf Me.FormControlChanged

				RemoveHandler Combo_Select1_Operator.SelectedValueChanged, AddressOf Me.FormControlChanged
				RemoveHandler Combo_Select1_Value.SelectedValueChanged, AddressOf Me.FormControlChanged
				RemoveHandler Combo_Select1_Value.KeyUp, AddressOf Me.Combo_FieldSelect_KeyUp
				RemoveHandler Combo_Select2_Operator.SelectedValueChanged, AddressOf Me.FormControlChanged
				RemoveHandler Combo_Select2_Value.SelectedValueChanged, AddressOf Me.FormControlChanged
				RemoveHandler Combo_Select2_Value.KeyUp, AddressOf Me.Combo_FieldSelect_KeyUp
				RemoveHandler Combo_Select3_Operator.SelectedValueChanged, AddressOf Me.FormControlChanged
				RemoveHandler Combo_Select3_Value.SelectedValueChanged, AddressOf Me.FormControlChanged
				RemoveHandler Combo_Select3_Value.KeyUp, AddressOf Me.Combo_FieldSelect_KeyUp

				RemoveHandler Combo_AndOr_1.SelectedValueChanged, AddressOf Me.FormControlChanged
				RemoveHandler Combo_AndOr_2.SelectedValueChanged, AddressOf Me.FormControlChanged

				RemoveHandler Check_OmitCompleted.CheckedChanged, AddressOf Me.FormControlChanged

				RemoveHandler Combo_Group.KeyUp, AddressOf MainForm.ComboSelectAsYouType
				RemoveHandler Combo_Entity.KeyUp, AddressOf MainForm.ComboSelectAsYouType
				RemoveHandler Combo_Item.KeyUp, AddressOf MainForm.ComboSelectAsYouType

				RemoveHandler Combo_Group.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
				RemoveHandler Combo_Entity.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
				RemoveHandler Combo_Item.GotFocus, AddressOf MainForm.GenericCombo_GotFocus

				RemoveHandler Combo_Group.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
				RemoveHandler Combo_Entity.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
				RemoveHandler Combo_Item.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged

				RemoveHandler Combo_Group.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
				RemoveHandler Combo_Entity.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
				RemoveHandler Combo_Item.LostFocus, AddressOf MainForm.GenericCombo_LostFocus

			Catch ex As Exception
			End Try
		End If

	End Sub


#End Region

	' Routine to handle changes / updates to tables by this and other windows.
	' If this, or any other, form posts a change to a table, then it will invoke an update event 
	' detailing what tables have been altered.
	' Each Form will receive this event through this 'AutoUpdate' routine whice is configured to handle
	' the 'FlorenceAutoUpdate' event of the main Venice form.
	' Each form may them react as appropriate to changes in any table that might impact it.
	'
    ''' <summary>
    ''' Autoes the update.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="RenaissanceGlobals.RenaissanceUpdateEventArgs"/> instance containing the event data.</param>
	Private Sub AutoUpdate(ByVal sender As System.Object, ByVal e As RenaissanceGlobals.RenaissanceUpdateEventArgs)
		Dim OrgInPaint As Boolean
		Dim KnowledgeDateChanged As Boolean
		Dim RefreshGrid As Boolean

		If (Me.IsDisposed) OrElse (Me.Disposing) OrElse (Me.InUse = False) Then Exit Sub

		OrgInPaint = InPaint

		Try
			InPaint = True
			KnowledgeDateChanged = False
			RefreshGrid = False

			If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True) Or (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.Connection) = True) Then
				KnowledgeDateChanged = True
			End If
		Catch ex As Exception
		End Try

		' ****************************************************************
		' Check for changes relevant to this form
		' ****************************************************************


		' Changes to the tblPrice table :-
		Try
			If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblFlorenceGroup) = True) Or KnowledgeDateChanged Then
				ItemGroupDS = Nothing
				Call Me.SetGroupCombo()
			End If
		Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate : tblFlorenceGroup", ex.StackTrace, True)
		End Try

		' Changes to the tblFund table :-
		Try
			If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblFlorenceEntity) = True) Or KnowledgeDateChanged Then
				Call Me.SetEntityCombo()
			End If
		Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate : tblFlorenceEntity", ex.StackTrace, True)
		End Try

		' Changes to the tblFlorenceItems table :-
		Try
			If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblFlorenceItems) = True) Or KnowledgeDateChanged Then
				Call Me.SetItemCombo()
			End If
		Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate : tblFlorenceItems", ex.StackTrace, True)
		End Try

		' Changes to the tblFlorenceStatus table :-
		Try
			If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblFlorenceStatus) = True) Or KnowledgeDateChanged Then
				ItemStatusDS = Nothing
				If (Me.DataItemChanged = False) Then
					Call Me.Grid_Transactions_SelChange(Me, New EventArgs())
				End If
			End If
		Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate : tblFlorenceStatus", ex.StackTrace, True)
		End Try

		' Changes to the tblFlorenceDataValues table :-
		Try
			Dim ObjectCounter As Integer

			' Refresh Text Combos
			For ObjectCounter = 0 To (DataItemControlArray.Length - 1)
				If (TypeOf DataItemControlArray(ObjectCounter) Is TextBoxCombo) Then
					SetTextDataItemCombo(CType(DataItemControlArray(ObjectCounter), TextBoxCombo))
				End If
			Next

			If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblFlorenceDataValues) = True) Or KnowledgeDateChanged Then
				If (Me.DataItemChanged = False) Then
					InPaint = False	' Sel_Change() won't paint otherwise !
					Call Me.Grid_Transactions_SelChange(Me, New EventArgs())
					InPaint = True
				End If
			End If
		Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate : tblFlorenceDataValues", ex.StackTrace, True)
		End Try


		' Changes to the KnowledgeDate :-
		Try
			If e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True Then
				RefreshGrid = True
			End If
		Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate: KnowledgeDate", ex.StackTrace, True)
		End Try

		' Check TableUpdate against first Select Field
		Try
			If Me.Combo_FieldSelect1.SelectedIndex >= 0 Then
				Try

					' If the First FieldSelect combo has selected a Transaction Field which is related to
					' another table, as defined in tblReferentialIntegrity (e.g. TransactionCounterparty)
					' Then if that related table is updated, the FieldSelect-Values combo must be updated.

					' If ChangedID is ChangeID of related table then ,..

					If (e.TableChanged(GetFieldChangeID(Combo_FieldSelect1.SelectedItem)) = True) Or KnowledgeDateChanged Then
						Dim SelectedValue As Object
						Dim TextValue As String

						' Save current Value Combo Value.

						SelectedValue = Nothing
						TextValue = ""

						If Me.Combo_Select1_Value.SelectedIndex >= 0 Then
							SelectedValue = Me.Combo_Select1_Value.SelectedValue
						ElseIf (SelectedValue Is Nothing) Then
							TextValue = Me.Combo_Select1_Value.Text
						End If

						' Update FieldSelect-Values Combo.

						Call UpdateSelectedValueCombo(Me.Combo_FieldSelect1.SelectedItem, Me.Combo_Select1_Value)

						' Restore Saved Value.

						If (Not (SelectedValue Is Nothing)) Then
							Me.Combo_Select1_Value.SelectedValue = SelectedValue
						ElseIf TextValue.Length > 0 Then
							Me.Combo_Select1_Value.Text = TextValue
						End If
					End If
				Catch ex As Exception
				End Try
			End If
		Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate: First SelectField", ex.StackTrace, True)
		End Try

		' Check TableUpdate against second Select Field
		Try
			If Me.Combo_FieldSelect2.SelectedIndex >= 0 Then
				Try
					' GetFieldChangeID
					If (e.TableChanged(GetFieldChangeID(Combo_FieldSelect2.SelectedItem)) = True) Or KnowledgeDateChanged Then
						Dim SelectedValue As Object
						Dim TextValue As String

						SelectedValue = Nothing
						TextValue = ""

						If Me.Combo_Select2_Value.SelectedIndex >= 0 Then
							SelectedValue = Me.Combo_Select2_Value.SelectedValue
						ElseIf (SelectedValue Is Nothing) Then
							TextValue = Me.Combo_Select2_Value.Text
						End If

						Call UpdateSelectedValueCombo(Me.Combo_FieldSelect2.SelectedItem, Me.Combo_Select2_Value)

						If (Not (SelectedValue Is Nothing)) Then
							Me.Combo_Select2_Value.SelectedValue = SelectedValue
						ElseIf TextValue.Length > 0 Then
							Me.Combo_Select2_Value.Text = TextValue
						End If
					End If
				Catch ex As Exception
				End Try
			End If
		Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate: Second SelectField", ex.StackTrace, True)
		End Try

		' Check TableUpdate against third Select Field
		Try
			If Me.Combo_FieldSelect3.SelectedIndex >= 0 Then
				Try
					' GetFieldChangeID
					If (e.TableChanged(GetFieldChangeID(Combo_FieldSelect3.SelectedItem)) = True) Or KnowledgeDateChanged Then
						Dim SelectedValue As Object
						Dim TextValue As String

						SelectedValue = Nothing
						TextValue = ""

						If Me.Combo_Select3_Value.SelectedIndex >= 0 Then
							SelectedValue = Me.Combo_Select3_Value.SelectedValue
						ElseIf (SelectedValue Is Nothing) Then
							TextValue = Me.Combo_Select3_Value.Text
						End If

						Call UpdateSelectedValueCombo(Me.Combo_FieldSelect3.SelectedItem, Me.Combo_Select3_Value)

						If (Not (SelectedValue Is Nothing)) Then
							Me.Combo_Select3_Value.SelectedValue = SelectedValue
						ElseIf TextValue.Length > 0 Then
							Me.Combo_Select3_Value.Text = TextValue
						End If
					End If
				Catch ex As Exception
				End Try
			End If
		Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate: Third SelectField", ex.StackTrace, True)
		End Try


		' Changes to the tblUserPermissions table :-

		Try
			If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblUserPermissions) = True) Or KnowledgeDateChanged Then

				' Check ongoing permissions.

				Call CheckPermissions()
				If (HasReadPermission = False) Then
					Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User has lost Read permission for this Form.", "", True)

					FormIsValid = False
					InPaint = OrgInPaint
					Me.Close()
					Exit Sub
				End If

				RefreshGrid = True

			End If
		Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate: tblUserPermissions", ex.StackTrace, True)
		End Try


		' ****************************************************************
		' Changes to the Main FORM table :-
		' ****************************************************************

		Try
			If (e.TableChanged(THIS_FORM_ChangeID) = True) Or _
				 (RefreshGrid = True) Or _
				 KnowledgeDateChanged Then

				' Re-Set Controls etc.
				Call SetSortedRows(True)

			End If
		Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate: Main Form table", ex.StackTrace, True)
		End Try

		InPaint = OrgInPaint

	End Sub

#Region " Workhorse functions : SetSortedRows / CheckPermission / ControlsChanged / Menu Events "

    ''' <summary>
    ''' Gets the work items select string.
    ''' </summary>
    ''' <returns>System.String.</returns>
	Private Function GetWorkItemsSelectString() As String
		' *******************************************************************************
		' Build a Select String appropriate to the form status.
		' *******************************************************************************

		Dim SelectString As String
		Dim FieldSelectString As String

		SelectString = ""
		FieldSelectString = ""
		Me.StatusBar_SelectTransactions.Text = ""

		Try

			' Select on an Item Group...
			If (Me.Combo_Group.SelectedIndex > 0) Then
				SelectString = "(ItemGroupID=" & Combo_Group.SelectedValue.ToString & ")"
			End If

			' Select on Entity

			If (Me.Combo_Entity.SelectedIndex > 0) Then
				If (SelectString.Length > 0) Then
					SelectString &= " AND "
				End If
				SelectString &= "(EntityID=" & Combo_Entity.SelectedValue.ToString & ")"
			End If

			' Select on Item...

			If (Me.Combo_Item.SelectedIndex > 0) Then
				If (SelectString.Length > 0) Then
					SelectString &= " AND "
				End If

				SelectString &= "(ItemID=" & Combo_Item.SelectedValue.ToString & ")"
			End If

			If Check_OmitCompleted.Checked Then
				If (SelectString.Length > 0) Then
					SelectString &= " AND "
				End If

				SelectString &= "(ItemStatusID<>" & CInt(RenaissanceGlobals.FlorenceStatusIDs.Completed).ToString & ")"
			End If

			If Check_OverdueOnly.Checked Then
				If (SelectString.Length > 0) Then
					SelectString &= " AND "
				End If

				SelectString &= "(IsDateOverdue<>0)"
			End If

			' Field Select ...

			FieldSelectString = "("
			Dim WorkItemsDataset As DataSet
			Dim WorkItemsTable As DataTable
			Dim FieldName As String

			WorkItemsDataset = MainForm.Load_Table(RenaissanceStandardDatasets.tblFlorenceWorkItemsData, False)
			WorkItemsTable = myDataset.Tables(0)

			' Select Field One.

			Try
				If (Me.Combo_FieldSelect1.SelectedIndex > 0) AndAlso (Me.Combo_Select1_Operator.SelectedIndex > 0) And (Me.Combo_Select1_Value.Text.Length > 0) Then
					' Get Field Name, Add 'Transaction' backon if necessary.

					FieldName = Combo_FieldSelect1.SelectedItem

					If (WorkItemsTable.Columns(FieldName).DataType Is GetType(System.String)) Then

						' If there is a value Selected, Use it - else use the Combo Text.

						If (Not (Combo_Select1_Value.SelectedValue Is Nothing)) Then
							FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select1_Operator.Text & " '" & Combo_Select1_Value.SelectedValue.ToString & "')"
						Else
							FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select1_Operator.Text & " '" & Combo_Select1_Value.Text.ToString & "')"
						End If

					ElseIf (WorkItemsTable.Columns(FieldName).DataType Is GetType(System.DateTime)) Then

						' For Dates, If there is no 'Selected' value use the Combo text if it is a Valid date, else
						' Use the 'Selected' Value if it is a date.

						If (Me.Combo_Select1_Value.SelectedValue Is Nothing) AndAlso (IsDate(Me.Combo_Select1_Value.Text)) Then
							FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select1_Operator.SelectedItem.ToString & " #" & CDate(Combo_Select1_Value.Text).ToString(QUERY_SHORTDATEFORMAT) & "#)"
						ElseIf (Not (Me.Combo_Select1_Value.SelectedValue Is Nothing)) And IsDate(Combo_Select1_Value.SelectedValue) Then
							FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select1_Operator.SelectedItem.ToString & " #" & CDate(Combo_Select1_Value.SelectedValue).ToString(QUERY_SHORTDATEFORMAT) & "#)"
						Else
							FieldSelectString &= "(1)"
						End If

					Else
						' Now Deemed to be numeric, or at least not in need of quotes / delimeters...
						' If there is no 'Selected' value use the Combo text if it is a Valid number, else
						' Use the 'Selected' Value if it is a valid numeric.

						If (Not (Combo_Select1_Value.SelectedValue Is Nothing)) AndAlso (IsNumeric(Combo_Select1_Value.SelectedValue)) Then
							FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select1_Operator.SelectedItem.ToString & " " & Combo_Select1_Value.SelectedValue.ToString & ")"
						ElseIf IsNumeric(Combo_Select1_Value.Text) Then
							FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select1_Operator.SelectedItem.ToString & " " & Combo_Select1_Value.Text & ")"
						Else
							FieldSelectString &= "(1)"
						End If
					End If
				End If

			Catch ex As Exception
			End Try

			' Select Field Two.

			Try
				If (Me.Combo_FieldSelect2.SelectedIndex > 0) AndAlso (Me.Combo_Select2_Operator.SelectedIndex > 0) And (Me.Combo_Select2_Value.Text.Length > 0) Then
					FieldName = Combo_FieldSelect2.SelectedItem

					If (FieldSelectString.Length > 1) Then
						FieldSelectString &= " " & Me.Combo_AndOr_1.SelectedItem.ToString & " "
					End If

					If (WorkItemsTable.Columns(FieldName).DataType Is GetType(System.String)) Then

						If (Not (Combo_Select2_Value.SelectedValue Is Nothing)) Then
							FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select2_Operator.Text & " '" & Combo_Select2_Value.SelectedValue.ToString & "')"
						Else
							FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select2_Operator.Text & " '" & Combo_Select2_Value.Text.ToString & "')"
						End If

					ElseIf (WorkItemsTable.Columns(FieldName).DataType Is GetType(System.DateTime)) Then

						If (Me.Combo_Select2_Value.SelectedValue Is Nothing) AndAlso (IsDate(Me.Combo_Select2_Value.Text)) Then
							FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select2_Operator.SelectedItem.ToString & " #" & CDate(Combo_Select2_Value.Text).ToString(QUERY_SHORTDATEFORMAT) & "#)"
						ElseIf (Not (Me.Combo_Select2_Value.SelectedValue Is Nothing)) And IsDate(Combo_Select2_Value.SelectedValue) Then
							FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select2_Operator.SelectedItem.ToString & " #" & CDate(Combo_Select2_Value.SelectedValue).ToString(QUERY_SHORTDATEFORMAT) & "#)"
						Else
							FieldSelectString &= "(1)"
						End If

					Else
						If (Not (Combo_Select2_Value.SelectedValue Is Nothing)) AndAlso (IsNumeric(Combo_Select2_Value.SelectedValue)) Then
							FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select2_Operator.SelectedItem.ToString & " " & Combo_Select2_Value.SelectedValue.ToString & ")"
						ElseIf IsNumeric(Combo_Select2_Value.Text) Then
							FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select2_Operator.SelectedItem.ToString & " " & Combo_Select2_Value.Text & ")"
						Else
							FieldSelectString &= "(1)"
						End If
					End If
				End If

			Catch ex As Exception
			End Try


			' Select Field Three.

			Try
				If (Me.Combo_FieldSelect3.SelectedIndex > 0) AndAlso (Me.Combo_Select3_Operator.SelectedIndex > 0) And (Me.Combo_Select3_Value.Text.Length > 0) Then
					FieldName = Combo_FieldSelect3.SelectedItem

					If (FieldSelectString.Length > 1) Then
						FieldSelectString &= " " & Me.Combo_AndOr_2.SelectedItem.ToString & " "
					End If

					If (WorkItemsTable.Columns(FieldName).DataType Is GetType(System.String)) Then

						If (Not (Combo_Select3_Value.SelectedValue Is Nothing)) Then
							FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select3_Operator.Text & " '" & Combo_Select3_Value.SelectedValue.ToString & "')"
						Else
							FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select3_Operator.Text & " '" & Combo_Select3_Value.Text.ToString & "')"
						End If

					ElseIf (WorkItemsTable.Columns(FieldName).DataType Is GetType(System.DateTime)) Then

						If (Me.Combo_Select3_Value.SelectedValue Is Nothing) AndAlso (IsDate(Me.Combo_Select3_Value.Text)) Then
							FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select3_Operator.SelectedItem.ToString & " #" & CDate(Combo_Select3_Value.Text).ToString(QUERY_SHORTDATEFORMAT) & "#)"
						ElseIf (Not (Me.Combo_Select3_Value.SelectedValue Is Nothing)) And IsDate(Combo_Select3_Value.SelectedValue) Then
							FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select3_Operator.SelectedItem.ToString & " #" & CDate(Combo_Select3_Value.SelectedValue).ToString(QUERY_SHORTDATEFORMAT) & "#)"
						Else
							FieldSelectString &= "(1)"
						End If

					Else
						If (Not (Combo_Select3_Value.SelectedValue Is Nothing)) AndAlso (IsNumeric(Combo_Select3_Value.SelectedValue)) Then
							FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select3_Operator.SelectedItem.ToString & " " & Combo_Select3_Value.SelectedValue.ToString & ")"
						ElseIf IsNumeric(Combo_Select3_Value.Text) Then
							FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select3_Operator.SelectedItem.ToString & " " & Combo_Select3_Value.Text & ")"
						Else
							FieldSelectString &= "(1)"
						End If
					End If
				End If

			Catch ex As Exception
			End Try

			FieldSelectString &= ")"

			' If the FieldSelect string is used, then tag it on to the main select string.

			If FieldSelectString.Length > 2 Then
				If (SelectString.Length > 0) Then
					SelectString &= " AND "
				End If
				SelectString &= FieldSelectString
			End If

			If SelectString.Length <= 0 Then
				SelectString = "true"
			End If

		Catch ex As Exception
			SelectString = "true"
		End Try

		Return SelectString

	End Function

    ''' <summary>
    ''' Sets the sorted rows.
    ''' </summary>
    ''' <param name="ForceRefresh">if set to <c>true</c> [force refresh].</param>
	Private Sub SetSortedRows(Optional ByVal ForceRefresh As Boolean = False)
		' *******************************************************************************
		' Build a Select String appropriate to the form status and apply it to the DataView object.
		' *******************************************************************************

		Dim SelectString As String

		SelectString = GetWorkItemsSelectString()

		Try
			Call Me.GetFormData(-1, 0)

			If (Not (myDataView.RowFilter = SelectString)) Or (ForceRefresh = True) Then
				If ForceRefresh = True Then
					myDataView.RowFilter = "False"
				End If

				myDataView.RowFilter = SelectString
				PaintTransactionGrid()

			End If
		Catch ex As Exception
			SelectString = "true"
			myDataView.RowFilter = SelectString
		End Try

		Me.StatusBar_SelectTransactions.Text = "(" & myDataView.Count.ToString & " Records) " & SelectString

	End Sub

	' Check User permissions
    ''' <summary>
    ''' Checks the permissions.
    ''' </summary>
	Private Sub CheckPermissions()
		' *******************************************************************************
		'
		' *******************************************************************************

		Dim Permissions As Integer

		Permissions = MainForm.CheckPermissions(THIS_FORM_PermissionArea, THIS_FORM_PermissionType)

		HasReadPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermRead) > 0)
		HasUpdatePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermUpdate) > 0)
		HasInsertPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermInsert) > 0)
		HasDeletePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermDelete) > 0)

	End Sub

    ''' <summary>
    ''' Forms the control changed.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub FormControlChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
		' *******************************************************************************
		' In the event of one of the Form Controls changing, refresh the data grid.
		' *******************************************************************************

		If InPaint = False Then

			Call SetSortedRows()

		End If
	End Sub

    ''' <summary>
    ''' Datas the item changed event.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub DataItemChangedEvent(ByVal sender As System.Object, ByVal e As System.EventArgs)
		' *******************************************************************************
		' 
		' *******************************************************************************

		Try
			Dim ThisControl As Control
			ThisControl = CType(sender, Control)
			If ThisControl.Visible = False Then
				Exit Sub
			End If
			If InPaint = False Then
				If (Me.HasUpdatePermission) Or (Me.HasInsertPermission) And (MainForm.Main_Knowledgedate = KNOWLEDGEDATE_NOW) Then
					DataItemChanged = True
					Me.btnSave.Enabled = True
					Me.btnCancel.Enabled = True

					' Set Changed Flag.
					Dim ArrayIndex As Integer = (-1)
					If (TypeOf sender Is RenaissanceTagProperty) Then
						Dim ThisRenaissanceTagObject As RenaissanceTagProperty

						ThisRenaissanceTagObject = sender
						If (Not (ThisRenaissanceTagObject.RenaissanceTag Is Nothing)) AndAlso (IsNumeric(ThisRenaissanceTagObject.RenaissanceTag)) Then
							ArrayIndex = CInt(ThisRenaissanceTagObject.RenaissanceTag)
						End If
					Else
						If (Not (ThisControl.Tag Is Nothing)) AndAlso (IsNumeric(ThisControl.Tag)) Then
							ArrayIndex = CInt(ThisControl.Tag)
						End If
					End If
					If (ArrayIndex >= 0) AndAlso (DataValueChangedArray.Length > ArrayIndex) Then
						DataValueChangedArray(ArrayIndex) = True
					End If

				End If
			End If
		Catch ex As Exception
		End Try
	End Sub

    ''' <summary>
    ''' Handles the KeyUp event of the Combo_FieldSelect control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.Windows.Forms.KeyEventArgs"/> instance containing the event data.</param>
	Private Sub Combo_FieldSelect_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Combo_Select1_Value.KeyUp
		' *******************************************************************************
		' In the event of one of the Form Controls changing, refresh the data grid.
		' *******************************************************************************

		If InPaint = False Then

			Call SetSortedRows()

		End If
	End Sub

    ''' <summary>
    ''' Builds the field select combos.
    ''' </summary>
	Private Sub BuildFieldSelectCombos()
		' *********************************************************************************
		' Build the FieldSelect combo boxes.
		'
		' Field Names starting with 'Transaction' are modified to start with '.'
		' This is done in order to make the name more readable.
		'
		' *********************************************************************************

		Dim TransactionDataset As DataSet
		Dim TransactionTable As DataTable

		Dim ColumnName As String
		Dim thisColumn As DataColumn

		TransactionDataset = MainForm.Load_Table(RenaissanceStandardDatasets.tblFlorenceWorkItemsData, False)
		TransactionTable = TransactionDataset.Tables(0)

		Combo_FieldSelect1.Items.Clear()
		Combo_Select1_Value.Items.Clear()
		Combo_FieldSelect1.Items.Add("")

		Combo_FieldSelect2.Items.Clear()
		Combo_Select2_Value.Items.Clear()
		Combo_FieldSelect2.Items.Add("")

		Combo_FieldSelect3.Items.Clear()
		Combo_Select3_Value.Items.Clear()
		Combo_FieldSelect3.Items.Add("")

		For Each thisColumn In myTable.Columns
			Try
				ColumnName = TransactionTable.Columns(thisColumn.ColumnName).ColumnName

				Combo_FieldSelect1.Items.Add(ColumnName)
				Combo_FieldSelect2.Items.Add(ColumnName)
				Combo_FieldSelect3.Items.Add(ColumnName)

			Catch ex As Exception
			End Try
		Next

	End Sub

    ''' <summary>
    ''' Updates the selected value combo.
    ''' </summary>
    ''' <param name="pFieldName">Name of the p field.</param>
    ''' <param name="pValueCombo">The p value combo.</param>
	Private Sub UpdateSelectedValueCombo(ByVal pFieldName As String, ByRef pValueCombo As ComboBox)
		' *******************************************************************************
		' Build a Value Combo Box appropriate to the chosen DataTable Select Field.
		'
		' By default the combo will contain all existing values for the chosen field, however
		' If a relationship is defined for this table and field, then a Combo will be built
		' which reflects this relationship.
		' e.g. the tblReferentialIntegrity table defined a relationship between the
		' TransactionCounterparty field and the tblCounterparty.Counterparty field, thus if
		' the chosen Select field ti 'Counterparty.' then the Value combo will be built using
		' the CounterpartyID & CounterpartyName details from the tblCounterparty table.
		'
		' *******************************************************************************

		Dim tblRefIntegrity As RenaissanceDataClass.DSReferentialIntegrity.tblReferentialIntegrityDataTable
		Dim IntegrityRows() As RenaissanceDataClass.DSReferentialIntegrity.tblReferentialIntegrityRow
		Dim TransactionDataset As DataSet
		Dim TransactionTable As DataTable
		Dim SortOrder As Boolean

		SortOrder = True

		' Clear the Value Combo.

		Try
			pValueCombo.DataSource = Nothing
			pValueCombo.DisplayMember = ""
			pValueCombo.ValueMember = ""
			pValueCombo.Items.Clear()
		Catch ex As Exception
		End Try

		' By default, make the Combo a Standard Dropdown, so that non-list items can be entered.

		pValueCombo.DropDownStyle = ComboBoxStyle.DropDown

		' Exit if no FiledName is given (having cleared the combo).

		If (pFieldName.Length <= 0) Then Exit Sub

		' Get a handle to the Standard Transactions Table.
		' This is so that the field types can be determined and used.

		Try
			TransactionDataset = MainForm.Load_Table(ThisStandardDataset, False)
			TransactionTable = myDataset.Tables(0)
		Catch ex As Exception
			Exit Sub
		End Try

		' If the selected field is a Date then sort the selection combo in reverse order.

		If (TransactionTable.Columns(pFieldName).DataType Is GetType(System.DateTime)) Then
			SortOrder = False
		End If

		' Get a handle to the Referential Integrity table and the row matching this table and field.
		' this table defines a relationship between Transaction Table fields and other tables.
		' This information can be used to build more meaningfull Value Combos.

		Try
			tblRefIntegrity = CType(MainForm.Load_Table(RenaissanceStandardDatasets.tblReferentialIntegrity, False), RenaissanceDataClass.DSReferentialIntegrity).tblReferentialIntegrity
			IntegrityRows = tblRefIntegrity.Select("(FeedsTable LIKE 'tblFlorenceWorkItemsData') AND (FeedsField = '" & pFieldName & "')", "RN")
			If (IntegrityRows.Length <= 0) Then
				GoTo StandardExit
			End If

		Catch ex As Exception
			Exit Sub
		End Try

		If (IntegrityRows(0).IsDescriptionFieldNull) Then
			' No Linked Description Field

			GoTo StandardExit
		End If

		' OK, a referential record exists.
		' Determine the Table and Field Names to use to build the Value Combo.

		Dim TableName As String
		Dim TableField As String
		Dim DescriptionField As String
		Dim stdDS As StandardDataset
		Dim thisChangeID As RenaissanceChangeID

		Try

			TableName = IntegrityRows(0).TableName
			TableField = IntegrityRows(0).TableField
			DescriptionField = IntegrityRows(0).DescriptionField

			thisChangeID = CType(System.Enum.Parse(GetType(RenaissanceChangeID), TableName), RenaissanceChangeID)

			stdDS = RenaissanceStandardDatasets.GetStandardDataset(thisChangeID)

			' Build the appropriate Values Combo.

			Call MainForm.SetTblGenericCombo( _
			pValueCombo, _
			stdDS, _
			DescriptionField, _
			TableField, _
			"", False, SortOrder, True)		 ' 

			' For Referential Integrity generated combo's, make the Combo a 
			' DropDownList

			pValueCombo.DropDownStyle = ComboBoxStyle.DropDownList

		Catch ex As Exception
			GoTo StandardExit

		End Try

		Exit Sub

StandardExit:

		' Build the standard Combo, just use the values from the given field.

		Call MainForm.SetTblGenericCombo( _
		pValueCombo, _
		RenaissanceStandardDatasets.tblFlorenceWorkItemsData, _
		pFieldName, _
		pFieldName, _
		"", True, SortOrder)	 ' 

	End Sub

    ''' <summary>
    ''' Gets the field change ID.
    ''' </summary>
    ''' <param name="pFieldName">Name of the p field.</param>
    ''' <returns>RenaissanceChangeID.</returns>
	Private Function GetFieldChangeID(ByVal pFieldName As String) As RenaissanceChangeID
		' *******************************************************************************
		'
		'
		' *******************************************************************************

		Dim tblRefIntegrity As RenaissanceDataClass.DSReferentialIntegrity.tblReferentialIntegrityDataTable
		Dim IntegrityRows() As RenaissanceDataClass.DSReferentialIntegrity.tblReferentialIntegrityRow

		Try
			tblRefIntegrity = CType(MainForm.Load_Table(RenaissanceStandardDatasets.tblReferentialIntegrity, False), RenaissanceDataClass.DSReferentialIntegrity).tblReferentialIntegrity
			IntegrityRows = tblRefIntegrity.Select("(FeedsTable LIKE 'tblFlorenceWorkItemsData') AND (FeedsField = '" & pFieldName & "')", "RN")
			If (IntegrityRows.Length <= 0) Then
				Return RenaissanceChangeID.None
				Exit Function
			End If

		Catch ex As Exception
			Return RenaissanceChangeID.None
			Exit Function
		End Try


		If (IntegrityRows(0).IsDescriptionFieldNull) Then
			Return RenaissanceChangeID.None
			Exit Function
		End If

		' OK, a referential record exists.
		' Determine the Table and Field Names to use to build the Value Combo.

		Dim TableName As String
		Dim TableField As String
		Dim DescriptionField As String
		Dim thisChangeID As RenaissanceChangeID

		Try

			TableName = IntegrityRows(0).TableName
			TableField = IntegrityRows(0).TableField
			DescriptionField = IntegrityRows(0).DescriptionField

			thisChangeID = CType(System.Enum.Parse(GetType(RenaissanceChangeID), TableName), RenaissanceChangeID)

			Return thisChangeID
			Exit Function
		Catch ex As Exception
		End Try

		Return RenaissanceChangeID.None
		Exit Function

	End Function

    ''' <summary>
    ''' Handles the SelectedIndexChanged event of the Combo_FieldSelect1 control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Combo_FieldSelect1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_FieldSelect1.SelectedIndexChanged
		' ***************************************************************************************
		' Field Select Combo Chosen, Update the associated Values Combo.
		'
		' ***************************************************************************************

		Call UpdateSelectedValueCombo(Me.Combo_FieldSelect1.SelectedItem, Me.Combo_Select1_Value)
		Me.Combo_Select1_Operator.SelectedIndex = 1

	End Sub

    ''' <summary>
    ''' Handles the SelectedIndexChanged event of the Combo_FieldSelect2 control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Combo_FieldSelect2_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_FieldSelect2.SelectedIndexChanged
		' ***************************************************************************************
		' Field Select Combo Chosen, Update the associated Values Combo.
		'
		' ***************************************************************************************
		Call UpdateSelectedValueCombo(Me.Combo_FieldSelect2.SelectedItem, Me.Combo_Select2_Value)
		Me.Combo_Select2_Operator.SelectedIndex = 1
	End Sub

    ''' <summary>
    ''' Handles the SelectedIndexChanged event of the Combo_FieldSelect3 control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Combo_FieldSelect3_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_FieldSelect3.SelectedIndexChanged
		' ***************************************************************************************
		' Field Select Combo Chosen, Update the associated Values Combo.
		'
		' ***************************************************************************************
		Call UpdateSelectedValueCombo(Me.Combo_FieldSelect3.SelectedItem, Me.Combo_Select3_Value)
		Me.Combo_Select3_Operator.SelectedIndex = 1
	End Sub

    ''' <summary>
    ''' Handles the CheckedChanged event of the Check_OverdueOnly control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Check_OverdueOnly_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Check_OverdueOnly.CheckedChanged
		' *******************************************************************************
		'
		'
		' *******************************************************************************

		If InPaint = False Then
			Call SetSortedRows(True)
		End If
	End Sub

    ''' <summary>
    ''' Handles the CheckedChanged event of the Check_OmitCompleted control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Check_OmitCompleted_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Check_OmitCompleted.CheckStateChanged
		' *******************************************************************************
		'
		'
		' *******************************************************************************

		If InPaint = False Then
			Call SetSortedRows(True)
		End If
	End Sub

    ''' <summary>
    ''' Handles the ValueChanged event of the DT_ReferenceDate control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub DT_ReferenceDate_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DT_ReferenceDate.ValueChanged
		' *******************************************************************************
		'
		'
		' *******************************************************************************

		If (InPaint) Then
			Exit Sub
		End If

		Dim DateCounter As Integer
		For DateCounter = 0 To (DataItemDateArray.Length - 1)
			Try
				If (Not (DataItemDateArray(DateCounter) Is Nothing)) Then
					Dim ThisCombo As DateCombo
					Dim ThisWorkItem As DSFlorenceWorkItemsData.tblFlorenceWorkItemsDataRow

					ThisWorkItem = CType(WorkItemArray(DateCounter), DSFlorenceWorkItemsData.tblFlorenceWorkItemsDataRow)
					ThisCombo = CType(DataItemDateArray(DateCounter), DateCombo)
					If (Not (ThisWorkItem Is Nothing)) AndAlso (ThisCombo.Visible = True) Then
						ThisCombo.Text = FitDateToPeriod(ThisWorkItem.DataUpdatePeriod, (DT_ReferenceDate.Value.Date)).ToString(DISPLAYMEMBER_DATEFORMAT)
					End If
				End If
			Catch ex As Exception
			End Try
		Next

	End Sub

#End Region

#Region " Report Menu Code"

    ''' <summary>
    ''' Sets the report menu.
    ''' </summary>
	Private Sub SetReportMenu()
		' **********************************************************************
		' Standard routine to build the Report Form menu.
		' **********************************************************************

		Dim ReportMenuItem As New ToolStripMenuItem("&Reports")

		ReportMenuItem.DropDownItems.Add("&Compact Status Report", Nothing, AddressOf StatusReportMenuEvent)

		ReportMenuItem.DropDownItems.Add(New ToolStripSeparator)

		ReportMenuItem.DropDownItems.Add("File &Cover Report", Nothing, AddressOf FileCoverReportMenuEvent)
		ReportMenuItem.DropDownItems.Add("File &Spine Insert Report", Nothing, AddressOf FileSpineReportMenuEvent)
		ReportMenuItem.DropDownItems.Add("&Key Details Report", Nothing, AddressOf KeyDetailsReportMenuEvent)
		ReportMenuItem.DropDownItems.Add("&File Status Report", Nothing, AddressOf FileStatusReportMenuEvent)
		ReportMenuItem.DropDownItems.Add("&Sign Off Report", Nothing, AddressOf FileSignOffReportMenuEvent)

		RootMenu.Items.Add(ReportMenuItem)

	End Sub

    ''' <summary>
    ''' Statuses the report menu event.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Public Sub StatusReportMenuEvent(ByVal sender As Object, ByVal e As System.EventArgs)
		' **********************************************************************
		' Run the Status Report based on the Selection criteria on the 'Select' Form.
		'
		' **********************************************************************

		Dim SelectString As String
		Dim GroupID As Integer = 0
		Dim EntityID As Integer = 0


		Try

			If (Me.Combo_Group.SelectedIndex > 0) AndAlso (IsNumeric(Me.Combo_Group.SelectedValue) = True) Then
				GroupID = CInt(Combo_Group.SelectedValue)
			End If

			If (Me.Combo_Entity.SelectedIndex > 0) AndAlso (IsNumeric(Me.Combo_Entity.SelectedValue) = True) Then
				EntityID = CInt(Combo_Entity.SelectedValue)
			End If

			SelectString = GetWorkItemsSelectString()

			MainForm.MainReportHandler.StatusReport("rptFlorenceStatusReport", GroupID, EntityID, SelectString, SelectStatus_ProgressBar)

		Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error running the Compact Due Dilligence Status Report", ex.StackTrace, True)
		End Try


	End Sub

    ''' <summary>
    ''' Files the status report menu event.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Public Sub FileStatusReportMenuEvent(ByVal sender As Object, ByVal e As System.EventArgs)
		' **********************************************************************
		' Run the Status Report based on the Selection criteria on the 'Select' Form.
		'
		' **********************************************************************

		Dim SelectString As String
		Dim GroupID As Integer = 0
		Dim EntityID As Integer = 0


		Try

			If (Me.Combo_Group.SelectedIndex > 0) AndAlso (IsNumeric(Me.Combo_Group.SelectedValue) = True) Then
				GroupID = CInt(Combo_Group.SelectedValue)
			End If

			If (Me.Combo_Entity.SelectedIndex > 0) AndAlso (IsNumeric(Me.Combo_Entity.SelectedValue) = True) Then
				EntityID = CInt(Combo_Entity.SelectedValue)
			End If

			SelectString = GetWorkItemsSelectString()

			MainForm.MainReportHandler.StatusReport("rptFileStatusReport", GroupID, EntityID, SelectString, SelectStatus_ProgressBar)

		Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error running the Due Dilligence File Status Report", ex.StackTrace, True)
		End Try

	End Sub


    ''' <summary>
    ''' Files the cover report menu event.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Public Sub FileCoverReportMenuEvent(ByVal sender As Object, ByVal e As System.EventArgs)
		' **********************************************************************
		' 
		'
		' **********************************************************************

		Try
			MainForm.MainReportHandler.DisplayReport("rptCoverInsert", Me.myDataView, 0, Now.Date, MainForm.Main_Knowledgedate, SelectStatus_ProgressBar)
		Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error running the Cover Insert report", ex.StackTrace, True)
		End Try

	End Sub

    ''' <summary>
    ''' Files the spine report menu event.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Public Sub FileSpineReportMenuEvent(ByVal sender As Object, ByVal e As System.EventArgs)
		' **********************************************************************
		' 
		'
		' **********************************************************************

		Try
			MainForm.MainReportHandler.DisplayReport("rptSpineInsert", Me.myDataView, 0, Now.Date, MainForm.Main_Knowledgedate, SelectStatus_ProgressBar)
		Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error running the Spine Insert report", ex.StackTrace, True)
		End Try

	End Sub

    ''' <summary>
    ''' Files the sign off report menu event.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Public Sub FileSignOffReportMenuEvent(ByVal sender As Object, ByVal e As System.EventArgs)
		' **********************************************************************
		' FileSignOffReportMenuEvent
		' 
		'
		' **********************************************************************

		Try
			MainForm.MainReportHandler.DisplayReport("rptSignOffSheet", Me.myDataView, 0, Now.Date, MainForm.Main_Knowledgedate, SelectStatus_ProgressBar)
		Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error running the Sign-Off Sheet report", ex.StackTrace, True)
		End Try

	End Sub

    ''' <summary>
    ''' Keys the details report menu event.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Public Sub KeyDetailsReportMenuEvent(ByVal sender As Object, ByVal e As System.EventArgs)
		' **********************************************************************
		' KeyDetailsReportMenuEvent
		' 
		'
		' **********************************************************************
		Dim SelectString As String
		Dim EntityID As Integer = 0


		Try

			If (Me.Combo_Entity.SelectedIndex > 0) AndAlso (IsNumeric(Me.Combo_Entity.SelectedValue) = True) Then
				EntityID = CInt(Combo_Entity.SelectedValue)
			End If

			SelectString = "(KeyDetail<>0) AND " & GetWorkItemsSelectString()

			MainForm.MainReportHandler.KeyDetailsReport(EntityID, SelectString, SelectStatus_ProgressBar)

		Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error running the Due Dilligence Key Details Report", ex.StackTrace, True)
		End Try

	End Sub

#End Region

#Region " Grid Event and Drawing Code"

    ''' <summary>
    ''' Paints the transaction grid.
    ''' </summary>
	Private Sub PaintTransactionGrid()
		' **********************************************************************************
		' Routine to Paint (In Outline) the Transactions Grid.
		'
		' This Routine populates the grid with the appropriate heirarchy of Funds and Instruments.
		' To improve performance, the Transaction lines are not added, they are in-filled as 
		' Instrument lines are expanded.
		'
		' **********************************************************************************

		Dim Counter As Integer
		Dim thisRowView As DataRowView
		Dim thisRow As RenaissanceDataClass.DSFlorenceWorkItemsData.tblFlorenceWorkItemsDataRow

		Dim LastGroupID As Integer
		Dim LastEntityID As Integer
		'Dim thisGroupID As Integer

		Dim LastEntityLine As Integer
		Dim IncompleteWorkItems As Boolean = False
		Dim ItemsNeedUpdating As Boolean = False

		Dim RowToSelect As Integer = (-1)

		' FlorenceStatusIDs

		Try

			' Clear Grid, to start
			Grid_Transactions.Redraw = False

			Grid_Transactions.Rows.Count = 1

			' Add Header Line.
			Grid_Transactions.Rows.Add()
            Grid_Transactions(Grid_Transactions.Rows.Count - 1, GridColumns.FirstCol) = "Primonial AM"
			Grid_Transactions(Grid_Transactions.Rows.Count - 1, GridColumns.StatusID) = (-1)	' 
			Grid_Transactions(Grid_Transactions.Rows.Count - 1, GridColumns.GroupID) = 0
			Grid_Transactions(Grid_Transactions.Rows.Count - 1, GridColumns.EntityID) = 0
			Grid_Transactions(Grid_Transactions.Rows.Count - 1, GridColumns.ItemID) = 0
			Grid_Transactions(Grid_Transactions.Rows.Count - 1, GridColumns.IsDataItem) = 0
			Grid_Transactions(Grid_Transactions.Rows.Count - 1, GridColumns.DataItemType) = 0

			Grid_Transactions.Rows(Grid_Transactions.Rows.Count - 1).IsNode = True
			Grid_Transactions.Rows(Grid_Transactions.Rows.Count - 1).Node.Level = 0

		Catch ex As Exception
		End Try

		' Initialise tracking variables.
		LastEntityID = (-1)
		LastGroupID = (-1)

		LastEntityLine = (-1)

		InPaint = True

		Try

			' Loop through the selected data (DataView) adding Fund and Instrument rows as appropriate.
			For Counter = 0 To (myDataView.Count - 1)

				Try

					' Resolve Transaction Row.
					thisRowView = myDataView.Item(Counter)
					thisRow = thisRowView.Row

					'thisInstrumentID = thisRow.StatusID

					' Add a New Fund Row if this transaction represents the start of a new Group.
					If thisRow.ItemGroupID <> LastGroupID Then

						Grid_Transactions.Rows.Add()
						Grid_Transactions(Grid_Transactions.Rows.Count - 1, GridColumns.FirstCol) = GetGroupDescription(thisRow.ItemGroupID)
						Grid_Transactions.Rows(Grid_Transactions.Rows.Count - 1).IsNode = True
						Grid_Transactions.Rows(Grid_Transactions.Rows.Count - 1).Node.Level = 1

						Grid_Transactions(Grid_Transactions.Rows.Count - 1, GridColumns.ItemStatus) = ""
						Grid_Transactions(Grid_Transactions.Rows.Count - 1, GridColumns.GroupID) = thisRow.ItemGroupID		' 
						Grid_Transactions(Grid_Transactions.Rows.Count - 1, GridColumns.EntityID) = 0		' 
						Grid_Transactions(Grid_Transactions.Rows.Count - 1, GridColumns.ItemID) = 0
						Grid_Transactions(Grid_Transactions.Rows.Count - 1, GridColumns.IsDataItem) = 0
						Grid_Transactions(Grid_Transactions.Rows.Count - 1, GridColumns.DataItemType) = 0
						Grid_Transactions(Grid_Transactions.Rows.Count - 1, GridColumns.StatusID) = (-1)	' 
						Grid_Transactions(Grid_Transactions.Rows.Count - 1, GridColumns.Counter) = (-1)	 ' Null Counter, Dont Add lines when the 'Fund' line is expanded.

						'LastInstrument = (-1)
					End If

					' Add a New Fund Row if this transaction represents the start of a new Fund.
					If thisRow.EntityID <> LastEntityID Then
						If (LastEntityLine >= 0) Then
							If (IncompleteWorkItems = True) Then
								Grid_Transactions(LastEntityLine, GridColumns.ItemStatus) = "Open"
								Grid_Transactions.SetCellStyle(LastEntityLine, GridColumns.ItemStatus, Grid_Transactions.Styles("HighlightOpen"))
							ElseIf (ItemsNeedUpdating = True) Then
								Grid_Transactions(LastEntityLine, GridColumns.ItemStatus) = "Overdue"
								Grid_Transactions.SetCellStyle(LastEntityLine, GridColumns.ItemStatus, Grid_Transactions.Styles("HighlightOverdue"))
							End If
						End If

						Grid_Transactions.Rows.Add()
						Grid_Transactions(Grid_Transactions.Rows.Count - 1, GridColumns.FirstCol) = thisRow.EntityName.ToString
						Grid_Transactions.Rows(Grid_Transactions.Rows.Count - 1).IsNode = True
						Grid_Transactions.Rows(Grid_Transactions.Rows.Count - 1).Node.Level = 2

						If (thisRow.ItemDiscontinued <> 0) Then
							Grid_Transactions(Grid_Transactions.Rows.Count - 1, GridColumns.ItemStatus) = "Discontinued"
						ElseIf (thisRow.IsDateOverdue = 0) Then
							Grid_Transactions(Grid_Transactions.Rows.Count - 1, GridColumns.ItemStatus) = "OK"
						Else
							Grid_Transactions(Grid_Transactions.Rows.Count - 1, GridColumns.ItemStatus) = "Overdue"
						End If

						Grid_Transactions(Grid_Transactions.Rows.Count - 1, GridColumns.GroupID) = thisRow.ItemGroupID		' 
						Grid_Transactions(Grid_Transactions.Rows.Count - 1, GridColumns.EntityID) = thisRow.EntityID	 ' 
						Grid_Transactions(Grid_Transactions.Rows.Count - 1, GridColumns.ItemID) = 0
						Grid_Transactions(Grid_Transactions.Rows.Count - 1, GridColumns.IsDataItem) = thisRow.IsDataItem
						Grid_Transactions(Grid_Transactions.Rows.Count - 1, GridColumns.DataItemType) = thisRow.DataItemType
						Grid_Transactions(Grid_Transactions.Rows.Count - 1, GridColumns.StatusID) = (-1)	' 
						Grid_Transactions(Grid_Transactions.Rows.Count - 1, GridColumns.Counter) = Counter

						LastEntityLine = Grid_Transactions.Rows.Count - 1

						IncompleteWorkItems = False
						ItemsNeedUpdating = False

					End If

					LastGroupID = thisRow.ItemGroupID
					LastEntityID = thisRow.EntityID

					If (thisRow.DataValueDate <= Renaissance_BaseDate) Then
						IncompleteWorkItems = True
					ElseIf (thisRow.IsDateOverdue > 0) Then
						ItemsNeedUpdating = True
					End If

				Catch Inner_Ex As Exception

				End Try
			Next Counter

			' Update Final Instrument Position.

			If (LastEntityLine >= 0) Then
				If (IncompleteWorkItems = True) Then
					Grid_Transactions(LastEntityLine, GridColumns.ItemStatus) = "Open"
					Grid_Transactions.SetCellStyle(LastEntityLine, GridColumns.ItemStatus, Grid_Transactions.Styles("HighlightOpen"))
				ElseIf (ItemsNeedUpdating = True) Then
					Grid_Transactions(LastEntityLine, GridColumns.ItemStatus) = "Overdue"
					Grid_Transactions.SetCellStyle(LastEntityLine, GridColumns.ItemStatus, Grid_Transactions.Styles("HighlightOverdue"))
				End If
			End If

			' Collapse rows.
			' Leave uncolapsed the section last opened by the user.
			Dim LevelOneRowsToExpand As New ArrayList
			Dim LevelTwoRowsToExpand As New ArrayList
			Dim LevelCount As Integer

			For Counter = (Grid_Transactions.Rows.Count - 1) To 1 Step -1
				Try
					If Grid_Transactions.Rows(Counter).IsNode Then
						If Grid_Transactions.Rows(Counter).Node.Level > 0 Then
							If (CInt(Grid_Transactions(Counter, GridColumns.GroupID)) = LastSelectedGroup) AndAlso (Grid_Transactions.Rows(Counter).Node.Level = 1) Then
								' Expand Group.
								' Grid_Transactions.Rows(Counter).Node.Expanded = True
								LevelOneRowsToExpand.Add(Counter)
							ElseIf (CInt(Grid_Transactions(Counter, GridColumns.EntityID)) = LastSelectedEntity) AndAlso (Grid_Transactions.Rows(Counter).Node.Level = 2) Then
								' Select specific Entity
								' RowToSelect = Counter
								LevelTwoRowsToExpand.Add(Counter)
							Else
								' Else Collapse
								'Grid_Transactions.Rows(Counter).Node.Expanded = False
							End If
							Grid_Transactions.Rows(Counter).Node.Expanded = False

						End If
					End If
				Catch Inner_Ex As Exception
				End Try
			Next

			For LevelCount = 0 To (LevelOneRowsToExpand.Count - 1)
				Grid_Transactions.Rows(CInt(LevelOneRowsToExpand(LevelCount))).Node.Expanded = True
			Next

			For LevelCount = 0 To (LevelTwoRowsToExpand.Count - 1)
				RowToSelect = (CInt(LevelTwoRowsToExpand(LevelCount)))
			Next

			LevelOneRowsToExpand.Clear()
			LevelOneRowsToExpand = Nothing
			LevelTwoRowsToExpand.Clear()
			LevelTwoRowsToExpand = Nothing

		Catch ex As Exception

		Finally
			InPaint = False

		End Try

		' Select Row when InPaint is False, otherwise it tends not to re-draw correctly.
		If (RowToSelect >= 0) Then
			Grid_Transactions.Row = RowToSelect

			Dim NewTopRow As Integer
			NewTopRow = RowToSelect - 1
			If (NewTopRow < 0) Then NewTopRow = 0

			Grid_Transactions.TopRow = NewTopRow
		End If

		Grid_Transactions.Redraw = True
		Grid_Transactions.Refresh()

	End Sub

    ''' <summary>
    ''' Handles the BeforeCollapse event of the Grid_Transactions control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="C1.Win.C1FlexGrid.RowColEventArgs"/> instance containing the event data.</param>
	Private Sub Grid_Transactions_BeforeCollapse(ByVal sender As Object, ByVal e As C1.Win.C1FlexGrid.RowColEventArgs) Handles Grid_Transactions.BeforeCollapse
		' **********************************************************************************
		' Handle the expanding of Instrument sections.
		'
		'
		' **********************************************************************************

		'If (e.Row > 0) AndAlso (Grid_Transactions.Rows(e.Row).IsNode) Then
		'  If Not Grid_Transactions.Rows(e.Row).Node.Expanded Then
		'    If Grid_Transactions.Rows(e.Row).Node.Level = 1 Then
		'      ' For Level One Grid nodes, EntityID actually equals GroupID
		'      LastSelectedGroup = CInt(Grid_Transactions.Item(e.Row, GridColumns.EntityID))
		'    End If
		'  End If
		'End If


	End Sub

    ''' <summary>
    ''' Handles the AfterDragColumn event of the Grid_Transactions control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="C1.Win.C1FlexGrid.DragRowColEventArgs"/> instance containing the event data.</param>
	Private Sub Grid_Transactions_AfterDragColumn(ByVal sender As Object, ByVal e As C1.Win.C1FlexGrid.DragRowColEventArgs) Handles Grid_Transactions.AfterDragColumn
		' **********************************************************************************
		' Handle the Moving of Grid columns
		'
		' Simply re-set the values in the GridColumns class
		'
		' **********************************************************************************
		Call Init_GridColumnsClass()
	End Sub

    ''' <summary>
    ''' Handles the Click event of the Grid_Transactions control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Grid_Transactions_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Grid_Transactions.Click
		' ***************************************************************************************
		' 
		' 
		' ***************************************************************************************
		Dim GroupID As Integer = 0
		Dim EntityID As Integer = 0

		If (InPaint) Then
			Exit Sub
		End If

		Try
			If (Grid_Transactions.RowSel > 0) AndAlso (Me.Grid_Transactions.Focused) Then
				GroupID = Grid_Transactions(Grid_Transactions.RowSel, GridColumns.GroupID)
				EntityID = Grid_Transactions(Grid_Transactions.RowSel, GridColumns.EntityID)

				If (Grid_Transactions.Rows(Grid_Transactions.RowSel).Node.Level = 1) Then
					If (LastSelectedGroup <> GroupID) Then
						LastSelectedGroup = GroupID
						LastSelectedEntity = (-1)
					End If
				ElseIf (Grid_Transactions.Rows(Grid_Transactions.RowSel).Node.Level = 2) Then
					LastSelectedGroup = GroupID
					LastSelectedEntity = EntityID
				End If
			End If
		Catch ex As Exception
		End Try

		Call Grid_Transactions_SelChange(sender, e)
	End Sub

    ''' <summary>
    ''' Handles the SelChange event of the Grid_Transactions control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Grid_Transactions_SelChange(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Grid_Transactions.SelChange
		' ***************************************************************************************
		' 
		' 
		' ***************************************************************************************

		Dim EntityID As Integer = 0

		If (DataItemChanged) Then
			SetFormData()
		End If

		If (InPaint) Then
			Exit Sub
		End If

		Try
			If (Grid_Transactions.RowSel > 0) Then

				EntityID = Grid_Transactions(Grid_Transactions.RowSel, GridColumns.EntityID)

				GetFormData(EntityID, Grid_Transactions.RowSel)
			Else

			End If

		Catch ex As Exception
			GetFormData(-1, 0)
		End Try

	End Sub

    ''' <summary>
    ''' Handles the DoubleClick event of the Grid_Transactions control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Grid_Transactions_DoubleClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Grid_Transactions.DoubleClick

		' ***************************************************************************************
		' 
		' 
		' ***************************************************************************************

		If (Grid_Transactions.RowSel > 0) AndAlso (Grid_Transactions.Rows(Grid_Transactions.RowSel).IsNode) Then
			Grid_Transactions.Rows(Grid_Transactions.RowSel).Node.Expanded = (Not Grid_Transactions.Rows(Grid_Transactions.RowSel).Node.Expanded)
		End If

	End Sub

    ''' <summary>
    ''' Gets the group description.
    ''' </summary>
    ''' <param name="ItemGroupID">The item group ID.</param>
    ''' <returns>System.String.</returns>
	Private Function GetGroupDescription(ByVal ItemGroupID As Integer) As String
		' *******************************************************************************
		'
		'
		' *******************************************************************************

		Static LastGroupID As Integer = (-1)
		Static LastGroupDescription As String = ""

		Try
			If (ItemGroupID = LastGroupID) OrElse (ItemGroupID < 0) Then
				If (ItemGroupID < 0) Then	' Facilitate the explicit clearing of Static variables
					LastGroupID = (-1)
					LastGroupDescription = ""
				End If

				Return LastGroupDescription
			End If
		Catch ex As Exception
			Return ""
		End Try

		Try
			If (ItemGroupDS Is Nothing) Then
				ItemGroupDS = MainForm.Load_Table(RenaissanceStandardDatasets.tblFlorenceGroup)
			End If

			If (ItemGroupDS Is Nothing) Then
				LastGroupID = 0
				LastGroupDescription = ""
				Return ""
				Exit Function
			End If
		Catch ex As Exception
			LastGroupID = 0
			LastGroupDescription = ""
			Return ""
			Exit Function
		End Try

		Try
			Dim SelectedRows() As RenaissanceDataClass.DSFlorenceGroup.tblFlorenceGroupRow
			SelectedRows = ItemGroupDS.tblFlorenceGroup.Select("ItemGroup=" & ItemGroupID.ToString)

			If (SelectedRows.Length > 0) Then
				LastGroupID = ItemGroupID
				LastGroupDescription = SelectedRows(0).ItemGroupDescription
				Return LastGroupDescription
			End If
		Catch ex As Exception
		End Try

		LastGroupID = 0
		LastGroupDescription = ""
		Return ""

	End Function

    ''' <summary>
    ''' Gets the status ID description.
    ''' </summary>
    ''' <param name="ItemStatusID">The item status ID.</param>
    ''' <returns>System.String.</returns>
	Private Function GetStatusIDDescription(ByVal ItemStatusID As Integer) As String
		' *******************************************************************************
		'
		'
		' *******************************************************************************

		Static LastStatusID As Integer = (-1)
		Static LastStatusDescription As String = ""

		Try
			If (ItemStatusID = LastStatusID) OrElse (ItemStatusID < 0) Then
				If (ItemStatusID < 0) Then ' Facilitate the explicit clearing of Static variables
					LastStatusID = (-1)
					LastStatusDescription = ""
				End If

				Return LastStatusDescription
			End If
		Catch ex As Exception
			Return ""
		End Try

		Try
			If (ItemStatusIDsDS Is Nothing) Then
				ItemStatusIDsDS = MainForm.Load_Table(RenaissanceStandardDatasets.tblFlorenceStatusIDs)
			End If

			If (ItemStatusIDsDS Is Nothing) Then
				LastStatusID = 0
				LastStatusDescription = ""
				Return ""
				Exit Function
			End If
		Catch ex As Exception
			LastStatusID = 0
			LastStatusDescription = ""
			Return ""
			Exit Function
		End Try

		Try
			Dim SelectedRows() As RenaissanceDataClass.DSFlorenceStatusIDs.tblFlorenceStatusIDsRow
			SelectedRows = ItemStatusIDsDS.tblFlorenceStatusIDs.Select("ItemStatusID=" & ItemStatusID.ToString)

			If (SelectedRows.Length > 0) Then
				LastStatusID = ItemStatusID
				LastStatusDescription = SelectedRows(0).ItemStatusDescription
				Return LastStatusDescription
			End If
		Catch ex As Exception
		End Try

		LastStatusID = 0
		LastStatusDescription = ""
		Return ""

	End Function

    ''' <summary>
    ''' Gets the status ID is complete.
    ''' </summary>
    ''' <param name="ItemStatusID">The item status ID.</param>
    ''' <returns>System.Int32.</returns>
	Private Function GetStatusIDIsComplete(ByVal ItemStatusID As Integer) As Integer
		' *******************************************************************************
		'
		'
		' *******************************************************************************

		Static LastStatusID As Integer = (-1)
		Static LastStatusIsComplete As Integer = (-1)

		Try
			If (ItemStatusID = LastStatusID) OrElse (ItemStatusID < 0) Then
				If (ItemStatusID < 0) Then ' Facilitate the explicit clearing of Static variables
					LastStatusID = (-1)
					LastStatusIsComplete = 0
				End If

				Return LastStatusIsComplete
			End If
		Catch ex As Exception
			Return 0
		End Try

		Try
			If (ItemStatusIDsDS Is Nothing) Then
				ItemStatusIDsDS = MainForm.Load_Table(RenaissanceStandardDatasets.tblFlorenceStatusIDs)
			End If

			If (ItemStatusIDsDS Is Nothing) Then
				LastStatusID = 0
				LastStatusIsComplete = 0
				Return LastStatusIsComplete
				Exit Function
			End If
		Catch ex As Exception
			LastStatusID = 0
			LastStatusIsComplete = 0
			Return LastStatusIsComplete
			Exit Function
		End Try

		Try
			Dim SelectedRows() As RenaissanceDataClass.DSFlorenceStatusIDs.tblFlorenceStatusIDsRow
			SelectedRows = ItemStatusIDsDS.tblFlorenceStatusIDs.Select("ItemStatusID=" & ItemStatusID.ToString)

			If (SelectedRows.Length > 0) Then
				LastStatusID = ItemStatusID
				LastStatusIsComplete = SelectedRows(0).ItemIsComplete
				Return LastStatusIsComplete
			End If
		Catch ex As Exception
		End Try

		LastStatusID = 0
		LastStatusIsComplete = 0
		Return LastStatusIsComplete

	End Function

    ''' <summary>
    ''' Gets the form data.
    ''' </summary>
    ''' <param name="EntityID">The entity ID.</param>
    ''' <param name="RowNumber">The row number.</param>
    ''' <param name="FormRefresh">if set to <c>true</c> [form refresh].</param>
	Private Sub GetFormData(ByVal EntityID As Integer, ByVal RowNumber As Integer, Optional ByVal FormRefresh As Boolean = False)
		' *************************************************************************************
		' 
		' 
		' *************************************************************************************
		Dim OrgInPaint As Boolean = False
		Dim thisControl As Control

		OrgInPaint = InPaint
		InPaint = True

		Me.btnSave.Enabled = False
		Me.btnCancel.Enabled = False

		' Start by clearing the status arrays.
		' If this is a null entry then this needs to be done, otherwise it also needs to be done before they are re-populated.

		Try
			Dim ClearCounter As Integer

			For ClearCounter = 0 To (WorkItemArray.Length - 1)
				If (Not (WorkItemArray(ClearCounter) Is Nothing)) Then
					WorkItemArray(ClearCounter) = Nothing
				End If

				If (Not (DataValueArray(ClearCounter) Is Nothing)) Then
					DataValueArray(ClearCounter) = Nothing
				End If

				DataValueChangedArray(ClearCounter) = False
				DataLastDateArray(ClearCounter) = Renaissance_BaseDate
			Next
		Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error Clearing Work and Data Item arrays.", ex.StackTrace, True)
		End Try

		' If this is a 'Null' Entity, just blank the form. i.e. make the controls invisible.
    ' Entity will be <= 0 if this routine is called explicitly to clear the form.
    ' The GridColumns.Counter value will be < 0 for Summary lines on the Grid.

		If (EntityID <= 0) OrElse (Grid_Transactions(RowNumber, GridColumns.Counter) < 0) Then
			Try

				Try
					For Each thisControl In DataItemControlArray
						If (Not (thisControl Is Nothing)) Then
							thisControl.Visible = False
						End If
					Next
				Catch ex As Exception
					MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error Clearing Data Item Control array.", ex.StackTrace, True)
				End Try

				Try
					For Each thisControl In DataItemNameLabelArray
						If (Not (thisControl Is Nothing)) Then
							thisControl.Visible = False
						End If
					Next
				Catch ex As Exception
					MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error Clearing Data Name Label array.", ex.StackTrace, True)
				End Try

				Try
					For Each thisControl In DataItemPeriodLabelArray
						If (Not (thisControl Is Nothing)) Then
							thisControl.Visible = False
						End If
					Next
				Catch ex As Exception
					MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error Clearing Data Period Label array.", ex.StackTrace, True)
				End Try

				Try
					For Each thisControl In DataItemDateArray
						If (Not (thisControl Is Nothing)) Then
							thisControl.Visible = False
						End If
					Next
				Catch ex As Exception
					MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error Clearing Data Item Date array.", ex.StackTrace, True)
				End Try

			Catch ex As Exception
				MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error Clearing the Data Item Control arrays.", ex.StackTrace, True)
			Finally
				InPaint = OrgInPaint
			End Try

			Exit Sub
		End If


		Try

			' Now, Work through the Work Items relating to this Entity and add Data Item editing controls as necessary.

			Dim SelectedWorkItemRows() As DSFlorenceWorkItemsData.tblFlorenceWorkItemsDataRow
			Dim thisWorkItem As DSFlorenceWorkItemsData.tblFlorenceWorkItemsDataRow
			Dim WorkItemCounter As Integer

			Dim thisRowView As DataRowView
			Dim thisRow As RenaissanceDataClass.DSFlorenceWorkItemsData.tblFlorenceWorkItemsDataRow

			thisRowView = myDataView.Item(Grid_Transactions(RowNumber, GridColumns.Counter))
			thisRow = thisRowView.Row

			SelectedWorkItemRows = myTable.Select("(IsDataItem<>0) AND (EntityID=" & EntityID.ToString & ") AND " & GetWorkItemsSelectString(), "ItemDescription")

			Dim SelectedDataRows() As DSFlorenceDataValues.tblFlorenceDataValuesRow
			Dim thisDataRow As DSFlorenceDataValues.tblFlorenceDataValuesRow

			SelectedDataRows = DataValueTable.Select("DataEntityID=" & EntityID.ToString, "DataItemID")

			Dim FoundIt As Boolean = False
			Dim ReferenceDate As Date = Now.Date

			If (DT_ReferenceDate.Value.CompareTo(Renaissance_BaseDate) > 0) Then
				ReferenceDate = DT_ReferenceDate.Value.Date
			End If

			' Are there any existing  Work Items
			If (SelectedWorkItemRows.Length <= 0) Then
				GetFormData(-1, 0)
				Exit Sub
			End If

			' Loop through the Work Items, displaying the appropriate Controls and DataValue Items.

			WorkItemCounter = 0
			For Each thisWorkItem In SelectedWorkItemRows
				FoundIt = False
				thisDataRow = Nothing

				' Redim basic arrays if necessary
				Try
					If WorkItemArray.Length <= WorkItemCounter Then
						ReDim Preserve WorkItemArray(WorkItemArray.Length + 5)
						ReDim Preserve DataValueArray(WorkItemArray.Length + 5)
						ReDim Preserve DataValueChangedArray(WorkItemArray.Length + 5)
						ReDim Preserve DataLastDateArray(WorkItemArray.Length + 5)
					End If

					If DataItemDateArray.Length <= WorkItemCounter Then
						ReDim Preserve DataItemDateArray(DataItemDateArray.Length + 5)
					End If

					If DataItemNameLabelArray.Length <= WorkItemCounter Then
						ReDim Preserve DataItemNameLabelArray(DataItemNameLabelArray.Length + 5)
						ReDim Preserve DataItemPeriodLabelArray(DataItemNameLabelArray.Length + 5)
					End If

					If DataItemControlArray.Length <= WorkItemCounter Then
						ReDim Preserve DataItemControlArray(DataItemControlArray.Length + 5)
					End If
				Catch ex As Exception
					MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error Re-Dimensioning Control and Status arrays.", ex.StackTrace, True)
					GetFormData(-1, 0)
					Exit Sub
				End Try

				' 
				WorkItemArray(WorkItemCounter) = thisWorkItem
				DataValueArray(WorkItemCounter) = Nothing

				' Show / Initialise Date Element
				Try

					If (DataItemDateArray(WorkItemCounter) Is Nothing) Then
						DataItemDateArray(WorkItemCounter) = GenerateNewDateCombo(ReferenceDate, thisWorkItem.DataUpdatePeriod, WorkItemCounter)
						DataItemDateArray(WorkItemCounter).Text = FitDateToPeriod(thisWorkItem.DataUpdatePeriod, ReferenceDate)	'  GetBestGuessDataDate(thisWorkItem.DataUpdatePeriod, ReferenceDate).ToString(DISPLAYMEMBER_DATEFORMAT)
					Else
						CheckDateComboControl(DataItemDateArray(WorkItemCounter), ReferenceDate, thisWorkItem.DataUpdatePeriod, WorkItemCounter)
					End If

					If (FormRefresh) Then
						DataItemDateArray(WorkItemCounter).Text = GetBestGuessDataDate(thisWorkItem.DataUpdatePeriod, ReferenceDate).ToString(DISPLAYMEMBER_DATEFORMAT)
					End If
					DataLastDateArray(WorkItemCounter) = CDate(DataItemDateArray(WorkItemCounter).Text)

				Catch ex As Exception
					MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error Initialising Item Value Date.", ex.StackTrace, True)
					GetFormData(-1, 0)
					Exit Sub
				End Try

				' Get Existing DataValue if it exists.
				Try
					For Each thisDataRow In SelectedDataRows
						If (thisDataRow.DataItemID = thisWorkItem.ItemID) AndAlso (thisDataRow.DataValueDate = CDate(DataItemDateArray(WorkItemCounter).Text)) Then
							FoundIt = True
							DataValueArray(WorkItemCounter) = thisDataRow
							Exit For
						End If
					Next
				Catch ex As Exception
					MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error Searching for Existing Item Data.", ex.StackTrace, True)
					GetFormData(-1, 0)
					Exit Sub
				End Try

				' Place Controls for This Work Item

				' Label

				Try
					If (DataItemNameLabelArray(WorkItemCounter) Is Nothing) Then
						DataItemNameLabelArray(WorkItemCounter) = GenerateNewDataLabel(WorkItemCounter)
						DataItemPeriodLabelArray(WorkItemCounter) = GenerateNewPeriodLabel(WorkItemCounter)
					End If
					DataItemNameLabelArray(WorkItemCounter).Text = thisWorkItem.ItemDescription
					DataItemPeriodLabelArray(WorkItemCounter).Text = CType(thisWorkItem.DataUpdatePeriod, DealingPeriod).ToString
				Catch ex As Exception
					MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error setting Item Data Labels.", ex.StackTrace, True)
					GetFormData(-1, 0)
					Exit Sub
				End Try

				If (thisWorkItem.DataItemType And RenaissanceGlobals.RenaissanceDataType.TextType) Then
					Try

						' Remove existing Control (If necessary)
						If (Not (DataItemControlArray(WorkItemCounter) Is Nothing)) AndAlso (Not (TypeOf DataItemControlArray(WorkItemCounter) Is TextBoxCombo)) Then
							DisposeOfControl(DataItemControlArray(WorkItemCounter))
							DataItemControlArray(WorkItemCounter) = Nothing
						End If

						' Create new Control (if necessary)
						If (DataItemControlArray(WorkItemCounter) Is Nothing) OrElse (Not (TypeOf DataItemControlArray(WorkItemCounter) Is TextBoxCombo)) Then
							DataItemControlArray(WorkItemCounter) = Nothing
							DataItemControlArray(WorkItemCounter) = GenerateNewTextBoxCombo(WorkItemCounter)
							SetTextDataItemCombo(CType(DataItemControlArray(WorkItemCounter), TextBoxCombo))
						End If
						If (FoundIt) Then
							CType(DataItemControlArray(WorkItemCounter), TextBoxCombo).Text = thisDataRow.TextData
						Else
							CType(DataItemControlArray(WorkItemCounter), TextBoxCombo).Text = ""
						End If

					Catch ex As Exception
						MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error setting 'TextType' Item Data Value.", ex.StackTrace, True)
						GetFormData(-1, 0)
						Exit Sub
					End Try

				ElseIf (thisWorkItem.DataItemType And RenaissanceGlobals.RenaissanceDataType.DateType) Then
					Try

						' Remove existing Control (If necessary)
						If (Not (DataItemControlArray(WorkItemCounter) Is Nothing)) AndAlso (Not (TypeOf DataItemControlArray(WorkItemCounter) Is DateTimePicker)) Then
							DisposeOfControl(DataItemControlArray(WorkItemCounter))
							DataItemControlArray(WorkItemCounter) = Nothing
						End If
						' Create new Control (if necessary)
						If (DataItemControlArray(WorkItemCounter) Is Nothing) OrElse (Not (TypeOf DataItemControlArray(WorkItemCounter) Is DateTimePicker)) Then
							DataItemControlArray(WorkItemCounter) = Nothing
							DataItemControlArray(WorkItemCounter) = GenerateNewDataTimePicker(WorkItemCounter)
						End If
						If (FoundIt) Then
							If (thisDataRow.IsDateDataNull) Then
								CType(DataItemControlArray(WorkItemCounter), DateTimePicker).Value = Renaissance_BaseDateDATA
							Else
								CType(DataItemControlArray(WorkItemCounter), DateTimePicker).Value = thisDataRow.DateData
							End If
						Else
							CType(DataItemControlArray(WorkItemCounter), DateTimePicker).Value = Renaissance_BaseDateDATA
						End If

					Catch ex As Exception
						MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error setting 'DateType' Item Data Value.", ex.StackTrace, True)
						GetFormData(-1, 0)
						Exit Sub
					End Try

				ElseIf (thisWorkItem.DataItemType And RenaissanceGlobals.RenaissanceDataType.BooleanType) Then
					Try

						' Remove existing Control (If necessary)
						If (Not (DataItemControlArray(WorkItemCounter) Is Nothing)) AndAlso (Not (TypeOf DataItemControlArray(WorkItemCounter) Is CheckBox)) Then
							DisposeOfControl(DataItemControlArray(WorkItemCounter))
							DataItemControlArray(WorkItemCounter) = Nothing
						End If
						' Create new Control (if necessary)
						If (DataItemControlArray(WorkItemCounter) Is Nothing) OrElse (Not (TypeOf DataItemControlArray(WorkItemCounter) Is CheckBox)) Then
							DataItemControlArray(WorkItemCounter) = Nothing
							DataItemControlArray(WorkItemCounter) = GenerateNewDataCheckBox(WorkItemCounter)
						End If
						If (FoundIt) Then
							CType(DataItemControlArray(WorkItemCounter), CheckBox).Checked = CBool(thisDataRow.BooleanData)
						Else
							CType(DataItemControlArray(WorkItemCounter), CheckBox).Checked = False
						End If

					Catch ex As Exception
						MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error setting 'BooleanType' Item Data Value.", ex.StackTrace, True)
						GetFormData(-1, 0)
						Exit Sub
					End Try

				ElseIf (thisWorkItem.DataItemType And RenaissanceGlobals.RenaissanceDataType.NumericType) Then
					Try

						' Remove existing Control (If necessary)
						If (Not (DataItemControlArray(WorkItemCounter) Is Nothing)) AndAlso (Not (TypeOf DataItemControlArray(WorkItemCounter) Is NumericTextBox)) Then
							DisposeOfControl(DataItemControlArray(WorkItemCounter))
							DataItemControlArray(WorkItemCounter) = Nothing
						End If
						' Create new Control (if necessary)
						If (DataItemControlArray(WorkItemCounter) Is Nothing) OrElse (Not (TypeOf DataItemControlArray(WorkItemCounter) Is NumericTextBox)) Then
							DataItemControlArray(WorkItemCounter) = Nothing
							DataItemControlArray(WorkItemCounter) = GenerateNewNumericTextBox(WorkItemCounter)
						End If
						If (FoundIt) Then
							CType(DataItemControlArray(WorkItemCounter), NumericTextBox).Value = thisDataRow.NumericData
						Else
							CType(DataItemControlArray(WorkItemCounter), NumericTextBox).Value = 0
						End If

					Catch ex As Exception
						MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error setting 'NumericType' Item Data Value.", ex.StackTrace, True)
						GetFormData(-1, 0)
						Exit Sub
					End Try

				ElseIf (thisWorkItem.DataItemType And RenaissanceGlobals.RenaissanceDataType.PercentageType) Then
					Try

						' Remove existing Control (If necessary)
						If (Not (DataItemControlArray(WorkItemCounter) Is Nothing)) AndAlso (Not (TypeOf DataItemControlArray(WorkItemCounter) Is PercentageTextBox)) Then
							DisposeOfControl(DataItemControlArray(WorkItemCounter))
							DataItemControlArray(WorkItemCounter) = Nothing
						End If
						' Create new Control (if necessary)
						If (DataItemControlArray(WorkItemCounter) Is Nothing) OrElse (Not (TypeOf DataItemControlArray(WorkItemCounter) Is PercentageTextBox)) Then
							DataItemControlArray(WorkItemCounter) = Nothing
							DataItemControlArray(WorkItemCounter) = GenerateNewPercentageTextBox(WorkItemCounter)
						End If
						If (FoundIt) Then
							CType(DataItemControlArray(WorkItemCounter), PercentageTextBox).Value = thisDataRow.PercentageData
						Else
							CType(DataItemControlArray(WorkItemCounter), PercentageTextBox).Value = 0
						End If

					Catch ex As Exception
						MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error setting 'PercentageType' Item Data Value.", ex.StackTrace, True)
						GetFormData(-1, 0)
						Exit Sub
					End Try

				End If

				' Position Controls on the form.
				Try
					If (Not (DataItemNameLabelArray(WorkItemCounter) Is Nothing)) Then
						DataItemNameLabelArray(WorkItemCounter).Top = (Data_ROW_HEIGHT * WorkItemCounter) + Data_ROW_TOP
						DataItemNameLabelArray(WorkItemCounter).Left = Data_LABEL_LEFT
						DataItemNameLabelArray(WorkItemCounter).Width = Data_LABEL_WIDTH
						DataItemNameLabelArray(WorkItemCounter).Visible = True
					End If

					If (Not (DataItemPeriodLabelArray(WorkItemCounter) Is Nothing)) Then
						DataItemPeriodLabelArray(WorkItemCounter).Top = (Data_ROW_HEIGHT * WorkItemCounter) + Data_ROW_TOP
						DataItemPeriodLabelArray(WorkItemCounter).Left = Data_PeriodLABEL_LEFT
						DataItemPeriodLabelArray(WorkItemCounter).Width = Data_PeriodLABEL_WIDTH
						DataItemPeriodLabelArray(WorkItemCounter).Visible = True
					End If

					If (Not (DataItemControlArray(WorkItemCounter) Is Nothing)) Then
						DataItemControlArray(WorkItemCounter).Top = (Data_ROW_HEIGHT * WorkItemCounter) + Data_ROW_TOP
						DataItemControlArray(WorkItemCounter).Left = Data_ITEM_LEFT
						DataItemControlArray(WorkItemCounter).Width = Data_ITEM_WIDTH
						DataItemControlArray(WorkItemCounter).Visible = True
					End If

					If (Not (DataItemDateArray(WorkItemCounter) Is Nothing)) Then
						DataItemDateArray(WorkItemCounter).Top = (Data_ROW_HEIGHT * WorkItemCounter) + Data_ROW_TOP
						DataItemDateArray(WorkItemCounter).Left = Data_DATE_LEFT
						DataItemDateArray(WorkItemCounter).Width = Data_DATE_WIDTH
						DataItemDateArray(WorkItemCounter).Visible = True
					End If
				Catch ex As Exception
					MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error Placing controls.", ex.StackTrace, True)
					GetFormData(-1, 0)
					Exit Sub
				End Try

				WorkItemCounter += 1
			Next

			' Hide any Un-Used Controls.
			Try
				Dim ClearCounter As Integer

				For ClearCounter = WorkItemCounter To (DataItemNameLabelArray.Length - 1)
					Try
						If (Not (DataItemNameLabelArray(ClearCounter)) Is Nothing) Then
							DataItemNameLabelArray(ClearCounter).Visible = False
						End If
						If (Not (DataItemPeriodLabelArray(ClearCounter)) Is Nothing) Then
							DataItemPeriodLabelArray(ClearCounter).Visible = False
						End If

					Catch ex As Exception
						MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error Hiding Unused Labels.", ex.StackTrace, True)
						Exit For
					End Try
				Next
				For ClearCounter = WorkItemCounter To (DataItemControlArray.Length - 1)
					Try
						If (Not (DataItemControlArray(ClearCounter)) Is Nothing) Then
							DataItemControlArray(ClearCounter).Visible = False
						End If
					Catch ex As Exception
						MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error Hiding Unused DataItem Controls.", ex.StackTrace, True)
						Exit For
					End Try
				Next
				For ClearCounter = WorkItemCounter To (DataItemDateArray.Length - 1)
					Try
						If (Not (DataItemDateArray(ClearCounter)) Is Nothing) Then
							DataItemDateArray(ClearCounter).Visible = False
						End If
					Catch ex As Exception
						MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error Hiding Data Value Dates.", ex.StackTrace, True)
						Exit For
					End Try
				Next
			Catch ex As Exception
				MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error hiding unused controls.", ex.StackTrace, True)
			End Try

		Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error painting the 'Edit Data Item' form.", ex.StackTrace, True)
			GetFormData(-1, 0)
		Finally
			InPaint = OrgInPaint

			DataItemChanged = False

			btnCancel.Enabled = False
			btnSave.Enabled = False

		End Try


	End Sub

    ''' <summary>
    ''' Generates the new date combo.
    ''' </summary>
    ''' <param name="ReferenceDate">The reference date.</param>
    ''' <param name="UpdatePeriod">The update period.</param>
    ''' <param name="WorkItemCounter">The work item counter.</param>
    ''' <returns>DateCombo.</returns>
	Private Function GenerateNewDateCombo(ByVal ReferenceDate As Date, ByVal UpdatePeriod As DealingPeriod, ByVal WorkItemCounter As Integer) As DateCombo
		' ******************************************************************************
		'
		'
		'
		' ******************************************************************************
		Dim ThisCombo As New DateCombo

		ThisCombo.Name = "DataCombo" & WorkItemCounter.ToString
		ThisCombo.RenaissanceTag = WorkItemCounter
		ThisCombo.TabStop = True
		ThisCombo.TabIndex = (WorkItemCounter * 2) + 1
		ThisCombo.DropDownStyle = ComboBoxStyle.DropDown
		ThisCombo.FlatStyle = FlatStyle.System
		ThisCombo.Size = New Size(Data_DATE_WIDTH, Data_DATE_HEIGHT)

		AddHandler ThisCombo.SelectedValueChanged, AddressOf Combo_DataDate_SelectedValueChanged
		AddHandler ThisCombo.LostFocus, AddressOf Combo_DataDate_SelectedValueChanged
		AddHandler ThisCombo.TextChanged, AddressOf Combo_DataDate_SelectedValueChanged

		Me.Panel_DataEntry.Controls.Add(ThisCombo)

		CheckDateComboControl(ThisCombo, ReferenceDate, UpdatePeriod, WorkItemCounter)

		Return ThisCombo
	End Function

    ''' <summary>
    ''' Checks the date combo control.
    ''' </summary>
    ''' <param name="thisCombo">The this combo.</param>
    ''' <param name="ReferenceDate">The reference date.</param>
    ''' <param name="UpdatePeriod">The update period.</param>
    ''' <param name="WorkItemCounter">The work item counter.</param>
	Private Sub CheckDateComboControl(ByRef thisCombo As DateCombo, ByVal ReferenceDate As Date, ByVal UpdatePeriod As DealingPeriod, ByVal WorkItemCounter As Integer)
		' Build Combo Box with last Ten period dates
		Dim ReferenceCounter As Integer = 0
		Dim ThisDate As Date = FitDateToPeriod(UpdatePeriod, ReferenceDate, True)

		Try
			If (TypeOf thisCombo Is DateCombo) Then
				thisCombo.RenaissanceTag = WorkItemCounter

				For ReferenceCounter = 0 To 9
					Try
						If thisCombo.Items.Count > ReferenceCounter Then
							thisCombo.Items(ReferenceCounter) = ThisDate.ToString(DISPLAYMEMBER_DATEFORMAT)
						Else
							thisCombo.Items.Add(ThisDate.ToString(DISPLAYMEMBER_DATEFORMAT))
						End If

					Catch ex As Exception
					End Try

					ThisDate = AddPeriodToDate(UpdatePeriod, ThisDate, -1)
				Next

			End If
		Catch ex As Exception
		End Try
	End Sub

    ''' <summary>
    ''' Handles the SelectedValueChanged event of the Combo_DataDate control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Combo_DataDate_SelectedValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
		' ******************************************************************************
		'
		'
		' ******************************************************************************

		If (TypeOf sender Is DateCombo) Then
			Dim ThisCombo As DateCombo = CType(sender, DateCombo)
			Dim ThisTagIndex As Integer
			Dim ThisDate As Date
			Dim FittedDate As Date
			Dim ThisWorkItemRow As DSFlorenceWorkItemsData.tblFlorenceWorkItemsDataRow
			Dim thisDataValueRow As DSFlorenceDataValues.tblFlorenceDataValuesRow

			' Validate

			Try
				If (ThisCombo.Visible = False) OrElse (InPaint = True) Then
					Exit Sub
				End If

				If (IsNumeric(ThisCombo.RenaissanceTag) = False) Then
					Exit Sub
				End If
				ThisTagIndex = CInt(ThisCombo.RenaissanceTag)

				If (IsDate(ThisCombo.Text) = False) Then
					Exit Sub
				End If
				ThisDate = CDate(ThisCombo.Text)

				' Is the current date before the Base date (1 jan 1900) ?
				If ThisDate.CompareTo(Renaissance_BaseDate) <= 0 Then

					' If so, just blank the data value

					If (TypeOf DataItemControlArray(ThisTagIndex) Is NumericTextBox) Then
						CType(DataItemControlArray(ThisTagIndex), NumericTextBox).Value = 0

					ElseIf (TypeOf DataItemControlArray(ThisTagIndex) Is PercentageTextBox) Then
						CType(DataItemControlArray(ThisTagIndex), PercentageTextBox).Value = 0

					ElseIf (TypeOf DataItemControlArray(ThisTagIndex) Is TextBoxCombo) Then
						CType(DataItemControlArray(ThisTagIndex), TextBoxCombo).Text = ""

					ElseIf (TypeOf DataItemControlArray(ThisTagIndex) Is DateTimePicker) Then
						CType(DataItemControlArray(ThisTagIndex), DateTimePicker).Value = Renaissance_BaseDate

					ElseIf (TypeOf DataItemControlArray(ThisTagIndex) Is CheckBox) Then
						CType(DataItemControlArray(ThisTagIndex), CheckBox).Checked = False
					End If

					Exit Sub
				End If
			Catch ex As Exception
				Call MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error Validating Data Item in 'Combo_DataDate_SelectedValueChanged'", ex.StackTrace, True)
				Exit Sub
			End Try

			' Check Work Item and DataRow Value.
			Try

				If (WorkItemArray.Length <= ThisTagIndex) OrElse (WorkItemArray(ThisTagIndex) Is Nothing) Then
					Exit Sub
				End If
				ThisWorkItemRow = WorkItemArray(ThisTagIndex)
				thisDataValueRow = DataValueArray(ThisTagIndex)

			Catch ex As Exception
				Call MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error getting WorkItem in 'Combo_DataDate_SelectedValueChanged'", ex.StackTrace, True)
				Exit Sub
			End Try

			' Fit date to period

			FittedDate = FitDateToPeriod(ThisWorkItemRow.DataUpdatePeriod, ThisDate, True)
			If ThisDate.CompareTo(FittedDate) <> 0 Then
				ThisCombo.Text = FittedDate.ToString(DISPLAYMEMBER_DATEFORMAT)
				Exit Sub
			End If

			' Has the Date changed ?


			If (ThisDate = DataLastDateArray(ThisTagIndex)) Then
				Exit Sub
			End If

			' Has the Value Changed ?
			Try

				' It has become necessary 
				If (DataValueChangedArray(ThisTagIndex) = True) Then
					DataValueChangedArray(ThisTagIndex) = False
					If (MessageBox.Show("The Value for this Item has changed. Do you wish to Save All changed Items now ?" & vbCrLf & "Selecting 'No' will lose any change made to THIS item.", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.Yes) Then
						DataValueChangedArray(ThisTagIndex) = True
						Call SetFormData()
						DataLastDateArray(ThisTagIndex) = ThisDate
						Exit Sub
					End If

					DataValueChangedArray(ThisTagIndex) = False

					Dim AnyChanges As Boolean = False
					Dim CheckChangeCounter As Integer

					' Check to see if any other Items have changed.
					' If not, then re-set the Form changes flag and the button status'

					Try
						For CheckChangeCounter = 0 To (DataValueChangedArray.Length - 1)
							AnyChanges = AnyChanges And DataValueChangedArray(CheckChangeCounter)
						Next
						If (AnyChanges = False) Then
							DataItemChanged = False
							btnCancel.Enabled = False
							btnSave.Enabled = False
						End If
					Catch ex As Exception
					End Try
				End If

				DataLastDateArray(ThisTagIndex) = ThisDate
			Catch ex As Exception
				Call MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error checking Value-Changed in DataDate Changed event.", ex.StackTrace, True)
			End Try


			' Get Data

			Dim SelectedDataRows() As DSFlorenceDataValues.tblFlorenceDataValuesRow
			Dim thisDataRow As DSFlorenceDataValues.tblFlorenceDataValuesRow = Nothing

			Try
				If (Not (thisDataValueRow Is Nothing)) AndAlso (thisDataValueRow.DataEntityID = ThisWorkItemRow.EntityID) AndAlso (thisDataValueRow.DataItemID = ThisWorkItemRow.ItemID) AndAlso (thisDataValueRow.DataValueDate = ThisDate) Then
					thisDataRow = thisDataValueRow
				Else
					SelectedDataRows = DataValueTable.Select("(DataEntityID=" & ThisWorkItemRow.EntityID.ToString & ") AND (DataItemID=" & ThisWorkItemRow.ItemID.ToString & ")", "DataValueDate Desc")
					For Each thisDataRow In SelectedDataRows
						If thisDataRow.DataValueDate = ThisDate Then
							Exit For
						End If
					Next
				End If
			Catch ex As Exception
				Call MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error finding existing Data Item in 'Combo_DataDate_SelectedValueChanged'", ex.StackTrace, True)
				thisDataRow = Nothing
			End Try

			Dim OrgInPaint As Boolean
			OrgInPaint = InPaint
			Try
				InPaint = True

				If (Not (thisDataRow Is Nothing)) AndAlso (thisDataRow.DataValueDate = ThisDate) Then
					DataValueArray(ThisTagIndex) = thisDataRow

					If (TypeOf DataItemControlArray(ThisTagIndex) Is NumericTextBox) Then
						CType(DataItemControlArray(ThisTagIndex), NumericTextBox).Value = thisDataRow.NumericData

					ElseIf (TypeOf DataItemControlArray(ThisTagIndex) Is PercentageTextBox) Then
						CType(DataItemControlArray(ThisTagIndex), PercentageTextBox).Value = thisDataRow.PercentageData

					ElseIf (TypeOf DataItemControlArray(ThisTagIndex) Is TextBoxCombo) Then
						CType(DataItemControlArray(ThisTagIndex), TextBoxCombo).Text = thisDataRow.TextData

					ElseIf (TypeOf DataItemControlArray(ThisTagIndex) Is DateTimePicker) Then
						If (thisDataRow.IsDateDataNull) Then
							CType(DataItemControlArray(ThisTagIndex), DateTimePicker).Value = Renaissance_BaseDateDATA
						Else
							CType(DataItemControlArray(ThisTagIndex), DateTimePicker).Value = thisDataRow.DateData.Date
						End If

					ElseIf (TypeOf DataItemControlArray(ThisTagIndex) Is CheckBox) Then
						CType(DataItemControlArray(ThisTagIndex), CheckBox).Checked = thisDataRow.BooleanData

					End If
				Else
					If (TypeOf DataItemControlArray(ThisTagIndex) Is NumericTextBox) Then
						CType(DataItemControlArray(ThisTagIndex), NumericTextBox).Value = 0

					ElseIf (TypeOf DataItemControlArray(ThisTagIndex) Is PercentageTextBox) Then
						CType(DataItemControlArray(ThisTagIndex), PercentageTextBox).Value = 0

					ElseIf (TypeOf DataItemControlArray(ThisTagIndex) Is TextBoxCombo) Then
						CType(DataItemControlArray(ThisTagIndex), TextBoxCombo).Text = ""

					ElseIf (TypeOf DataItemControlArray(ThisTagIndex) Is DateTimePicker) Then
						CType(DataItemControlArray(ThisTagIndex), DateTimePicker).Value = Renaissance_BaseDateDATA

					ElseIf (TypeOf DataItemControlArray(ThisTagIndex) Is CheckBox) Then
						CType(DataItemControlArray(ThisTagIndex), CheckBox).Checked = False
					End If

				End If

			Catch ex As Exception
				Call MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error displaying Data Item in 'Combo_DataDate_SelectedValueChanged'", ex.StackTrace, True)
			Finally
				InPaint = OrgInPaint
			End Try

		End If

	End Sub


    ''' <summary>
    ''' Generates the new data label.
    ''' </summary>
    ''' <param name="WorkItemCounter">The work item counter.</param>
    ''' <returns>Label.</returns>
	Private Function GenerateNewDataLabel(ByVal WorkItemCounter As Integer) As Label
		' ******************************************************************************
		'
		'
		'
		' ******************************************************************************
		Dim ThisLabel As New Label

		ThisLabel.Name = "DataLabel" & WorkItemCounter.ToString
		ThisLabel.Tag = WorkItemCounter
		ThisLabel.TabStop = False
		ThisLabel.TabIndex = 9999
		ThisLabel.FlatStyle = FlatStyle.System
		ThisLabel.Size = New Size(Data_LABEL_WIDTH, Data_LABEL_HEIGHT)

		Me.Panel_DataEntry.Controls.Add(ThisLabel)

		Return ThisLabel
	End Function

    ''' <summary>
    ''' Generates the new period label.
    ''' </summary>
    ''' <param name="WorkItemCounter">The work item counter.</param>
    ''' <returns>Label.</returns>
	Private Function GenerateNewPeriodLabel(ByVal WorkItemCounter As Integer) As Label
		' ******************************************************************************
		'
		'
		'
		' ******************************************************************************
		Dim ThisLabel As New Label

		ThisLabel.Name = "DataPeriodLabel" & WorkItemCounter.ToString
		ThisLabel.Tag = WorkItemCounter
		ThisLabel.TabStop = False
		ThisLabel.TabIndex = 9999
		ThisLabel.FlatStyle = FlatStyle.System
		ThisLabel.Size = New Size(Data_PeriodLABEL_WIDTH, Data_PeriodLABEL_HEIGHT)

		Me.Panel_DataEntry.Controls.Add(ThisLabel)

		Return ThisLabel
	End Function

    ''' <summary>
    ''' Generates the new text box combo.
    ''' </summary>
    ''' <param name="WorkItemCounter">The work item counter.</param>
    ''' <returns>TextBoxCombo.</returns>
	Private Function GenerateNewTextBoxCombo(ByVal WorkItemCounter As Integer) As TextBoxCombo
		' ******************************************************************************
		'
		'
		'
		' ******************************************************************************
		Dim ThisTextBoxCombo As New TextBoxCombo

		ThisTextBoxCombo.Name = "DataTextBox" & WorkItemCounter.ToString
		ThisTextBoxCombo.RenaissanceTag = WorkItemCounter
		ThisTextBoxCombo.TabStop = True
		ThisTextBoxCombo.DropDownStyle = ComboBoxStyle.DropDown
		ThisTextBoxCombo.TabIndex = (WorkItemCounter * 2)
		ThisTextBoxCombo.Size = New Size(Data_ITEM_WIDTH, Data_ITEM_HEIGHT)

		AddHandler ThisTextBoxCombo.TextChanged, AddressOf Me.DataItemChangedEvent
		AddHandler ThisTextBoxCombo.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
		AddHandler ThisTextBoxCombo.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
		AddHandler ThisTextBoxCombo.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
		AddHandler ThisTextBoxCombo.KeyUp, AddressOf MainForm.ComboSelectAsYouType_NoMatch

		Me.Panel_DataEntry.Controls.Add(ThisTextBoxCombo)

		Return ThisTextBoxCombo
	End Function

    ''' <summary>
    ''' Generates the new data time picker.
    ''' </summary>
    ''' <param name="WorkItemCounter">The work item counter.</param>
    ''' <returns>DateTimePicker.</returns>
	Private Function GenerateNewDataTimePicker(ByVal WorkItemCounter As Integer) As DateTimePicker
		' ******************************************************************************
		'
		'
		'
		' ******************************************************************************
		Dim ThisDTPicker As New DateTimePicker

		ThisDTPicker.Name = "DataDateTimePicker" & WorkItemCounter.ToString
		ThisDTPicker.Tag = WorkItemCounter
		ThisDTPicker.TabStop = True
		ThisDTPicker.TabIndex = (WorkItemCounter * 2)
		ThisDTPicker.Size = New Size(Data_ITEM_WIDTH, Data_ITEM_HEIGHT)

		AddHandler ThisDTPicker.ValueChanged, AddressOf DataItemChangedEvent

		Me.Panel_DataEntry.Controls.Add(ThisDTPicker)

		Return ThisDTPicker
	End Function

    ''' <summary>
    ''' Generates the new data check box.
    ''' </summary>
    ''' <param name="WorkItemCounter">The work item counter.</param>
    ''' <returns>CheckBox.</returns>
	Private Function GenerateNewDataCheckBox(ByVal WorkItemCounter As Integer) As CheckBox
		' ******************************************************************************
		'
		'
		'
		' ******************************************************************************
		Dim ThisCheckBox As New CheckBox

		ThisCheckBox.Name = "DataCheckBox" & WorkItemCounter.ToString
		ThisCheckBox.Tag = WorkItemCounter
		ThisCheckBox.TabStop = True
		ThisCheckBox.TabIndex = (WorkItemCounter * 2)
		ThisCheckBox.Size = New Size(Data_ITEM_WIDTH, Data_ITEM_HEIGHT)
		ThisCheckBox.Text = ""
		ThisCheckBox.CheckAlign = ContentAlignment.MiddleLeft

		AddHandler ThisCheckBox.CheckedChanged, AddressOf DataItemChangedEvent

		Me.Panel_DataEntry.Controls.Add(ThisCheckBox)

		Return ThisCheckBox
	End Function

    ''' <summary>
    ''' Generates the new numeric text box.
    ''' </summary>
    ''' <param name="WorkItemCounter">The work item counter.</param>
    ''' <returns>NumericTextBox.</returns>
	Private Function GenerateNewNumericTextBox(ByVal WorkItemCounter As Integer) As NumericTextBox
		' ******************************************************************************
		'
		'
		'
		' ******************************************************************************
		Dim ThisNumericTextBox As New NumericTextBox

		ThisNumericTextBox.Name = "DataNumericTextBox" & WorkItemCounter.ToString
		ThisNumericTextBox.RenaissanceTag = WorkItemCounter
		ThisNumericTextBox.TabStop = True
		ThisNumericTextBox.TabIndex = (WorkItemCounter * 2)
		ThisNumericTextBox.Size = New Size(Data_ITEM_WIDTH, Data_ITEM_HEIGHT)
		ThisNumericTextBox.Value = 0
    ThisNumericTextBox.MaxLength = 30

		AddHandler ThisNumericTextBox.ValueChanged, AddressOf DataItemChangedEvent

		Me.Panel_DataEntry.Controls.Add(ThisNumericTextBox)

		Return ThisNumericTextBox
	End Function

    ''' <summary>
    ''' Generates the new percentage text box.
    ''' </summary>
    ''' <param name="WorkItemCounter">The work item counter.</param>
    ''' <returns>PercentageTextBox.</returns>
	Private Function GenerateNewPercentageTextBox(ByVal WorkItemCounter As Integer) As PercentageTextBox
		' ******************************************************************************
		'
		'
		'
		' ******************************************************************************
		Dim ThisPercentageTextBox As New PercentageTextBox

		ThisPercentageTextBox.Name = "DataPercentageTextBox" & WorkItemCounter.ToString
		ThisPercentageTextBox.RenaissanceTag = WorkItemCounter
		ThisPercentageTextBox.TabStop = True
		ThisPercentageTextBox.TabIndex = (WorkItemCounter * 2)
		ThisPercentageTextBox.Size = New Size(Data_ITEM_WIDTH, Data_ITEM_HEIGHT)
		ThisPercentageTextBox.Value = 0

		AddHandler ThisPercentageTextBox.ValueChanged, AddressOf DataItemChangedEvent

		Me.Panel_DataEntry.Controls.Add(ThisPercentageTextBox)

		Return ThisPercentageTextBox
	End Function

    ''' <summary>
    ''' Disposes the of control.
    ''' </summary>
    ''' <param name="thisControl">The this control.</param>
	Private Sub DisposeOfControl(ByVal thisControl As Object)
		' ******************************************************************************
		'
		' Attempt to dispose of a DataValue Conreol in an orderly fashion.
		'
		' ******************************************************************************

		Try
			If (thisControl Is Nothing) Then
				Exit Sub
			End If
		Catch ex As Exception
			Exit Sub
		End Try

		If (TypeOf thisControl Is NumericTextBox) Then ' GenerateNewNumericTextBox
			Dim ThisNumericBox As NumericTextBox

			Try
				ThisNumericBox = CType(thisControl, NumericTextBox)

				Try
					RemoveHandler ThisNumericBox.ValueChanged, AddressOf DataItemChangedEvent
				Catch ex As Exception
				End Try

				ThisNumericBox.Visible = False
				Me.Panel_DataEntry.Controls.Remove(ThisNumericBox)
			Catch ex As Exception
			End Try
			ThisNumericBox = Nothing

		ElseIf (TypeOf thisControl Is PercentageTextBox) Then	' GenerateNewPercentageTextBox
			Dim ThisPercentageBox As PercentageTextBox

			Try
				ThisPercentageBox = CType(thisControl, PercentageTextBox)

				Try
					RemoveHandler ThisPercentageBox.ValueChanged, AddressOf DataItemChangedEvent
				Catch ex As Exception
				End Try

				ThisPercentageBox.Visible = False
				Me.Panel_DataEntry.Controls.Remove(ThisPercentageBox)
			Catch ex As Exception
			End Try
			ThisPercentageBox = Nothing

		ElseIf (TypeOf thisControl Is TextBoxCombo) Then ' GenerateNewTextBox
			Dim ThisTextBoxCombo As TextBoxCombo

			Try
				ThisTextBoxCombo = CType(thisControl, TextBoxCombo)

				Try
					RemoveHandler ThisTextBoxCombo.TextChanged, AddressOf Me.DataItemChangedEvent
					RemoveHandler ThisTextBoxCombo.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
					RemoveHandler ThisTextBoxCombo.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
					RemoveHandler ThisTextBoxCombo.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
					RemoveHandler ThisTextBoxCombo.KeyUp, AddressOf MainForm.ComboSelectAsYouType_NoMatch
				Catch ex As Exception
				End Try

				ThisTextBoxCombo.Visible = False
				Me.Panel_DataEntry.Controls.Remove(ThisTextBoxCombo)
			Catch ex As Exception
			End Try
			ThisTextBoxCombo = Nothing

		ElseIf (TypeOf thisControl Is TextBox) Then	' GenerateNewTextBox
			Dim ThisBox As TextBox

			Try
				ThisBox = CType(thisControl, TextBox)

				Try
					RemoveHandler ThisBox.TextChanged, AddressOf DataItemChangedEvent
				Catch ex As Exception
				End Try

				ThisBox.Visible = False
				Me.Panel_DataEntry.Controls.Remove(ThisBox)
			Catch ex As Exception
			End Try
			ThisBox = Nothing

		ElseIf (TypeOf thisControl Is DateTimePicker) Then ' GenerateNewDataTimePicker
			Dim ThisDTPicker As DateTimePicker

			Try
				ThisDTPicker = CType(thisControl, DateTimePicker)

				Try
					RemoveHandler ThisDTPicker.ValueChanged, AddressOf DataItemChangedEvent
				Catch ex As Exception
				End Try

				ThisDTPicker.Visible = False
				Me.Panel_DataEntry.Controls.Remove(ThisDTPicker)
			Catch ex As Exception
			End Try
			ThisDTPicker = Nothing

		ElseIf (TypeOf thisControl Is CheckBox) Then ' GenerateNewDataCheckBox
			Dim ThisCheckBox As CheckBox

			Try
				ThisCheckBox = CType(thisControl, CheckBox)

				Try
					RemoveHandler ThisCheckBox.CheckedChanged, AddressOf DataItemChangedEvent
				Catch ex As Exception
				End Try

				ThisCheckBox.Visible = False
				Me.Panel_DataEntry.Controls.Remove(ThisCheckBox)
			Catch ex As Exception
			End Try
			ThisCheckBox = Nothing

		Else
			Try
				Dim UnknownControl As Control

				UnknownControl = CType(thisControl, Control)
				UnknownControl.Visible = False
				Me.Panel_DataEntry.Controls.Remove(UnknownControl)
				UnknownControl = Nothing

			Catch ex As Exception
			End Try
		End If

	End Sub

#End Region

#Region " Set Form Combos (Form Specific Code) "


    ''' <summary>
    ''' Sets the group combo.
    ''' </summary>
  Private Sub SetGroupCombo()

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_Group, _
    RenaissanceStandardDatasets.tblFlorenceGroup, _
    "ItemGroupDescription", _
    "ItemGroup", _
    "", False, True, True, 0)   ' 

  End Sub

    ''' <summary>
    ''' Sets the entity combo.
    ''' </summary>
  Private Sub SetEntityCombo()

    Dim SelectString As String = "(EntityIsInactive = 0)"

    If (Me.Combo_Group.SelectedIndex > 0) AndAlso (IsNumeric(Combo_Group.SelectedValue)) Then
      SelectString = "(EntityIsInactive = 0) AND ItemGroupID=" & Combo_Group.SelectedValue.ToString
    End If

		Call MainForm.SetTblGenericCombo( _
		Me.Combo_Entity, _
		RenaissanceStandardDatasets.tblFlorenceEntity, _
		"EntityName,ItemGroupID", _
		"EntityID", _
		SelectString, False, True, True, 0)		 ' 
  End Sub

    ''' <summary>
    ''' Sets the item combo.
    ''' </summary>
  Private Sub SetItemCombo()

		Call MainForm.SetTblGenericCombo( _
		Me.Combo_Item, _
		RenaissanceStandardDatasets.tblFlorenceItems, _
		"ItemDescription,ItemGroup", _
		"ItemID", _
		"", False, True, True, 0)		 ' 

  End Sub


    ''' <summary>
    ''' Sets the text data item combo.
    ''' </summary>
    ''' <param name="ThisTextBoxCombo">The this text box combo.</param>
  Private Sub SetTextDataItemCombo(ByVal ThisTextBoxCombo As TextBoxCombo)
    Dim ThisWorkID As Int32 = (-1)
    Dim ThisItemID As Int32 = (-1)
    Dim SelectString As String = "True"

    Try
      If (ThisTextBoxCombo.RenaissanceTag Is Nothing) OrElse (IsNumeric(ThisTextBoxCombo.RenaissanceTag) = False) Then
        Exit Sub
      End If
      ThisWorkID = CInt(ThisTextBoxCombo.RenaissanceTag)

      If (WorkItemArray.Length <= ThisWorkID) Then
        Exit Sub
      End If

      ThisItemID = WorkItemArray(ThisWorkID).ItemID

      If (ThisItemID <= 0) Then
        Exit Sub
      End If

      SelectString = "DataItemID=" & ThisItemID

      Call MainForm.SetTblGenericCombo( _
        ThisTextBoxCombo, _
        RenaissanceStandardDatasets.tblFlorenceDataValues, _
        "TextData", _
        "TextData", _
        SelectString, True, True, False, "", "")    ' 

    Catch ex As Exception
    End Try

  End Sub
#End Region

#Region " Transaction report Menu"






#End Region

#Region " Bug hunting "

  ' AddHandler myAdaptor.RowUpdating, AddressOf OnRowUpdating
  ' AddHandler myAdaptor.RowUpdated, AddressOf OnRowUpdated
  ' AddHandler myAdaptor.FillError, AddressOf OnRowFillError

    ''' <summary>
    ''' Called when [row updating].
    ''' </summary>
    ''' <param name="Sender">The sender.</param>
    ''' <param name="e">The <see cref="SqlRowUpdatingEventArgs"/> instance containing the event data.</param>
  Protected Shared Sub OnRowUpdating(ByVal Sender As Object, ByVal e As SqlRowUpdatingEventArgs)
    Dim a As Integer

    If Not e.Command Is Nothing Then
      a = 1
    End If

    If Not e.Errors Is Nothing Then
      a = 2
    End If
  End Sub

    ''' <summary>
    ''' Called when [row updated].
    ''' </summary>
    ''' <param name="Sender">The sender.</param>
    ''' <param name="e">The <see cref="SqlRowUpdatedEventArgs"/> instance containing the event data.</param>
  Protected Shared Sub OnRowUpdated(ByVal Sender As Object, ByVal e As SqlRowUpdatedEventArgs)
    Dim a As Integer

    If Not e.Command Is Nothing Then
      a = 1
    End If

    If Not e.Errors Is Nothing Then
      a = 2
    End If
  End Sub

    ''' <summary>
    ''' Called when [row fill error].
    ''' </summary>
    ''' <param name="Sender">The sender.</param>
    ''' <param name="e">The <see cref="FillErrorEventArgs"/> instance containing the event data.</param>
  Protected Shared Sub OnRowFillError(ByVal Sender As Object, ByVal e As FillErrorEventArgs)
    Dim a As Integer

    a = 1
  End Sub

#End Region

    ''' <summary>
    ''' Handles the SelectedIndexChanged event of the Combo_Group control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Combo_Group_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_Group.SelectedIndexChanged
    ' ****************************************************************************************
    '
    '
    '
    ' ****************************************************************************************

    SetEntityCombo()
  End Sub

    ''' <summary>
    ''' Handles the Click event of the btnCancel control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
    ' ****************************************************************************************
    '
    '
    '
    ' ****************************************************************************************

    GetFormData(LastSelectedEntity, Grid_Transactions.RowSel)
  End Sub

    ''' <summary>
    ''' Handles the Click event of the btnSave control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
    ' ****************************************************************************************
    '
    '
    '
    ' ****************************************************************************************

    Call SetFormData()

  End Sub

    ''' <summary>
    ''' Sets the form data.
    ''' </summary>
    ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
  Private Function SetFormData() As Boolean
    ' ****************************************************************************************
    '
    '
    '
    ' ****************************************************************************************

    Dim thisValueindex As Integer
    Dim ValueChanged As Boolean = False
    Dim AnyValueChanged As Boolean = False
    Dim AnyStatusUpdated As Boolean = False

    Dim thisDataValueRow As DSFlorenceDataValues.tblFlorenceDataValuesRow
    Dim ThisWorkItemRow As DSFlorenceWorkItemsData.tblFlorenceWorkItemsDataRow
    Dim ThisDate As Date

    Try
      ' DataItemControlArray
      For thisValueindex = 0 To (DataItemControlArray.Length - 1)
        ThisWorkItemRow = WorkItemArray(thisValueindex)

        If (DataValueChangedArray(thisValueindex) = True) AndAlso _
           (Not (ThisWorkItemRow Is Nothing)) AndAlso _
           (Not (DataItemControlArray(thisValueindex) Is Nothing) AndAlso _
           (CType(DataItemControlArray(thisValueindex), Control).Visible = True)) Then

          ThisDate = DataLastDateArray(thisValueindex)
          thisDataValueRow = DataValueArray(thisValueindex)
          DataValueChangedArray(thisValueindex) = False

          If (Not (thisDataValueRow Is Nothing)) AndAlso (thisDataValueRow.DataEntityID = ThisWorkItemRow.EntityID) AndAlso (thisDataValueRow.DataItemID = ThisWorkItemRow.ItemID) AndAlso (thisDataValueRow.DataValueDate = ThisDate) Then
            ValueChanged = False

            If (TypeOf DataItemControlArray(thisValueindex) Is NumericTextBox) Then
              If (Not (thisDataValueRow.NumericData = CType(DataItemControlArray(thisValueindex), NumericTextBox).Value)) Then
                thisDataValueRow.NumericData = CType(DataItemControlArray(thisValueindex), NumericTextBox).Value
                thisDataValueRow.DataValueType = (thisDataValueRow.DataValueType Or RenaissanceDataType.NumericType)
                ValueChanged = True
              End If

            ElseIf (TypeOf DataItemControlArray(thisValueindex) Is PercentageTextBox) Then
              If (Not (thisDataValueRow.PercentageData = (CType(DataItemControlArray(thisValueindex), PercentageTextBox).Value))) Then
                thisDataValueRow.PercentageData = (CType(DataItemControlArray(thisValueindex), PercentageTextBox).Value)
                thisDataValueRow.DataValueType = (thisDataValueRow.DataValueType Or RenaissanceDataType.PercentageType)
                ValueChanged = True
              End If

            ElseIf (TypeOf DataItemControlArray(thisValueindex) Is TextBoxCombo) Then
              If (Not (thisDataValueRow.TextData = CType(DataItemControlArray(thisValueindex), TextBoxCombo).Text)) Then
                thisDataValueRow.TextData = CType(DataItemControlArray(thisValueindex), TextBoxCombo).Text
                thisDataValueRow.DataValueType = (thisDataValueRow.DataValueType Or RenaissanceDataType.TextType)
                ValueChanged = True
              End If

            ElseIf (TypeOf DataItemControlArray(thisValueindex) Is DateTimePicker) Then
              If (thisDataValueRow.IsDateDataNull) OrElse (Not (thisDataValueRow.DateData = CType(DataItemControlArray(thisValueindex), DateTimePicker).Value)) Then
                thisDataValueRow.DateData = CType(DataItemControlArray(thisValueindex), DateTimePicker).Value
                thisDataValueRow.DataValueType = (thisDataValueRow.DataValueType Or RenaissanceDataType.DateType)
                ValueChanged = True
              End If

            ElseIf (TypeOf DataItemControlArray(thisValueindex) Is CheckBox) Then
              If (Not (thisDataValueRow.BooleanData = CType(DataItemControlArray(thisValueindex), CheckBox).Checked)) Then
                thisDataValueRow.BooleanData = CType(DataItemControlArray(thisValueindex), CheckBox).Checked
                thisDataValueRow.DataValueType = (thisDataValueRow.DataValueType Or RenaissanceDataType.BooleanType)
                ValueChanged = True
              End If
            End If

            If (ValueChanged) Then
              DataValueAdaptor.InsertCommand.Parameters("@Knowledgedate").Value = MainForm.Main_Knowledgedate
              DataValueAdaptor.UpdateCommand.Parameters("@Knowledgedate").Value = MainForm.Main_Knowledgedate

              MainForm.AdaptorUpdate(Me.Name, DataValueAdaptor, New DataRow() {thisDataValueRow})
              DataValueArray(thisValueindex) = thisDataValueRow
              AnyValueChanged = True
            End If

          Else
            ' New Row ?

            ' Find Existing Row ?
            Dim FoundIt As Boolean = False

            FoundIt = False
            thisDataValueRow = Nothing
            Try
              Dim SelectedDataRows() As DSFlorenceDataValues.tblFlorenceDataValuesRow

              SelectedDataRows = DataValueTable.Select("(DataEntityID=" & ThisWorkItemRow.EntityID.ToString & ") AND (DataItemID=" & ThisWorkItemRow.ItemID.ToString & ")", "DataValueDate DESC")

              For Each thisDataValueRow In SelectedDataRows
                If (thisDataValueRow.DataValueDate = ThisDate) Then
                  FoundIt = True
                  Exit For
                End If
              Next
            Catch ex As Exception
            End Try

            ValueChanged = False
            If (FoundIt = False) Then
              Try
                ValueChanged = True
                thisDataValueRow = DataValueTable.NewtblFlorenceDataValuesRow

                thisDataValueRow.DataValueID = 0
                thisDataValueRow.DataEntityID = ThisWorkItemRow.EntityID
                thisDataValueRow.DataItemID = ThisWorkItemRow.ItemID
                thisDataValueRow.DataValueDate = ThisDate
                thisDataValueRow.DataValueType = ThisWorkItemRow.DataItemType
                thisDataValueRow.TextData = ""
                thisDataValueRow.NumericData = 0
                thisDataValueRow.PercentageData = 0
                thisDataValueRow.SetDateDataNull()
                thisDataValueRow.BooleanData = False

                DataValueTable.Rows.Add(thisDataValueRow)

              Catch ex As Exception

              End Try
            End If

            If (TypeOf DataItemControlArray(thisValueindex) Is NumericTextBox) Then
              If (Not (thisDataValueRow.NumericData = CType(DataItemControlArray(thisValueindex), NumericTextBox).Value)) Then
                thisDataValueRow.NumericData = CType(DataItemControlArray(thisValueindex), NumericTextBox).Value
                thisDataValueRow.DataValueType = (thisDataValueRow.DataValueType Or RenaissanceDataType.NumericType)
                ValueChanged = True
              End If

            ElseIf (TypeOf DataItemControlArray(thisValueindex) Is PercentageTextBox) Then
              If (Not (thisDataValueRow.PercentageData = (CType(DataItemControlArray(thisValueindex), PercentageTextBox).Value))) Then
                thisDataValueRow.PercentageData = (CType(DataItemControlArray(thisValueindex), PercentageTextBox).Value)
                thisDataValueRow.DataValueType = (thisDataValueRow.DataValueType Or RenaissanceDataType.PercentageType)
                ValueChanged = True
              End If

            ElseIf (TypeOf DataItemControlArray(thisValueindex) Is TextBoxCombo) Then
              If (Not (thisDataValueRow.TextData = CType(DataItemControlArray(thisValueindex), TextBoxCombo).Text)) Then
                thisDataValueRow.TextData = CType(DataItemControlArray(thisValueindex), TextBoxCombo).Text
                thisDataValueRow.DataValueType = (thisDataValueRow.DataValueType Or RenaissanceDataType.TextType)
                ValueChanged = True
              End If

            ElseIf (TypeOf DataItemControlArray(thisValueindex) Is DateTimePicker) Then
              If (thisDataValueRow.IsDateDataNull) OrElse (Not (thisDataValueRow.DateData = CType(DataItemControlArray(thisValueindex), DateTimePicker).Value)) Then
                thisDataValueRow.DateData = CType(DataItemControlArray(thisValueindex), DateTimePicker).Value
                thisDataValueRow.DataValueType = (thisDataValueRow.DataValueType Or RenaissanceDataType.DateType)
                ValueChanged = True
              End If

            ElseIf (TypeOf DataItemControlArray(thisValueindex) Is CheckBox) Then
              If (Not (thisDataValueRow.BooleanData = CType(DataItemControlArray(thisValueindex), CheckBox).Checked)) Then
                thisDataValueRow.BooleanData = CType(DataItemControlArray(thisValueindex), CheckBox).Checked
                thisDataValueRow.DataValueType = (thisDataValueRow.DataValueType Or RenaissanceDataType.BooleanType)
                ValueChanged = True
              End If
            End If

            If (ValueChanged) Then
              Try
                DataValueAdaptor.InsertCommand.Parameters("@Knowledgedate").Value = MainForm.Main_Knowledgedate
                DataValueAdaptor.UpdateCommand.Parameters("@Knowledgedate").Value = MainForm.Main_Knowledgedate

                MainForm.AdaptorUpdate(Me.Name, DataValueAdaptor, New DataRow() {thisDataValueRow})
                DataValueArray(thisValueindex) = thisDataValueRow
                AnyValueChanged = True
              Catch ex As Exception
                MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error Saving Data Item.", ex.StackTrace, True)
              End Try
            End If

          End If


          If (ThisWorkItemRow.UpdateStatusOnDataUpdate = True) And (ValueChanged) Then
            Try
              ' Update Status Item.
              Dim StatusAdaptor As SqlDataAdapter
							Dim StatusDataset As RenaissanceDataClass.DSFlorenceStatus
							Dim StatusTable As RenaissanceDataClass.DSFlorenceStatus.tblFlorenceStatusDataTable
							Dim StatusRows() As RenaissanceDataClass.DSFlorenceStatus.tblFlorenceStatusRow
							Dim StatusRow As RenaissanceDataClass.DSFlorenceStatus.tblFlorenceStatusRow

              AnyStatusUpdated = True

              StatusAdaptor = MainForm.MainDataHandler.Get_Adaptor(RenaissanceStandardDatasets.tblFlorenceStatus.Adaptorname, FLORENCE_CONNECTION, RenaissanceStandardDatasets.tblFlorenceStatus.TableName)
              StatusDataset = MainForm.Load_Table(RenaissanceStandardDatasets.tblFlorenceStatus, False)
              StatusTable = StatusDataset.Tables(0)

              StatusRows = StatusTable.Select("(EntityID=" & ThisWorkItemRow.EntityID.ToString & ") AND (ItemID=" & ThisWorkItemRow.ItemID.ToString & ")")
              If (StatusRows.Length > 0) Then
                ' Edit Record
                StatusRow = StatusRows(0)
                StatusRow.ItemStatusID = RenaissanceGlobals.FlorenceStatusIDs.Completed
                StatusRow.DateCompleted = ThisDate

              Else
                ' Add Record
                StatusRow = StatusTable.NewtblFlorenceStatusRow
                StatusRow.EntityID = ThisWorkItemRow.EntityID
                StatusRow.ItemID = ThisWorkItemRow.ItemID
                StatusRow.ItemStatusID = RenaissanceGlobals.FlorenceStatusIDs.Completed
                StatusRow.DateCompleted = ThisDate
                StatusRow.Comment = ""
              End If

              If (StatusRow.RowState And DataRowState.Detached) = DataRowState.Detached Then
                Try
                  SyncLock StatusTable
                    StatusTable.Rows.Add(StatusRow)
                  End SyncLock
                Catch ex As Exception
                  MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error adding Item Status row to tblFlorenceStatus.", ex.StackTrace, True)
                End Try
              End If

              Try
                StatusAdaptor.InsertCommand.Parameters("@Knowledgedate").Value = MainForm.Main_Knowledgedate
                StatusAdaptor.UpdateCommand.Parameters("@Knowledgedate").Value = MainForm.Main_Knowledgedate

                Call MainForm.AdaptorUpdate(Me.Name, StatusAdaptor, New DSFlorenceStatus.tblFlorenceStatusRow() {StatusRow})
              Catch ex As Exception
                MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error Saving Item Status.", ex.StackTrace, True)
              End Try

            Catch ex As Exception
              MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error Automatically updating Status record relating to this data item.", ex.StackTrace, True)
            End Try

          End If
        End If
      Next
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in SetFormData().", ex.StackTrace, True)
    End Try

    ' Reset Form Changed variable.
    DataItemChanged = False

    Try
      ' Propogate changes.
      If (AnyStatusUpdated) Then
        Call MainForm.Main_RaiseEvent(New RenaissanceGlobals.RenaissanceUpdateEventArgs(RenaissanceChangeID.tblFlorenceStatus))
      End If
      If (AnyValueChanged) Then
        Call MainForm.Main_RaiseEvent(New RenaissanceGlobals.RenaissanceUpdateEventArgs(RenaissanceChangeID.tblFlorenceDataValues))
      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error Raising Update event.", ex.StackTrace, True)
    End Try

    Return True
  End Function



End Class
