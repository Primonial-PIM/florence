' ***********************************************************************
' Assembly         : Florence
' Author           : Nicholas Pennington
' Created          : 11-28-2012
'
' Last Modified By : acullig
' Last Modified On : 11-27-2012
' ***********************************************************************
' <copyright file="frmSelectStatusList.vb" company="">
'     Copyright (c) . All rights reserved.
' </copyright>
' <summary></summary>
' ***********************************************************************
Imports System.Data.SqlClient
Imports RenaissanceGlobals
Imports RenaissanceGlobals.Globals
Imports RenaissanceDataClass
Imports RenaissanceUtilities.DatePeriodFunctions
Imports C1.Win.C1FlexGrid

''' <summary>
''' Class frmSelectStatusList
''' </summary>
Public Class frmSelectStatusList

  Inherits System.Windows.Forms.Form
  Implements StandardFlorenceForm

#Region " Windows Form Designer generated code "

    ''' <summary>
    ''' Prevents a default instance of the <see cref="frmSelectStatusList"/> class from being created.
    ''' </summary>
  Private Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
    ''' <summary>
    ''' Disposes of the resources (other than memory) used by the <see cref="T:System.Windows.Forms.Form" />.
    ''' </summary>
    ''' <param name="disposing">true to release both managed and unmanaged resources; false to release only unmanaged resources.</param>
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
    ''' <summary>
    ''' The components
    ''' </summary>
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
    ''' <summary>
    ''' The combo_ group
    ''' </summary>
  Friend WithEvents Combo_Group As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The label_ cpty is fund
    ''' </summary>
  Friend WithEvents label_CptyIsFund As System.Windows.Forms.Label
    ''' <summary>
    ''' The combo_ entity
    ''' </summary>
  Friend WithEvents Combo_Entity As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The label2
    ''' </summary>
  Friend WithEvents Label2 As System.Windows.Forms.Label
    ''' <summary>
    ''' The combo_ field select1
    ''' </summary>
  Friend WithEvents Combo_FieldSelect1 As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The combo_ select1_ operator
    ''' </summary>
  Friend WithEvents Combo_Select1_Operator As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The combo_ select1_ value
    ''' </summary>
  Friend WithEvents Combo_Select1_Value As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The combo_ select2_ value
    ''' </summary>
  Friend WithEvents Combo_Select2_Value As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The combo_ field select2
    ''' </summary>
  Friend WithEvents Combo_FieldSelect2 As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The combo_ select2_ operator
    ''' </summary>
  Friend WithEvents Combo_Select2_Operator As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The combo_ select3_ value
    ''' </summary>
  Friend WithEvents Combo_Select3_Value As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The combo_ field select3
    ''' </summary>
  Friend WithEvents Combo_FieldSelect3 As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The combo_ select3_ operator
    ''' </summary>
  Friend WithEvents Combo_Select3_Operator As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The combo_ and or_1
    ''' </summary>
  Friend WithEvents Combo_AndOr_1 As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The combo_ and or_2
    ''' </summary>
  Friend WithEvents Combo_AndOr_2 As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The check_ omit completed
    ''' </summary>
  Friend WithEvents Check_OmitCompleted As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The grid_ status
    ''' </summary>
  Friend WithEvents Grid_Status As C1.Win.C1FlexGrid.C1FlexGrid
    ''' <summary>
    ''' The check_ overdue only
    ''' </summary>
  Friend WithEvents Check_OverdueOnly As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The combo_ item
    ''' </summary>
  Friend WithEvents Combo_Item As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The label1
    ''' </summary>
  Friend WithEvents Label1 As System.Windows.Forms.Label
    ''' <summary>
    ''' The combo_ item status
    ''' </summary>
  Friend WithEvents Combo_ItemStatus As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The label3
    ''' </summary>
  Friend WithEvents Label3 As System.Windows.Forms.Label
    ''' <summary>
    ''' The text_ item comment
    ''' </summary>
  Friend WithEvents Text_ItemComment As System.Windows.Forms.TextBox
    ''' <summary>
    ''' The label5
    ''' </summary>
  Friend WithEvents Label5 As System.Windows.Forms.Label
    ''' <summary>
    ''' The D t_ item date completed
    ''' </summary>
  Friend WithEvents DT_ItemDateCompleted As System.Windows.Forms.DateTimePicker
    ''' <summary>
    ''' The label4
    ''' </summary>
  Friend WithEvents Label4 As System.Windows.Forms.Label
    ''' <summary>
    ''' The BTN save
    ''' </summary>
  Friend WithEvents btnSave As System.Windows.Forms.Button
    ''' <summary>
    ''' The root menu
    ''' </summary>
  Friend WithEvents RootMenu As System.Windows.Forms.MenuStrip
    ''' <summary>
    ''' The text_ item item ID
    ''' </summary>
  Friend WithEvents Text_ItemItemID As System.Windows.Forms.TextBox
    ''' <summary>
    ''' The text_ item entity ID
    ''' </summary>
  Friend WithEvents Text_ItemEntityID As System.Windows.Forms.TextBox
    ''' <summary>
    ''' The text_ item status ID
    ''' </summary>
  Friend WithEvents Text_ItemStatusID As System.Windows.Forms.TextBox
    ''' <summary>
    ''' The label_ item name
    ''' </summary>
  Friend WithEvents Label_ItemName As System.Windows.Forms.Label
    ''' <summary>
    ''' The label6
    ''' </summary>
  Friend WithEvents Label6 As System.Windows.Forms.Label
    ''' <summary>
    ''' The label_ item period
    ''' </summary>
  Friend WithEvents Label_ItemPeriod As System.Windows.Forms.Label
    ''' <summary>
    ''' The label8
    ''' </summary>
  Friend WithEvents Label8 As System.Windows.Forms.Label
    ''' <summary>
    ''' The select status_ status strip
    ''' </summary>
  Friend WithEvents SelectStatus_StatusStrip As System.Windows.Forms.StatusStrip
    ''' <summary>
    ''' The status bar_ select transactions
    ''' </summary>
  Friend WithEvents StatusBar_SelectTransactions As System.Windows.Forms.ToolStripStatusLabel
    ''' <summary>
    ''' The select status_ progress bar
    ''' </summary>
  Friend WithEvents SelectStatus_ProgressBar As System.Windows.Forms.ToolStripProgressBar
    ''' <summary>
    ''' The BTN set date completed
    ''' </summary>
  Friend WithEvents btnSetDateCompleted As System.Windows.Forms.Button
    ''' <summary>
    ''' The label7
    ''' </summary>
  Friend WithEvents Label7 As System.Windows.Forms.Label
    ''' <summary>
    ''' The label_ data item type
    ''' </summary>
  Friend WithEvents Label_DataItemType As System.Windows.Forms.Label
    ''' <summary>
    ''' The text_ is data item
    ''' </summary>
  Friend WithEvents Text_IsDataItem As System.Windows.Forms.TextBox
    ''' <summary>
    ''' The split container1
    ''' </summary>
  Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
    ''' <summary>
    ''' The check_ data item
    ''' </summary>
  Friend WithEvents Check_DataItem As System.Windows.Forms.CheckBox
    ''' <summary>
    ''' The D t_ data item
    ''' </summary>
  Friend WithEvents DT_DataItem As System.Windows.Forms.DateTimePicker
    ''' <summary>
    ''' The numeric_ data item
    ''' </summary>
  Friend WithEvents Numeric_DataItem As C1.Win.C1Input.C1NumericEdit
    ''' <summary>
    ''' The text_ data item type
    ''' </summary>
  Friend WithEvents Text_DataItemType As System.Windows.Forms.TextBox
    ''' <summary>
    ''' The percentage_ data item
    ''' </summary>
  Friend WithEvents Percentage_DataItem As C1.Win.C1Input.C1NumericEdit
    ''' <summary>
    ''' The label9
    ''' </summary>
  Friend WithEvents Label9 As System.Windows.Forms.Label
    ''' <summary>
    ''' The D t_ date of data item
    ''' </summary>
  Friend WithEvents DT_DateOfDataItem As System.Windows.Forms.DateTimePicker
    ''' <summary>
    ''' The text_ data item
    ''' </summary>
  Friend WithEvents Text_DataItem As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The label_ item message
    ''' </summary>
  Friend WithEvents Label_ItemMessage As System.Windows.Forms.Label
    ''' <summary>
    ''' The BTN cancel
    ''' </summary>
  Friend WithEvents btnCancel As System.Windows.Forms.Button
    ''' <summary>
    ''' Initializes the component.
    ''' </summary>
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmSelectStatusList))
    Me.Combo_Group = New System.Windows.Forms.ComboBox
    Me.label_CptyIsFund = New System.Windows.Forms.Label
    Me.Combo_Entity = New System.Windows.Forms.ComboBox
    Me.Label2 = New System.Windows.Forms.Label
    Me.Combo_Select1_Operator = New System.Windows.Forms.ComboBox
    Me.Combo_FieldSelect1 = New System.Windows.Forms.ComboBox
    Me.Combo_Select1_Value = New System.Windows.Forms.ComboBox
    Me.Combo_Select2_Value = New System.Windows.Forms.ComboBox
    Me.Combo_FieldSelect2 = New System.Windows.Forms.ComboBox
    Me.Combo_Select2_Operator = New System.Windows.Forms.ComboBox
    Me.Combo_Select3_Value = New System.Windows.Forms.ComboBox
    Me.Combo_FieldSelect3 = New System.Windows.Forms.ComboBox
    Me.Combo_Select3_Operator = New System.Windows.Forms.ComboBox
    Me.Combo_AndOr_1 = New System.Windows.Forms.ComboBox
    Me.Combo_AndOr_2 = New System.Windows.Forms.ComboBox
    Me.Check_OmitCompleted = New System.Windows.Forms.CheckBox
    Me.Grid_Status = New C1.Win.C1FlexGrid.C1FlexGrid
    Me.Check_OverdueOnly = New System.Windows.Forms.CheckBox
    Me.Combo_Item = New System.Windows.Forms.ComboBox
    Me.Label1 = New System.Windows.Forms.Label
    Me.Text_IsDataItem = New System.Windows.Forms.TextBox
    Me.Label_DataItemType = New System.Windows.Forms.Label
    Me.Label7 = New System.Windows.Forms.Label
    Me.btnSetDateCompleted = New System.Windows.Forms.Button
    Me.Label_ItemPeriod = New System.Windows.Forms.Label
    Me.Label8 = New System.Windows.Forms.Label
    Me.Label_ItemName = New System.Windows.Forms.Label
    Me.Label6 = New System.Windows.Forms.Label
    Me.Text_ItemStatusID = New System.Windows.Forms.TextBox
    Me.Text_ItemItemID = New System.Windows.Forms.TextBox
    Me.Text_ItemEntityID = New System.Windows.Forms.TextBox
    Me.btnSave = New System.Windows.Forms.Button
    Me.btnCancel = New System.Windows.Forms.Button
    Me.Text_ItemComment = New System.Windows.Forms.TextBox
    Me.Label5 = New System.Windows.Forms.Label
    Me.DT_ItemDateCompleted = New System.Windows.Forms.DateTimePicker
    Me.Label4 = New System.Windows.Forms.Label
    Me.Combo_ItemStatus = New System.Windows.Forms.ComboBox
    Me.Label3 = New System.Windows.Forms.Label
    Me.RootMenu = New System.Windows.Forms.MenuStrip
    Me.SelectStatus_StatusStrip = New System.Windows.Forms.StatusStrip
    Me.SelectStatus_ProgressBar = New System.Windows.Forms.ToolStripProgressBar
    Me.StatusBar_SelectTransactions = New System.Windows.Forms.ToolStripStatusLabel
    Me.SplitContainer1 = New System.Windows.Forms.SplitContainer
    Me.Label_ItemMessage = New System.Windows.Forms.Label
    Me.Label9 = New System.Windows.Forms.Label
    Me.DT_DateOfDataItem = New System.Windows.Forms.DateTimePicker
    Me.Text_DataItemType = New System.Windows.Forms.TextBox
    Me.Check_DataItem = New System.Windows.Forms.CheckBox
    Me.DT_DataItem = New System.Windows.Forms.DateTimePicker
    Me.Numeric_DataItem = New C1.Win.C1Input.C1NumericEdit
    Me.Percentage_DataItem = New C1.Win.C1Input.C1NumericEdit
    Me.Text_DataItem = New System.Windows.Forms.ComboBox
    CType(Me.Grid_Status, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.SelectStatus_StatusStrip.SuspendLayout()
    Me.SplitContainer1.Panel1.SuspendLayout()
    Me.SplitContainer1.Panel2.SuspendLayout()
    Me.SplitContainer1.SuspendLayout()
    CType(Me.Numeric_DataItem, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.Percentage_DataItem, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.SuspendLayout()
    '
    'Combo_Group
    '
    Me.Combo_Group.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_Group.Location = New System.Drawing.Point(128, 29)
    Me.Combo_Group.Name = "Combo_Group"
    Me.Combo_Group.Size = New System.Drawing.Size(782, 21)
    Me.Combo_Group.TabIndex = 0
    '
    'label_CptyIsFund
    '
    Me.label_CptyIsFund.Location = New System.Drawing.Point(16, 33)
    Me.label_CptyIsFund.Name = "label_CptyIsFund"
    Me.label_CptyIsFund.Size = New System.Drawing.Size(104, 16)
    Me.label_CptyIsFund.TabIndex = 82
    Me.label_CptyIsFund.Text = "Group"
    '
    'Combo_Entity
    '
    Me.Combo_Entity.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_Entity.Location = New System.Drawing.Point(128, 56)
    Me.Combo_Entity.Name = "Combo_Entity"
    Me.Combo_Entity.Size = New System.Drawing.Size(782, 21)
    Me.Combo_Entity.TabIndex = 1
    '
    'Label2
    '
    Me.Label2.Location = New System.Drawing.Point(16, 60)
    Me.Label2.Name = "Label2"
    Me.Label2.Size = New System.Drawing.Size(104, 16)
    Me.Label2.TabIndex = 84
    Me.Label2.Text = "Entity"
    '
    'Combo_Select1_Operator
    '
    Me.Combo_Select1_Operator.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.Combo_Select1_Operator.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_Select1_Operator.Items.AddRange(New Object() {"", "=", "<>", "<", "<=", ">", ">=", "Like", "Not Like"})
    Me.Combo_Select1_Operator.Location = New System.Drawing.Point(272, 111)
    Me.Combo_Select1_Operator.Name = "Combo_Select1_Operator"
    Me.Combo_Select1_Operator.Size = New System.Drawing.Size(96, 21)
    Me.Combo_Select1_Operator.TabIndex = 4
    '
    'Combo_FieldSelect1
    '
    Me.Combo_FieldSelect1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.Combo_FieldSelect1.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_FieldSelect1.Location = New System.Drawing.Point(128, 111)
    Me.Combo_FieldSelect1.Name = "Combo_FieldSelect1"
    Me.Combo_FieldSelect1.Size = New System.Drawing.Size(136, 21)
    Me.Combo_FieldSelect1.Sorted = True
    Me.Combo_FieldSelect1.TabIndex = 3
    '
    'Combo_Select1_Value
    '
    Me.Combo_Select1_Value.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_Select1_Value.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_Select1_Value.Location = New System.Drawing.Point(376, 111)
    Me.Combo_Select1_Value.Name = "Combo_Select1_Value"
    Me.Combo_Select1_Value.Size = New System.Drawing.Size(246, 21)
    Me.Combo_Select1_Value.TabIndex = 5
    '
    'Combo_Select2_Value
    '
    Me.Combo_Select2_Value.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_Select2_Value.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_Select2_Value.Location = New System.Drawing.Point(376, 137)
    Me.Combo_Select2_Value.Name = "Combo_Select2_Value"
    Me.Combo_Select2_Value.Size = New System.Drawing.Size(246, 21)
    Me.Combo_Select2_Value.TabIndex = 9
    '
    'Combo_FieldSelect2
    '
    Me.Combo_FieldSelect2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.Combo_FieldSelect2.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_FieldSelect2.Location = New System.Drawing.Point(128, 137)
    Me.Combo_FieldSelect2.Name = "Combo_FieldSelect2"
    Me.Combo_FieldSelect2.Size = New System.Drawing.Size(136, 21)
    Me.Combo_FieldSelect2.Sorted = True
    Me.Combo_FieldSelect2.TabIndex = 7
    '
    'Combo_Select2_Operator
    '
    Me.Combo_Select2_Operator.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.Combo_Select2_Operator.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_Select2_Operator.Items.AddRange(New Object() {"", "=", "<>", "<", "<=", ">", ">=", "Like", "Not Like"})
    Me.Combo_Select2_Operator.Location = New System.Drawing.Point(272, 137)
    Me.Combo_Select2_Operator.Name = "Combo_Select2_Operator"
    Me.Combo_Select2_Operator.Size = New System.Drawing.Size(96, 21)
    Me.Combo_Select2_Operator.TabIndex = 8
    '
    'Combo_Select3_Value
    '
    Me.Combo_Select3_Value.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_Select3_Value.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_Select3_Value.Location = New System.Drawing.Point(376, 163)
    Me.Combo_Select3_Value.Name = "Combo_Select3_Value"
    Me.Combo_Select3_Value.Size = New System.Drawing.Size(246, 21)
    Me.Combo_Select3_Value.TabIndex = 13
    '
    'Combo_FieldSelect3
    '
    Me.Combo_FieldSelect3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.Combo_FieldSelect3.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_FieldSelect3.Location = New System.Drawing.Point(128, 163)
    Me.Combo_FieldSelect3.Name = "Combo_FieldSelect3"
    Me.Combo_FieldSelect3.Size = New System.Drawing.Size(136, 21)
    Me.Combo_FieldSelect3.Sorted = True
    Me.Combo_FieldSelect3.TabIndex = 11
    '
    'Combo_Select3_Operator
    '
    Me.Combo_Select3_Operator.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.Combo_Select3_Operator.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_Select3_Operator.Items.AddRange(New Object() {"", "=", "<>", "<", "<=", ">", ">=", "Like", "Not Like"})
    Me.Combo_Select3_Operator.Location = New System.Drawing.Point(272, 163)
    Me.Combo_Select3_Operator.Name = "Combo_Select3_Operator"
    Me.Combo_Select3_Operator.Size = New System.Drawing.Size(96, 21)
    Me.Combo_Select3_Operator.TabIndex = 12
    '
    'Combo_AndOr_1
    '
    Me.Combo_AndOr_1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_AndOr_1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.Combo_AndOr_1.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_AndOr_1.Items.AddRange(New Object() {"AND", "OR"})
    Me.Combo_AndOr_1.Location = New System.Drawing.Point(634, 111)
    Me.Combo_AndOr_1.Name = "Combo_AndOr_1"
    Me.Combo_AndOr_1.Size = New System.Drawing.Size(96, 21)
    Me.Combo_AndOr_1.TabIndex = 6
    '
    'Combo_AndOr_2
    '
    Me.Combo_AndOr_2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_AndOr_2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.Combo_AndOr_2.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_AndOr_2.Items.AddRange(New Object() {"AND", "OR"})
    Me.Combo_AndOr_2.Location = New System.Drawing.Point(634, 137)
    Me.Combo_AndOr_2.Name = "Combo_AndOr_2"
    Me.Combo_AndOr_2.Size = New System.Drawing.Size(96, 21)
    Me.Combo_AndOr_2.TabIndex = 10
    '
    'Check_OmitCompleted
    '
    Me.Check_OmitCompleted.Location = New System.Drawing.Point(128, 187)
    Me.Check_OmitCompleted.Name = "Check_OmitCompleted"
    Me.Check_OmitCompleted.Size = New System.Drawing.Size(236, 16)
    Me.Check_OmitCompleted.TabIndex = 14
    Me.Check_OmitCompleted.Text = "Omit Completed"
    '
    'Grid_Status
    '
    Me.Grid_Status.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Grid_Status.ColumnInfo = resources.GetString("Grid_Status.ColumnInfo")
    Me.Grid_Status.Location = New System.Drawing.Point(1, 1)
    Me.Grid_Status.Name = "Grid_Status"
    Me.Grid_Status.Rows.DefaultSize = 17
    Me.Grid_Status.ShowThemedHeaders = C1.Win.C1FlexGrid.ShowThemedHeadersEnum.Columns
    Me.Grid_Status.Size = New System.Drawing.Size(419, 460)
    Me.Grid_Status.TabIndex = 0
    Me.Grid_Status.Tree.Column = 0
    '
    'Check_OverdueOnly
    '
    Me.Check_OverdueOnly.Location = New System.Drawing.Point(376, 187)
    Me.Check_OverdueOnly.Name = "Check_OverdueOnly"
    Me.Check_OverdueOnly.Size = New System.Drawing.Size(236, 16)
    Me.Check_OverdueOnly.TabIndex = 15
    Me.Check_OverdueOnly.Text = "Overdue Only"
    '
    'Combo_Item
    '
    Me.Combo_Item.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_Item.Location = New System.Drawing.Point(128, 82)
    Me.Combo_Item.Name = "Combo_Item"
    Me.Combo_Item.Size = New System.Drawing.Size(782, 21)
    Me.Combo_Item.TabIndex = 2
    '
    'Label1
    '
    Me.Label1.Location = New System.Drawing.Point(16, 86)
    Me.Label1.Name = "Label1"
    Me.Label1.Size = New System.Drawing.Size(104, 16)
    Me.Label1.TabIndex = 108
    Me.Label1.Text = "Item"
    '
    'Text_IsDataItem
    '
    Me.Text_IsDataItem.Enabled = False
    Me.Text_IsDataItem.Location = New System.Drawing.Point(403, 86)
    Me.Text_IsDataItem.Name = "Text_IsDataItem"
    Me.Text_IsDataItem.Size = New System.Drawing.Size(25, 20)
    Me.Text_IsDataItem.TabIndex = 125
    Me.Text_IsDataItem.Visible = False
    '
    'Label_DataItemType
    '
    Me.Label_DataItemType.Location = New System.Drawing.Point(3, 191)
    Me.Label_DataItemType.Name = "Label_DataItemType"
    Me.Label_DataItemType.Size = New System.Drawing.Size(77, 16)
    Me.Label_DataItemType.TabIndex = 124
    Me.Label_DataItemType.Text = "."
    '
    'Label7
    '
    Me.Label7.Location = New System.Drawing.Point(3, 175)
    Me.Label7.Name = "Label7"
    Me.Label7.Size = New System.Drawing.Size(104, 16)
    Me.Label7.TabIndex = 122
    Me.Label7.Text = "Data Item"
    '
    'btnSetDateCompleted
    '
    Me.btnSetDateCompleted.Location = New System.Drawing.Point(319, 113)
    Me.btnSetDateCompleted.Name = "btnSetDateCompleted"
    Me.btnSetDateCompleted.Size = New System.Drawing.Size(118, 21)
    Me.btnSetDateCompleted.TabIndex = 4
    Me.btnSetDateCompleted.Text = "<- Set to Today"
    Me.btnSetDateCompleted.UseVisualStyleBackColor = True
    '
    'Label_ItemPeriod
    '
    Me.Label_ItemPeriod.Location = New System.Drawing.Point(112, 65)
    Me.Label_ItemPeriod.Name = "Label_ItemPeriod"
    Me.Label_ItemPeriod.Size = New System.Drawing.Size(347, 18)
    Me.Label_ItemPeriod.TabIndex = 1
    Me.Label_ItemPeriod.Text = "Period"
    '
    'Label8
    '
    Me.Label8.Location = New System.Drawing.Point(3, 65)
    Me.Label8.Name = "Label8"
    Me.Label8.Size = New System.Drawing.Size(104, 16)
    Me.Label8.TabIndex = 119
    Me.Label8.Text = "Period"
    '
    'Label_ItemName
    '
    Me.Label_ItemName.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Label_ItemName.CausesValidation = False
    Me.Label_ItemName.Location = New System.Drawing.Point(112, 5)
    Me.Label_ItemName.Name = "Label_ItemName"
    Me.Label_ItemName.Size = New System.Drawing.Size(347, 16)
    Me.Label_ItemName.TabIndex = 0
    Me.Label_ItemName.Text = "Status"
    '
    'Label6
    '
    Me.Label6.Location = New System.Drawing.Point(3, 5)
    Me.Label6.Name = "Label6"
    Me.Label6.Size = New System.Drawing.Size(104, 16)
    Me.Label6.TabIndex = 117
    Me.Label6.Text = "Item"
    '
    'Text_ItemStatusID
    '
    Me.Text_ItemStatusID.Enabled = False
    Me.Text_ItemStatusID.Location = New System.Drawing.Point(320, 86)
    Me.Text_ItemStatusID.Name = "Text_ItemStatusID"
    Me.Text_ItemStatusID.Size = New System.Drawing.Size(24, 20)
    Me.Text_ItemStatusID.TabIndex = 116
    Me.Text_ItemStatusID.Visible = False
    '
    'Text_ItemItemID
    '
    Me.Text_ItemItemID.Enabled = False
    Me.Text_ItemItemID.Location = New System.Drawing.Point(374, 86)
    Me.Text_ItemItemID.Name = "Text_ItemItemID"
    Me.Text_ItemItemID.Size = New System.Drawing.Size(26, 20)
    Me.Text_ItemItemID.TabIndex = 115
    Me.Text_ItemItemID.Visible = False
    '
    'Text_ItemEntityID
    '
    Me.Text_ItemEntityID.Enabled = False
    Me.Text_ItemEntityID.Location = New System.Drawing.Point(347, 86)
    Me.Text_ItemEntityID.Name = "Text_ItemEntityID"
    Me.Text_ItemEntityID.Size = New System.Drawing.Size(24, 20)
    Me.Text_ItemEntityID.TabIndex = 114
    Me.Text_ItemEntityID.Visible = False
    '
    'btnSave
    '
    Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnSave.Location = New System.Drawing.Point(113, 429)
    Me.btnSave.Name = "btnSave"
    Me.btnSave.Size = New System.Drawing.Size(127, 28)
    Me.btnSave.TabIndex = 13
    Me.btnSave.Text = "&Save"
    '
    'btnCancel
    '
    Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
    Me.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnCancel.Location = New System.Drawing.Point(266, 429)
    Me.btnCancel.Name = "btnCancel"
    Me.btnCancel.Size = New System.Drawing.Size(75, 28)
    Me.btnCancel.TabIndex = 14
    Me.btnCancel.Text = "&Cancel"
    '
    'Text_ItemComment
    '
    Me.Text_ItemComment.AcceptsReturn = True
    Me.Text_ItemComment.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Text_ItemComment.Location = New System.Drawing.Point(113, 205)
    Me.Text_ItemComment.MaxLength = 0
    Me.Text_ItemComment.Multiline = True
    Me.Text_ItemComment.Name = "Text_ItemComment"
    Me.Text_ItemComment.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
    Me.Text_ItemComment.Size = New System.Drawing.Size(366, 208)
    Me.Text_ItemComment.TabIndex = 12
    '
    'Label5
    '
    Me.Label5.Location = New System.Drawing.Point(3, 205)
    Me.Label5.Name = "Label5"
    Me.Label5.Size = New System.Drawing.Size(104, 16)
    Me.Label5.TabIndex = 113
    Me.Label5.Text = "Comment"
    '
    'DT_ItemDateCompleted
    '
    Me.DT_ItemDateCompleted.Location = New System.Drawing.Point(113, 113)
    Me.DT_ItemDateCompleted.Name = "DT_ItemDateCompleted"
    Me.DT_ItemDateCompleted.Size = New System.Drawing.Size(200, 20)
    Me.DT_ItemDateCompleted.TabIndex = 3
    '
    'Label4
    '
    Me.Label4.Location = New System.Drawing.Point(3, 117)
    Me.Label4.Name = "Label4"
    Me.Label4.Size = New System.Drawing.Size(104, 16)
    Me.Label4.TabIndex = 111
    Me.Label4.Text = "Date Completed"
    '
    'Combo_ItemStatus
    '
    Me.Combo_ItemStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.Combo_ItemStatus.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_ItemStatus.Location = New System.Drawing.Point(113, 86)
    Me.Combo_ItemStatus.Name = "Combo_ItemStatus"
    Me.Combo_ItemStatus.Size = New System.Drawing.Size(200, 21)
    Me.Combo_ItemStatus.TabIndex = 2
    '
    'Label3
    '
    Me.Label3.Location = New System.Drawing.Point(3, 89)
    Me.Label3.Name = "Label3"
    Me.Label3.Size = New System.Drawing.Size(104, 16)
    Me.Label3.TabIndex = 109
    Me.Label3.Text = "Status"
    '
    'RootMenu
    '
    Me.RootMenu.Location = New System.Drawing.Point(0, 0)
    Me.RootMenu.Name = "RootMenu"
    Me.RootMenu.Size = New System.Drawing.Size(918, 24)
    Me.RootMenu.TabIndex = 17
    Me.RootMenu.Text = "RootMenu"
    '
    'SelectStatus_StatusStrip
    '
    Me.SelectStatus_StatusStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.SelectStatus_ProgressBar, Me.StatusBar_SelectTransactions})
    Me.SelectStatus_StatusStrip.Location = New System.Drawing.Point(0, 680)
    Me.SelectStatus_StatusStrip.Name = "SelectStatus_StatusStrip"
    Me.SelectStatus_StatusStrip.Size = New System.Drawing.Size(918, 22)
    Me.SelectStatus_StatusStrip.TabIndex = 110
    Me.SelectStatus_StatusStrip.Text = "StatusStrip1"
    '
    'SelectStatus_ProgressBar
    '
    Me.SelectStatus_ProgressBar.Maximum = 20
    Me.SelectStatus_ProgressBar.Name = "SelectStatus_ProgressBar"
    Me.SelectStatus_ProgressBar.Size = New System.Drawing.Size(150, 17)
    Me.SelectStatus_ProgressBar.Step = 1
    Me.SelectStatus_ProgressBar.Visible = False
    '
    'StatusBar_SelectTransactions
    '
    Me.StatusBar_SelectTransactions.Name = "StatusBar_SelectTransactions"
    Me.StatusBar_SelectTransactions.Size = New System.Drawing.Size(121, 17)
    Me.StatusBar_SelectTransactions.Text = "ToolStripStatusLabel1"
    '
    'SplitContainer1
    '
    Me.SplitContainer1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.SplitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.SplitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1
    Me.SplitContainer1.Location = New System.Drawing.Point(0, 209)
    Me.SplitContainer1.MinimumSize = New System.Drawing.Size(700, 400)
    Me.SplitContainer1.Name = "SplitContainer1"
    '
    'SplitContainer1.Panel1
    '
    Me.SplitContainer1.Panel1.Controls.Add(Me.Grid_Status)
    Me.SplitContainer1.Panel1MinSize = 200
    '
    'SplitContainer1.Panel2
    '
    Me.SplitContainer1.Panel2.Controls.Add(Me.Label_ItemMessage)
    Me.SplitContainer1.Panel2.Controls.Add(Me.Label9)
    Me.SplitContainer1.Panel2.Controls.Add(Me.DT_DateOfDataItem)
    Me.SplitContainer1.Panel2.Controls.Add(Me.Text_DataItemType)
    Me.SplitContainer1.Panel2.Controls.Add(Me.Text_IsDataItem)
    Me.SplitContainer1.Panel2.Controls.Add(Me.Label_ItemName)
    Me.SplitContainer1.Panel2.Controls.Add(Me.Label_DataItemType)
    Me.SplitContainer1.Panel2.Controls.Add(Me.Label3)
    Me.SplitContainer1.Panel2.Controls.Add(Me.Combo_ItemStatus)
    Me.SplitContainer1.Panel2.Controls.Add(Me.Label7)
    Me.SplitContainer1.Panel2.Controls.Add(Me.Label4)
    Me.SplitContainer1.Panel2.Controls.Add(Me.btnSetDateCompleted)
    Me.SplitContainer1.Panel2.Controls.Add(Me.DT_ItemDateCompleted)
    Me.SplitContainer1.Panel2.Controls.Add(Me.Label_ItemPeriod)
    Me.SplitContainer1.Panel2.Controls.Add(Me.Label5)
    Me.SplitContainer1.Panel2.Controls.Add(Me.Label8)
    Me.SplitContainer1.Panel2.Controls.Add(Me.Text_ItemComment)
    Me.SplitContainer1.Panel2.Controls.Add(Me.btnCancel)
    Me.SplitContainer1.Panel2.Controls.Add(Me.Label6)
    Me.SplitContainer1.Panel2.Controls.Add(Me.btnSave)
    Me.SplitContainer1.Panel2.Controls.Add(Me.Text_ItemStatusID)
    Me.SplitContainer1.Panel2.Controls.Add(Me.Text_ItemEntityID)
    Me.SplitContainer1.Panel2.Controls.Add(Me.Text_ItemItemID)
    Me.SplitContainer1.Panel2.Controls.Add(Me.Check_DataItem)
    Me.SplitContainer1.Panel2.Controls.Add(Me.DT_DataItem)
    Me.SplitContainer1.Panel2.Controls.Add(Me.Numeric_DataItem)
    Me.SplitContainer1.Panel2.Controls.Add(Me.Percentage_DataItem)
    Me.SplitContainer1.Panel2.Controls.Add(Me.Text_DataItem)
    Me.SplitContainer1.Panel2MinSize = 300
    Me.SplitContainer1.Size = New System.Drawing.Size(918, 465)
    Me.SplitContainer1.SplitterDistance = 425
    Me.SplitContainer1.TabIndex = 16
    '
    'Label_ItemMessage
    '
    Me.Label_ItemMessage.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Label_ItemMessage.CausesValidation = False
    Me.Label_ItemMessage.Location = New System.Drawing.Point(112, 21)
    Me.Label_ItemMessage.Name = "Label_ItemMessage"
    Me.Label_ItemMessage.Size = New System.Drawing.Size(347, 39)
    Me.Label_ItemMessage.TabIndex = 134
    Me.Label_ItemMessage.Text = "Status"
    '
    'Label9
    '
    Me.Label9.Location = New System.Drawing.Point(3, 150)
    Me.Label9.Name = "Label9"
    Me.Label9.Size = New System.Drawing.Size(104, 16)
    Me.Label9.TabIndex = 132
    Me.Label9.Text = "Data Date"
    '
    'DT_DateOfDataItem
    '
    Me.DT_DateOfDataItem.Enabled = False
    Me.DT_DateOfDataItem.Location = New System.Drawing.Point(113, 146)
    Me.DT_DateOfDataItem.Name = "DT_DateOfDataItem"
    Me.DT_DateOfDataItem.Size = New System.Drawing.Size(194, 20)
    Me.DT_DateOfDataItem.TabIndex = 6
    '
    'Text_DataItemType
    '
    Me.Text_DataItemType.Enabled = False
    Me.Text_DataItemType.Location = New System.Drawing.Point(431, 86)
    Me.Text_DataItemType.Name = "Text_DataItemType"
    Me.Text_DataItemType.Size = New System.Drawing.Size(25, 20)
    Me.Text_DataItemType.TabIndex = 129
    Me.Text_DataItemType.Visible = False
    '
    'Check_DataItem
    '
    Me.Check_DataItem.AutoSize = True
    Me.Check_DataItem.Location = New System.Drawing.Point(113, 175)
    Me.Check_DataItem.Name = "Check_DataItem"
    Me.Check_DataItem.Size = New System.Drawing.Size(29, 17)
    Me.Check_DataItem.TabIndex = 11
    Me.Check_DataItem.Text = " "
    Me.Check_DataItem.UseVisualStyleBackColor = True
    Me.Check_DataItem.Visible = False
    '
    'DT_DataItem
    '
    Me.DT_DataItem.Location = New System.Drawing.Point(113, 172)
    Me.DT_DataItem.Name = "DT_DataItem"
    Me.DT_DataItem.Size = New System.Drawing.Size(194, 20)
    Me.DT_DataItem.TabIndex = 9
    Me.DT_DataItem.Visible = False
    '
    'Numeric_DataItem
    '
    Me.Numeric_DataItem.DisplayFormat.CustomFormat = "#,##0.00"
    Me.Numeric_DataItem.DisplayFormat.Inherit = CType(((((C1.Win.C1Input.FormatInfoInheritFlags.FormatType Or C1.Win.C1Input.FormatInfoInheritFlags.NullText) _
                Or C1.Win.C1Input.FormatInfoInheritFlags.EmptyAsNull) _
                Or C1.Win.C1Input.FormatInfoInheritFlags.TrimStart) _
                Or C1.Win.C1Input.FormatInfoInheritFlags.TrimEnd), C1.Win.C1Input.FormatInfoInheritFlags)
    Me.Numeric_DataItem.EditFormat.CustomFormat = "##########0.0######"
    Me.Numeric_DataItem.EditFormat.Inherit = CType(((((C1.Win.C1Input.FormatInfoInheritFlags.FormatType Or C1.Win.C1Input.FormatInfoInheritFlags.NullText) _
                Or C1.Win.C1Input.FormatInfoInheritFlags.EmptyAsNull) _
                Or C1.Win.C1Input.FormatInfoInheritFlags.TrimStart) _
                Or C1.Win.C1Input.FormatInfoInheritFlags.TrimEnd), C1.Win.C1Input.FormatInfoInheritFlags)
    Me.Numeric_DataItem.FormatType = C1.Win.C1Input.FormatTypeEnum.CustomFormat
    Me.Numeric_DataItem.Location = New System.Drawing.Point(113, 172)
    Me.Numeric_DataItem.Name = "Numeric_DataItem"
    Me.Numeric_DataItem.Size = New System.Drawing.Size(194, 20)
    Me.Numeric_DataItem.TabIndex = 10
    Me.Numeric_DataItem.Tag = Nothing
    Me.Numeric_DataItem.Value = New Decimal(New Integer() {0, 0, 0, 0})
    Me.Numeric_DataItem.Visible = False
    '
    'Percentage_DataItem
    '
    Me.Percentage_DataItem.CustomFormat = "#,##0.0000%"
    Me.Percentage_DataItem.FormatType = C1.Win.C1Input.FormatTypeEnum.CustomFormat
    Me.Percentage_DataItem.Location = New System.Drawing.Point(113, 172)
    Me.Percentage_DataItem.Name = "Percentage_DataItem"
    Me.Percentage_DataItem.Size = New System.Drawing.Size(194, 20)
    Me.Percentage_DataItem.TabIndex = 11
    Me.Percentage_DataItem.Tag = Nothing
    Me.Percentage_DataItem.Value = New Decimal(New Integer() {0, 0, 0, 0})
    Me.Percentage_DataItem.Visible = False
    '
    'Text_DataItem
    '
    Me.Text_DataItem.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Text_DataItem.FormattingEnabled = True
    Me.Text_DataItem.Location = New System.Drawing.Point(113, 172)
    Me.Text_DataItem.Name = "Text_DataItem"
    Me.Text_DataItem.Size = New System.Drawing.Size(366, 21)
    Me.Text_DataItem.TabIndex = 133
    '
    'frmSelectStatusList
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
    Me.ClientSize = New System.Drawing.Size(918, 702)
    Me.Controls.Add(Me.SplitContainer1)
    Me.Controls.Add(Me.SelectStatus_StatusStrip)
    Me.Controls.Add(Me.Combo_Item)
    Me.Controls.Add(Me.Label1)
    Me.Controls.Add(Me.Check_OverdueOnly)
    Me.Controls.Add(Me.Check_OmitCompleted)
    Me.Controls.Add(Me.Combo_AndOr_2)
    Me.Controls.Add(Me.Combo_AndOr_1)
    Me.Controls.Add(Me.Combo_Select3_Value)
    Me.Controls.Add(Me.Combo_FieldSelect3)
    Me.Controls.Add(Me.Combo_Select3_Operator)
    Me.Controls.Add(Me.Combo_Select2_Value)
    Me.Controls.Add(Me.Combo_FieldSelect2)
    Me.Controls.Add(Me.Combo_Select2_Operator)
    Me.Controls.Add(Me.Combo_Select1_Value)
    Me.Controls.Add(Me.Combo_FieldSelect1)
    Me.Controls.Add(Me.Combo_Select1_Operator)
    Me.Controls.Add(Me.Combo_Entity)
    Me.Controls.Add(Me.Label2)
    Me.Controls.Add(Me.Combo_Group)
    Me.Controls.Add(Me.label_CptyIsFund)
    Me.Controls.Add(Me.RootMenu)
    Me.MainMenuStrip = Me.RootMenu
    Me.MinimumSize = New System.Drawing.Size(800, 700)
    Me.Name = "frmSelectStatusList"
    Me.Text = "Select Due Dilligence Status"
    CType(Me.Grid_Status, System.ComponentModel.ISupportInitialize).EndInit()
    Me.SelectStatus_StatusStrip.ResumeLayout(False)
    Me.SelectStatus_StatusStrip.PerformLayout()
    Me.SplitContainer1.Panel1.ResumeLayout(False)
    Me.SplitContainer1.Panel2.ResumeLayout(False)
    Me.SplitContainer1.Panel2.PerformLayout()
    Me.SplitContainer1.ResumeLayout(False)
    CType(Me.Numeric_DataItem, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.Percentage_DataItem, System.ComponentModel.ISupportInitialize).EndInit()
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub

#End Region

#Region " Form Locals and Constants "

  ' Form 'Parent', the Main Venice form.
  ' Generally only accessed through the 'MainForm' property.
    ''' <summary>
    ''' The _ main form
    ''' </summary>
  Private WithEvents _MainForm As FlorenceMain

  ' Form ToolTip
    ''' <summary>
    ''' The form tooltip
    ''' </summary>
  Private FormTooltip As New ToolTip()

  ' Form Constants, specific to the table being updated.

    ''' <summary>
    ''' The ALWAY s_ CLOS e_ THI s_ FORM
    ''' </summary>
  Private ALWAYS_CLOSE_THIS_FORM As Boolean = False

  ' Form Locals, initialised on 'New' defining what standard data items to use
    ''' <summary>
    ''' The THI s_ TABLENAME
    ''' </summary>
  Private THIS_TABLENAME As String
    ''' <summary>
    ''' The THI s_ ADAPTORNAME
    ''' </summary>
  Private THIS_ADAPTORNAME As String
    ''' <summary>
    ''' The THI s_ DATASETNAME
    ''' </summary>
  Private THIS_DATASETNAME As String


  ' The standard ChangeID for this form. e.g. tblFund
    ''' <summary>
    ''' The THI s_ FOR m_ change ID
    ''' </summary>
  Private THIS_FORM_ChangeID As RenaissanceGlobals.RenaissanceChangeID

  ' Form Specific Order fields
    ''' <summary>
    ''' The THI s_ FOR m_ order by
    ''' </summary>
  Private THIS_FORM_OrderBy As String

  ' Form specific Permissioning variables
    ''' <summary>
    ''' The THI s_ FOR m_ permission area
    ''' </summary>
  Private THIS_FORM_PermissionArea As String
    ''' <summary>
    ''' The THI s_ FOR m_ permission type
    ''' </summary>
  Private THIS_FORM_PermissionType As RenaissanceGlobals.PermissionFeatureType

  ' Form specific Form type 
    ''' <summary>
    ''' The THI s_ FOR m_ form ID
    ''' </summary>
  Private THIS_FORM_FormID As FlorenceFormID

  ' Data Structures

    ''' <summary>
    ''' My dataset
    ''' </summary>
  Private myDataset As DataSet
    ''' <summary>
    ''' My table
    ''' </summary>
  Private myTable As DataTable
    ''' <summary>
    ''' My connection
    ''' </summary>
  Private myConnection As SqlConnection
    ''' <summary>
    ''' My adaptor
    ''' </summary>
  Private myAdaptor As SqlDataAdapter
    ''' <summary>
    ''' My data view
    ''' </summary>
  Private myDataView As DataView

    ''' <summary>
    ''' The data value dataset
    ''' </summary>
  Private DataValueDataset As DataSet
    ''' <summary>
    ''' The data value table
    ''' </summary>
  Private DataValueTable As RenaissanceDataClass.DSFlorenceDataValues.tblFlorenceDataValuesDataTable
    ''' <summary>
    ''' The data value adaptor
    ''' </summary>
  Private DataValueAdaptor As SqlDataAdapter

    ''' <summary>
    ''' The this standard dataset
    ''' </summary>
  Private ThisStandardDataset As RenaissanceGlobals.StandardDataset

    ''' <summary>
    ''' The item status I ds DS
    ''' </summary>
  Dim ItemStatusIDsDS As RenaissanceDataClass.DSFlorenceStatusIDs = Nothing
    ''' <summary>
    ''' The item group DS
    ''' </summary>
  Dim ItemGroupDS As RenaissanceDataClass.DSFlorenceGroup = Nothing
    ''' <summary>
    ''' The item status DS
    ''' </summary>
  Dim ItemStatusDS As RenaissanceDataClass.DSFlorenceStatus = Nothing

    ''' <summary>
    ''' The status item changed
    ''' </summary>
  Private StatusItemChanged As Boolean = False
    ''' <summary>
    ''' The data item changed
    ''' </summary>
  Private DataItemChanged As Boolean = False

  ' Active Element.

    ''' <summary>
    ''' The __ is over cancel button
    ''' </summary>
  Private __IsOverCancelButton As Boolean
    ''' <summary>
    ''' The _ in use
    ''' </summary>
  Private _InUse As Boolean

    ''' <summary>
    ''' The m_ expanding node
    ''' </summary>
  Private m_ExpandingNode As Boolean = False
    ''' <summary>
    ''' The last expanded entity
    ''' </summary>
  Private LastExpandedEntity As Integer = (-1)
    ''' <summary>
    ''' The last expanded group
    ''' </summary>
  Private LastExpandedGroup As Integer = (-1)
    ''' <summary>
    ''' The last selected entity
    ''' </summary>
  Private LastSelectedEntity As Integer = (-1)
    ''' <summary>
    ''' The last selected item
    ''' </summary>
  Private LastSelectedItem As Integer = (-1)

  ' Form Status Flags

    ''' <summary>
    ''' The form is valid
    ''' </summary>
  Private FormIsValid As Boolean
    ''' <summary>
    ''' The in paint
    ''' </summary>
  Private InPaint As Boolean
    ''' <summary>
    ''' The _ form open failed
    ''' </summary>
  Private _FormOpenFailed As Boolean

  ' User Permission Flags

    ''' <summary>
    ''' The has read permission
    ''' </summary>
  Private HasReadPermission As Boolean
    ''' <summary>
    ''' The has update permission
    ''' </summary>
  Private HasUpdatePermission As Boolean
    ''' <summary>
    ''' The has insert permission
    ''' </summary>
  Private HasInsertPermission As Boolean
    ''' <summary>
    ''' The has delete permission
    ''' </summary>
  Private HasDeletePermission As Boolean

    ''' <summary>
    ''' Class GridColumns
    ''' </summary>
  Private Class GridColumns
        ''' <summary>
        ''' The first col
        ''' </summary>
    Public Shared FirstCol As Integer = 0
        ''' <summary>
        ''' The item status
        ''' </summary>
    Public Shared ItemStatus As Integer = 1
        ''' <summary>
        ''' The date completed
        ''' </summary>
    Public Shared DateCompleted As Integer = 2
        ''' <summary>
        ''' The overdue
        ''' </summary>
    Public Shared Overdue As Integer = 3
        ''' <summary>
        ''' The group ID
        ''' </summary>
    Public Shared GroupID As Integer = 4
        ''' <summary>
        ''' The entity ID
        ''' </summary>
    Public Shared EntityID As Integer = 5
        ''' <summary>
        ''' The item ID
        ''' </summary>
    Public Shared ItemID As Integer = 6
        ''' <summary>
        ''' The status ID
        ''' </summary>
    Public Shared StatusID As Integer = 7
        ''' <summary>
        ''' The is data item
        ''' </summary>
    Public Shared IsDataItem As Integer = 8
        ''' <summary>
        ''' The data item type
        ''' </summary>
    Public Shared DataItemType As Integer = 9
        ''' <summary>
        ''' The temp bool
        ''' </summary>
    Public Shared TempBool As Integer = 10
        ''' <summary>
        ''' The counter
        ''' </summary>
    Public Shared Counter As Integer = 11
  End Class

    ''' <summary>
    ''' Init_s the grid columns class.
    ''' </summary>
  Private Sub Init_GridColumnsClass()
    GridColumns.FirstCol = Me.Grid_Status.Cols("FirstCol").SafeIndex
    GridColumns.ItemStatus = Me.Grid_Status.Cols("ItemStatus").SafeIndex
    GridColumns.DateCompleted = Me.Grid_Status.Cols("DateCompleted").SafeIndex
    GridColumns.Overdue = Me.Grid_Status.Cols("Overdue").SafeIndex
    GridColumns.GroupID = Me.Grid_Status.Cols("GroupID").SafeIndex
    GridColumns.EntityID = Me.Grid_Status.Cols("EntityID").SafeIndex
    GridColumns.ItemID = Me.Grid_Status.Cols("ItemID").SafeIndex
    GridColumns.StatusID = Me.Grid_Status.Cols("StatusID").SafeIndex
    GridColumns.Counter = Me.Grid_Status.Cols("Counter").SafeIndex
    GridColumns.IsDataItem = Me.Grid_Status.Cols("IsDataItem").SafeIndex
    GridColumns.DataItemType = Me.Grid_Status.Cols("DataItemType").SafeIndex
    GridColumns.TempBool = Me.Grid_Status.Cols("TempBool").SafeIndex

  End Sub

#End Region

#Region " Form 'Properties' "

    ''' <summary>
    ''' Gets the main form.
    ''' </summary>
    ''' <value>The main form.</value>
  Public ReadOnly Property MainForm() As FlorenceMain Implements Globals.StandardFlorenceForm.MainForm
    ' Public property to return handle to the 'Main' Venice form, where in reside most of the 
    ' data structures and many common utilities.
    Get
      Return _MainForm
    End Get
  End Property

    ''' <summary>
    ''' Gets or sets a value indicating whether this instance is over cancel button.
    ''' </summary>
    ''' <value><c>true</c> if this instance is over cancel button; otherwise, <c>false</c>.</value>
  Public Property IsOverCancelButton() As Boolean Implements Globals.StandardFlorenceForm.IsOverCancelButton
    ' Public property maintaining a value indicating if the cursor is over the 'Cancel'
    ' Button on this form.
    ' This property is specifically designed for use by the field formating Event functions
    ' In order that they do not impose format restrictions if the user is about to click the 
    ' 'Cancel' button.
    '
    Get
      Return __IsOverCancelButton
    End Get
    Set(ByVal Value As Boolean)
      __IsOverCancelButton = Value
    End Set
  End Property

    ''' <summary>
    ''' Gets a value indicating whether this instance is in paint.
    ''' </summary>
    ''' <value><c>true</c> if this instance is in paint; otherwise, <c>false</c>.</value>
  Public ReadOnly Property IsInPaint() As Boolean Implements Globals.StandardFlorenceForm.IsInPaint
    Get
      Return InPaint
    End Get
  End Property

    ''' <summary>
    ''' Gets a value indicating whether [in use].
    ''' </summary>
    ''' <value><c>true</c> if [in use]; otherwise, <c>false</c>.</value>
  Public ReadOnly Property InUse() As Boolean Implements Globals.StandardFlorenceForm.InUse
    Get
      Return _InUse
    End Get
  End Property

    ''' <summary>
    ''' Gets a value indicating whether [form open failed].
    ''' </summary>
    ''' <value><c>true</c> if [form open failed]; otherwise, <c>false</c>.</value>
  Public ReadOnly Property FormOpenFailed() As Boolean Implements Globals.StandardFlorenceForm.FormOpenFailed
    Get
      Return _FormOpenFailed
    End Get
  End Property

#End Region

    ''' <summary>
    ''' Initializes a new instance of the <see cref="frmSelectStatusList"/> class.
    ''' </summary>
    ''' <param name="pMainForm">The p main form.</param>
  Public Sub New(ByVal pMainForm As FlorenceMain)
    ' *************************************************************
    ' Custom 'New'. 
    ' Passes in the reference to the parent form.
    ' 
    ' Establishes form specific variables.
    ' Establishes Form specific Data connection / data structures.
    '
    ' *************************************************************

    Me.New()


    _MainForm = pMainForm
    AddHandler _MainForm.FlorenceAutoUpdate, AddressOf Me.AutoUpdate

    _FormOpenFailed = False
    _InUse = True

    ' Default Select and Order fields.

    THIS_FORM_OrderBy = "ItemGroupID, EntityName, EntityID, ItemDescription"

    ' Form Permissioning :-

    THIS_FORM_PermissionArea = "frmSelectStatusList"
    THIS_FORM_PermissionType = RenaissanceGlobals.PermissionFeatureType.TypeForm

    ' 'This' form ID

    THIS_FORM_FormID = FlorenceFormID.frmSelectStatusList

    ' This form's dataset type.

    ThisStandardDataset = RenaissanceStandardDatasets.tblFlorenceWorkItems  ' This Defines the Form Data !!! 


    ' Data object names standard to this Form type.

    THIS_TABLENAME = ThisStandardDataset.TableName
    THIS_ADAPTORNAME = ThisStandardDataset.Adaptorname
    THIS_DATASETNAME = ThisStandardDataset.DatasetName

    THIS_FORM_ChangeID = ThisStandardDataset.ChangeID

    ' Establish / Retrieve data objects for this form.

    myConnection = MainForm.MainDataHandler.Get_Connection(FLORENCE_CONNECTION)
    myAdaptor = MainForm.MainDataHandler.Get_Adaptor(THIS_ADAPTORNAME, FLORENCE_CONNECTION, THIS_TABLENAME)
    myDataset = MainForm.Load_Table(ThisStandardDataset, False)
    myTable = myDataset.Tables(0)

    ' Get DataValues Tables
    DataValueDataset = MainForm.Load_Table(RenaissanceStandardDatasets.tblFlorenceDataValues)
    DataValueAdaptor = MainForm.MainDataHandler.Get_Adaptor(RenaissanceStandardDatasets.tblFlorenceDataValues.Adaptorname)
    DataValueTable = CType(DataValueDataset, RenaissanceDataClass.DSFlorenceDataValues).tblFlorenceDataValues

    ' Report
    SetReportMenu()
    RootMenu.PerformLayout()

    ' Grid :- Grid_Status

    Grid_Status.Rows.Count = 1
    Grid_Status.Cols.Fixed = 0
    '    Grid_Status.ExtendLastCol = True

    'styles
    Dim cs As CellStyle = Grid_Status.Styles.Normal
    cs.Border.Direction = BorderDirEnum.Vertical
    cs.WordWrap = False

    cs = Grid_Status.Styles.Add("Data", Grid_Status.Styles.Normal)
    cs.ForeColor = Color.Black

    cs = Grid_Status.Styles.Add("HighlightOpen", Grid_Status.Styles.Normal)
    cs.ForeColor = Color.Blue

    cs = Grid_Status.Styles.Add("HighlightOverdue", Grid_Status.Styles.Normal)
    'cs.BackColor = SystemColors.Info
    cs.ForeColor = Color.SeaGreen

    'outline tree
    Grid_Status.Tree.Column = 0
    Grid_Status.Tree.Style = TreeStyleFlags.Simple
    Grid_Status.AllowMerging = AllowMergingEnum.Nodes

    'other
    Grid_Status.AllowResizing = AllowResizingEnum.Columns
    Grid_Status.SelectionMode = SelectionModeEnum.Cell

    Call Init_GridColumnsClass()

    ' Establish initial DataView and Initialise Transactions Grig.

    Try
      myDataView = New DataView(myTable, "True", THIS_FORM_OrderBy, DataViewRowState.CurrentRows)
      myDataView.Sort = THIS_FORM_OrderBy
    Catch ex As Exception

    End Try

    ' Initialise Field select area

    Call BuildFieldSelectCombos()
    Me.Combo_AndOr_1.SelectedIndex = 0
    Me.Combo_AndOr_2.SelectedIndex = 0

    ' Form Control Changed events
    AddHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

    AddHandler Combo_Group.SelectedValueChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_Entity.SelectedValueChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_Item.SelectedValueChanged, AddressOf Me.FormControlChanged

    AddHandler Combo_Select1_Operator.SelectedValueChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_Select1_Value.SelectedValueChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_Select1_Value.KeyUp, AddressOf Me.Combo_FieldSelect_KeyUp
    AddHandler Combo_Select2_Operator.SelectedValueChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_Select2_Value.SelectedValueChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_Select2_Value.KeyUp, AddressOf Me.Combo_FieldSelect_KeyUp
    AddHandler Combo_Select3_Operator.SelectedValueChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_Select3_Value.SelectedValueChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_Select3_Value.KeyUp, AddressOf Me.Combo_FieldSelect_KeyUp

    AddHandler Combo_AndOr_1.SelectedValueChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_AndOr_2.SelectedValueChanged, AddressOf Me.FormControlChanged

    AddHandler Check_OmitCompleted.CheckedChanged, AddressOf Me.FormControlChanged

    AddHandler Combo_Group.KeyUp, AddressOf MainForm.ComboSelectAsYouType
    AddHandler Combo_Entity.KeyUp, AddressOf MainForm.ComboSelectAsYouType
    AddHandler Combo_Item.KeyUp, AddressOf MainForm.ComboSelectAsYouType

    AddHandler Combo_Group.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
    AddHandler Combo_Entity.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
    AddHandler Combo_Item.GotFocus, AddressOf MainForm.GenericCombo_GotFocus

    AddHandler Combo_Group.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
    AddHandler Combo_Entity.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
    AddHandler Combo_Item.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged

    AddHandler Combo_Group.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
    AddHandler Combo_Entity.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
    AddHandler Combo_Item.LostFocus, AddressOf MainForm.GenericCombo_LostFocus

    AddHandler Combo_ItemStatus.SelectedValueChanged, AddressOf Me.WorkItemChangedEvent
    AddHandler Text_ItemComment.TextChanged, AddressOf Me.WorkItemChangedEvent
    AddHandler DT_ItemDateCompleted.ValueChanged, AddressOf Me.WorkItemChangedEvent

    AddHandler DT_DataItem.ValueChanged, AddressOf Me.DataItemChangedEvent
    AddHandler Check_DataItem.CheckedChanged, AddressOf Me.DataItemChangedEvent
    AddHandler Numeric_DataItem.ValueChanged, AddressOf Me.DataItemChangedEvent
    AddHandler Percentage_DataItem.ValueChanged, AddressOf Me.DataItemChangedEvent

    AddHandler Text_DataItem.SelectedValueChanged, AddressOf Me.DataItemChangedEvent
    AddHandler Text_DataItem.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
    AddHandler Text_DataItem.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
    AddHandler Text_DataItem.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
    AddHandler Text_DataItem.KeyUp, AddressOf MainForm.ComboSelectAsYouType_NoMatch
    AddHandler Text_DataItem.KeyUp, AddressOf Me.DataItemChangedEventKeyEvent

    ' Set up the ToolTip
    MainForm.SetFormToolTip(Me, FormTooltip)


  End Sub

#Region " This Form Event handlers : FormLoad / FormClose "

  ' Form Initialisation code.
  '
    ''' <summary>
    ''' Resets the form.
    ''' </summary>
  Public Sub ResetForm() Implements StandardFlorenceForm.ResetForm
    THIS_FORM_OrderBy = "ItemGroupID, EntityName, EntityID, ItemDescription"

    Call Form_Load(Me, New System.EventArgs)
  End Sub

    ''' <summary>
    ''' Closes the form.
    ''' </summary>
  Public Sub CloseForm() Implements StandardFlorenceForm.CloseForm
    ALWAYS_CLOSE_THIS_FORM = True
    Me.Close()
  End Sub

    ''' <summary>
    ''' Handles the Load event of the Form control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    Me.MainForm.LogError(Me.Name, LOG_LEVELS.Audit, "", "Form Loaded", "", False)
    _FormOpenFailed = False
    _InUse = True

    ' Initialise Data structures. Connection, Adaptor and Dataset.

    If Not (MainForm Is Nothing) Then
      FormIsValid = True
    Else
      MessageBox.Show("'Main' form is not set. Closing form", "no Main Form", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
      FormIsValid = False
      _FormOpenFailed = True
      Exit Sub
    End If

    If (myConnection Is Nothing) Then
      Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Database connection is not established. Closing form.", "", True)

      FormIsValid = False
      _FormOpenFailed = True
      Exit Sub
    End If

    If (myAdaptor Is Nothing) Then
      Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Data Adaptor is not established. Closing form.", "", True)

      FormIsValid = False
      _FormOpenFailed = True
      Exit Sub
    End If

    If (myDataset Is Nothing) Then
      Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Form Dataset is not established. Closing form.", "", True)

      FormIsValid = False
      _FormOpenFailed = True
      Exit Sub
    End If


    ' Check User permissions
    Try
      Call CheckPermissions()
      If (HasReadPermission = False) Then
        Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User does not have Read permission for this Form..", "", True)

        FormIsValid = False
        _FormOpenFailed = True

        Exit Sub
      End If
    Catch ex As Exception
      FormIsValid = False
      _FormOpenFailed = True

      Exit Sub
    End Try

    ' Initialse form

    InPaint = True
    IsOverCancelButton = False

    ' Initialise main select controls
    ' Build Sorted data list from which this form operates

    Try
      Call SetGroupCombo()
      Call SetEntityCombo()
      Call SetItemCombo()
      Call SetItemStatusCombo()
      Call SetTextDataItemCombo(True)

      If Combo_Group.Items.Count > 0 Then
        Combo_Group.SelectedIndex = 0
      End If
      If Combo_Entity.Items.Count > 0 Then
        Combo_Entity.SelectedIndex = 0
      End If
      If Combo_Item.Items.Count > 0 Then
        Combo_Item.SelectedIndex = 0
      End If

      Me.Combo_FieldSelect1.SelectedIndex = 0
      Me.Combo_FieldSelect2.SelectedIndex = 0
      Me.Combo_FieldSelect3.SelectedIndex = 0

      Me.Check_OmitCompleted.Checked = False
      Me.Check_OverdueOnly.Checked = False

      Call SetSortedRows()
      If (Grid_Status.Rows.Count > 0) Then
        Me.Grid_Status.Row = 0
      End If

      Call MainForm.SetComboSelectionLengths(Me)
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error initialising Select Status Form.", ex.StackTrace, True)
    End Try

    InPaint = False

  End Sub

    ''' <summary>
    ''' Handles the Closing event of the frm control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.ComponentModel.CancelEventArgs"/> instance containing the event data.</param>
  Private Sub frm_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
    Dim HideForm As Boolean

    ' Hide or Close this form ?
    ' All depends on how many of this form type are Open or in Cache...

    _InUse = False

    If (ALWAYS_CLOSE_THIS_FORM = True) Or (FormIsValid = False) Then
      HideForm = False
    Else
      HideForm = True
      If MainForm.FlorenceForms.CountOf(THIS_FORM_FormID) > MainForm.EntryForm_CacheCount Then
        HideForm = False
      End If
    End If

    If HideForm = True Then
      MainForm.HideInFormsCollection(Me)
      Me.Hide()   ' NPP Fix

      e.Cancel = True
    Else
      Try
        MainForm.RemoveFromFormsCollection(Me)
        RemoveHandler _MainForm.FlorenceAutoUpdate, AddressOf Me.AutoUpdate

        RemoveHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

        RemoveHandler Combo_Group.SelectedValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_Entity.SelectedValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_Item.SelectedValueChanged, AddressOf Me.FormControlChanged

        RemoveHandler Combo_Select1_Operator.SelectedValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_Select1_Value.SelectedValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_Select1_Value.KeyUp, AddressOf Me.Combo_FieldSelect_KeyUp
        RemoveHandler Combo_Select2_Operator.SelectedValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_Select2_Value.SelectedValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_Select2_Value.KeyUp, AddressOf Me.Combo_FieldSelect_KeyUp
        RemoveHandler Combo_Select3_Operator.SelectedValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_Select3_Value.SelectedValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_Select3_Value.KeyUp, AddressOf Me.Combo_FieldSelect_KeyUp

        RemoveHandler Combo_AndOr_1.SelectedValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_AndOr_2.SelectedValueChanged, AddressOf Me.FormControlChanged

        RemoveHandler Check_OmitCompleted.CheckedChanged, AddressOf Me.FormControlChanged

        RemoveHandler Combo_Group.KeyUp, AddressOf MainForm.ComboSelectAsYouType
        RemoveHandler Combo_Entity.KeyUp, AddressOf MainForm.ComboSelectAsYouType
        RemoveHandler Combo_Item.KeyUp, AddressOf MainForm.ComboSelectAsYouType

        RemoveHandler Combo_Group.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_Entity.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_Item.GotFocus, AddressOf MainForm.GenericCombo_GotFocus

        RemoveHandler Combo_Group.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_Entity.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_Item.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged

        RemoveHandler Combo_Group.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_Entity.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_Item.LostFocus, AddressOf MainForm.GenericCombo_LostFocus

        RemoveHandler Combo_ItemStatus.SelectedValueChanged, AddressOf Me.WorkItemChangedEvent
        RemoveHandler Text_ItemComment.TextChanged, AddressOf Me.WorkItemChangedEvent
        RemoveHandler DT_ItemDateCompleted.ValueChanged, AddressOf Me.WorkItemChangedEvent

        RemoveHandler Text_DataItem.TextChanged, AddressOf Me.DataItemChangedEvent
        RemoveHandler DT_DataItem.ValueChanged, AddressOf Me.DataItemChangedEvent
        RemoveHandler Check_DataItem.CheckedChanged, AddressOf Me.DataItemChangedEvent
        RemoveHandler Numeric_DataItem.ValueChanged, AddressOf Me.DataItemChangedEvent
        RemoveHandler Percentage_DataItem.ValueChanged, AddressOf Me.DataItemChangedEvent

      Catch ex As Exception
      End Try
    End If

  End Sub


#End Region

  ' Routine to handle changes / updates to tables by this and other windows.
  ' If this, or any other, form posts a change to a table, then it will invoke an update event 
  ' detailing what tables have been altered.
  ' Each Form will receive this event through this 'AutoUpdate' routine whice is configured to handle
  ' the 'FlorenceAutoUpdate' event of the main Venice form.
  ' Each form may them react as appropriate to changes in any table that might impact it.
  '
    ''' <summary>
    ''' Autoes the update.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="RenaissanceGlobals.RenaissanceUpdateEventArgs"/> instance containing the event data.</param>
  Private Sub AutoUpdate(ByVal sender As System.Object, ByVal e As RenaissanceGlobals.RenaissanceUpdateEventArgs)
    Dim OrgInPaint As Boolean
    Dim KnowledgeDateChanged As Boolean
    Dim RefreshGrid As Boolean

    If (Me.IsDisposed) OrElse (Me.Disposing) OrElse (Me.InUse = False) Then Exit Sub

    OrgInPaint = InPaint

    Try
      InPaint = True
      KnowledgeDateChanged = False
      RefreshGrid = False

      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True) Or (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.Connection) = True) Then
        KnowledgeDateChanged = True
      End If
    Catch ex As Exception
    End Try

    ' ****************************************************************
    ' Check for changes relevant to this form
    ' ****************************************************************


    ' Changes to the tblPrice table :-
    Try
      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblFlorenceGroup) = True) Or KnowledgeDateChanged Then
        ItemGroupDS = Nothing
        Call Me.SetGroupCombo()
      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate : tblFlorenceGroup", ex.StackTrace, True)
    End Try

    ' Changes to the tblFund table :-
    Try
      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblFlorenceEntity) = True) Or KnowledgeDateChanged Then
        Call Me.SetEntityCombo()
      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate : tblFlorenceEntity", ex.StackTrace, True)
    End Try

    ' Changes to the tblInstrument table :-
    Try
      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblFlorenceItems) = True) Or KnowledgeDateChanged Then
        Call Me.SetItemCombo()
      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate : tblFlorenceItems", ex.StackTrace, True)
    End Try

    ' Changes to the tblFlorenceStatusIDs table :-
    Try
      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblFlorenceStatusIDs) = True) Or KnowledgeDateChanged Then
        Call SetItemStatusCombo()
        ItemStatusIDsDS = Nothing
      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate : tblFlorenceStatusIDs", ex.StackTrace, True)
    End Try

    ' Changes to the tblFlorenceStatus table :-
    Try
      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblFlorenceStatus) = True) Or KnowledgeDateChanged Then
        ItemStatusDS = Nothing
        If (Me.StatusItemChanged = False) And (Me.DataItemChanged = False) Then
          Call Me.Grid_Status_SelChange(Me, New EventArgs())
        End If
      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate : tblFlorenceStatus", ex.StackTrace, True)
    End Try

    ' Changes to the tblFlorenceDataValues table :-
    Try
      Call SetTextDataItemCombo(True)

      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblFlorenceDataValues) = True) Or KnowledgeDateChanged Then
        If (Me.StatusItemChanged = False) And (Me.DataItemChanged = False) Then
          InPaint = False ' Sel_Change() won't paint otherwise !
          Call Me.Grid_Status_SelChange(Me, New EventArgs())
          InPaint = True
        End If
      End If

    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate : tblFlorenceDataValues", ex.StackTrace, True)
    End Try


    ' Changes to the KnowledgeDate :-
    Try
      If e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True Then
        RefreshGrid = True
      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate: KnowledgeDate", ex.StackTrace, True)
    End Try

    ' Check TableUpdate against first Select Field
    Try
      If Me.Combo_FieldSelect1.SelectedIndex >= 0 Then
        Try

          ' If the First FieldSelect combo has selected a Transaction Field which is related to
          ' another table, as defined in tblReferentialIntegrity (e.g. TransactionCounterparty)
          ' Then if that related table is updated, the FieldSelect-Values combo must be updated.

          ' If ChangedID is ChangeID of related table then ,..

          If (e.TableChanged(GetFieldChangeID(Combo_FieldSelect1.SelectedItem)) = True) Or KnowledgeDateChanged Then
            Dim SelectedValue As Object
            Dim TextValue As String

            ' Save current Value Combo Value.

            SelectedValue = Nothing
            TextValue = ""

            If Me.Combo_Select1_Value.SelectedIndex >= 0 Then
              SelectedValue = Me.Combo_Select1_Value.SelectedValue
            ElseIf (SelectedValue Is Nothing) Then
              TextValue = Me.Combo_Select1_Value.Text
            End If

            ' Update FieldSelect-Values Combo.

            Call UpdateSelectedValueCombo(Me.Combo_FieldSelect1.SelectedItem, Me.Combo_Select1_Value)

            ' Restore Saved Value.

            If (Not (SelectedValue Is Nothing)) Then
              Me.Combo_Select1_Value.SelectedValue = SelectedValue
            ElseIf TextValue.Length > 0 Then
              Me.Combo_Select1_Value.Text = TextValue
            End If
          End If
        Catch ex As Exception
        End Try
      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate: First SelectField", ex.StackTrace, True)
    End Try

    ' Check TableUpdate against second Select Field
    Try
      If Me.Combo_FieldSelect2.SelectedIndex >= 0 Then
        Try
          ' GetFieldChangeID
          If (e.TableChanged(GetFieldChangeID(Combo_FieldSelect2.SelectedItem)) = True) Or KnowledgeDateChanged Then
            Dim SelectedValue As Object
            Dim TextValue As String

            SelectedValue = Nothing
            TextValue = ""

            If Me.Combo_Select2_Value.SelectedIndex >= 0 Then
              SelectedValue = Me.Combo_Select2_Value.SelectedValue
            ElseIf (SelectedValue Is Nothing) Then
              TextValue = Me.Combo_Select2_Value.Text
            End If

            Call UpdateSelectedValueCombo(Me.Combo_FieldSelect2.SelectedItem, Me.Combo_Select2_Value)

            If (Not (SelectedValue Is Nothing)) Then
              Me.Combo_Select2_Value.SelectedValue = SelectedValue
            ElseIf TextValue.Length > 0 Then
              Me.Combo_Select2_Value.Text = TextValue
            End If
          End If
        Catch ex As Exception
        End Try
      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate: Second SelectField", ex.StackTrace, True)
    End Try

    ' Check TableUpdate against third Select Field
    Try
      If Me.Combo_FieldSelect3.SelectedIndex >= 0 Then
        Try
          ' GetFieldChangeID
          If (e.TableChanged(GetFieldChangeID(Combo_FieldSelect3.SelectedItem)) = True) Or KnowledgeDateChanged Then
            Dim SelectedValue As Object
            Dim TextValue As String

            SelectedValue = Nothing
            TextValue = ""

            If Me.Combo_Select3_Value.SelectedIndex >= 0 Then
              SelectedValue = Me.Combo_Select3_Value.SelectedValue
            ElseIf (SelectedValue Is Nothing) Then
              TextValue = Me.Combo_Select3_Value.Text
            End If

            Call UpdateSelectedValueCombo(Me.Combo_FieldSelect3.SelectedItem, Me.Combo_Select3_Value)

            If (Not (SelectedValue Is Nothing)) Then
              Me.Combo_Select3_Value.SelectedValue = SelectedValue
            ElseIf TextValue.Length > 0 Then
              Me.Combo_Select3_Value.Text = TextValue
            End If
          End If
        Catch ex As Exception
        End Try
      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate: Third SelectField", ex.StackTrace, True)
    End Try


    ' Changes to the tblUserPermissions table :-

    Try
      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblUserPermissions) = True) Or KnowledgeDateChanged Then

        ' Check ongoing permissions.

        Call CheckPermissions()
        If (HasReadPermission = False) Then
          Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User has lost Read permission for this Form.", "", True)

          FormIsValid = False
          InPaint = OrgInPaint
          Me.Close()
          Exit Sub
        End If

        RefreshGrid = True

      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate: tblUserPermissions", ex.StackTrace, True)
    End Try


    ' ****************************************************************
    ' Changes to the Main FORM table :-
    ' ****************************************************************

    Try
      If (e.TableChanged(THIS_FORM_ChangeID) = True) Or _
       (RefreshGrid = True) Or _
       KnowledgeDateChanged Then

        ' Re-Set Controls etc.
        Call SetSortedRows(True)

      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Autoupdate: Main Form table", ex.StackTrace, True)
    End Try

    InPaint = OrgInPaint

  End Sub

#Region " Workhorse functions : SetSortedRows / CheckPermission / ControlsChanged / Menu Events "

    ''' <summary>
    ''' Gets the work items select string.
    ''' </summary>
    ''' <returns>System.String.</returns>
  Private Function GetWorkItemsSelectString() As String
    ' *******************************************************************************
    ' Build a Select String appropriate to the form status.
    ' *******************************************************************************

    Dim SelectString As String
    Dim FieldSelectString As String

    SelectString = ""
    FieldSelectString = ""
    Me.StatusBar_SelectTransactions.Text = ""

    Try

      ' Select on an Item Group...
      If (Me.Combo_Group.SelectedIndex > 0) Then
        SelectString = "(ItemGroupID=" & Combo_Group.SelectedValue.ToString & ")"
      End If

      ' Select on Entity

      If (Me.Combo_Entity.SelectedIndex > 0) Then
        If (SelectString.Length > 0) Then
          SelectString &= " AND "
        End If
        SelectString &= "(EntityID=" & Combo_Entity.SelectedValue.ToString & ")"
      End If

      ' Select on Item...

      If (Me.Combo_Item.SelectedIndex > 0) Then
        If (SelectString.Length > 0) Then
          SelectString &= " AND "
        End If

        SelectString &= "(ItemID=" & Combo_Item.SelectedValue.ToString & ")"
      End If

      If Check_OmitCompleted.Checked Then
        If (SelectString.Length > 0) Then
          SelectString &= " AND "
        End If

        SelectString &= "(ItemStatusID<>" & CInt(RenaissanceGlobals.FlorenceStatusIDs.Completed).ToString & ")"
      End If

      If Check_OverdueOnly.Checked Then
        If (SelectString.Length > 0) Then
          SelectString &= " AND "
        End If

        SelectString &= "(IsDateOverdue<>0)"
      End If

      ' Field Select ...

      FieldSelectString = "("
      Dim WorkItemsDataset As DataSet
      Dim WorkItemsTable As DataTable
      Dim FieldName As String

      WorkItemsDataset = MainForm.Load_Table(RenaissanceStandardDatasets.tblFlorenceWorkItems, False)
      WorkItemsTable = myDataset.Tables(0)

      ' Select Field One.

      Try
        If (Me.Combo_FieldSelect1.SelectedIndex > 0) AndAlso (Me.Combo_Select1_Operator.SelectedIndex > 0) And (Me.Combo_Select1_Value.Text.Length > 0) Then
          ' Get Field Name, Add 'Transaction' backon if necessary.

          FieldName = Combo_FieldSelect1.SelectedItem

          If (WorkItemsTable.Columns(FieldName).DataType Is GetType(System.String)) Then

            ' If there is a value Selected, Use it - else use the Combo Text.

            If (Not (Combo_Select1_Value.SelectedValue Is Nothing)) Then
              FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select1_Operator.Text & " '" & Combo_Select1_Value.SelectedValue.ToString & "')"
            Else
              FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select1_Operator.Text & " '" & Combo_Select1_Value.Text.ToString & "')"
            End If

          ElseIf (WorkItemsTable.Columns(FieldName).DataType Is GetType(System.DateTime)) Then

            ' For Dates, If there is no 'Selected' value use the Combo text if it is a Valid date, else
            ' Use the 'Selected' Value if it is a date.

            If (Me.Combo_Select1_Value.SelectedValue Is Nothing) AndAlso (IsDate(Me.Combo_Select1_Value.Text)) Then
              FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select1_Operator.SelectedItem.ToString & " #" & CDate(Combo_Select1_Value.Text).ToString(QUERY_SHORTDATEFORMAT) & "#)"
            ElseIf (Not (Me.Combo_Select1_Value.SelectedValue Is Nothing)) And IsDate(Combo_Select1_Value.SelectedValue) Then
              FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select1_Operator.SelectedItem.ToString & " #" & CDate(Combo_Select1_Value.SelectedValue).ToString(QUERY_SHORTDATEFORMAT) & "#)"
            Else
              FieldSelectString &= "(1)"
            End If

          Else
            ' Now Deemed to be numeric, or at least not in need of quotes / delimeters...
            ' If there is no 'Selected' value use the Combo text if it is a Valid number, else
            ' Use the 'Selected' Value if it is a valid numeric.

            If (Not (Combo_Select1_Value.SelectedValue Is Nothing)) AndAlso (IsNumeric(Combo_Select1_Value.SelectedValue)) Then
              FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select1_Operator.SelectedItem.ToString & " " & Combo_Select1_Value.SelectedValue.ToString & ")"
            ElseIf IsNumeric(Combo_Select1_Value.Text) Then
              FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select1_Operator.SelectedItem.ToString & " " & Combo_Select1_Value.Text & ")"
            Else
              FieldSelectString &= "(1)"
            End If
          End If
        End If

      Catch ex As Exception
      End Try

      ' Select Field Two.

      Try
        If (Me.Combo_FieldSelect2.SelectedIndex > 0) AndAlso (Me.Combo_Select2_Operator.SelectedIndex > 0) And (Me.Combo_Select2_Value.Text.Length > 0) Then
          FieldName = Combo_FieldSelect2.SelectedItem

          If (FieldSelectString.Length > 1) Then
            FieldSelectString &= " " & Me.Combo_AndOr_1.SelectedItem.ToString & " "
          End If

          If (WorkItemsTable.Columns(FieldName).DataType Is GetType(System.String)) Then

            If (Not (Combo_Select2_Value.SelectedValue Is Nothing)) Then
              FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select2_Operator.Text & " '" & Combo_Select2_Value.SelectedValue.ToString & "')"
            Else
              FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select2_Operator.Text & " '" & Combo_Select2_Value.Text.ToString & "')"
            End If

          ElseIf (WorkItemsTable.Columns(FieldName).DataType Is GetType(System.DateTime)) Then

            If (Me.Combo_Select2_Value.SelectedValue Is Nothing) AndAlso (IsDate(Me.Combo_Select2_Value.Text)) Then
              FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select2_Operator.SelectedItem.ToString & " #" & CDate(Combo_Select2_Value.Text).ToString(QUERY_SHORTDATEFORMAT) & "#)"
            ElseIf (Not (Me.Combo_Select2_Value.SelectedValue Is Nothing)) And IsDate(Combo_Select2_Value.SelectedValue) Then
              FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select2_Operator.SelectedItem.ToString & " #" & CDate(Combo_Select2_Value.SelectedValue).ToString(QUERY_SHORTDATEFORMAT) & "#)"
            Else
              FieldSelectString &= "(1)"
            End If

          Else
            If (Not (Combo_Select2_Value.SelectedValue Is Nothing)) AndAlso (IsNumeric(Combo_Select2_Value.SelectedValue)) Then
              FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select2_Operator.SelectedItem.ToString & " " & Combo_Select2_Value.SelectedValue.ToString & ")"
            ElseIf IsNumeric(Combo_Select2_Value.Text) Then
              FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select2_Operator.SelectedItem.ToString & " " & Combo_Select2_Value.Text & ")"
            Else
              FieldSelectString &= "(1)"
            End If
          End If
        End If

      Catch ex As Exception
      End Try


      ' Select Field Three.

      Try
        If (Me.Combo_FieldSelect3.SelectedIndex > 0) AndAlso (Me.Combo_Select3_Operator.SelectedIndex > 0) And (Me.Combo_Select3_Value.Text.Length > 0) Then
          FieldName = Combo_FieldSelect3.SelectedItem

          If (FieldSelectString.Length > 1) Then
            FieldSelectString &= " " & Me.Combo_AndOr_2.SelectedItem.ToString & " "
          End If

          If (WorkItemsTable.Columns(FieldName).DataType Is GetType(System.String)) Then

            If (Not (Combo_Select3_Value.SelectedValue Is Nothing)) Then
              FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select3_Operator.Text & " '" & Combo_Select3_Value.SelectedValue.ToString & "')"
            Else
              FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select3_Operator.Text & " '" & Combo_Select3_Value.Text.ToString & "')"
            End If

          ElseIf (WorkItemsTable.Columns(FieldName).DataType Is GetType(System.DateTime)) Then

            If (Me.Combo_Select3_Value.SelectedValue Is Nothing) AndAlso (IsDate(Me.Combo_Select3_Value.Text)) Then
              FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select3_Operator.SelectedItem.ToString & " #" & CDate(Combo_Select3_Value.Text).ToString(QUERY_SHORTDATEFORMAT) & "#)"
            ElseIf (Not (Me.Combo_Select3_Value.SelectedValue Is Nothing)) And IsDate(Combo_Select3_Value.SelectedValue) Then
              FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select3_Operator.SelectedItem.ToString & " #" & CDate(Combo_Select3_Value.SelectedValue).ToString(QUERY_SHORTDATEFORMAT) & "#)"
            Else
              FieldSelectString &= "(1)"
            End If

          Else
            If (Not (Combo_Select3_Value.SelectedValue Is Nothing)) AndAlso (IsNumeric(Combo_Select3_Value.SelectedValue)) Then
              FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select3_Operator.SelectedItem.ToString & " " & Combo_Select3_Value.SelectedValue.ToString & ")"
            ElseIf IsNumeric(Combo_Select3_Value.Text) Then
              FieldSelectString &= "(" & FieldName & " " & Me.Combo_Select3_Operator.SelectedItem.ToString & " " & Combo_Select3_Value.Text & ")"
            Else
              FieldSelectString &= "(1)"
            End If
          End If
        End If

      Catch ex As Exception
      End Try

      FieldSelectString &= ")"

      ' If the FieldSelect string is used, then tag it on to the main select string.

      If FieldSelectString.Length > 2 Then
        If (SelectString.Length > 0) Then
          SelectString &= " AND "
        End If
        SelectString &= FieldSelectString
      End If

      If SelectString.Length <= 0 Then
        SelectString = "true"
      End If

    Catch ex As Exception
      SelectString = "true"
    End Try

    Return SelectString

  End Function

    ''' <summary>
    ''' Sets the sorted rows.
    ''' </summary>
    ''' <param name="ForceRefresh">if set to <c>true</c> [force refresh].</param>
  Private Sub SetSortedRows(Optional ByVal ForceRefresh As Boolean = False)
    ' *******************************************************************************
    ' Build a Select String appropriate to the form status and apply it to the DataView object.
    ' *******************************************************************************

    Dim SelectString As String

    SelectString = GetWorkItemsSelectString()

    Try
      Call Me.GetFormData(-1, 0, 0)

      If (Not (myDataView.RowFilter = SelectString)) Or (ForceRefresh = True) Then
        If ForceRefresh = True Then
          myDataView.RowFilter = "False"
        End If

        myDataView.RowFilter = SelectString
        PaintStatusGrid()

      End If
    Catch ex As Exception
      SelectString = "true"
      myDataView.RowFilter = SelectString
    End Try

    Me.StatusBar_SelectTransactions.Text = "(" & myDataView.Count.ToString & " Records) " & SelectString

  End Sub

  ' Check User permissions
    ''' <summary>
    ''' Checks the permissions.
    ''' </summary>
  Private Sub CheckPermissions()
    ' *******************************************************************************
    '
    ' *******************************************************************************

    Dim Permissions As Integer

    Permissions = MainForm.CheckPermissions(THIS_FORM_PermissionArea, THIS_FORM_PermissionType)

    HasReadPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermRead) > 0)
    HasUpdatePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermUpdate) > 0)
    HasInsertPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermInsert) > 0)
    HasDeletePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermDelete) > 0)

  End Sub

    ''' <summary>
    ''' Forms the control changed.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub FormControlChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    ' *******************************************************************************
    ' In the event of one of the Form Controls changing, refresh the data grid.
    ' *******************************************************************************

    If InPaint = False Then

      Call SetSortedRows()

    End If
  End Sub

    ''' <summary>
    ''' Works the item changed event.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub WorkItemChangedEvent(ByVal sender As System.Object, ByVal e As System.EventArgs)
    ' *******************************************************************************
    ' 
    ' *******************************************************************************

    If InPaint = False Then
      If (Me.HasUpdatePermission) Or (Me.HasInsertPermission) And (MainForm.Main_Knowledgedate = KNOWLEDGEDATE_NOW) Then
        StatusItemChanged = True
        Me.btnSave.Enabled = True
        Me.btnCancel.Enabled = True
      End If
    End If
  End Sub

    ''' <summary>
    ''' Datas the item changed event.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub DataItemChangedEvent(ByVal sender As System.Object, ByVal e As System.EventArgs)
    ' *******************************************************************************
    ' 
    ' *******************************************************************************

    If InPaint = False Then
      If (Me.HasUpdatePermission) Or (Me.HasInsertPermission) And (MainForm.Main_Knowledgedate = KNOWLEDGEDATE_NOW) Then
        DataItemChanged = True
        Me.btnSave.Enabled = True
        Me.btnCancel.Enabled = True
      End If
    End If
  End Sub


    ''' <summary>
    ''' Datas the item changed event key event.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="KeyEventArgs"/> instance containing the event data.</param>
  Private Sub DataItemChangedEventKeyEvent(ByVal sender As System.Object, ByVal e As KeyEventArgs)
    ' *******************************************************************************
    ' 
    ' *******************************************************************************
    Call DataItemChangedEvent(sender, New EventArgs)
  End Sub

    ''' <summary>
    ''' Handles the KeyUp event of the Combo_FieldSelect control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.Windows.Forms.KeyEventArgs"/> instance containing the event data.</param>
  Private Sub Combo_FieldSelect_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Combo_Select1_Value.KeyUp
    ' *******************************************************************************
    ' In the event of one of the Form Controls changing, refresh the data grid.
    ' *******************************************************************************

    If InPaint = False Then

      Call SetSortedRows()

    End If
  End Sub

    ''' <summary>
    ''' Builds the field select combos.
    ''' </summary>
  Private Sub BuildFieldSelectCombos()
    ' *********************************************************************************
    ' Build the FieldSelect combo boxes.
    '
    ' Field Names starting with 'Transaction' are modified to start with '.'
    ' This is done in order to make the name more readable.
    '
    ' *********************************************************************************

    Dim TransactionDataset As DataSet
    Dim TransactionTable As DataTable

    Dim ColumnName As String
    Dim thisColumn As DataColumn

    TransactionDataset = MainForm.Load_Table(RenaissanceStandardDatasets.tblFlorenceWorkItems, False)
    TransactionTable = TransactionDataset.Tables(0)

    Combo_FieldSelect1.Items.Clear()
    Combo_Select1_Value.Items.Clear()
    Combo_FieldSelect1.Items.Add("")

    Combo_FieldSelect2.Items.Clear()
    Combo_Select2_Value.Items.Clear()
    Combo_FieldSelect2.Items.Add("")

    Combo_FieldSelect3.Items.Clear()
    Combo_Select3_Value.Items.Clear()
    Combo_FieldSelect3.Items.Add("")

    For Each thisColumn In myTable.Columns
      Try
        ColumnName = TransactionTable.Columns(thisColumn.ColumnName).ColumnName

        Combo_FieldSelect1.Items.Add(ColumnName)
        Combo_FieldSelect2.Items.Add(ColumnName)
        Combo_FieldSelect3.Items.Add(ColumnName)

      Catch ex As Exception
      End Try
    Next

  End Sub

    ''' <summary>
    ''' Updates the selected value combo.
    ''' </summary>
    ''' <param name="pFieldName">Name of the p field.</param>
    ''' <param name="pValueCombo">The p value combo.</param>
  Private Sub UpdateSelectedValueCombo(ByVal pFieldName As String, ByRef pValueCombo As ComboBox)
    ' *******************************************************************************
    ' Build a Value Combo Box appropriate to the chosen DataTable Select Field.
    '
    ' By default the combo will contain all existing values for the chosen field, however
    ' If a relationship is defined for this table and field, then a Combo will be built
    ' which reflects this relationship.
    ' e.g. the tblReferentialIntegrity table defined a relationship between the
    ' TransactionCounterparty field and the tblCounterparty.Counterparty field, thus if
    ' the chosen Select field ti 'Counterparty.' then the Value combo will be built using
    ' the CounterpartyID & CounterpartyName details from the tblCounterparty table.
    '
    ' *******************************************************************************

    Dim tblRefIntegrity As RenaissanceDataClass.DSReferentialIntegrity.tblReferentialIntegrityDataTable
    Dim IntegrityRows() As RenaissanceDataClass.DSReferentialIntegrity.tblReferentialIntegrityRow
    Dim TransactionDataset As DataSet
    Dim TransactionTable As DataTable
    Dim SortOrder As Boolean

    SortOrder = True

    ' Clear the Value Combo.

    Try
      pValueCombo.DataSource = Nothing
      pValueCombo.DisplayMember = ""
      pValueCombo.ValueMember = ""
      pValueCombo.Items.Clear()
    Catch ex As Exception
    End Try

    ' By default, make the Combo a Standard Dropdown, so that non-list items can be entered.

    pValueCombo.DropDownStyle = ComboBoxStyle.DropDown

    ' Exit if no FiledName is given (having cleared the combo).

    If (pFieldName.Length <= 0) Then Exit Sub

    ' Get a handle to the Standard Transactions Table.
    ' This is so that the field types can be determined and used.

    Try
      TransactionDataset = MainForm.Load_Table(ThisStandardDataset, False)
      TransactionTable = myDataset.Tables(0)
    Catch ex As Exception
      Exit Sub
    End Try

    ' If the selected field is a Date then sort the selection combo in reverse order.

    If (TransactionTable.Columns(pFieldName).DataType Is GetType(System.DateTime)) Then
      SortOrder = False
    End If

    ' Get a handle to the Referential Integrity table and the row matching this table and field.
    ' this table defines a relationship between Transaction Table fields and other tables.
    ' This information can be used to build more meaningfull Value Combos.

    Try
      tblRefIntegrity = CType(MainForm.Load_Table(RenaissanceStandardDatasets.tblReferentialIntegrity, False), RenaissanceDataClass.DSReferentialIntegrity).tblReferentialIntegrity
      IntegrityRows = tblRefIntegrity.Select("(FeedsTable LIKE 'tblFlorenceWorkItems') AND (FeedsField = '" & pFieldName & "')", "RN")
      If (IntegrityRows.Length <= 0) Then
        GoTo StandardExit
      End If

    Catch ex As Exception
      Exit Sub
    End Try

    If (IntegrityRows(0).IsDescriptionFieldNull) Then
      ' No Linked Description Field

      GoTo StandardExit
    End If

    ' OK, a referential record exists.
    ' Determine the Table and Field Names to use to build the Value Combo.

    Dim TableName As String
    Dim TableField As String
    Dim DescriptionField As String
    Dim stdDS As StandardDataset
    Dim thisChangeID As RenaissanceChangeID

    Try

      TableName = IntegrityRows(0).TableName
      TableField = IntegrityRows(0).TableField
      DescriptionField = IntegrityRows(0).DescriptionField

      thisChangeID = CType(System.Enum.Parse(GetType(RenaissanceChangeID), TableName), RenaissanceChangeID)

      stdDS = RenaissanceStandardDatasets.GetStandardDataset(thisChangeID)

      ' Build the appropriate Values Combo.

      Call MainForm.SetTblGenericCombo( _
      pValueCombo, _
      stdDS, _
      DescriptionField, _
      TableField, _
      "", False, SortOrder, True)      ' 

      ' For Referential Integrity generated combo's, make the Combo a 
      ' DropDownList

      pValueCombo.DropDownStyle = ComboBoxStyle.DropDownList

    Catch ex As Exception
      GoTo StandardExit

    End Try

    Exit Sub

StandardExit:

    ' Build the standard Combo, just use the values from the given field.

    Call MainForm.SetTblGenericCombo( _
    pValueCombo, _
    RenaissanceStandardDatasets.tblFlorenceWorkItems, _
    pFieldName, _
    pFieldName, _
    "", True, SortOrder)     ' 

  End Sub

    ''' <summary>
    ''' Gets the field change ID.
    ''' </summary>
    ''' <param name="pFieldName">Name of the p field.</param>
    ''' <returns>RenaissanceChangeID.</returns>
  Private Function GetFieldChangeID(ByVal pFieldName As String) As RenaissanceChangeID
    Dim tblRefIntegrity As RenaissanceDataClass.DSReferentialIntegrity.tblReferentialIntegrityDataTable
    Dim IntegrityRows() As RenaissanceDataClass.DSReferentialIntegrity.tblReferentialIntegrityRow

    Try
      tblRefIntegrity = CType(MainForm.Load_Table(RenaissanceStandardDatasets.tblReferentialIntegrity, False), RenaissanceDataClass.DSReferentialIntegrity).tblReferentialIntegrity
      IntegrityRows = tblRefIntegrity.Select("(FeedsTable LIKE 'tblFlorenceWorkItems') AND (FeedsField = '" & pFieldName & "')", "RN")
      If (IntegrityRows.Length <= 0) Then
        Return RenaissanceChangeID.None
        Exit Function
      End If

    Catch ex As Exception
      Return RenaissanceChangeID.None
      Exit Function
    End Try


    If (IntegrityRows(0).IsDescriptionFieldNull) Then
      Return RenaissanceChangeID.None
      Exit Function
    End If

    ' OK, a referential record exists.
    ' Determine the Table and Field Names to use to build the Value Combo.

    Dim TableName As String
    Dim TableField As String
    Dim DescriptionField As String
    Dim thisChangeID As RenaissanceChangeID

    Try

      TableName = IntegrityRows(0).TableName
      TableField = IntegrityRows(0).TableField
      DescriptionField = IntegrityRows(0).DescriptionField

      thisChangeID = CType(System.Enum.Parse(GetType(RenaissanceChangeID), TableName), RenaissanceChangeID)

      Return thisChangeID
      Exit Function
    Catch ex As Exception
    End Try

    Return RenaissanceChangeID.None
    Exit Function

  End Function

    ''' <summary>
    ''' Handles the SelectedIndexChanged event of the Combo_FieldSelect1 control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Combo_FieldSelect1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_FieldSelect1.SelectedIndexChanged
    ' ***************************************************************************************
    ' Field Select Combo Chosen, Update the associated Values Combo.
    '
    ' ***************************************************************************************

    Call UpdateSelectedValueCombo(Me.Combo_FieldSelect1.SelectedItem, Me.Combo_Select1_Value)
    Me.Combo_Select1_Operator.SelectedIndex = 1

  End Sub

    ''' <summary>
    ''' Handles the SelectedIndexChanged event of the Combo_FieldSelect2 control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Combo_FieldSelect2_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_FieldSelect2.SelectedIndexChanged
    ' ***************************************************************************************
    ' Field Select Combo Chosen, Update the associated Values Combo.
    '
    ' ***************************************************************************************
    Call UpdateSelectedValueCombo(Me.Combo_FieldSelect2.SelectedItem, Me.Combo_Select2_Value)
    Me.Combo_Select2_Operator.SelectedIndex = 1
  End Sub

    ''' <summary>
    ''' Handles the SelectedIndexChanged event of the Combo_FieldSelect3 control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Combo_FieldSelect3_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_FieldSelect3.SelectedIndexChanged
    ' ***************************************************************************************
    ' Field Select Combo Chosen, Update the associated Values Combo.
    '
    ' ***************************************************************************************
    Call UpdateSelectedValueCombo(Me.Combo_FieldSelect3.SelectedItem, Me.Combo_Select3_Value)
    Me.Combo_Select3_Operator.SelectedIndex = 1
  End Sub

    ''' <summary>
    ''' Handles the CheckedChanged event of the Check_OverdueOnly control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Check_OverdueOnly_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Check_OverdueOnly.CheckedChanged
    If InPaint = False Then
      Call SetSortedRows(True)
    End If
  End Sub

    ''' <summary>
    ''' Handles the CheckedChanged event of the Check_OmitCompleted control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Check_OmitCompleted_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Check_OmitCompleted.CheckStateChanged
    If InPaint = False Then
      Call SetSortedRows(True)
    End If
  End Sub

#End Region

#Region " Report Menu Code"

    ''' <summary>
    ''' Sets the report menu.
    ''' </summary>
  Private Sub SetReportMenu()
    ' **********************************************************************
    ' Standard routine to build the Report Form menu.
    ' **********************************************************************

    Dim ReportMenuItem As New ToolStripMenuItem("&Reports")

    ReportMenuItem.DropDownItems.Add("&Compact Status Report", Nothing, AddressOf StatusReportMenuEvent)

    ReportMenuItem.DropDownItems.Add(New ToolStripSeparator)

    ReportMenuItem.DropDownItems.Add("File &Cover Report", Nothing, AddressOf FileCoverReportMenuEvent)
    ReportMenuItem.DropDownItems.Add("File &Spine Insert Report", Nothing, AddressOf FileSpineReportMenuEvent)
    ReportMenuItem.DropDownItems.Add("&Key Details Report", Nothing, AddressOf KeyDetailsReportMenuEvent)
    ReportMenuItem.DropDownItems.Add("&File Status Report", Nothing, AddressOf FileStatusReportMenuEvent)
    ReportMenuItem.DropDownItems.Add("&Sign Off Report", Nothing, AddressOf FileSignOffReportMenuEvent)

    RootMenu.Items.Add(ReportMenuItem)

  End Sub

    ''' <summary>
    ''' Statuses the report menu event.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Public Sub StatusReportMenuEvent(ByVal sender As Object, ByVal e As System.EventArgs)
    ' **********************************************************************
    ' Run the Status Report based on the Selection criteria on the 'Select' Form.
    '
    ' **********************************************************************

    Dim SelectString As String
    Dim GroupID As Integer = 0
    Dim EntityID As Integer = 0


    Try

      If (Me.Combo_Group.SelectedIndex > 0) AndAlso (IsNumeric(Me.Combo_Group.SelectedValue) = True) Then
        GroupID = CInt(Combo_Group.SelectedValue)
      End If

      If (Me.Combo_Entity.SelectedIndex > 0) AndAlso (IsNumeric(Me.Combo_Entity.SelectedValue) = True) Then
        EntityID = CInt(Combo_Entity.SelectedValue)
      End If

      SelectString = GetWorkItemsSelectString()

      MainForm.MainReportHandler.StatusReport("rptFlorenceStatusReport", GroupID, EntityID, SelectString, SelectStatus_ProgressBar)

    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error running the Compact Due Dilligence Status Report", ex.StackTrace, True)
    End Try


  End Sub

    ''' <summary>
    ''' Files the status report menu event.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Public Sub FileStatusReportMenuEvent(ByVal sender As Object, ByVal e As System.EventArgs)
    ' **********************************************************************
    ' Run the Status Report based on the Selection criteria on the 'Select' Form.
    '
    ' **********************************************************************

    Dim SelectString As String
    Dim GroupID As Integer = 0
    Dim EntityID As Integer = 0


    Try

      If (Me.Combo_Group.SelectedIndex > 0) AndAlso (IsNumeric(Me.Combo_Group.SelectedValue) = True) Then
        GroupID = CInt(Combo_Group.SelectedValue)
      End If

      If (Me.Combo_Entity.SelectedIndex > 0) AndAlso (IsNumeric(Me.Combo_Entity.SelectedValue) = True) Then
        EntityID = CInt(Combo_Entity.SelectedValue)
      End If

      SelectString = GetWorkItemsSelectString()

      MainForm.MainReportHandler.StatusReport("rptFileStatusReport", GroupID, EntityID, SelectString, SelectStatus_ProgressBar)

    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error running the Due Dilligence File Status Report", ex.StackTrace, True)
    End Try

  End Sub


    ''' <summary>
    ''' Files the cover report menu event.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Public Sub FileCoverReportMenuEvent(ByVal sender As Object, ByVal e As System.EventArgs)
    ' **********************************************************************
    ' 
    '
    ' **********************************************************************

    Try
      MainForm.MainReportHandler.DisplayReport("rptCoverInsert", Me.myDataView, 0, Now.Date, MainForm.Main_Knowledgedate, SelectStatus_ProgressBar)
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error running the Cover Insert report", ex.StackTrace, True)
    End Try

  End Sub

    ''' <summary>
    ''' Files the spine report menu event.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Public Sub FileSpineReportMenuEvent(ByVal sender As Object, ByVal e As System.EventArgs)
    ' **********************************************************************
    ' 
    '
    ' **********************************************************************

    Try
      MainForm.MainReportHandler.DisplayReport("rptSpineInsert", Me.myDataView, 0, Now.Date, MainForm.Main_Knowledgedate, SelectStatus_ProgressBar)
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error running the Spine Insert report", ex.StackTrace, True)
    End Try

  End Sub

    ''' <summary>
    ''' Files the sign off report menu event.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Public Sub FileSignOffReportMenuEvent(ByVal sender As Object, ByVal e As System.EventArgs)
    ' **********************************************************************
    ' FileSignOffReportMenuEvent
    ' 
    '
    ' **********************************************************************

    Try
      MainForm.MainReportHandler.DisplayReport("rptSignOffSheet", Me.myDataView, 0, Now.Date, MainForm.Main_Knowledgedate, SelectStatus_ProgressBar)
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error running the Sign-Off Sheet report", ex.StackTrace, True)
    End Try

  End Sub

    ''' <summary>
    ''' Keys the details report menu event.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Public Sub KeyDetailsReportMenuEvent(ByVal sender As Object, ByVal e As System.EventArgs)
    ' **********************************************************************
    ' KeyDetailsReportMenuEvent
    ' 
    '
    ' **********************************************************************
    Dim SelectString As String
    Dim EntityID As Integer = 0


    Try

      If (Me.Combo_Entity.SelectedIndex > 0) AndAlso (IsNumeric(Me.Combo_Entity.SelectedValue) = True) Then
        EntityID = CInt(Combo_Entity.SelectedValue)
      End If

      SelectString = "(KeyDetail<>0) AND " & GetWorkItemsSelectString()

      MainForm.MainReportHandler.KeyDetailsReport(EntityID, SelectString, SelectStatus_ProgressBar)

    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error running the Due Dilligence Key Details Report", ex.StackTrace, True)
    End Try

  End Sub

#End Region

#Region " Grid Event and Drawing Code"

    ''' <summary>
    ''' Paints the status grid.
    ''' </summary>
  Private Sub PaintStatusGrid()
    ' **********************************************************************************
    ' Routine to Paint (In Outline) the Status Grid.
    '
    ' This Routine populates the grid with the appropriate heirarchy of Funds and Status Items.
    ' To improve performance, the Status lines are not added, they are in-filled as 
    ' Instrument lines are expanded.
    '
    ' **********************************************************************************

    Dim Counter As Integer
    Dim thisRowView As DataRowView
    Dim thisRow As RenaissanceDataClass.DSFlorenceWorkItems.tblFlorenceWorkItemsRow

    Dim LastGroupID As Integer
    Dim LastEntityID As Integer
    'Dim thisGroupID As Integer

    Dim LastEntityLine As Integer
    Dim IncompleteWorkItems As Boolean = False
    Dim ItemsNeedUpdating As Boolean = False

    ' FlorenceStatusIDs

    Try

      ' Clear Grid, to start
      Grid_Status.Redraw = False

      Grid_Status.Rows.Count = 1

      ' Add Header Line.
      Grid_Status.Rows.Add()
      Grid_Status(Grid_Status.Rows.Count - 1, GridColumns.FirstCol) = "Primonial"
      Grid_Status(Grid_Status.Rows.Count - 1, GridColumns.StatusID) = (-1)    ' 
      Grid_Status(Grid_Status.Rows.Count - 1, GridColumns.GroupID) = 0
      Grid_Status(Grid_Status.Rows.Count - 1, GridColumns.EntityID) = 0
      Grid_Status(Grid_Status.Rows.Count - 1, GridColumns.ItemID) = 0
      Grid_Status(Grid_Status.Rows.Count - 1, GridColumns.IsDataItem) = 0
      Grid_Status(Grid_Status.Rows.Count - 1, GridColumns.DataItemType) = 0

      Grid_Status.Rows(Grid_Status.Rows.Count - 1).IsNode = True
      Grid_Status.Rows(Grid_Status.Rows.Count - 1).Node.Level = 0

    Catch ex As Exception
    End Try

    ' Initialise tracking variables.
    LastEntityID = (-1)
    LastGroupID = (-1)

    LastEntityLine = (-1)

    InPaint = True


    Try

      ' Loop through the selected data (DataView) adding Fund and Instrument rows as appropriate.
      For Counter = 0 To (myDataView.Count - 1)

        Try

          ' Resolve Work Item Row.
          thisRowView = myDataView.Item(Counter)
          thisRow = thisRowView.Row

          ' Add a New Group Row if this Work Item represents the start of a new Group.
          If thisRow.ItemGroupID <> LastGroupID Then

            Grid_Status.Rows.Add()
            Grid_Status(Grid_Status.Rows.Count - 1, GridColumns.FirstCol) = GetGroupDescription(thisRow.ItemGroupID)
            Grid_Status.Rows(Grid_Status.Rows.Count - 1).IsNode = True
            Grid_Status.Rows(Grid_Status.Rows.Count - 1).Node.Level = 1

            Grid_Status(Grid_Status.Rows.Count - 1, GridColumns.ItemStatus) = ""
            Grid_Status(Grid_Status.Rows.Count - 1, GridColumns.GroupID) = thisRow.ItemGroupID      ' 
            Grid_Status(Grid_Status.Rows.Count - 1, GridColumns.EntityID) = 0        ' 
            Grid_Status(Grid_Status.Rows.Count - 1, GridColumns.ItemID) = 0
            Grid_Status(Grid_Status.Rows.Count - 1, GridColumns.IsDataItem) = 0
            Grid_Status(Grid_Status.Rows.Count - 1, GridColumns.DataItemType) = 0
            Grid_Status(Grid_Status.Rows.Count - 1, GridColumns.StatusID) = (-1)    ' 
            Grid_Status(Grid_Status.Rows.Count - 1, GridColumns.Counter) = (-1)  ' Null Counter, Dont Add lines when the 'Fund' line is expanded.

          End If

          ' Add a New Entity Row if this transaction represents the start of a new Entity.
          If thisRow.EntityID <> LastEntityID Then
            If (LastEntityLine >= 0) Then
              If (IncompleteWorkItems = True) Then
                Grid_Status(LastEntityLine, GridColumns.ItemStatus) = "Open"
                Grid_Status.SetCellStyle(LastEntityLine, GridColumns.ItemStatus, Grid_Status.Styles("HighlightOpen"))
              ElseIf (ItemsNeedUpdating = True) Then
                Grid_Status(LastEntityLine, GridColumns.ItemStatus) = "Overdue"
                Grid_Status.SetCellStyle(LastEntityLine, GridColumns.ItemStatus, Grid_Status.Styles("HighlightOverdue"))
              End If
            End If

            Grid_Status.Rows.Add()
            Grid_Status(Grid_Status.Rows.Count - 1, GridColumns.FirstCol) = thisRow.EntityName.ToString
            Grid_Status.Rows(Grid_Status.Rows.Count - 1).IsNode = True
            Grid_Status.Rows(Grid_Status.Rows.Count - 1).Node.Level = 2

            Grid_Status(Grid_Status.Rows.Count - 1, GridColumns.ItemStatus) = GetStatusIDDescription(thisRow.ItemStatusID)
            Grid_Status(Grid_Status.Rows.Count - 1, GridColumns.GroupID) = thisRow.ItemGroupID      ' 
            Grid_Status(Grid_Status.Rows.Count - 1, GridColumns.EntityID) = thisRow.EntityID     ' 
            Grid_Status(Grid_Status.Rows.Count - 1, GridColumns.ItemID) = 0
            Grid_Status(Grid_Status.Rows.Count - 1, GridColumns.IsDataItem) = 0
            Grid_Status(Grid_Status.Rows.Count - 1, GridColumns.DataItemType) = 0
            Grid_Status(Grid_Status.Rows.Count - 1, GridColumns.StatusID) = (-1)    ' 
            Grid_Status(Grid_Status.Rows.Count - 1, GridColumns.Counter) = Counter

            LastEntityLine = Grid_Status.Rows.Count - 1

            IncompleteWorkItems = False
            ItemsNeedUpdating = False

            ' Add Dummy Row - Facilitates in-fill later.

            Grid_Status.Rows.Add()
            Grid_Status(Grid_Status.Rows.Count - 1, GridColumns.FirstCol) = ""
            Grid_Status.Rows(Grid_Status.Rows.Count - 1).IsNode = False
          End If

          LastGroupID = thisRow.ItemGroupID
          LastEntityID = thisRow.EntityID

          If (GetStatusIDIsComplete(thisRow.ItemStatusID) = 0) Or (thisRow.StatusID = 0) Then
            IncompleteWorkItems = True
          ElseIf (thisRow.IsDateOverdue > 0) Then
            ItemsNeedUpdating = True
          End If

        Catch Inner_Ex As Exception

        End Try
      Next Counter

      ' Update Final Entity Position.

      If (LastEntityLine >= 0) Then
        If (IncompleteWorkItems = True) Then
          Grid_Status(LastEntityLine, GridColumns.ItemStatus) = "Open"
          Grid_Status.SetCellStyle(LastEntityLine, GridColumns.ItemStatus, Grid_Status.Styles("HighlightOpen"))
        ElseIf (ItemsNeedUpdating = True) Then
          Grid_Status(LastEntityLine, GridColumns.ItemStatus) = "Overdue"
          Grid_Status.SetCellStyle(LastEntityLine, GridColumns.ItemStatus, Grid_Status.Styles("HighlightOverdue"))
        End If
      End If

      ' Collapse rows.
      ' Leave uncolapsed the section last opened by the user.
      Dim LevelOneRowsToExpand As New ArrayList
      Dim LevelTwoRowsToExpand As New ArrayList
      Dim LevelCount As Integer

      For Counter = (Grid_Status.Rows.Count - 1) To 1 Step -1
        Try
          If Grid_Status.Rows(Counter).IsNode Then
            If Grid_Status.Rows(Counter).Node.Level > 0 Then
              If (CInt(Grid_Status(Counter, GridColumns.GroupID)) = LastExpandedGroup) AndAlso (Grid_Status.Rows(Counter).Node.Level = 1) Then
                'Grid_Status.Rows(Counter).Node.Expanded = True
                LevelOneRowsToExpand.Add(Counter)
              ElseIf (CInt(Grid_Status(Counter, GridColumns.EntityID)) = LastExpandedEntity) AndAlso (Grid_Status.Rows(Counter).Node.Level = 2) Then
                LevelTwoRowsToExpand.Add(Counter)

                'Grid_Status.Rows(Counter).Node.Expanded = True

                'm_ExpandingNode = True
                'ExpandRow(Counter)
                'm_ExpandingNode = False
              Else
                'Grid_Status.Rows(Counter).Node.Expanded = False
              End If
              Grid_Status.Rows(Counter).Node.Expanded = False

            End If
          End If
        Catch Inner_Ex As Exception
        End Try
      Next

      For LevelCount = 0 To (LevelOneRowsToExpand.Count - 1)
        Grid_Status.Rows(CInt(LevelOneRowsToExpand(LevelCount))).Node.Expanded = True
      Next

      For LevelCount = 0 To (LevelTwoRowsToExpand.Count - 1)
        Grid_Status.Rows(CInt(LevelTwoRowsToExpand(LevelCount))).Node.Expanded = True

        m_ExpandingNode = True
        ExpandRow(CInt(LevelTwoRowsToExpand(LevelCount)))
        m_ExpandingNode = False
      Next

      LevelOneRowsToExpand.Clear()
      LevelOneRowsToExpand = Nothing
      LevelTwoRowsToExpand.Clear()
      LevelTwoRowsToExpand = Nothing

    Catch ex As Exception

    Finally
      InPaint = False

    End Try

    Grid_Status.Redraw = True
    Grid_Status.Refresh()

  End Sub

    ''' <summary>
    ''' Paints the status grid_ new.
    ''' </summary>
  Private Sub PaintStatusGrid_New()
    ' **********************************************************************************
    ' Routine to Paint (In Outline) the Transactions Grid.
    '
    ' This Routine populates the grid with the appropriate heirarchy of Funds and Instruments.
    ' To improve performance, the Transaction lines are not added, they are in-filled as 
    ' Instrument lines are expanded.
    '
    ' **********************************************************************************

    Dim Counter As Integer
    Dim thisRowView As DataRowView
    Dim thisRow As RenaissanceDataClass.DSFlorenceWorkItems.tblFlorenceWorkItemsRow

    Dim GridUpdateline As Integer

    Dim LastGroupID As Integer
    Dim LastEntityID As Integer
    'Dim thisGroupID As Integer

    Dim LastEntityLine As Integer
    Dim IncompleteWorkItems As Boolean = False
    Dim ItemsNeedUpdating As Boolean = False

    ' FlorenceStatusIDs

    Try

      ' Clear Grid, to start
      Grid_Status.Redraw = False

      If (Grid_Status.Rows.Count <= 1) Then
        ' Add Header Line.
        Grid_Status.Rows.Add()
        Grid_Status(Grid_Status.Rows.Count - 1, GridColumns.FirstCol) = "Primonial"
        Grid_Status(Grid_Status.Rows.Count - 1, GridColumns.StatusID) = (-1)    ' 
        Grid_Status(Grid_Status.Rows.Count - 1, GridColumns.GroupID) = 0
        Grid_Status(Grid_Status.Rows.Count - 1, GridColumns.EntityID) = 0
        Grid_Status(Grid_Status.Rows.Count - 1, GridColumns.ItemID) = 0
        Grid_Status(Grid_Status.Rows.Count - 1, GridColumns.IsDataItem) = 0
        Grid_Status(Grid_Status.Rows.Count - 1, GridColumns.DataItemType) = 0

        Grid_Status.Rows(Grid_Status.Rows.Count - 1).IsNode = True
        Grid_Status.Rows(Grid_Status.Rows.Count - 1).Node.Level = 0
      End If

    Catch ex As Exception
    End Try

    ' Initialise tracking variables.
    LastEntityID = (-1)
    LastGroupID = (-1)

    LastEntityLine = (-1)

    ' Initialise Temporary elements

    For Counter = 2 To (Grid_Status.Rows.Count - 1)
      Grid_Status(Counter, GridColumns.TempBool) = True
    Next
    GridUpdateline = 2

    InPaint = True

    Try

      ' Loop through the selected data (DataView) adding Fund and Instrument rows as appropriate.
      For Counter = 0 To (myDataView.Count - 1)

        Try

          ' Resolve Transaction Row.
          thisRowView = myDataView.Item(Counter)
          thisRow = thisRowView.Row

          'thisInstrumentID = thisRow.StatusID

          ' Add a New Fund Row if this transaction represents the start of a new Fund.
          If thisRow.ItemGroupID <> LastGroupID Then

            Grid_Status.Rows.Insert(GridUpdateline)
            Grid_Status(GridUpdateline, GridColumns.FirstCol) = GetGroupDescription(thisRow.ItemGroupID)
            Grid_Status.Rows(GridUpdateline).IsNode = True
            Grid_Status.Rows(GridUpdateline).Node.Level = 1
            Grid_Status.Rows(GridUpdateline).Node.Collapsed = True

            Grid_Status(GridUpdateline, GridColumns.ItemStatus) = ""
            Grid_Status(GridUpdateline, GridColumns.GroupID) = thisRow.ItemGroupID      ' 
            Grid_Status(GridUpdateline, GridColumns.EntityID) = 0        ' 
            Grid_Status(GridUpdateline, GridColumns.ItemID) = 0
            Grid_Status(GridUpdateline, GridColumns.IsDataItem) = 0
            Grid_Status(GridUpdateline, GridColumns.DataItemType) = 0
            Grid_Status(GridUpdateline, GridColumns.StatusID) = (-1)    ' 
            Grid_Status(GridUpdateline, GridColumns.Counter) = (-1)  ' Null Counter, Dont Add lines when the 'Fund' line is expanded.

            GridUpdateline += 1

            'LastInstrument = (-1)
          End If

          ' Add a New Fund Row if this transaction represents the start of a new Fund.
          If thisRow.EntityID <> LastEntityID Then
            If (LastEntityLine >= 0) Then
              If (IncompleteWorkItems = True) Then
                Grid_Status(LastEntityLine, GridColumns.ItemStatus) = "Open"
                Grid_Status.SetCellStyle(LastEntityLine, GridColumns.ItemStatus, Grid_Status.Styles("HighlightOpen"))
              ElseIf (ItemsNeedUpdating = True) Then
                Grid_Status(LastEntityLine, GridColumns.ItemStatus) = "Overdue"
                Grid_Status.SetCellStyle(LastEntityLine, GridColumns.ItemStatus, Grid_Status.Styles("HighlightOverdue"))
              End If
            End If

            Grid_Status.Rows.Add()
            Grid_Status(Grid_Status.Rows.Count - 1, GridColumns.FirstCol) = thisRow.EntityName.ToString
            Grid_Status.Rows(Grid_Status.Rows.Count - 1).IsNode = True
            Grid_Status.Rows(Grid_Status.Rows.Count - 1).Node.Level = 2
            Grid_Status.Rows(Grid_Status.Rows.Count - 1).Node.Collapsed = True

            Grid_Status(Grid_Status.Rows.Count - 1, GridColumns.ItemStatus) = GetStatusIDDescription(thisRow.ItemStatusID)
            Grid_Status(Grid_Status.Rows.Count - 1, GridColumns.GroupID) = thisRow.ItemGroupID      ' 
            Grid_Status(Grid_Status.Rows.Count - 1, GridColumns.EntityID) = thisRow.EntityID     ' 
            Grid_Status(Grid_Status.Rows.Count - 1, GridColumns.ItemID) = 0
            Grid_Status(Grid_Status.Rows.Count - 1, GridColumns.IsDataItem) = 0
            Grid_Status(Grid_Status.Rows.Count - 1, GridColumns.DataItemType) = 0
            Grid_Status(Grid_Status.Rows.Count - 1, GridColumns.StatusID) = (-1)    ' 
            Grid_Status(Grid_Status.Rows.Count - 1, GridColumns.Counter) = Counter

            LastEntityLine = Grid_Status.Rows.Count - 1

            IncompleteWorkItems = False
            ItemsNeedUpdating = False


            ' Add Dummy Row - Facilitates in-fill later.

            ' Check_This()

            Grid_Status.Rows.Add()
            Grid_Status(Grid_Status.Rows.Count - 1, GridColumns.FirstCol) = ""
            Grid_Status.Rows(Grid_Status.Rows.Count - 1).IsNode = False
          End If


          ' Update existing Expanded Lines if they exist.

          '  More_Code_Here()



          LastGroupID = thisRow.ItemGroupID
          LastEntityID = thisRow.EntityID

          If (GetStatusIDIsComplete(thisRow.ItemStatusID) = 0) Or (thisRow.StatusID = 0) Then
            IncompleteWorkItems = True
          ElseIf (thisRow.IsDateOverdue > 0) Then
            ItemsNeedUpdating = True
          End If

        Catch Inner_Ex As Exception

        End Try
      Next Counter

      ' Update Final Instrument Position.

      If (LastEntityLine >= 0) Then
        If (IncompleteWorkItems = True) Then
          Grid_Status(LastEntityLine, GridColumns.ItemStatus) = "Open"
          Grid_Status.SetCellStyle(LastEntityLine, GridColumns.ItemStatus, Grid_Status.Styles("HighlightOpen"))
        ElseIf (ItemsNeedUpdating = True) Then
          Grid_Status(LastEntityLine, GridColumns.ItemStatus) = "Overdue"
          Grid_Status.SetCellStyle(LastEntityLine, GridColumns.ItemStatus, Grid_Status.Styles("HighlightOverdue"))
        End If
      End If

      ' Collapse rows.
      ' Leave uncolapsed the section last opened by the user.

      For Counter = (Grid_Status.Rows.Count - 1) To 1 Step -1
        Try
          If Grid_Status.Rows(Counter).IsNode Then
            If Grid_Status.Rows(Counter).Node.Level > 0 Then
              If (CInt(Grid_Status(Counter, GridColumns.GroupID)) = LastExpandedGroup) AndAlso (Grid_Status.Rows(Counter).Node.Level = 1) Then
                Grid_Status.Rows(Counter).Node.Expanded = True
              ElseIf (CInt(Grid_Status(Counter, GridColumns.EntityID)) = LastExpandedEntity) AndAlso (Grid_Status.Rows(Counter).Node.Level = 2) Then
                Grid_Status.Rows(Counter).Node.Expanded = True

                m_ExpandingNode = True
                ExpandRow(Counter)
                m_ExpandingNode = False
              Else
                Grid_Status.Rows(Counter).Node.Expanded = False
              End If

            End If
          End If
        Catch Inner_Ex As Exception
        End Try
      Next

    Catch ex As Exception

    Finally
      InPaint = False

    End Try

    Grid_Status.Redraw = True
    Grid_Status.Refresh()

  End Sub

    ''' <summary>
    ''' Handles the BeforeCollapse event of the Grid_Status control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="C1.Win.C1FlexGrid.RowColEventArgs"/> instance containing the event data.</param>
  Private Sub Grid_Status_BeforeCollapse(ByVal sender As Object, ByVal e As C1.Win.C1FlexGrid.RowColEventArgs) Handles Grid_Status.BeforeCollapse
    ' **********************************************************************************
    ' Handle the expanding of Instrument sections.
    '
    '
    ' **********************************************************************************

    ' if we're working, ignore this
    If m_ExpandingNode Then
      Return
    End If

    ' don't allow collapsing/expanding groups of rows (with shift-click)
    If e.Row < 0 Then
      e.Cancel = True
      Return
    End If

    ' if we're already collapsed, populate node
    ' before expanding
    If Grid_Status.Rows(e.Row).Node.Collapsed Then
      m_ExpandingNode = True
      ExpandRow(e.Row)
      m_ExpandingNode = False
    End If

  End Sub

    ''' <summary>
    ''' Handles the AfterDragColumn event of the Grid_Status control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="C1.Win.C1FlexGrid.DragRowColEventArgs"/> instance containing the event data.</param>
  Private Sub Grid_Status_AfterDragColumn(ByVal sender As Object, ByVal e As C1.Win.C1FlexGrid.DragRowColEventArgs) Handles Grid_Status.AfterDragColumn
    ' **********************************************************************************
    ' Handle the Moving of Grid columns
    '
    ' Simply re-set the values in the GridColumns class
    '
    ' **********************************************************************************
    Call Init_GridColumnsClass()
  End Sub

    ''' <summary>
    ''' Expands the row.
    ''' </summary>
    ''' <param name="RowNumber">The row number.</param>
  Private Sub ExpandRow(ByVal RowNumber As Integer)
    ' **********************************************************************************
    ' Process the expanding of Instrument sections.
    '
    ' Inserts the appropriate Work Item lines under an Instrument group.
    ' Use the 'Counter' column to determine whether in-fill has already occurred.
    ' 'Counter' value < 0 indicates in-fill has occurred.
    '
    ' The 'Counter' column will contain the Index reference in the DataView at which transactions
    ' relating to the selected Instrument & Fund are located
    ' The List and Counter values are updated each time the Dataview is updated in order to keep
    ' these value in-sync.
    '
    ' **********************************************************************************
    Dim Counter As Integer
    Dim thisRowView As DataRowView
    Dim thisRow As RenaissanceDataClass.DSFlorenceWorkItems.tblFlorenceWorkItemsRow

    Dim GroupID As Integer
    Dim EntityID As Integer
    Dim StartCounter As Integer

    Dim FirstLine As Boolean = True
    Dim UpdateRow As Integer = RowNumber + 1

    Dim RowToSelect As Integer = (RowNumber)

    Dim OrgInPaint As Boolean = False

    Try
      OrgInPaint = InPaint
      InPaint = True

      ' Get Fund and InstrumentIDs

      GroupID = CInt(Grid_Status.Item(RowNumber, GridColumns.GroupID))
      EntityID = CInt(Grid_Status.Item(RowNumber, GridColumns.EntityID))
      StartCounter = CInt(Grid_Status.Item(RowNumber, GridColumns.Counter))

      ' Are the IDs OK, has In-Fill already occured ?

      If (StartCounter >= 0) AndAlso (EntityID > 0) Then

        ' Mark Instrument Group Line as having been filled : Set Counter to Negative
        Grid_Status(RowNumber, GridColumns.Counter) = (-1)

        ' Validate StartCounter
        If (StartCounter > (myDataView.Count - 1)) Then
          Exit Sub
        End If

        ' Add Lines.
        For Counter = StartCounter To (myDataView.Count - 1)

          ' Get Item Row.
          thisRowView = myDataView.Item(Counter)
          thisRow = thisRowView.Row

          ' If this Work Item is appropriate to the selected Entity....

          If (thisRow.EntityID = EntityID) Then

            ' If this is the first Item, then overwrite the 'Dummy' grid line, else insert
            ' a new grid line.

            If (FirstLine = False) Then
              ' Add Line
              UpdateRow += 1

              Grid_Status.Rows.Insert(UpdateRow)
              Grid_Status.Rows(UpdateRow).IsNode = False
            End If

            ' Set Values / Styles

            Grid_Status(UpdateRow, GridColumns.FirstCol) = thisRow.ItemDescription.ToString
            Grid_Status(UpdateRow, GridColumns.GroupID) = GroupID
            Grid_Status(UpdateRow, GridColumns.EntityID) = thisRow.EntityID      ' 
            Grid_Status(UpdateRow, GridColumns.ItemID) = thisRow.ItemID
            Grid_Status(UpdateRow, GridColumns.StatusID) = thisRow.StatusID
            Grid_Status(UpdateRow, GridColumns.IsDataItem) = thisRow.IsDataItem
            Grid_Status(UpdateRow, GridColumns.DataItemType) = thisRow.DataItemType
            Grid_Status(UpdateRow, GridColumns.Counter) = Counter

            If (GetStatusIDIsComplete(thisRow.ItemStatusID) = 0) Or (thisRow.StatusID = 0) Then
              Grid_Status(UpdateRow, GridColumns.ItemStatus) = GetStatusIDDescription(thisRow.ItemStatusID)
              Grid_Status.SetCellStyle(UpdateRow, GridColumns.ItemStatus, Grid_Status.Styles("HighlightOpen"))
            ElseIf (thisRow.IsDateOverdue > 0) Then
              Grid_Status(UpdateRow, GridColumns.ItemStatus) = "Overdue"
              Grid_Status.SetCellStyle(UpdateRow, GridColumns.ItemStatus, Grid_Status.Styles("HighlightOverdue"))
            Else
              Grid_Status(UpdateRow, GridColumns.ItemStatus) = GetStatusIDDescription(thisRow.ItemStatusID)
              Grid_Status.SetCellStyle(UpdateRow, GridColumns.ItemStatus, Grid_Status.Styles("Data"))
            End If

            If (thisRow.EntityID = LastSelectedEntity) AndAlso (thisRow.ItemID = LastSelectedItem) Then
              RowToSelect = UpdateRow
            End If

            FirstLine = False
          Else
            ' If the current Entity does not match, and we have added some lines already, then
            ' assume that we have come to the end of the section and exit.
            ' This logic works on the assumption that the DataView is ordered correctly by EntityID and Item. 

            If (FirstLine = False) Then
              ' If we have worked through the required Fund / Instrument, then exit.
              If (RowToSelect >= 0) Then
                Grid_Status.TopRow = 0

                If (RowToSelect - RowNumber) <= (Grid_Status.BottomRow - Grid_Status.TopRow) Then
                  Grid_Status.TopRow = RowNumber - 1
                Else
                  Grid_Status.TopRow = RowToSelect
                End If
                InPaint = False
                Me.Grid_Status.Row = RowToSelect
                Application.DoEvents()
              End If

              Exit Sub
            End If

          End If

        Next Counter
      End If


      If (RowToSelect >= 0) Then
        Grid_Status.TopRow = 0
        If (RowToSelect - RowNumber) <= (Grid_Status.BottomRow - Grid_Status.TopRow) Then
          Grid_Status.TopRow = RowNumber - 1
        Else
          Grid_Status.TopRow = RowToSelect
        End If

        InPaint = False
        Me.Grid_Status.Row = RowToSelect
        Application.DoEvents()
      End If

    Catch ex As Exception
    Finally
      InPaint = OrgInPaint
    End Try

  End Sub


    ''' <summary>
    ''' Handles the Click event of the Grid_Status control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Grid_Status_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Grid_Status.Click
    ' ***************************************************************************************
    ' 
    ' 
    ' ***************************************************************************************
    If (InPaint) Then
      Exit Sub
    End If

    Try
      If (Grid_Status.RowSel > 0) AndAlso (Me.Grid_Status.Focused) Then
        If (Grid_Status.Rows(Grid_Status.RowSel).Node.Level = 1) Then   ' Group Level
          If (LastExpandedGroup <> CInt(Grid_Status.Item(Grid_Status.RowSel, GridColumns.GroupID))) Then
            ' If a different Group is selected, set these values, otherwise ignore - It prevents the 
            ' Entity value beiny un-necessarily cleared.
            LastExpandedGroup = CInt(Grid_Status.Item(Grid_Status.RowSel, GridColumns.GroupID))
            LastExpandedEntity = CInt(Grid_Status.Item(Grid_Status.RowSel, GridColumns.EntityID))
          End If

        ElseIf (Grid_Status.Rows(Grid_Status.RowSel).Node.Level = 2) Then   ' Entity Level
          LastExpandedGroup = CInt(Grid_Status.Item(Grid_Status.RowSel, GridColumns.GroupID))
          LastExpandedEntity = CInt(Grid_Status.Item(Grid_Status.RowSel, GridColumns.EntityID))
        ElseIf (Grid_Status.Rows(Grid_Status.RowSel).Node.Level = 3) Then   ' Item Level
          LastExpandedGroup = CInt(Grid_Status.Item(Grid_Status.RowSel, GridColumns.GroupID))
          LastExpandedEntity = CInt(Grid_Status.Item(Grid_Status.RowSel, GridColumns.EntityID))
        End If
      End If
    Catch ex As Exception
    End Try

    Call Grid_Status_SelChange(sender, e)

  End Sub

    ''' <summary>
    ''' Handles the SelChange event of the Grid_Status control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Grid_Status_SelChange(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Grid_Status.Click, Grid_Status.SelChange
    ' ***************************************************************************************
    ' 
    ' 
    ' ***************************************************************************************
    Dim StatusID As Integer = 0
    Dim EntityID As Integer = 0
    Dim ItemID As Integer = 0
    Dim Counter As Integer

    If (StatusItemChanged) Then
      SetFormData()
    End If

    If (InPaint) Then
      Exit Sub
    End If

    Try
      If (Grid_Status.RowSel > 0) Then
        Text_ItemEntityID.Text = Nz(Grid_Status(Grid_Status.RowSel, GridColumns.EntityID), "").ToString
        Text_ItemItemID.Text = Nz(Grid_Status(Grid_Status.RowSel, GridColumns.ItemID), "").ToString
        Text_IsDataItem.Text = Nz(Grid_Status(Grid_Status.RowSel, GridColumns.IsDataItem), "").ToString
        Text_DataItemType.Text = Nz(Grid_Status(Grid_Status.RowSel, GridColumns.DataItemType), "").ToString

        StatusID = CInt(Nz(Grid_Status(Grid_Status.RowSel, GridColumns.StatusID), 0))
        EntityID = CInt(Nz(Grid_Status(Grid_Status.RowSel, GridColumns.EntityID), 0))
        ItemID = CInt(Nz(Grid_Status(Grid_Status.RowSel, GridColumns.ItemID), 0))
        Counter = CInt(Nz(Grid_Status(Grid_Status.RowSel, GridColumns.Counter), 0))

        If (InPaint = False) Then
          LastSelectedEntity = EntityID
          LastSelectedItem = ItemID
        End If

        GetFormData(StatusID, EntityID, ItemID, Counter)
      Else
        Text_ItemEntityID.Text = "0"
        Text_ItemItemID.Text = "0"
        Text_IsDataItem.Text = "0"
      End If

    Catch ex As Exception
      Text_ItemEntityID.Text = "0"
      Text_ItemItemID.Text = "0"
      Text_IsDataItem.Text = "0"
      GetFormData(-1, 0, 0)
    End Try

  End Sub

    ''' <summary>
    ''' Handles the DoubleClick event of the Grid_Status control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Grid_Status_DoubleClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Grid_Status.DoubleClick

    ' ***************************************************************************************
    ' 
    ' 
    ' ***************************************************************************************

    If (Grid_Status.RowSel > 0) AndAlso (Grid_Status.Rows(Grid_Status.RowSel).IsNode) Then
      Grid_Status.Rows(Grid_Status.RowSel).Node.Expanded = (Not Grid_Status.Rows(Grid_Status.RowSel).Node.Expanded)
    End If

  End Sub

    ''' <summary>
    ''' Gets the group description.
    ''' </summary>
    ''' <param name="ItemGroupID">The item group ID.</param>
    ''' <returns>System.String.</returns>
  Private Function GetGroupDescription(ByVal ItemGroupID As Integer) As String
    Static LastGroupID As Integer = (-1)
    Static LastGroupDescription As String = ""

    Try
      If (ItemGroupID = LastGroupID) OrElse (ItemGroupID < 0) Then
        If (ItemGroupID < 0) Then   ' Facilitate the explicit clearing of Static variables
          LastGroupID = (-1)
          LastGroupDescription = ""
        End If

        Return LastGroupDescription
      End If
    Catch ex As Exception
      Return ""
    End Try

    Try
      If (ItemGroupDS Is Nothing) Then
        ItemGroupDS = MainForm.Load_Table(RenaissanceStandardDatasets.tblFlorenceGroup)
      End If

      If (ItemGroupDS Is Nothing) Then
        LastGroupID = 0
        LastGroupDescription = ""
        Return ""
        Exit Function
      End If
    Catch ex As Exception
      LastGroupID = 0
      LastGroupDescription = ""
      Return ""
      Exit Function
    End Try

    Try
      Dim SelectedRows() As RenaissanceDataClass.DSFlorenceGroup.tblFlorenceGroupRow
      SelectedRows = ItemGroupDS.tblFlorenceGroup.Select("ItemGroup=" & ItemGroupID.ToString)

      If (SelectedRows.Length > 0) Then
        LastGroupID = ItemGroupID
        LastGroupDescription = SelectedRows(0).ItemGroupDescription
        Return LastGroupDescription
      End If
    Catch ex As Exception
    End Try

    LastGroupID = 0
    LastGroupDescription = ""
    Return ""

  End Function

    ''' <summary>
    ''' Gets the status ID description.
    ''' </summary>
    ''' <param name="ItemStatusID">The item status ID.</param>
    ''' <returns>System.String.</returns>
  Private Function GetStatusIDDescription(ByVal ItemStatusID As Integer) As String
    Static LastStatusID As Integer = (-1)
    Static LastStatusDescription As String = ""

    Try
      If (ItemStatusID = LastStatusID) OrElse (ItemStatusID < 0) Then
        If (ItemStatusID < 0) Then ' Facilitate the explicit clearing of Static variables
          LastStatusID = (-1)
          LastStatusDescription = ""
        End If

        Return LastStatusDescription
      End If
    Catch ex As Exception
      Return ""
    End Try

    Try
      If (ItemStatusIDsDS Is Nothing) Then
        ItemStatusIDsDS = MainForm.Load_Table(RenaissanceStandardDatasets.tblFlorenceStatusIDs)
      End If

      If (ItemStatusIDsDS Is Nothing) Then
        LastStatusID = 0
        LastStatusDescription = ""
        Return ""
        Exit Function
      End If
    Catch ex As Exception
      LastStatusID = 0
      LastStatusDescription = ""
      Return ""
      Exit Function
    End Try

    Try
      Dim SelectedRows() As RenaissanceDataClass.DSFlorenceStatusIDs.tblFlorenceStatusIDsRow
      SelectedRows = ItemStatusIDsDS.tblFlorenceStatusIDs.Select("ItemStatusID=" & ItemStatusID.ToString)

      If (SelectedRows.Length > 0) Then
        LastStatusID = ItemStatusID
        LastStatusDescription = SelectedRows(0).ItemStatusDescription
        Return LastStatusDescription
      End If
    Catch ex As Exception
    End Try

    LastStatusID = 0
    LastStatusDescription = ""
    Return ""

  End Function

    ''' <summary>
    ''' Gets the status ID is complete.
    ''' </summary>
    ''' <param name="ItemStatusID">The item status ID.</param>
    ''' <returns>System.Int32.</returns>
  Private Function GetStatusIDIsComplete(ByVal ItemStatusID As Integer) As Integer
    Static LastStatusID As Integer = (-1)
    Static LastStatusIsComplete As Integer = (-1)

    Try
      If (ItemStatusID = LastStatusID) OrElse (ItemStatusID < 0) Then
        If (ItemStatusID < 0) Then ' Facilitate the explicit clearing of Static variables
          LastStatusID = (-1)
          LastStatusIsComplete = 0
        End If

        Return LastStatusIsComplete
      End If
    Catch ex As Exception
      Return 0
    End Try

    Try
      If (ItemStatusIDsDS Is Nothing) Then
        ItemStatusIDsDS = MainForm.Load_Table(RenaissanceStandardDatasets.tblFlorenceStatusIDs)
      End If

      If (ItemStatusIDsDS Is Nothing) Then
        LastStatusID = 0
        LastStatusIsComplete = 0
        Return LastStatusIsComplete
        Exit Function
      End If
    Catch ex As Exception
      LastStatusID = 0
      LastStatusIsComplete = 0
      Return LastStatusIsComplete
      Exit Function
    End Try

    Try
      Dim SelectedRows() As RenaissanceDataClass.DSFlorenceStatusIDs.tblFlorenceStatusIDsRow
      SelectedRows = ItemStatusIDsDS.tblFlorenceStatusIDs.Select("ItemStatusID=" & ItemStatusID.ToString)

      If (SelectedRows.Length > 0) Then
        LastStatusID = ItemStatusID
        LastStatusIsComplete = SelectedRows(0).ItemIsComplete
        Return LastStatusIsComplete
      End If
    Catch ex As Exception
    End Try

    LastStatusID = 0
    LastStatusIsComplete = 0
    Return LastStatusIsComplete

  End Function

    ''' <summary>
    ''' Gets the form data.
    ''' </summary>
    ''' <param name="thisRow">The this row.</param>
  Private Sub GetFormData(ByRef thisRow As RenaissanceDataClass.DSFlorenceStatus.tblFlorenceStatusRow)

    If (Combo_ItemStatus.Items.Count) > 0 Then
      Combo_ItemStatus.SelectedValue = CInt(thisRow.ItemStatusID)
    End If
    DT_ItemDateCompleted.Value = CDate(thisRow.DateCompleted)
    Text_ItemComment.Text = CStr(thisRow.Comment)

    Me.Text_DataItem.Visible = False
    Me.Check_DataItem.Visible = False
    Me.Numeric_DataItem.Visible = False
    Me.Percentage_DataItem.Visible = False
    Me.DT_DataItem.Visible = False

    Dim ThisDataType As RenaissanceDataType
    If (IsNumeric(Text_DataItemType.Text)) Then
      ThisDataType = CType(CInt(Me.Text_DataItemType.Text), RenaissanceDataType)
      Label_DataItemType.Text = ThisDataType.ToString
    Else
      ThisDataType = RenaissanceDataType.None
      Label_DataItemType.Text = ""
    End If

    If (IsNumeric(Text_IsDataItem.Text)) AndAlso (CInt(Text_IsDataItem.Text) <> 0) Then
      Dim DataValueSelectedRow() As RenaissanceDataClass.DSFlorenceDataValues.tblFlorenceDataValuesRow
      DataValueSelectedRow = DataValueTable.Select("(DataEntityID=" & thisRow.EntityID.ToString & ") AND (DataItemID=" & thisRow.ItemID.ToString & ")", "DataValueDate Desc")

      If (DataValueSelectedRow.Length > 0) Then

        ' Get the First data type recognised. The DataType is a Bit field, but the form
        ' is only equipped at present to enter one type at a time.
        If (ThisDataType And RenaissanceDataType.NumericType) Then
          Me.Numeric_DataItem.Visible = True
          Me.Numeric_DataItem.Value = DataValueSelectedRow(0).NumericData
        ElseIf (ThisDataType And RenaissanceDataType.PercentageType) Then
          Me.Percentage_DataItem.Visible = True
          Me.Percentage_DataItem.Value = DataValueSelectedRow(0).PercentageData
        ElseIf (ThisDataType And RenaissanceDataType.TextType) Then
          Call SetTextDataItemCombo()
          Me.Text_DataItem.Visible = True
          Me.Text_DataItem.Enabled = True
          Me.Text_DataItem.Text = DataValueSelectedRow(0).TextData
        ElseIf (ThisDataType And RenaissanceDataType.BooleanType) Then
          Me.Check_DataItem.Visible = True
          Me.Check_DataItem.Checked = DataValueSelectedRow(0).BooleanData
        ElseIf (ThisDataType And RenaissanceDataType.DateType) Then
          Me.DT_DataItem.Visible = True
          Me.DT_DataItem.Value = DataValueSelectedRow(0).DateData
        End If

      Else

        ' Get the First data type recognised. The DataType is a Bit field, but the form
        ' is only equipped at present to enter one type at a time.
        If (ThisDataType And RenaissanceDataType.NumericType) Then
          Me.Numeric_DataItem.Visible = True
          Me.Numeric_DataItem.Value = 0
        ElseIf (ThisDataType And RenaissanceDataType.PercentageType) Then
          Me.Percentage_DataItem.Visible = True
          Me.Percentage_DataItem.Value = 0
        ElseIf (ThisDataType And RenaissanceDataType.TextType) Then
          Call SetTextDataItemCombo()
          Me.Text_DataItem.Visible = True
          Me.Text_DataItem.Enabled = True
          Me.Text_DataItem.Text = ""
        ElseIf (ThisDataType And RenaissanceDataType.BooleanType) Then
          Me.Check_DataItem.Visible = True
          Me.Check_DataItem.Checked = False
        ElseIf (ThisDataType And RenaissanceDataType.DateType) Then
          Me.DT_DataItem.Visible = True
          Me.DT_DataItem.Value = Now.Date
        End If
      End If
    Else
      Me.Text_DataItem.Text = "0"
      Me.Text_DataItem.Enabled = False
    End If
    Exit Sub

  End Sub

    ''' <summary>
    ''' Gets the form data.
    ''' </summary>
    ''' <param name="StatusID">The status ID.</param>
    ''' <param name="EntityID">The entity ID.</param>
    ''' <param name="ItemID">The item ID.</param>
    ''' <param name="StartCounter">The start counter.</param>
  Private Sub GetFormData(ByVal StatusID As Integer, ByVal EntityID As Integer, ByVal ItemID As Integer, Optional ByVal StartCounter As Integer = (-1))
    ' *************************************************************************************
    ' Routine to Display the Work Item associated with a given StatusID
    ' 
    ' *************************************************************************************
    Dim OrgInPaint As Boolean = False

    Text_ItemStatusID.Text = StatusID.ToString

    OrgInPaint = InPaint
    InPaint = True

    StatusItemChanged = False
    Me.btnSave.Enabled = False
    Me.btnCancel.Enabled = False

    ' Paint Form
    Try
      Label_ItemName.Text = ""
      Label_ItemMessage.Text = ""
      Label_ItemPeriod.Text = ""
      DT_DateOfDataItem.Value = Now.Date

      ' Set field Status's.
      If (StatusID < 0) Or (HasUpdatePermission = False) Or (HasInsertPermission = False) Then
        Me.Combo_ItemStatus.Enabled = False
        Me.DT_ItemDateCompleted.Enabled = False
        Me.btnSetDateCompleted.Enabled = False
        Me.Text_ItemComment.Enabled = False
      Else
        Me.Combo_ItemStatus.Enabled = True
        Me.DT_ItemDateCompleted.Enabled = True
        Me.btnSetDateCompleted.Enabled = True
        Me.Text_ItemComment.Enabled = True
      End If

      Me.Text_DataItem.Visible = False
      Me.Check_DataItem.Visible = False
      Me.Numeric_DataItem.Visible = False
      Me.Percentage_DataItem.Visible = False
      Me.DT_DataItem.Visible = False

      Dim ThisDataType As RenaissanceDataType
      If (IsNumeric(Text_DataItemType.Text)) Then
        ThisDataType = CType(CInt(Me.Text_DataItemType.Text), RenaissanceDataType)
        Label_DataItemType.Text = ThisDataType.ToString
      Else
        ThisDataType = RenaissanceDataType.None
        Label_DataItemType.Text = ""
      End If

      If (ThisDataType And RenaissanceDataType.NumericType) Then
        Me.Numeric_DataItem.Visible = True
        Me.Numeric_DataItem.Value = 0
      ElseIf (ThisDataType And RenaissanceDataType.PercentageType) Then
        Me.Percentage_DataItem.Visible = True
        Me.Percentage_DataItem.Value = 0
      ElseIf (ThisDataType And RenaissanceDataType.TextType) Then
        Call SetTextDataItemCombo()
        Text_DataItem.Visible = True
        Me.Text_DataItem.Text = ""
      ElseIf (ThisDataType And RenaissanceDataType.BooleanType) Then
        Me.Check_DataItem.Visible = True
        Me.Check_DataItem.Checked = False
      ElseIf (ThisDataType And RenaissanceDataType.DateType) Then
        Me.DT_DataItem.Visible = True
        Me.DT_DataItem.Value = Now.Date
      End If

      If (EntityID = 0) Or (ItemID = 0) Then
        If (Combo_ItemStatus.Items.Count) > 0 Then
          Combo_ItemStatus.SelectedValue = RenaissanceGlobals.FlorenceStatusIDs.NoAction
        End If
        DT_ItemDateCompleted.Value = Now.Date
        Text_ItemComment.Text = ""
        Exit Sub
      End If

      If (ItemStatusDS Is Nothing) Then
        ItemStatusDS = MainForm.Load_Table(RenaissanceStandardDatasets.tblFlorenceStatus)
      End If
      If (ItemStatusDS Is Nothing) Then
        GetFormData(-1, 0, 0)
        InPaint = OrgInPaint
        Exit Sub
      End If

      Dim SelectedRow(-1) As RenaissanceDataClass.DSFlorenceStatus.tblFlorenceStatusRow

      ' Use Given DataRow View index to get the StatusID if it seems OK.

      If (StartCounter >= 0) AndAlso (StartCounter < myDataView.Count) Then
        Dim thisRowView As DataRowView
        Dim thisRow As RenaissanceDataClass.DSFlorenceWorkItems.tblFlorenceWorkItemsRow

        thisRowView = myDataView.Item(StartCounter)
        thisRow = thisRowView.Row

        If (thisRow.EntityID = EntityID) And (thisRow.ItemID = ItemID) Then
          If (StatusID <= 0) Then StatusID = thisRow.StatusID
          Label_ItemName.Text = thisRow.ItemDescription.ToString
          Label_ItemMessage.Text = LookupTableValue(MainForm, RenaissanceGlobals.RenaissanceStandardDatasets.GetStandardDataset(RenaissanceChangeID.tblFlorenceItems), thisRow.ItemID, "ItemMessage").ToString

          If (thisRow.UpdatePeriod = 0) Then
            Label_ItemPeriod.Text = "No Update"
          Else
            Dim thisPeriod As DealingPeriod
            ' Dim ReferenceDate As Date = Now.Date

            thisPeriod = CType(CInt(thisRow.UpdatePeriod), RenaissanceGlobals.DealingPeriod)
            Label_ItemPeriod.Text = thisPeriod.ToString

            DT_DateOfDataItem.Value = GetBestGuessDataDate(thisPeriod, Now.Date)
            'If Not ((thisPeriod = DealingPeriod.Daily) Or (thisPeriod = DealingPeriod.Weekly) Or (thisPeriod = DealingPeriod.Fortnightly)) Then
            '  ReferenceDate = ReferenceDate.AddDays(-7)
            'End If
            'DT_DateOfDataItem.Value = FitDateToPeriod(thisPeriod, AddPeriodToDate(thisPeriod, ReferenceDate, -1), True)
          End If
        End If
      End If

      ' Search using the StatusID, if Given.
      If (StatusID > 0) And (SelectedRow.Length <= 0) Then
        SelectedRow = ItemStatusDS.tblFlorenceStatus.Select("(StatusID=" & StatusID.ToString & ")")
        If (SelectedRow.Length <= 0) Then
          StatusID = 0
        End If
      End If

      ' If StatusID not given, or nothing returned, then Search using the Entity and Item IDs
      If (StatusID = 0) And (SelectedRow.Length <= 0) Then
        SelectedRow = ItemStatusDS.tblFlorenceStatus.Select("(EntityID=" & EntityID.ToString & ") AND (ItemID=" & ItemID.ToString & ")")
      End If

      If (SelectedRow.Length > 0) Then
        GetFormData(SelectedRow(0))

        InPaint = OrgInPaint
        Exit Sub
      Else
        If (Combo_ItemStatus.Items.Count) > 0 Then
          Combo_ItemStatus.SelectedValue = RenaissanceGlobals.FlorenceStatusIDs.NoAction
        End If
        DT_ItemDateCompleted.Value = Now.Date
        Text_ItemComment.Text = ""

        ' Get Data, If it exists
        Dim DataValueSelectedRow() As RenaissanceDataClass.DSFlorenceDataValues.tblFlorenceDataValuesRow
        DataValueSelectedRow = DataValueTable.Select("(DataEntityID=" & EntityID.ToString & ") AND (DataItemID=" & ItemID.ToString & ")", "DataValueDate Desc")

				'If (DataValueSelectedRow.Length > 0) Then

				'  ' Get the First data type recognised. The DataType is a Bit field, but the form
				'  ' is only equipped at present to enter one type at a time.
				'  If (ThisDataType And RenaissanceDataType.NumericType) Then
				'    Me.Numeric_DataItem.Visible = True
				'    Me.Numeric_DataItem.Value = DataValueSelectedRow(0).NumericData
				'  ElseIf (ThisDataType And RenaissanceDataType.PercentageType) Then
				'    Me.Percentage_DataItem.Visible = True
				'    Me.Percentage_DataItem.Value = DataValueSelectedRow(0).PercentageData
				'  ElseIf (ThisDataType And RenaissanceDataType.TextType) Then
				'    Call SetTextDataItemCombo()
				'    Text_DataItem.Visible = True
				'    Me.Text_DataItem.Text = DataValueSelectedRow(0).TextData
				'  ElseIf (ThisDataType And RenaissanceDataType.BooleanType) Then
				'    Me.Check_DataItem.Visible = True
				'    Me.Check_DataItem.Checked = DataValueSelectedRow(0).BooleanData
				'  ElseIf (ThisDataType And RenaissanceDataType.DateType) Then
				'    Me.DT_DataItem.Visible = True
				'    Me.DT_DataItem.Value = DataValueSelectedRow(0).DateData
				'  End If
				'End If

				If (DataValueSelectedRow.Length > 0) Then

					' Get the First data type recognised. The DataType is a Bit field, but the form
					' is only equipped at present to enter one type at a time.
					If (ThisDataType And RenaissanceDataType.NumericType) Then
						Me.Numeric_DataItem.Visible = True
						Me.Numeric_DataItem.Value = DataValueSelectedRow(0).NumericData
					ElseIf (ThisDataType And RenaissanceDataType.PercentageType) Then
						Me.Percentage_DataItem.Visible = True
						Me.Percentage_DataItem.Value = DataValueSelectedRow(0).PercentageData
					ElseIf (ThisDataType And RenaissanceDataType.TextType) Then
						Call SetTextDataItemCombo()
						Me.Text_DataItem.Visible = True
						Me.Text_DataItem.Enabled = True
						Me.Text_DataItem.Text = DataValueSelectedRow(0).TextData
					ElseIf (ThisDataType And RenaissanceDataType.BooleanType) Then
						Me.Check_DataItem.Visible = True
						Me.Check_DataItem.Checked = DataValueSelectedRow(0).BooleanData
					ElseIf (ThisDataType And RenaissanceDataType.DateType) Then
						Me.DT_DataItem.Visible = True
						Me.DT_DataItem.Value = DataValueSelectedRow(0).DateData
					End If

				Else

					' Get the First data type recognised. The DataType is a Bit field, but the form
					' is only equipped at present to enter one type at a time.
					If (ThisDataType And RenaissanceDataType.NumericType) Then
						Me.Numeric_DataItem.Visible = True
						Me.Numeric_DataItem.Value = 0
					ElseIf (ThisDataType And RenaissanceDataType.PercentageType) Then
						Me.Percentage_DataItem.Visible = True
						Me.Percentage_DataItem.Value = 0
					ElseIf (ThisDataType And RenaissanceDataType.TextType) Then
						Call SetTextDataItemCombo()
						Me.Text_DataItem.Visible = True
						Me.Text_DataItem.Enabled = True
						Me.Text_DataItem.Text = ""
					ElseIf (ThisDataType And RenaissanceDataType.BooleanType) Then
						Me.Check_DataItem.Visible = True
						Me.Check_DataItem.Checked = False
					ElseIf (ThisDataType And RenaissanceDataType.DateType) Then
						Me.DT_DataItem.Visible = True
						Me.DT_DataItem.Value = Now.Date
					End If
				End If


        Exit Sub
      End If

    Catch ex As Exception
      GetFormData(-1, 0, 0)
    Finally
      InPaint = OrgInPaint
    End Try


  End Sub

#End Region

#Region " Set Form Combos (Form Specific Code) "

    ''' <summary>
    ''' Sets the group combo.
    ''' </summary>
  Private Sub SetGroupCombo()

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_Group, _
    RenaissanceStandardDatasets.tblFlorenceGroup, _
    "ItemGroupDescription", _
    "ItemGroup", _
    "", False, True, True, 0)   ' 

  End Sub

    ''' <summary>
    ''' Sets the entity combo.
    ''' </summary>
  Private Sub SetEntityCombo()

    Dim SelectString As String = "(EntityIsInactive = 0)"

    If (Me.Combo_Group.SelectedIndex > 0) AndAlso (IsNumeric(Combo_Group.SelectedValue)) Then
      SelectString = "(EntityIsInactive = 0) AND ItemGroupID=" & Combo_Group.SelectedValue.ToString
    End If

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_Entity, _
    RenaissanceStandardDatasets.tblFlorenceEntity, _
    "EntityName,ItemGroupID", _
    "EntityID", _
    SelectString, False, True, True, 0)    ' 

  End Sub

    ''' <summary>
    ''' Sets the item combo.
    ''' </summary>
  Private Sub SetItemCombo()

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_Item, _
    RenaissanceStandardDatasets.tblFlorenceItems, _
    "ItemDescription,ItemGroup", _
    "ItemID", _
    "", False, True, True, 0)    ' 

  End Sub

    ''' <summary>
    ''' Sets the item status combo.
    ''' </summary>
  Private Sub SetItemStatusCombo()

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_ItemStatus, _
    RenaissanceStandardDatasets.tblFlorenceStatusIDs, _
    "ItemStatusDescription", _
    "ItemStatusID", _
    "", False, True, False)    ' 

  End Sub

    ''' <summary>
    ''' Sets the text data item combo.
    ''' </summary>
    ''' <param name="pForceRefresh">if set to <c>true</c> [p force refresh].</param>
  Private Sub SetTextDataItemCombo(Optional ByVal pForceRefresh As Boolean = False)
    Static LastItemID As Int32 = (-1)
    Dim ThisItemID As Int32 = (-1)
    Dim SelectString As String = "True"
    Dim DoRefresh As Boolean = pForceRefresh

    Try

      If (IsNumeric(Text_ItemItemID.Text)) Then
        ThisItemID = CInt(Text_ItemItemID.Text)

        If (ThisItemID <= 0) And (pForceRefresh = False) Then
          Exit Sub
        End If

        SelectString = "DataItemID=" & ThisItemID

        If (ThisItemID <> LastItemID) Then
          DoRefresh = True
        End If
      End If

      If (DoRefresh) Then
        Call MainForm.SetTblGenericCombo( _
        Me.Text_DataItem, _
        RenaissanceStandardDatasets.tblFlorenceDataValues, _
        "TextData", _
        "TextData", _
        SelectString, True, True, False, "", "")    ' 

        LastItemID = ThisItemID
      End If

    Catch ex As Exception
    End Try

  End Sub

#End Region

#Region " Transaction report Menu"






#End Region

#Region " Bug hunting "

  ' AddHandler myAdaptor.RowUpdating, AddressOf OnRowUpdating
  ' AddHandler myAdaptor.RowUpdated, AddressOf OnRowUpdated
  ' AddHandler myAdaptor.FillError, AddressOf OnRowFillError

    ''' <summary>
    ''' Called when [row updating].
    ''' </summary>
    ''' <param name="Sender">The sender.</param>
    ''' <param name="e">The <see cref="SqlRowUpdatingEventArgs"/> instance containing the event data.</param>
  Protected Shared Sub OnRowUpdating(ByVal Sender As Object, ByVal e As SqlRowUpdatingEventArgs)
    Dim a As Integer

    If Not e.Command Is Nothing Then
      a = 1
    End If

    If Not e.Errors Is Nothing Then
      a = 2
    End If
  End Sub

    ''' <summary>
    ''' Called when [row updated].
    ''' </summary>
    ''' <param name="Sender">The sender.</param>
    ''' <param name="e">The <see cref="SqlRowUpdatedEventArgs"/> instance containing the event data.</param>
  Protected Shared Sub OnRowUpdated(ByVal Sender As Object, ByVal e As SqlRowUpdatedEventArgs)
    Dim a As Integer

    If Not e.Command Is Nothing Then
      a = 1
    End If

    If Not e.Errors Is Nothing Then
      a = 2
    End If
  End Sub

    ''' <summary>
    ''' Called when [row fill error].
    ''' </summary>
    ''' <param name="Sender">The sender.</param>
    ''' <param name="e">The <see cref="FillErrorEventArgs"/> instance containing the event data.</param>
  Protected Shared Sub OnRowFillError(ByVal Sender As Object, ByVal e As FillErrorEventArgs)
    Dim a As Integer

    a = 1
  End Sub

#End Region

    ''' <summary>
    ''' Handles the SelectedIndexChanged event of the Combo_Group control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Combo_Group_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_Group.SelectedIndexChanged
    ' ****************************************************************************************
    '
    '
    '
    ' ****************************************************************************************

    SetEntityCombo()
  End Sub

    ''' <summary>
    ''' Handles the Click event of the btnSetDateCompleted control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnSetDateCompleted_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSetDateCompleted.Click
    ' ****************************************************************************************
    '
    '
    '
    ' ****************************************************************************************

    Me.DT_ItemDateCompleted.Value = Now.Date()
  End Sub

    ''' <summary>
    ''' Handles the TextChanged event of the Text_ItemComment control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Text_ItemComment_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Text_ItemComment.TextChanged
    ' ****************************************************************************************
    '
    '
    '
    ' ****************************************************************************************

    If InPaint = False Then
      If (Combo_ItemStatus.Items.Count > 0) Then
        If (Me.Combo_ItemStatus.SelectedIndex >= 0) Then
          If (IsNumeric(Combo_ItemStatus.SelectedValue)) Then
            If CInt(Combo_ItemStatus.SelectedValue) = CInt(RenaissanceGlobals.FlorenceStatusIDs.NoAction) Then
              Combo_ItemStatus.SelectedValue = CInt(RenaissanceGlobals.FlorenceStatusIDs.Completed)
            End If
          Else
          End If
        Else
          Combo_ItemStatus.SelectedValue = CInt(RenaissanceGlobals.FlorenceStatusIDs.Completed)
        End If
      End If
    End If
  End Sub

    ''' <summary>
    ''' Handles the Click event of the btnCancel control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
    ' ****************************************************************************************
    '
    '
    '
    ' ****************************************************************************************

    Dim StatusID As Integer = 0
    Dim EntityID As Integer = 0
    Dim ItemID As Integer = 0

    If IsNumeric(Text_ItemStatusID.Text) Then
      StatusID = CInt(Text_ItemStatusID.Text)
    End If

    If IsNumeric(Text_ItemEntityID.Text) Then
      EntityID = CInt(Text_ItemEntityID.Text)
    End If

    If IsNumeric(Text_ItemItemID.Text) Then
      ItemID = CInt(Text_ItemItemID.Text)
    End If

    Call GetFormData(StatusID, EntityID, ItemID)

  End Sub

    ''' <summary>
    ''' Handles the Click event of the btnSave control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
    ' ****************************************************************************************
    '
    '
    '
    ' ****************************************************************************************

    Call SetFormData()

  End Sub

    ''' <summary>
    ''' Sets the form data.
    ''' </summary>
    ''' <returns>System.Int32.</returns>
  Private Function SetFormData() As Integer
    ' ****************************************************************************************
    '
    '
    '
    ' ****************************************************************************************

    Dim StatusID As Integer = 0
    Dim EntityID As Integer = 0
    Dim ItemID As Integer = 0
    Dim DataItemType As RenaissanceDataType = RenaissanceDataType.None

    Dim _UpdatedStatusItem As Boolean = False
    Dim _UpdatedDataItem As Boolean = False

    Dim thisStatusRow As RenaissanceDataClass.DSFlorenceStatus.tblFlorenceStatusRow = Nothing
    Dim thisDataRow As RenaissanceDataClass.DSFlorenceDataValues.tblFlorenceDataValuesRow = Nothing

    Try
      If IsNumeric(Text_ItemStatusID.Text) Then
        StatusID = CInt(Text_ItemStatusID.Text)
      End If

      If IsNumeric(Text_ItemEntityID.Text) Then
        EntityID = CInt(Text_ItemEntityID.Text)
      End If

      If IsNumeric(Text_ItemItemID.Text) Then
        ItemID = CInt(Text_ItemItemID.Text)
      End If

      If IsNumeric(Text_DataItemType.Text) Then
        DataItemType = CType(CInt(Text_DataItemType.Text), RenaissanceDataType)
      End If
    Catch ex As Exception
    End Try

    If (StatusItemChanged = True) OrElse (StatusID = 0) Then
      StatusItemChanged = False

      If (EntityID > 0) And (ItemID > 0) Then
        Try
          Dim SelectedRow() As RenaissanceDataClass.DSFlorenceStatus.tblFlorenceStatusRow
          Dim myAdaptor As SqlDataAdapter
          Dim IsNewRow As Boolean = False

          If (ItemStatusDS Is Nothing) Then
            ItemStatusDS = MainForm.Load_Table(RenaissanceGlobals.RenaissanceStandardDatasets.tblFlorenceStatus)
          End If
          If (ItemStatusDS Is Nothing) Then
            MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Failed to load Florence Status table", "", True)
            Return StatusID
            Exit Function
          End If

          SelectedRow = ItemStatusDS.tblFlorenceStatus.Select("(EntityID=" & EntityID.ToString & ") AND (ItemID=" & ItemID.ToString & ")")

          If (SelectedRow.Length > 0) Then
            thisStatusRow = SelectedRow(0)
            StatusID = thisStatusRow.StatusID
          Else
            thisStatusRow = ItemStatusDS.tblFlorenceStatus.NewtblFlorenceStatusRow
            StatusID = 0
            IsNewRow = True
          End If

          If (IsNewRow) Then
            thisStatusRow.StatusID = 0
            thisStatusRow.EntityID = EntityID
            thisStatusRow.ItemID = ItemID
          End If

          If (Combo_ItemStatus.SelectedIndex >= 0) AndAlso (IsNumeric(Combo_ItemStatus.SelectedValue)) Then
            thisStatusRow.ItemStatusID = CInt(Combo_ItemStatus.SelectedValue)
          Else
            thisStatusRow.ItemStatusID = RenaissanceGlobals.FlorenceStatusIDs.NoAction
          End If

          thisStatusRow.DateCompleted = Me.DT_ItemDateCompleted.Value.Date
          thisStatusRow.Comment = Me.Text_ItemComment.Text

          If (IsNewRow) Then
            ItemStatusDS.tblFlorenceStatus.Rows.Add(thisStatusRow)
          End If

          myAdaptor = MainForm.MainDataHandler.Get_Adaptor(RenaissanceGlobals.RenaissanceStandardDatasets.tblFlorenceStatus.Adaptorname)
          myAdaptor.InsertCommand.Parameters("@Knowledgedate").Value = MainForm.Main_Knowledgedate
          myAdaptor.UpdateCommand.Parameters("@Knowledgedate").Value = MainForm.Main_Knowledgedate

          MainForm.AdaptorUpdate(Me.Name, myAdaptor, New DataRow() {thisStatusRow})

          If (StatusID > 0) AndAlso (StatusID <> thisStatusRow.StatusID) Then
            ItemStatusDS = MainForm.Load_Table(RenaissanceGlobals.RenaissanceStandardDatasets.tblFlorenceStatus, True)
          End If

          _UpdatedStatusItem = True

          StatusID = thisStatusRow.StatusID

        Catch ex As Exception

          MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Failed to Save work item", ex.StackTrace, True)
        End Try


      End If
    End If

    If (DataItemChanged = True) Then
      DataItemChanged = False

      Dim DateToSaveFor As Date
      Dim DataValueSelectedRow() As RenaissanceDataClass.DSFlorenceDataValues.tblFlorenceDataValuesRow

      DateToSaveFor = DT_DateOfDataItem.Value ' AddPeriodToDate(DealingPeriod.Monthly, FitDateToPeriod(DealingPeriod.Monthly, Now.Date), -1)

      DataValueSelectedRow = DataValueTable.Select("(DataEntityID=" & EntityID.ToString & ") AND (DataItemID=" & ItemID.ToString & ") AND (DataValueDate='" & DateToSaveFor.ToString(QUERY_SHORTDATEFORMAT) & "')")
      If (DataValueSelectedRow.Length > 0) AndAlso (DataValueSelectedRow(0).DataValueDate.CompareTo(DateToSaveFor) >= 0) Then
        thisDataRow = DataValueSelectedRow(0)
      Else
        thisDataRow = DataValueTable.NewRow
        thisDataRow.DataEntityID = EntityID
        thisDataRow.DataItemID = ItemID
        thisDataRow.DataValueDate = DateToSaveFor
        thisDataRow.DataValueType = DataItemType
        thisDataRow.SetTextDataNull()
        thisDataRow.SetDateDataNull()
        thisDataRow.NumericData = 0
        thisDataRow.BooleanData = False
      End If

      thisDataRow.SetTextDataNull()
      thisDataRow.SetDateDataNull()
      thisDataRow.NumericData = 0
      thisDataRow.PercentageData = 0
      thisDataRow.BooleanData = False

      If (DataItemType And RenaissanceDataType.NumericType) Then
        thisDataRow.NumericData = Numeric_DataItem.Value
      End If
      If (DataItemType And RenaissanceDataType.PercentageType) Then
        thisDataRow.PercentageData = Percentage_DataItem.Value
      End If
      If (DataItemType And RenaissanceDataType.TextType) Then
        thisDataRow.TextData = Text_DataItem.Text
      End If
      If (DataItemType And RenaissanceDataType.BooleanType) Then
        thisDataRow.BooleanData = Check_DataItem.Checked
      End If
      If (DataItemType And RenaissanceDataType.DateType) Then
        thisDataRow.DateData = DT_DataItem.Value.Date
      End If

      If thisDataRow.RowState = DataRowState.Detached Then
        DataValueTable.Rows.Add(thisDataRow)
      End If

      DataValueAdaptor.InsertCommand.Parameters("@Knowledgedate").Value = MainForm.Main_Knowledgedate
      DataValueAdaptor.UpdateCommand.Parameters("@Knowledgedate").Value = MainForm.Main_Knowledgedate

      _UpdatedDataItem = True

    End If

    ' Propogate Changes....

    If (_UpdatedStatusItem) Then
      GetFormData(thisStatusRow)

      ' Propagate changes

      Call MainForm.Main_RaiseEvent(New RenaissanceGlobals.RenaissanceUpdateEventArgs(RenaissanceGlobals.RenaissanceChangeID.tblFlorenceStatus))

    End If

    If (_UpdatedDataItem) Then
      Call MainForm.AdaptorUpdate(Me.Name, DataValueAdaptor, New DSFlorenceDataValues.tblFlorenceDataValuesRow() {thisDataRow})

      ' Propagate changes

      Call MainForm.Main_RaiseEvent(New RenaissanceGlobals.RenaissanceUpdateEventArgs(RenaissanceGlobals.RenaissanceChangeID.tblFlorenceDataValues))
    End If

    Return StatusID

  End Function





End Class
