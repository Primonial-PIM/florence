' ***********************************************************************
' Assembly         : Florence
' Author           : Nicholas Pennington
' Created          : 11-28-2012
'
' Last Modified By : acullig
' Last Modified On : 11-05-2012
' ***********************************************************************
' <copyright file="FlorenceSplash.vb" company="">
'     Copyright (c) . All rights reserved.
' </copyright>
' <summary></summary>
' ***********************************************************************
''' <summary>
''' Class FlorenceSplash
''' </summary>
Public NotInheritable Class FlorenceSplash


    ''' <summary>
    ''' Handles the Load event of the FlorenceSplash control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub FlorenceSplash_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		''    Version.Text = System.String.Format(Version.Text, My.Application.Info.Version.Major, My.Application.Info.Version.Minor, My.Application.Info.Version.Build, My.Application.Info.Version.Revision)

		'Version.Text = System.String.Format(Version.Text, My.Application.Info.Version.Major, My.Application.Info.Version.Minor)

		''Copyright info
		'Copyright.Text = My.Application.Info.Copyright

  End Sub

    ''' <summary>
    ''' The end time
    ''' </summary>
  Public EndTime As Date

    ''' <summary>
    ''' Handles the FormClosing event of the Splash control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.Windows.Forms.FormClosingEventArgs"/> instance containing the event data.</param>
	Private Sub Splash_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
		Try
			ClosingTimer.Stop()
		Catch ex As Exception
		End Try
	End Sub

    ''' <summary>
    ''' Starts the timer.
    ''' </summary>
    ''' <param name="pEndTime">The p end time.</param>
  Public Sub StartTimer(ByVal pEndTime As Date)
    EndTime = pEndTime

    Me.ClosingTimer.Start()

  End Sub
 
    ''' <summary>
    ''' Handles the Tick event of the ClosingTimer control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub ClosingTimer_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ClosingTimer.Tick
		If (Now > EndTime) Then
			Me.ClosingTimer.Stop()
			Me.Close()
		End If
  End Sub
End Class
