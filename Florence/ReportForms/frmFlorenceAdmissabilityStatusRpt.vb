' ***********************************************************************
' Assembly         : Florence
' Author           : Nicholas Pennington
' Created          : 11-28-2012
'
' Last Modified By : acullig
' Last Modified On : 11-19-2012
' ***********************************************************************
' <copyright file="frmFlorenceAdmissabilityStatusRpt.vb" company="">
'     Copyright (c) . All rights reserved.
' </copyright>
' <summary></summary>
' ***********************************************************************
Imports System.Data.SqlClient
Imports RenaissanceGlobals

''' <summary>
''' Class frmFlorenceAdmissibilityStatusRpt
''' </summary>
Public Class frmFlorenceAdmissibilityStatusRpt

	Inherits System.Windows.Forms.Form
	Implements StandardFlorenceForm

#Region " Windows Form Designer generated code "

    ''' <summary>
    ''' Prevents a default instance of the <see cref="frmFlorenceAdmissibilityStatusRpt"/> class from being created.
    ''' </summary>
	Private Sub New()
		MyBase.New()

		'This call is required by the Windows Form Designer.
		InitializeComponent()

		'Add any initialization after the InitializeComponent() call

	End Sub

	'Form overrides dispose to clean up the component list.
    ''' <summary>
    ''' Disposes of the resources (other than memory) used by the <see cref="T:System.Windows.Forms.Form" />.
    ''' </summary>
    ''' <param name="disposing">true to release both managed and unmanaged resources; false to release only unmanaged resources.</param>
	Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
		If disposing Then
			If Not (components Is Nothing) Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(disposing)
	End Sub

	'Required by the Windows Form Designer
    ''' <summary>
    ''' The components
    ''' </summary>
	Private components As System.ComponentModel.IContainer

	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.  
	'Do not modify it using the code editor.
    ''' <summary>
    ''' The BTN run report
    ''' </summary>
	Friend WithEvents btnRunReport As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN close
    ''' </summary>
	Friend WithEvents btnClose As System.Windows.Forms.Button
    ''' <summary>
    ''' The label2
    ''' </summary>
	Friend WithEvents Label2 As System.Windows.Forms.Label
    ''' <summary>
    ''' The combo_ entity
    ''' </summary>
	Friend WithEvents Combo_Entity As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The label1
    ''' </summary>
	Friend WithEvents Label1 As System.Windows.Forms.Label
    ''' <summary>
    ''' The status1
    ''' </summary>
	Friend WithEvents Status1 As System.Windows.Forms.StatusStrip
    ''' <summary>
    ''' The status label1
    ''' </summary>
	Friend WithEvents StatusLabel1 As System.Windows.Forms.ToolStripStatusLabel
    ''' <summary>
    ''' The tool strip progress bar1
    ''' </summary>
	Friend WithEvents ToolStripProgressBar1 As System.Windows.Forms.ToolStripProgressBar
    ''' <summary>
    ''' The RPT version panel
    ''' </summary>
	Friend WithEvents RptVersionPanel As System.Windows.Forms.Panel
    ''' <summary>
    ''' The radio_ file status report
    ''' </summary>
	Friend WithEvents Radio_FileStatusReport As System.Windows.Forms.RadioButton
    ''' <summary>
    ''' The radio_ compact status report
    ''' </summary>
	Friend WithEvents Radio_CompactStatusReport As System.Windows.Forms.RadioButton
    ''' <summary>
    ''' The combo_ fund
    ''' </summary>
	Friend WithEvents Combo_Fund As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The label3
    ''' </summary>
	Friend WithEvents Label3 As System.Windows.Forms.Label
    ''' <summary>
    ''' The combo_ group
    ''' </summary>
	Friend WithEvents Combo_Group As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' Initializes the component.
    ''' </summary>
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Me.btnRunReport = New System.Windows.Forms.Button
		Me.btnClose = New System.Windows.Forms.Button
		Me.Label2 = New System.Windows.Forms.Label
		Me.Combo_Group = New System.Windows.Forms.ComboBox
		Me.Combo_Entity = New System.Windows.Forms.ComboBox
		Me.Label1 = New System.Windows.Forms.Label
		Me.Status1 = New System.Windows.Forms.StatusStrip
		Me.ToolStripProgressBar1 = New System.Windows.Forms.ToolStripProgressBar
		Me.StatusLabel1 = New System.Windows.Forms.ToolStripStatusLabel
		Me.RptVersionPanel = New System.Windows.Forms.Panel
		Me.Radio_FileStatusReport = New System.Windows.Forms.RadioButton
		Me.Radio_CompactStatusReport = New System.Windows.Forms.RadioButton
		Me.Combo_Fund = New System.Windows.Forms.ComboBox
		Me.Label3 = New System.Windows.Forms.Label
		Me.Status1.SuspendLayout()
		Me.RptVersionPanel.SuspendLayout()
		Me.SuspendLayout()
		'
		'btnRunReport
		'
		Me.btnRunReport.Location = New System.Drawing.Point(50, 139)
		Me.btnRunReport.Name = "btnRunReport"
		Me.btnRunReport.Size = New System.Drawing.Size(148, 28)
		Me.btnRunReport.TabIndex = 4
		Me.btnRunReport.Text = "Run Report"
		'
		'btnClose
		'
		Me.btnClose.Location = New System.Drawing.Point(266, 139)
		Me.btnClose.Name = "btnClose"
		Me.btnClose.Size = New System.Drawing.Size(75, 28)
		Me.btnClose.TabIndex = 5
		Me.btnClose.Text = "&Close"
		'
		'Label2
		'
		Me.Label2.Location = New System.Drawing.Point(16, 41)
		Me.Label2.Name = "Label2"
		Me.Label2.Size = New System.Drawing.Size(77, 20)
		Me.Label2.TabIndex = 53
		Me.Label2.Text = "Group"
		'
		'Combo_Group
		'
		Me.Combo_Group.Location = New System.Drawing.Point(99, 37)
		Me.Combo_Group.Name = "Combo_Group"
		Me.Combo_Group.Size = New System.Drawing.Size(293, 21)
		Me.Combo_Group.TabIndex = 1
		'
		'Combo_Entity
		'
		Me.Combo_Entity.Location = New System.Drawing.Point(99, 64)
		Me.Combo_Entity.Name = "Combo_Entity"
		Me.Combo_Entity.Size = New System.Drawing.Size(293, 21)
		Me.Combo_Entity.TabIndex = 2
		'
		'Label1
		'
		Me.Label1.Location = New System.Drawing.Point(16, 68)
		Me.Label1.Name = "Label1"
		Me.Label1.Size = New System.Drawing.Size(77, 20)
		Me.Label1.TabIndex = 68
		Me.Label1.Text = "Entity"
		'
		'Status1
		'
		Me.Status1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripProgressBar1, Me.StatusLabel1})
		Me.Status1.Location = New System.Drawing.Point(0, 178)
		Me.Status1.Name = "Status1"
		Me.Status1.Size = New System.Drawing.Size(402, 22)
		Me.Status1.TabIndex = 69
		Me.Status1.Text = "StatusStrip1"
		'
		'ToolStripProgressBar1
		'
		Me.ToolStripProgressBar1.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
		Me.ToolStripProgressBar1.Maximum = 20
		Me.ToolStripProgressBar1.Name = "ToolStripProgressBar1"
		Me.ToolStripProgressBar1.Size = New System.Drawing.Size(150, 16)
		Me.ToolStripProgressBar1.Step = 1
		Me.ToolStripProgressBar1.Visible = False
		'
		'StatusLabel1
		'
		Me.StatusLabel1.Name = "StatusLabel1"
		Me.StatusLabel1.Size = New System.Drawing.Size(111, 17)
		Me.StatusLabel1.Text = "ToolStripStatusLabel1"
		'
		'RptVersionPanel
		'
		Me.RptVersionPanel.Controls.Add(Me.Radio_FileStatusReport)
		Me.RptVersionPanel.Controls.Add(Me.Radio_CompactStatusReport)
		Me.RptVersionPanel.Location = New System.Drawing.Point(19, 95)
		Me.RptVersionPanel.Name = "RptVersionPanel"
		Me.RptVersionPanel.Size = New System.Drawing.Size(372, 25)
		Me.RptVersionPanel.TabIndex = 3
		'
		'Radio_FileStatusReport
		'
		Me.Radio_FileStatusReport.AutoSize = True
		Me.Radio_FileStatusReport.Location = New System.Drawing.Point(210, 3)
		Me.Radio_FileStatusReport.Name = "Radio_FileStatusReport"
		Me.Radio_FileStatusReport.Size = New System.Drawing.Size(133, 17)
		Me.Radio_FileStatusReport.TabIndex = 1
		Me.Radio_FileStatusReport.TabStop = True
		Me.Radio_FileStatusReport.Text = "D/D File Status Report"
		Me.Radio_FileStatusReport.UseVisualStyleBackColor = True
		'
		'Radio_CompactStatusReport
		'
		Me.Radio_CompactStatusReport.AutoSize = True
		Me.Radio_CompactStatusReport.Location = New System.Drawing.Point(37, 3)
		Me.Radio_CompactStatusReport.Name = "Radio_CompactStatusReport"
		Me.Radio_CompactStatusReport.Size = New System.Drawing.Size(135, 17)
		Me.Radio_CompactStatusReport.TabIndex = 0
		Me.Radio_CompactStatusReport.TabStop = True
		Me.Radio_CompactStatusReport.Text = "Compact Status Report"
		Me.Radio_CompactStatusReport.UseVisualStyleBackColor = True
		'
		'Combo_Fund
		'
		Me.Combo_Fund.Location = New System.Drawing.Point(99, 10)
		Me.Combo_Fund.Name = "Combo_Fund"
		Me.Combo_Fund.Size = New System.Drawing.Size(293, 21)
		Me.Combo_Fund.TabIndex = 0
		'
		'Label3
		'
		Me.Label3.Location = New System.Drawing.Point(16, 14)
		Me.Label3.Name = "Label3"
		Me.Label3.Size = New System.Drawing.Size(77, 20)
		Me.Label3.TabIndex = 71
		Me.Label3.Text = "PAM Fund"
		'
		'frmFlorenceAdmissibilityStatusRpt
		'
		Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
		Me.ClientSize = New System.Drawing.Size(402, 200)
		Me.Controls.Add(Me.Combo_Fund)
		Me.Controls.Add(Me.Label3)
		Me.Controls.Add(Me.RptVersionPanel)
		Me.Controls.Add(Me.Status1)
		Me.Controls.Add(Me.Combo_Entity)
		Me.Controls.Add(Me.Label1)
		Me.Controls.Add(Me.Combo_Group)
		Me.Controls.Add(Me.Label2)
		Me.Controls.Add(Me.btnClose)
		Me.Controls.Add(Me.btnRunReport)
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
		Me.MaximizeBox = False
		Me.MinimizeBox = False
		Me.Name = "frmFlorenceAdmissibilityStatusRpt"
		Me.Text = "Due Dilligence Status Report"
		Me.Status1.ResumeLayout(False)
		Me.Status1.PerformLayout()
		Me.RptVersionPanel.ResumeLayout(False)
		Me.RptVersionPanel.PerformLayout()
		Me.ResumeLayout(False)
		Me.PerformLayout()

	End Sub

#End Region

#Region " Form Locals and Constants "


	' Form 'Parent', the Main Venice form.
	' Generally only accessed through the 'MainForm' property.
    ''' <summary>
    ''' The _ main form
    ''' </summary>
	Private WithEvents _MainForm As FlorenceMain

	' Form ToolTip
    ''' <summary>
    ''' The form tooltip
    ''' </summary>
	Private FormTooltip As New ToolTip()

	' Form Constants, specific to the table being updated.

    ''' <summary>
    ''' The ALWAY s_ CLOS e_ THI s_ FORM
    ''' </summary>
	Private ALWAYS_CLOSE_THIS_FORM As Boolean = True

	' The standard ChangeID for this form. e.g. tblPerson
    ''' <summary>
    ''' The THI s_ FOR m_ change ID
    ''' </summary>
	Private THIS_FORM_ChangeID As RenaissanceGlobals.RenaissanceChangeID

	' Form specific Permissioning variables
    ''' <summary>
    ''' The THI s_ FOR m_ permission area
    ''' </summary>
	Private THIS_FORM_PermissionArea As String
    ''' <summary>
    ''' The THI s_ FOR m_ permission type
    ''' </summary>
	Private THIS_FORM_PermissionType As RenaissanceGlobals.PermissionFeatureType

	' Form specific Form type 
    ''' <summary>
    ''' The THI s_ FOR m_ form ID
    ''' </summary>
	Private THIS_FORM_FormID As FlorenceFormID

	' Form Status Flags

    ''' <summary>
    ''' The form is valid
    ''' </summary>
	Private FormIsValid As Boolean
    ''' <summary>
    ''' The form changed
    ''' </summary>
	Private FormChanged As Boolean
    ''' <summary>
    ''' The _ form open failed
    ''' </summary>
	Private _FormOpenFailed As Boolean

	' User Permission Flags

    ''' <summary>
    ''' The has read permission
    ''' </summary>
	Private HasReadPermission As Boolean
    ''' <summary>
    ''' The has update permission
    ''' </summary>
	Private HasUpdatePermission As Boolean
    ''' <summary>
    ''' The has insert permission
    ''' </summary>
	Private HasInsertPermission As Boolean
    ''' <summary>
    ''' The has delete permission
    ''' </summary>
	Private HasDeletePermission As Boolean

#End Region

#Region " Form 'Properties' "

    ''' <summary>
    ''' Gets the main form.
    ''' </summary>
    ''' <value>The main form.</value>
	Public ReadOnly Property MainForm() As FlorenceMain Implements Globals.StandardFlorenceForm.MainForm
		' Public property to return handle to the 'Main' Venice form, where in reside most of the 
		' data structures and many common utilities.
		Get
			Return _MainForm
		End Get
	End Property

    ''' <summary>
    ''' Gets or sets a value indicating whether this instance is over cancel button.
    ''' </summary>
    ''' <value><c>true</c> if this instance is over cancel button; otherwise, <c>false</c>.</value>
	Public Property IsOverCancelButton() As Boolean Implements Globals.StandardFlorenceForm.IsOverCancelButton
		' Public property maintaining a value indicating if the cursor is over the 'Cancel'
		' Button on this form.
		' This property is specifically designed for use by the field formating Event functions
		' In order that they do not impose format restrictions if the user is about to click the 
		' 'Cancel' button.
		'
		Get
			Return False
		End Get
		Set(ByVal Value As Boolean)
		End Set
	End Property

    ''' <summary>
    ''' Gets a value indicating whether this instance is in paint.
    ''' </summary>
    ''' <value><c>true</c> if this instance is in paint; otherwise, <c>false</c>.</value>
	Public ReadOnly Property IsInPaint() As Boolean Implements Globals.StandardFlorenceForm.IsInPaint
		Get
			Return False
		End Get
	End Property

    ''' <summary>
    ''' Gets a value indicating whether [in use].
    ''' </summary>
    ''' <value><c>true</c> if [in use]; otherwise, <c>false</c>.</value>
	Public ReadOnly Property InUse() As Boolean Implements Globals.StandardFlorenceForm.InUse
		Get
			Return True
		End Get
	End Property

    ''' <summary>
    ''' Gets a value indicating whether [form open failed].
    ''' </summary>
    ''' <value><c>true</c> if [form open failed]; otherwise, <c>false</c>.</value>
	Public ReadOnly Property FormOpenFailed() As Boolean Implements Globals.StandardFlorenceForm.FormOpenFailed
		Get
			Return _FormOpenFailed
		End Get
	End Property

#End Region

    ''' <summary>
    ''' Initializes a new instance of the <see cref="frmFlorenceAdmissibilityStatusRpt"/> class.
    ''' </summary>
    ''' <param name="pMainForm">The p main form.</param>
	Public Sub New(ByVal pMainForm As FlorenceMain)
		' *************************************************************
		' Custom 'New'. 
		' Passes in the reference to the parent form.
		' 
		' Establishes form specific variables.
		' Establishes Form specific Data connection / data structures.
		'
		' *************************************************************

		Me.New()

		_MainForm = pMainForm
		AddHandler _MainForm.FlorenceAutoUpdate, AddressOf Me.AutoUpdate

		_FormOpenFailed = False

		' ******************************************************
		' Form Specific Settings :
		' ******************************************************

		' Form Permissioning :-

		THIS_FORM_PermissionArea = Me.Name
		THIS_FORM_PermissionType = RenaissanceGlobals.PermissionFeatureType.TypeForm

		' 'This' form ID

		THIS_FORM_FormID = FlorenceFormID.frmFlorenceAdmissibilityStatusRpt

		' Format Event Handlers for form controls

		AddHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

		AddHandler Combo_Fund.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
		AddHandler Combo_Fund.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
		AddHandler Combo_Fund.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
		AddHandler Combo_Fund.KeyUp, AddressOf MainForm.ComboSelectAsYouType_NoMatch

		AddHandler Combo_Group.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
		AddHandler Combo_Group.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
		AddHandler Combo_Group.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
		AddHandler Combo_Group.KeyUp, AddressOf MainForm.ComboSelectAsYouType_NoMatch

		AddHandler Combo_Entity.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
		AddHandler Combo_Entity.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
		AddHandler Combo_Entity.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
		AddHandler Combo_Entity.KeyUp, AddressOf MainForm.ComboSelectAsYouType_NoMatch

		' Set up the ToolTip
		MainForm.SetFormToolTip(Me, FormTooltip)

		' ******************************************************
		' End Form Specific.
		' ******************************************************

		THIS_FORM_ChangeID = RenaissanceChangeID.KnowledgeDate

	End Sub

#Region " This Form Event handlers : FormLoad / FormClose "

	' Form Initialisation code.
	'
    ''' <summary>
    ''' Resets the form.
    ''' </summary>
	Public Sub ResetForm() Implements StandardFlorenceForm.ResetForm

		Call Form_Load(Me, New System.EventArgs)
	End Sub

    ''' <summary>
    ''' Closes the form.
    ''' </summary>
	Public Sub CloseForm() Implements StandardFlorenceForm.CloseForm
		ALWAYS_CLOSE_THIS_FORM = True
		Me.Close()
	End Sub

    ''' <summary>
    ''' Handles the Load event of the Form control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

		Me.MainForm.LogError(Me.Name, LOG_LEVELS.Audit, "", "Form Loaded", "", False)
		_FormOpenFailed = False

		If Not (MainForm Is Nothing) Then
			FormIsValid = True
		Else
			MessageBox.Show("'Main' form is not set. Closing form", "no Main Form", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
			FormIsValid = False
			_FormOpenFailed = True
			Exit Sub
		End If


		' Check User permissions
		Call CheckPermissions()
		If (HasReadPermission = False) Then
			Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User does not have Read permission for this Form..", "", True)

			FormIsValid = False
			_FormOpenFailed = True
			Exit Sub
		End If

		' Build Combos

		Try

			Call SetFundCombo()
			Call SetGroupCombo()
			Call SetEntityCombo()

			If (Combo_Group.Items.Count > 0) Then
				Combo_Group.SelectedIndex = 0
			End If

			If (Me.Combo_Entity.Items.Count > 0) Then
				Combo_Entity.SelectedIndex = 0
			End If

			MainForm.SetComboSelectionLengths(Me)

			Me.Radio_CompactStatusReport.Checked = True

		Catch ex As Exception
		End Try

	End Sub

    ''' <summary>
    ''' Handles the Closing event of the Form control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.ComponentModel.CancelEventArgs"/> instance containing the event data.</param>
	Private Sub Form_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
		Dim HideForm As Boolean

		' Hide or Close this form ?
		' All depends on how many of this form type are Open or in Cache...


		If (ALWAYS_CLOSE_THIS_FORM = True) Or (FormIsValid = False) Then
			HideForm = False
		Else
			HideForm = True
			If MainForm.FlorenceForms.CountOf(THIS_FORM_FormID) > MainForm.ReportForm_CacheCount Then
				HideForm = False
			End If
		End If

		If HideForm = True Then
			MainForm.HideInFormsCollection(Me)
			Me.Hide()	' NPP Fix

			e.Cancel = True
		Else
			Try
				MainForm.RemoveFromFormsCollection(Me)
				RemoveHandler _MainForm.FlorenceAutoUpdate, AddressOf Me.AutoUpdate

				RemoveHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

				RemoveHandler Combo_Fund.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
				RemoveHandler Combo_Fund.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
				RemoveHandler Combo_Fund.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
				RemoveHandler Combo_Fund.KeyUp, AddressOf MainForm.ComboSelectAsYouType_NoMatch

				RemoveHandler Combo_Group.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
				RemoveHandler Combo_Group.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
				RemoveHandler Combo_Group.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
				RemoveHandler Combo_Group.KeyUp, AddressOf MainForm.ComboSelectAsYouType_NoMatch

				RemoveHandler Combo_Entity.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
				RemoveHandler Combo_Entity.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
				RemoveHandler Combo_Entity.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
				RemoveHandler Combo_Entity.KeyUp, AddressOf MainForm.ComboSelectAsYouType_NoMatch

			Catch ex As Exception
			End Try
		End If

	End Sub


#End Region

	' Routine to handle changes / updates to tables by this and other windows.
	' If this, or any other, form posts a change to a table, then it will invoke an update event 
	' detailing what tables have been altered.
	' Each Form will receive this event through this 'AutoUpdate' routine whice is configured to handle
	' the 'FlorenceAutoUpdate' event of the main Venice form.
	' Each form may them react as appropriate to changes in any table that might impact it.
	'
    ''' <summary>
    ''' Autoes the update.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="RenaissanceGlobals.RenaissanceUpdateEventArgs"/> instance containing the event data.</param>
	Private Sub AutoUpdate(ByVal sender As System.Object, ByVal e As RenaissanceGlobals.RenaissanceUpdateEventArgs)
		Dim KnowledgeDateChanged As Boolean
		Dim SetButtonStatus_Flag As Boolean

		If (Me.IsDisposed) OrElse (Me.Disposing) OrElse (Me.InUse = False) Then Exit Sub

		KnowledgeDateChanged = False
		SetButtonStatus_Flag = False

		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True) Or (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.Connection) = True) Then
			KnowledgeDateChanged = True
		End If

		' ****************************************************************
		' Check for changes relevant to this form
		' ****************************************************************

		' Changes to the KnowledgeDate :-
		If e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True Then
			SetButtonStatus_Flag = True
		End If

		' Changes to the tblFlorenceGroup table :-
		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblFlorenceGroup) = True) Or KnowledgeDateChanged Then

			' Re-Set combo.
			Call SetGroupCombo()
		End If

		' Changes to the tblFlorenceItems table :-
		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblFlorenceItems) = True) Or KnowledgeDateChanged Then

			' Re-Set combo.
			Call SetEntityCombo()
		End If


		' Changes to the tblUserPermissions table :-
		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblUserPermissions) = True) Or KnowledgeDateChanged Then

			' Check ongoing permissions.

			Call CheckPermissions()
			If (HasReadPermission = False) Then
				Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User has lost Read permission for this Form.", "", True)

				FormIsValid = False
				Me.Close()
				Exit Sub
			End If

			SetButtonStatus_Flag = True

		End If

		' ****************************************************************
		'
		' ****************************************************************


	End Sub

#Region " Workhorse functions : SetSortedRows / CheckPermission / ControlsChanged / Menu Events "

	' Check User permissions
    ''' <summary>
    ''' Checks the permissions.
    ''' </summary>
	Private Sub CheckPermissions()
		' *****************************************************************************
		'
		' *****************************************************************************

		Dim Permissions As Integer

		Permissions = MainForm.CheckPermissions(THIS_FORM_PermissionArea, THIS_FORM_PermissionType)

		HasReadPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermRead) > 0)
		HasUpdatePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermUpdate) > 0)
		HasInsertPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermInsert) > 0)
		HasDeletePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermDelete) > 0)

	End Sub


#End Region

#Region " Set Form Combos (Form Specific Code) "

	' SetFundCombo
    ''' <summary>
    ''' Sets the fund combo.
    ''' </summary>
	Private Sub SetFundCombo()

		Call MainForm.SetTblGenericCombo( _
		Me.Combo_Fund, _
		RenaissanceStandardDatasets.tblFund, _
		"FundName", _
		"FundID", _
		"(FundParentFund = FundID) OR (FundParentFund = 0)", False, True, True, 0, "All Funds")	 ' 

	End Sub

    ''' <summary>
    ''' Sets the group combo.
    ''' </summary>
	Private Sub SetGroupCombo()

		Call MainForm.SetTblGenericCombo( _
		Me.Combo_Group, _
		RenaissanceStandardDatasets.tblFlorenceGroup, _
		"ItemGroupDescription", _
		"ItemGroup", _
		"", False, True, True, 0, "All Groups")	 ' 

	End Sub

    ''' <summary>
    ''' Sets the entity combo.
    ''' </summary>
	Private Sub SetEntityCombo()

		Call MainForm.SetTblGenericCombo( _
		Me.Combo_Entity, _
		RenaissanceStandardDatasets.tblFlorenceEntity, _
		"EntityName,ItemGroupID", _
		"EntityID", _
		"", False, True, True, 0, "All Entities")	 ' 

	End Sub


#End Region

#Region " Buttons : Add / Delete / Cancel / Save and Close (Generic Code) "


    ''' <summary>
    ''' Handles the Click event of the btnRunReport control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnRunReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRunReport.Click
		' *****************************************************************************
		' Run Report
		' *****************************************************************************

		Dim FundID As Integer = 0
		Dim GroupID As Integer = 0
		Dim EntityID As Integer = 0

		Try
			Me.btnClose.Enabled = False
			Me.btnRunReport.Enabled = False

			Me.StatusLabel1.Text = "Validating Form"
			Me.Refresh()
			Application.DoEvents()

			If (Me.Combo_Fund.SelectedIndex > 0) AndAlso (IsNumeric(Me.Combo_Fund.SelectedValue) = True) Then
				FundID = CInt(Combo_Fund.SelectedValue)
			End If

			If (Me.Combo_Group.SelectedIndex > 0) AndAlso (IsNumeric(Me.Combo_Group.SelectedValue) = True) Then
				GroupID = CInt(Combo_Group.SelectedValue)
			End If

			If (Me.Combo_Entity.SelectedIndex > 0) AndAlso (IsNumeric(Me.Combo_Entity.SelectedValue) = True) Then
				EntityID = CInt(Combo_Entity.SelectedValue)
			End If

			Me.StatusLabel1.Text = "Processing Report"
			Me.Refresh()
			Application.DoEvents()

			If (Radio_FileStatusReport.Checked) Then
        MainForm.MainReportHandler.AdmissiblilityStatusReport("rptFlorenceAdmissibilityStatusReport", FundID, GroupID, EntityID, "True", ToolStripProgressBar1)
			Else
        MainForm.MainReportHandler.AdmissiblilityStatusReport("rptFlorenceAdmissibilityStatusReport", FundID, GroupID, EntityID, "True", ToolStripProgressBar1)
			End If

		Catch ex As Exception

		Finally
			Me.btnClose.Enabled = True
			Me.btnRunReport.Enabled = True

		End Try

		Me.StatusLabel1.Text = ""
		Me.btnRunReport.Enabled = True

	End Sub


    ''' <summary>
    ''' Handles the Click event of the btnClose control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
		' *****************************************************************************
		' Close Form
		' *****************************************************************************

		Me.Close()

	End Sub

#End Region


End Class
