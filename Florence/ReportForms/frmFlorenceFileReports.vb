' ***********************************************************************
' Assembly         : Florence
' Author           : Nicholas Pennington
' Created          : 11-28-2012
'
' Last Modified By : acullig
' Last Modified On : 11-05-2012
' ***********************************************************************
' <copyright file="frmFlorenceFileReports.vb" company="">
'     Copyright (c) . All rights reserved.
' </copyright>
' <summary></summary>
' ***********************************************************************
Imports System.Data.SqlClient
Imports RenaissanceGlobals

''' <summary>
''' Class frmFlorenceFileReports
''' </summary>
Public Class frmFlorenceFileReports

  Inherits System.Windows.Forms.Form
  Implements StandardFlorenceForm

#Region " Windows Form Designer generated code "

    ''' <summary>
    ''' Prevents a default instance of the <see cref="frmFlorenceFileReports"/> class from being created.
    ''' </summary>
  Private Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
    ''' <summary>
    ''' Disposes of the resources (other than memory) used by the <see cref="T:System.Windows.Forms.Form" />.
    ''' </summary>
    ''' <param name="disposing">true to release both managed and unmanaged resources; false to release only unmanaged resources.</param>
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
    ''' <summary>
    ''' The components
    ''' </summary>
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
    ''' <summary>
    ''' The BTN cover insert report
    ''' </summary>
  Friend WithEvents btnCoverInsertReport As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN close
    ''' </summary>
  Friend WithEvents btnClose As System.Windows.Forms.Button
    ''' <summary>
    ''' The combo_ entity
    ''' </summary>
  Friend WithEvents Combo_Entity As System.Windows.Forms.ComboBox
    ''' <summary>
    ''' The label1
    ''' </summary>
  Friend WithEvents Label1 As System.Windows.Forms.Label
    ''' <summary>
    ''' The status1
    ''' </summary>
  Friend WithEvents Status1 As System.Windows.Forms.StatusStrip
    ''' <summary>
    ''' The status label1
    ''' </summary>
  Friend WithEvents StatusLabel1 As System.Windows.Forms.ToolStripStatusLabel
    ''' <summary>
    ''' The BTN_ key details report
    ''' </summary>
  Friend WithEvents btn_KeyDetailsReport As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN_ spine insert report
    ''' </summary>
  Friend WithEvents btn_SpineInsertReport As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN_ sign off sheet report
    ''' </summary>
  Friend WithEvents btn_SignOffSheetReport As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN_ file status report
    ''' </summary>
  Friend WithEvents btn_FileStatusReport As System.Windows.Forms.Button
    ''' <summary>
    ''' The BTN_ status index report
    ''' </summary>
  Friend WithEvents btn_StatusIndexReport As System.Windows.Forms.Button
    ''' <summary>
    ''' The panel1
    ''' </summary>
  Friend WithEvents Panel1 As System.Windows.Forms.Panel
    ''' <summary>
    ''' The BTN_ run all reports
    ''' </summary>
  Friend WithEvents btn_RunAllReports As System.Windows.Forms.Button
    ''' <summary>
    ''' The tool strip progress bar1
    ''' </summary>
  Friend WithEvents ToolStripProgressBar1 As System.Windows.Forms.ToolStripProgressBar
    ''' <summary>
    ''' Initializes the component.
    ''' </summary>
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Me.btnCoverInsertReport = New System.Windows.Forms.Button
		Me.btnClose = New System.Windows.Forms.Button
		Me.Combo_Entity = New System.Windows.Forms.ComboBox
		Me.Label1 = New System.Windows.Forms.Label
		Me.Status1 = New System.Windows.Forms.StatusStrip
		Me.ToolStripProgressBar1 = New System.Windows.Forms.ToolStripProgressBar
		Me.StatusLabel1 = New System.Windows.Forms.ToolStripStatusLabel
		Me.btn_KeyDetailsReport = New System.Windows.Forms.Button
		Me.btn_SpineInsertReport = New System.Windows.Forms.Button
		Me.btn_SignOffSheetReport = New System.Windows.Forms.Button
		Me.btn_FileStatusReport = New System.Windows.Forms.Button
		Me.btn_StatusIndexReport = New System.Windows.Forms.Button
		Me.Panel1 = New System.Windows.Forms.Panel
		Me.btn_RunAllReports = New System.Windows.Forms.Button
		Me.Status1.SuspendLayout()
		Me.Panel1.SuspendLayout()
		Me.SuspendLayout()
		'
		'btnCoverInsertReport
		'
		Me.btnCoverInsertReport.Location = New System.Drawing.Point(18, 56)
		Me.btnCoverInsertReport.Name = "btnCoverInsertReport"
		Me.btnCoverInsertReport.Size = New System.Drawing.Size(255, 28)
		Me.btnCoverInsertReport.TabIndex = 1
		Me.btnCoverInsertReport.Text = "Run Cover Page Report"
		'
		'btnClose
		'
		Me.btnClose.Location = New System.Drawing.Point(315, 56)
		Me.btnClose.Name = "btnClose"
		Me.btnClose.Size = New System.Drawing.Size(75, 28)
		Me.btnClose.TabIndex = 7
		Me.btnClose.Text = "&Close"
		'
		'Combo_Entity
		'
		Me.Combo_Entity.Location = New System.Drawing.Point(98, 12)
		Me.Combo_Entity.Name = "Combo_Entity"
		Me.Combo_Entity.Size = New System.Drawing.Size(293, 21)
		Me.Combo_Entity.TabIndex = 0
		'
		'Label1
		'
		Me.Label1.Location = New System.Drawing.Point(15, 16)
		Me.Label1.Name = "Label1"
		Me.Label1.Size = New System.Drawing.Size(77, 20)
		Me.Label1.TabIndex = 68
		Me.Label1.Text = "Entity"
		'
		'Status1
		'
		Me.Status1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripProgressBar1, Me.StatusLabel1})
		Me.Status1.Location = New System.Drawing.Point(0, 308)
		Me.Status1.Name = "Status1"
		Me.Status1.Size = New System.Drawing.Size(402, 22)
		Me.Status1.TabIndex = 69
		Me.Status1.Text = "StatusStrip1"
		'
		'ToolStripProgressBar1
		'
		Me.ToolStripProgressBar1.Maximum = 20
		Me.ToolStripProgressBar1.Name = "ToolStripProgressBar1"
		Me.ToolStripProgressBar1.Size = New System.Drawing.Size(150, 16)
		Me.ToolStripProgressBar1.Step = 1
		Me.ToolStripProgressBar1.Visible = False
		'
		'StatusLabel1
		'
		Me.StatusLabel1.Name = "StatusLabel1"
		Me.StatusLabel1.Size = New System.Drawing.Size(111, 17)
		Me.StatusLabel1.Text = "ToolStripStatusLabel1"
		'
		'btn_KeyDetailsReport
		'
		Me.btn_KeyDetailsReport.Location = New System.Drawing.Point(18, 124)
		Me.btn_KeyDetailsReport.Name = "btn_KeyDetailsReport"
		Me.btn_KeyDetailsReport.Size = New System.Drawing.Size(255, 28)
		Me.btn_KeyDetailsReport.TabIndex = 3
		Me.btn_KeyDetailsReport.Text = "Run Key Details Report"
		'
		'btn_SpineInsertReport
		'
		Me.btn_SpineInsertReport.Location = New System.Drawing.Point(18, 90)
		Me.btn_SpineInsertReport.Name = "btn_SpineInsertReport"
		Me.btn_SpineInsertReport.Size = New System.Drawing.Size(255, 28)
		Me.btn_SpineInsertReport.TabIndex = 2
		Me.btn_SpineInsertReport.Text = "Run Spine Insert Report"
		'
		'btn_SignOffSheetReport
		'
		Me.btn_SignOffSheetReport.Location = New System.Drawing.Point(18, 226)
		Me.btn_SignOffSheetReport.Name = "btn_SignOffSheetReport"
		Me.btn_SignOffSheetReport.Size = New System.Drawing.Size(255, 28)
		Me.btn_SignOffSheetReport.TabIndex = 6
		Me.btn_SignOffSheetReport.Text = "Run Sign Off Sheet Report"
		'
		'btn_FileStatusReport
		'
		Me.btn_FileStatusReport.Location = New System.Drawing.Point(18, 158)
		Me.btn_FileStatusReport.Name = "btn_FileStatusReport"
		Me.btn_FileStatusReport.Size = New System.Drawing.Size(255, 28)
		Me.btn_FileStatusReport.TabIndex = 4
		Me.btn_FileStatusReport.Text = "Run Status Report"
		'
		'btn_StatusIndexReport
		'
		Me.btn_StatusIndexReport.Location = New System.Drawing.Point(18, 192)
		Me.btn_StatusIndexReport.Name = "btn_StatusIndexReport"
		Me.btn_StatusIndexReport.Size = New System.Drawing.Size(255, 28)
		Me.btn_StatusIndexReport.TabIndex = 5
		Me.btn_StatusIndexReport.Text = "Run Status Index Report"
		'
		'Panel1
		'
		Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.Panel1.Controls.Add(Me.btn_RunAllReports)
		Me.Panel1.Location = New System.Drawing.Point(18, 260)
		Me.Panel1.Name = "Panel1"
		Me.Panel1.Size = New System.Drawing.Size(254, 40)
		Me.Panel1.TabIndex = 70
		'
		'btn_RunAllReports
		'
		Me.btn_RunAllReports.Location = New System.Drawing.Point(3, 5)
		Me.btn_RunAllReports.Name = "btn_RunAllReports"
		Me.btn_RunAllReports.Size = New System.Drawing.Size(244, 28)
		Me.btn_RunAllReports.TabIndex = 71
		Me.btn_RunAllReports.Text = "Run ALL these Reports"
		'
		'frmFlorenceFileReports
		'
		Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
		Me.ClientSize = New System.Drawing.Size(402, 330)
		Me.Controls.Add(Me.Panel1)
		Me.Controls.Add(Me.btn_StatusIndexReport)
		Me.Controls.Add(Me.btn_FileStatusReport)
		Me.Controls.Add(Me.btn_SignOffSheetReport)
		Me.Controls.Add(Me.btn_SpineInsertReport)
		Me.Controls.Add(Me.btn_KeyDetailsReport)
		Me.Controls.Add(Me.Status1)
		Me.Controls.Add(Me.Combo_Entity)
		Me.Controls.Add(Me.Label1)
		Me.Controls.Add(Me.btnClose)
		Me.Controls.Add(Me.btnCoverInsertReport)
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
		Me.MaximizeBox = False
		Me.MinimizeBox = False
		Me.Name = "frmFlorenceFileReports"
		Me.Text = "Due Dilligence File Reports"
		Me.Status1.ResumeLayout(False)
		Me.Status1.PerformLayout()
		Me.Panel1.ResumeLayout(False)
		Me.ResumeLayout(False)
		Me.PerformLayout()

	End Sub

#End Region

#Region " Form Locals and Constants "


  ' Form 'Parent', the Main Venice form.
  ' Generally only accessed through the 'MainForm' property.
    ''' <summary>
    ''' The _ main form
    ''' </summary>
  Private WithEvents _MainForm As FlorenceMain

  ' Form ToolTip
    ''' <summary>
    ''' The form tooltip
    ''' </summary>
  Private FormTooltip As New ToolTip()

  ' Form Constants, specific to the table being updated.

    ''' <summary>
    ''' The ALWAY s_ CLOS e_ THI s_ FORM
    ''' </summary>
  Private ALWAYS_CLOSE_THIS_FORM As Boolean = True

  ' The standard ChangeID for this form. e.g. tblPerson
    ''' <summary>
    ''' The THI s_ FOR m_ change ID
    ''' </summary>
  Private THIS_FORM_ChangeID As RenaissanceGlobals.RenaissanceChangeID

  ' Form specific Permissioning variables
    ''' <summary>
    ''' The THI s_ FOR m_ permission area
    ''' </summary>
  Private THIS_FORM_PermissionArea As String
    ''' <summary>
    ''' The THI s_ FOR m_ permission type
    ''' </summary>
  Private THIS_FORM_PermissionType As RenaissanceGlobals.PermissionFeatureType

  ' Form specific Form type 
    ''' <summary>
    ''' The THI s_ FOR m_ form ID
    ''' </summary>
  Private THIS_FORM_FormID As FlorenceFormID

  ' Form Status Flags

    ''' <summary>
    ''' The form is valid
    ''' </summary>
  Private FormIsValid As Boolean
    ''' <summary>
    ''' The form changed
    ''' </summary>
  Private FormChanged As Boolean
    ''' <summary>
    ''' The _ form open failed
    ''' </summary>
  Private _FormOpenFailed As Boolean

  ' User Permission Flags

    ''' <summary>
    ''' The has read permission
    ''' </summary>
  Private HasReadPermission As Boolean
    ''' <summary>
    ''' The has update permission
    ''' </summary>
  Private HasUpdatePermission As Boolean
    ''' <summary>
    ''' The has insert permission
    ''' </summary>
  Private HasInsertPermission As Boolean
    ''' <summary>
    ''' The has delete permission
    ''' </summary>
  Private HasDeletePermission As Boolean

#End Region

#Region " Form 'Properties' "

    ''' <summary>
    ''' Gets the main form.
    ''' </summary>
    ''' <value>The main form.</value>
  Public ReadOnly Property MainForm() As FlorenceMain Implements Globals.StandardFlorenceForm.MainForm
    ' Public property to return handle to the 'Main' Venice form, where in reside most of the 
    ' data structures and many common utilities.
    Get
      Return _MainForm
    End Get
  End Property

    ''' <summary>
    ''' Gets or sets a value indicating whether this instance is over cancel button.
    ''' </summary>
    ''' <value><c>true</c> if this instance is over cancel button; otherwise, <c>false</c>.</value>
  Public Property IsOverCancelButton() As Boolean Implements Globals.StandardFlorenceForm.IsOverCancelButton
    ' Public property maintaining a value indicating if the cursor is over the 'Cancel'
    ' Button on this form.
    ' This property is specifically designed for use by the field formating Event functions
    ' In order that they do not impose format restrictions if the user is about to click the 
    ' 'Cancel' button.
    '
    Get
      Return False
    End Get
    Set(ByVal Value As Boolean)
    End Set
  End Property

    ''' <summary>
    ''' Gets a value indicating whether this instance is in paint.
    ''' </summary>
    ''' <value><c>true</c> if this instance is in paint; otherwise, <c>false</c>.</value>
  Public ReadOnly Property IsInPaint() As Boolean Implements Globals.StandardFlorenceForm.IsInPaint
    Get
      Return False
    End Get
  End Property

    ''' <summary>
    ''' Gets a value indicating whether [in use].
    ''' </summary>
    ''' <value><c>true</c> if [in use]; otherwise, <c>false</c>.</value>
  Public ReadOnly Property InUse() As Boolean Implements Globals.StandardFlorenceForm.InUse
    Get
      Return True
    End Get
  End Property

    ''' <summary>
    ''' Gets a value indicating whether [form open failed].
    ''' </summary>
    ''' <value><c>true</c> if [form open failed]; otherwise, <c>false</c>.</value>
  Public ReadOnly Property FormOpenFailed() As Boolean Implements Globals.StandardFlorenceForm.FormOpenFailed
    Get
      Return _FormOpenFailed
    End Get
  End Property

#End Region

    ''' <summary>
    ''' Initializes a new instance of the <see cref="frmFlorenceFileReports"/> class.
    ''' </summary>
    ''' <param name="pMainForm">The p main form.</param>
  Public Sub New(ByVal pMainForm As FlorenceMain)
    ' *************************************************************
    ' Custom 'New'. 
    ' Passes in the reference to the parent form.
    ' 
    ' Establishes form specific variables.
    ' Establishes Form specific Data connection / data structures.
    '
    ' *************************************************************

    Me.New()

    _MainForm = pMainForm
    AddHandler _MainForm.FlorenceAutoUpdate, AddressOf Me.AutoUpdate

    _FormOpenFailed = False

    ' ******************************************************
    ' Form Specific Settings :
    ' ******************************************************

    ' Form Permissioning :-

    THIS_FORM_PermissionArea = Me.Name
    THIS_FORM_PermissionType = RenaissanceGlobals.PermissionFeatureType.TypeForm

    ' 'This' form ID

    THIS_FORM_FormID = FlorenceFormID.frmFlorenceFileReports

    ' Format Event Handlers for form controls

    AddHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

    AddHandler Combo_Entity.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
    AddHandler Combo_Entity.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
    AddHandler Combo_Entity.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
    AddHandler Combo_Entity.KeyUp, AddressOf MainForm.ComboSelectAsYouType_NoMatch

    ' Set up the ToolTip
    MainForm.SetFormToolTip(Me, FormTooltip)

    ' ******************************************************
    ' End Form Specific.
    ' ******************************************************

    THIS_FORM_ChangeID = RenaissanceChangeID.KnowledgeDate

  End Sub

#Region " This Form Event handlers : FormLoad / FormClose "

  ' Form Initialisation code.
  '
    ''' <summary>
    ''' Resets the form.
    ''' </summary>
  Public Sub ResetForm() Implements StandardFlorenceForm.ResetForm

    Call Form_Load(Me, New System.EventArgs)
  End Sub

    ''' <summary>
    ''' Closes the form.
    ''' </summary>
  Public Sub CloseForm() Implements StandardFlorenceForm.CloseForm
    ALWAYS_CLOSE_THIS_FORM = True
    Me.Close()
  End Sub

    ''' <summary>
    ''' Handles the Load event of the Form control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    Me.MainForm.LogError(Me.Name, LOG_LEVELS.Audit, "", "Form Loaded", "", False)
    _FormOpenFailed = False

    If Not (MainForm Is Nothing) Then
      FormIsValid = True
    Else
      MessageBox.Show("'Main' form is not set. Closing form", "no Main Form", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
      FormIsValid = False
      _FormOpenFailed = True
      Exit Sub
    End If


    ' Check User permissions
    Call CheckPermissions()
    If (HasReadPermission = False) Then
      Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User does not have Read permission for this Form..", "", True)

      FormIsValid = False
      _FormOpenFailed = True
      Exit Sub
    End If

    ' Build Combos

    Try

      Call SetEntityCombo()

      If (Me.Combo_Entity.Items.Count > 0) Then
        Combo_Entity.SelectedIndex = 0
      End If

      MainForm.SetComboSelectionLengths(Me)

    Catch ex As Exception
    End Try

  End Sub

    ''' <summary>
    ''' Handles the Closing event of the Form control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.ComponentModel.CancelEventArgs"/> instance containing the event data.</param>
  Private Sub Form_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
    Dim HideForm As Boolean

    ' Hide or Close this form ?
    ' All depends on how many of this form type are Open or in Cache...


    If (ALWAYS_CLOSE_THIS_FORM = True) Or (FormIsValid = False) Then
      HideForm = False
    Else
      HideForm = True
      If MainForm.FlorenceForms.CountOf(THIS_FORM_FormID) > MainForm.ReportForm_CacheCount Then
        HideForm = False
      End If
    End If

    If HideForm = True Then
      MainForm.HideInFormsCollection(Me)
      Me.Hide() ' NPP Fix

      e.Cancel = True
    Else
      Try
        MainForm.RemoveFromFormsCollection(Me)
        RemoveHandler _MainForm.FlorenceAutoUpdate, AddressOf Me.AutoUpdate

        RemoveHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

        RemoveHandler Combo_Entity.KeyUp, AddressOf MainForm.ComboSelectAsYouType

      Catch ex As Exception
      End Try
    End If

  End Sub


#End Region

  ' Routine to handle changes / updates to tables by this and other windows.
  ' If this, or any other, form posts a change to a table, then it will invoke an update event 
  ' detailing what tables have been altered.
  ' Each Form will receive this event through this 'AutoUpdate' routine whice is configured to handle
  ' the 'FlorenceAutoUpdate' event of the main Venice form.
  ' Each form may them react as appropriate to changes in any table that might impact it.
  '
    ''' <summary>
    ''' Autoes the update.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="RenaissanceGlobals.RenaissanceUpdateEventArgs"/> instance containing the event data.</param>
  Private Sub AutoUpdate(ByVal sender As System.Object, ByVal e As RenaissanceGlobals.RenaissanceUpdateEventArgs)
    Dim KnowledgeDateChanged As Boolean
    Dim SetButtonStatus_Flag As Boolean

		If (Me.IsDisposed) OrElse (Me.Disposing) OrElse (Me.InUse = False) Then Exit Sub

    KnowledgeDateChanged = False
    SetButtonStatus_Flag = False

    If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True) Or (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.Connection) = True) Then
      KnowledgeDateChanged = True
    End If

    ' ****************************************************************
    ' Check for changes relevant to this form
    ' ****************************************************************

    ' Changes to the KnowledgeDate :-
    If e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True Then
      SetButtonStatus_Flag = True
    End If

    ' Changes to the tblFlorenceItems table :-
    If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblFlorenceItems) = True) Or KnowledgeDateChanged Then

      ' Re-Set combo.
      Call SetEntityCombo()
    End If


    ' Changes to the tblUserPermissions table :-
    If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblUserPermissions) = True) Or KnowledgeDateChanged Then

      ' Check ongoing permissions.

      Call CheckPermissions()
      If (HasReadPermission = False) Then
        Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User has lost Read permission for this Form.", "", True)

        FormIsValid = False
        Me.Close()
        Exit Sub
      End If

      SetButtonStatus_Flag = True

    End If

    ' ****************************************************************
    '
    ' ****************************************************************


  End Sub

#Region " Workhorse functions : SetSortedRows / CheckPermission / ControlsChanged / Menu Events "

  ' Check User permissions
    ''' <summary>
    ''' Checks the permissions.
    ''' </summary>
  Private Sub CheckPermissions()
    ' *****************************************************************************
    '
    ' *****************************************************************************

    Dim Permissions As Integer

    Permissions = MainForm.CheckPermissions(THIS_FORM_PermissionArea, THIS_FORM_PermissionType)

    HasReadPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermRead) > 0)
    HasUpdatePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermUpdate) > 0)
    HasInsertPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermInsert) > 0)
    HasDeletePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermDelete) > 0)

  End Sub


#End Region

#Region " Set Form Combos (Form Specific Code) "


    ''' <summary>
    ''' Sets the entity combo.
    ''' </summary>
  Private Sub SetEntityCombo()

		Call MainForm.SetTblGenericCombo( _
		Me.Combo_Entity, _
		RenaissanceStandardDatasets.tblFlorenceEntity, _
		"EntityName,ItemGroupID", _
		"EntityID", _
		"", False, True, True, 0, "All Entities")	 ' 

  End Sub


#End Region


#Region " Buttons : Add / Delete / Cancel / Save and Close (Generic Code) "


    ''' <summary>
    ''' Handles the Click event of the btnCoverInsertReport control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnCoverInsertReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCoverInsertReport.Click
    ' *****************************************************************************
    ' Run Report
    ' *****************************************************************************

    Dim EntityID As Integer = 0

    Try
      Me.btnClose.Enabled = False
      Me.btnCoverInsertReport.Enabled = False

      Me.StatusLabel1.Text = "Validating Form"
      Me.Refresh()
      Application.DoEvents()


      If (Me.Combo_Entity.SelectedIndex > 0) AndAlso (IsNumeric(Me.Combo_Entity.SelectedValue) = True) Then
        EntityID = CInt(Combo_Entity.SelectedValue)
      End If

      Me.StatusLabel1.Text = "Processing Report"
      Me.Refresh()
      Application.DoEvents()

      MainForm.MainReportHandler.CoverInsertReport(EntityID, ToolStripProgressBar1)

    Catch ex As Exception

    Finally
      Me.btnClose.Enabled = True
      Me.btnCoverInsertReport.Enabled = True

    End Try

    Me.StatusLabel1.Text = ""
    Me.btnCoverInsertReport.Enabled = True

  End Sub


    ''' <summary>
    ''' Handles the Click event of the btn_KeyDetailsReport control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btn_KeyDetailsReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_KeyDetailsReport.Click
    ' *****************************************************************************
    ' Run Report
    ' *****************************************************************************

    Dim EntityID As Integer = 0

    Try
      Me.btnClose.Enabled = False
      Me.btn_KeyDetailsReport.Enabled = False

      Me.StatusLabel1.Text = "Validating Form"
      Me.Refresh()
      Application.DoEvents()


      If (Me.Combo_Entity.SelectedIndex > 0) AndAlso (IsNumeric(Me.Combo_Entity.SelectedValue) = True) Then
        EntityID = CInt(Combo_Entity.SelectedValue)
      End If

      Me.StatusLabel1.Text = "Processing Report"
      Me.Refresh()
      Application.DoEvents()

      MainForm.MainReportHandler.KeyDetailsReport(EntityID, "KeyDetail<>0", ToolStripProgressBar1)

    Catch ex As Exception

    Finally
      Me.btnClose.Enabled = True
      Me.btn_KeyDetailsReport.Enabled = True

    End Try

    Me.StatusLabel1.Text = ""
    Me.btn_KeyDetailsReport.Enabled = True
  End Sub

    ''' <summary>
    ''' Handles the Click event of the btn_FileStatusReport control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btn_FileStatusReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_FileStatusReport.Click
    ' *****************************************************************************
    ' Run Report
    ' rptSpineInsert
    ' *****************************************************************************

    Dim EntityID As Integer = 0

    Try
      Me.btnClose.Enabled = False
      Me.btn_FileStatusReport.Enabled = False

      Me.StatusLabel1.Text = "Validating Form"
      Me.Refresh()
      Application.DoEvents()


      If (Me.Combo_Entity.SelectedIndex > 0) AndAlso (IsNumeric(Me.Combo_Entity.SelectedValue) = True) Then
        EntityID = CInt(Combo_Entity.SelectedValue)
      End If

      ' Validate EntityID
      If EntityID = 0 Then
        If Not (MessageBox.Show("Are you sure that you want to run this report for ALL Entities ?", "", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes) Then
          Exit Sub
        End If
      End If

      Me.StatusLabel1.Text = "Processing Report"
      Me.Refresh()
      Application.DoEvents()

      MainForm.MainReportHandler.StatusReport("rptFileStatusReport", 0, EntityID, "True", ToolStripProgressBar1)

    Catch ex As Exception

    Finally
      Me.btnClose.Enabled = True
      Me.btn_FileStatusReport.Enabled = True

    End Try

    Me.StatusLabel1.Text = ""
    Me.btn_FileStatusReport.Enabled = True
  End Sub

    ''' <summary>
    ''' Handles the Click event of the btn_StatusIndexReport control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btn_StatusIndexReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_StatusIndexReport.Click
    ' *****************************************************************************
    '
    '
    ' *****************************************************************************

    Dim EntityID As Integer = 0

    Try
      Me.btnClose.Enabled = False
      Me.btn_StatusIndexReport.Enabled = False

      Me.StatusLabel1.Text = "Validating Form"
      Me.Refresh()
      Application.DoEvents()

      ' Get Entity ID
      If (Me.Combo_Entity.SelectedIndex > 0) AndAlso (IsNumeric(Me.Combo_Entity.SelectedValue) = True) Then
        EntityID = CInt(Combo_Entity.SelectedValue)
      End If

      ' Validate EntityID
      If EntityID = 0 Then
        If Not (MessageBox.Show("Are you sure that you want to run this report for ALL Entities ?", "", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes) Then
          Exit Sub
        End If
      End If

      Me.StatusLabel1.Text = "Processing Report"
      Me.Refresh()
      Application.DoEvents()

      MainForm.MainReportHandler.StatusIndexReport(EntityID, ToolStripProgressBar1)

    Catch ex As Exception

    Finally
      Me.btnClose.Enabled = True
      Me.btn_StatusIndexReport.Enabled = True

    End Try

    Me.StatusLabel1.Text = ""
    Me.btn_StatusIndexReport.Enabled = True

  End Sub

    ''' <summary>
    ''' Handles the Click event of the btn_SpineInsertReport control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btn_SpineInsertReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_SpineInsertReport.Click
    ' *****************************************************************************
    ' Run Report
    ' rptSpineInsert
    ' *****************************************************************************

    Dim EntityID As Integer = 0

    Try
      Me.btnClose.Enabled = False
      Me.btn_SpineInsertReport.Enabled = False

      Me.StatusLabel1.Text = "Validating Form"
      Me.Refresh()
      Application.DoEvents()


      If (Me.Combo_Entity.SelectedIndex > 0) AndAlso (IsNumeric(Me.Combo_Entity.SelectedValue) = True) Then
        EntityID = CInt(Combo_Entity.SelectedValue)
      End If

      Me.StatusLabel1.Text = "Processing Report"
      Me.Refresh()
      Application.DoEvents()

      MainForm.MainReportHandler.SpineInsertReport(EntityID, ToolStripProgressBar1)

    Catch ex As Exception

    Finally
      Me.btnClose.Enabled = True
      Me.btn_SpineInsertReport.Enabled = True

    End Try

    Me.StatusLabel1.Text = ""
    Me.btn_SpineInsertReport.Enabled = True
  End Sub

    ''' <summary>
    ''' Handles the Click event of the btn_SignOffSheetReport control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btn_SignOffSheetReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_SignOffSheetReport.Click
    ' *****************************************************************************
    ' Run Report
    ' rptSpineInsert
    ' *****************************************************************************

    Dim EntityID As Integer = 0

    Try
      Me.btnClose.Enabled = False
      Me.btn_SignOffSheetReport.Enabled = False

      Me.StatusLabel1.Text = "Validating Form"
      Me.Refresh()
      Application.DoEvents()


      If (Me.Combo_Entity.SelectedIndex > 0) AndAlso (IsNumeric(Me.Combo_Entity.SelectedValue) = True) Then
        EntityID = CInt(Combo_Entity.SelectedValue)
      End If

      Me.StatusLabel1.Text = "Processing Report"
      Me.Refresh()
      Application.DoEvents()

      MainForm.MainReportHandler.SignOffSheetReport(EntityID, ToolStripProgressBar1)

    Catch ex As Exception

    Finally
      Me.btnClose.Enabled = True
      Me.btn_SignOffSheetReport.Enabled = True

    End Try

    Me.StatusLabel1.Text = ""
    Me.btn_SignOffSheetReport.Enabled = True
  End Sub

    ''' <summary>
    ''' Handles the Click event of the btn_RunAllReports control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btn_RunAllReports_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_RunAllReports.Click
    ' *****************************************************************************
    ' Run All Six File Reports 
    '
    '
    ' *****************************************************************************

    Dim EntityID As Integer = 0
    If (Me.Combo_Entity.SelectedIndex > 0) AndAlso (IsNumeric(Me.Combo_Entity.SelectedValue) = True) Then
      EntityID = CInt(Combo_Entity.SelectedValue)
    End If

    If (EntityID = 0) Then
      MessageBox.Show("An Entity must be selected if you are going to run all the reports.", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1)
      Exit Sub
    End If

    Call btnCoverInsertReport_Click(sender, e)
    Application.DoEvents()
    Call btn_SpineInsertReport_Click(sender, e)
    Application.DoEvents()
    Call btn_KeyDetailsReport_Click(sender, e)
    Application.DoEvents()
    Call btn_FileStatusReport_Click(sender, e)
    Application.DoEvents()
    Call btn_StatusIndexReport_Click(sender, e)
    Application.DoEvents()
    Call btn_SignOffSheetReport_Click(sender, e)

  End Sub

    ''' <summary>
    ''' Handles the Click event of the btnClose control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
    ' *****************************************************************************
    ' Close Form
    ' *****************************************************************************

    Me.Close()

  End Sub

#End Region


End Class
