<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FlorenceMain
  Inherits System.Windows.Forms.Form

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing AndAlso components IsNot Nothing Then
      components.Dispose()
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
		Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FlorenceMain))
		Me.FlorenceMenu = New System.Windows.Forms.MenuStrip
		Me.Menu_File = New System.Windows.Forms.ToolStripMenuItem
		Me.Menu_File_ToggleMDI = New System.Windows.Forms.ToolStripMenuItem
		Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator
		Me.Menu_File_Close = New System.Windows.Forms.ToolStripMenuItem
		Me.Menu_StaticData = New System.Windows.Forms.ToolStripMenuItem
		Me.Menu_Edit_Groups = New System.Windows.Forms.ToolStripMenuItem
		Me.Menu_Edit_Entity = New System.Windows.Forms.ToolStripMenuItem
		Me.Menu_Edit_Items = New System.Windows.Forms.ToolStripMenuItem
		Me.Menu_Update = New System.Windows.Forms.ToolStripMenuItem
		Me.Menu_Update_WorkItems = New System.Windows.Forms.ToolStripMenuItem
		Me.Menu_Update_UpdateDataValues = New System.Windows.Forms.ToolStripMenuItem
		Me.Menu_About = New System.Windows.Forms.ToolStripMenuItem
		Me.Menu_Reports = New System.Windows.Forms.ToolStripMenuItem
		Me.Menu_Report_StaticReports = New System.Windows.Forms.ToolStripMenuItem
		Me.Menu_Rpt_Static_Groups = New System.Windows.Forms.ToolStripMenuItem
		Me.Menu_Rpt_Static_Entity = New System.Windows.Forms.ToolStripMenuItem
		Me.Menu_Rpt_Static_Items = New System.Windows.Forms.ToolStripMenuItem
		Me.Menu_Rpt_StatusChecklist = New System.Windows.Forms.ToolStripMenuItem
		Me.Menu_Rpt_StatusReport = New System.Windows.Forms.ToolStripMenuItem
		Me.ToolStripSeparator4 = New System.Windows.Forms.ToolStripSeparator
		Me.Menu_Rpt_DueDilligenceFileReports = New System.Windows.Forms.ToolStripMenuItem
		Me.ToolStripSeparator5 = New System.Windows.Forms.ToolStripSeparator
		Me.AdmissibilityReportsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
		Me.Menu_Rpt_FlorenceAdmissibilityStatusRpt = New System.Windows.Forms.ToolStripMenuItem
		Me.Menu_Rpt_FlorenceRiskRpt = New System.Windows.Forms.ToolStripMenuItem
		Me.ToolStripSeparator6 = New System.Windows.Forms.ToolStripSeparator
		Me.Menu_Rpt_ReportData = New System.Windows.Forms.ToolStripMenuItem
		Me.Menu_Rpt_ReportData_SaveData = New System.Windows.Forms.ToolStripMenuItem
		Me.Menu_KnowledgeDate = New System.Windows.Forms.ToolStripMenuItem
		Me.Menu_KD_SetKD = New System.Windows.Forms.ToolStripMenuItem
		Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator
		Me.Menu_KD_Refresh = New System.Windows.Forms.ToolStripMenuItem
		Me.ToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator
		Me.Menu_KD_Display = New System.Windows.Forms.ToolStripMenuItem
		Me.Menu_Windows = New System.Windows.Forms.ToolStripMenuItem
		Me.Menu_System = New System.Windows.Forms.ToolStripMenuItem
		Me.Menu_System_UserPermissions = New System.Windows.Forms.ToolStripMenuItem
		Me.FlorenceStatusStrip = New System.Windows.Forms.StatusStrip
		Me.MainProgressBar = New System.Windows.Forms.ToolStripProgressBar
		Me.FlorenceStatusLabel = New System.Windows.Forms.ToolStripStatusLabel
		Me.FlorenceMenu.SuspendLayout()
		Me.FlorenceStatusStrip.SuspendLayout()
		Me.SuspendLayout()
		'
		'FlorenceMenu
		'
		Me.FlorenceMenu.AllowMerge = False
		Me.FlorenceMenu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Menu_File, Me.Menu_StaticData, Me.Menu_Update, Me.Menu_About, Me.Menu_Reports, Me.Menu_KnowledgeDate, Me.Menu_Windows, Me.Menu_System})
		Me.FlorenceMenu.Location = New System.Drawing.Point(0, 0)
		Me.FlorenceMenu.Name = "FlorenceMenu"
		Me.FlorenceMenu.Size = New System.Drawing.Size(891, 24)
		Me.FlorenceMenu.TabIndex = 0
		Me.FlorenceMenu.Text = "FlorenceMenu"
		'
		'Menu_File
		'
		Me.Menu_File.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Menu_File_ToggleMDI, Me.ToolStripSeparator1, Me.Menu_File_Close})
		Me.Menu_File.Name = "Menu_File"
		Me.Menu_File.Size = New System.Drawing.Size(35, 20)
		Me.Menu_File.Text = "&File"
		'
		'Menu_File_ToggleMDI
		'
		Me.Menu_File_ToggleMDI.Name = "Menu_File_ToggleMDI"
		Me.Menu_File_ToggleMDI.Size = New System.Drawing.Size(139, 22)
		Me.Menu_File_ToggleMDI.Text = "Toggle &MDI"
		'
		'ToolStripSeparator1
		'
		Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
		Me.ToolStripSeparator1.Size = New System.Drawing.Size(136, 6)
		'
		'Menu_File_Close
		'
		Me.Menu_File_Close.Name = "Menu_File_Close"
		Me.Menu_File_Close.Size = New System.Drawing.Size(139, 22)
		Me.Menu_File_Close.Text = "&Close"
		'
		'Menu_StaticData
		'
		Me.Menu_StaticData.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Menu_Edit_Groups, Me.Menu_Edit_Entity, Me.Menu_Edit_Items})
		Me.Menu_StaticData.Name = "Menu_StaticData"
		Me.Menu_StaticData.Size = New System.Drawing.Size(72, 20)
		Me.Menu_StaticData.Text = "&Static Data"
		'
		'Menu_Edit_Groups
		'
		Me.Menu_Edit_Groups.Name = "Menu_Edit_Groups"
		Me.Menu_Edit_Groups.Size = New System.Drawing.Size(137, 22)
		Me.Menu_Edit_Groups.Tag = "frmGroup"
		Me.Menu_Edit_Groups.Text = "&Groups"
		'
		'Menu_Edit_Entity
		'
		Me.Menu_Edit_Entity.Name = "Menu_Edit_Entity"
		Me.Menu_Edit_Entity.Size = New System.Drawing.Size(137, 22)
		Me.Menu_Edit_Entity.Tag = "frmEntity"
		Me.Menu_Edit_Entity.Text = "DD &Entities"
		'
		'Menu_Edit_Items
		'
		Me.Menu_Edit_Items.Name = "Menu_Edit_Items"
		Me.Menu_Edit_Items.Size = New System.Drawing.Size(137, 22)
		Me.Menu_Edit_Items.Tag = "frmItems"
		Me.Menu_Edit_Items.Text = "&Items"
		'
		'Menu_Update
		'
		Me.Menu_Update.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Menu_Update_WorkItems, Me.Menu_Update_UpdateDataValues})
		Me.Menu_Update.Name = "Menu_Update"
		Me.Menu_Update.Size = New System.Drawing.Size(54, 20)
		Me.Menu_Update.Text = "&Update"
		'
		'Menu_Update_WorkItems
		'
		Me.Menu_Update_WorkItems.Name = "Menu_Update_WorkItems"
		Me.Menu_Update_WorkItems.Size = New System.Drawing.Size(180, 22)
		Me.Menu_Update_WorkItems.Tag = "frmSelectStatusList"
		Me.Menu_Update_WorkItems.Text = "View Work Items"
		'
		'Menu_Update_UpdateDataValues
		'
		Me.Menu_Update_UpdateDataValues.Name = "Menu_Update_UpdateDataValues"
		Me.Menu_Update_UpdateDataValues.Size = New System.Drawing.Size(180, 22)
		Me.Menu_Update_UpdateDataValues.Tag = "frmUpdateDataItems"
		Me.Menu_Update_UpdateDataValues.Text = "Update &Data Values"
		'
		'Menu_About
		'
		Me.Menu_About.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
		Me.Menu_About.Name = "Menu_About"
		Me.Menu_About.Size = New System.Drawing.Size(48, 20)
		Me.Menu_About.Tag = "frmAbout"
		Me.Menu_About.Text = "&About"
		'
		'Menu_Reports
		'
		Me.Menu_Reports.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Menu_Report_StaticReports, Me.Menu_Rpt_StatusChecklist, Me.Menu_Rpt_StatusReport, Me.ToolStripSeparator4, Me.Menu_Rpt_DueDilligenceFileReports, Me.ToolStripSeparator5, Me.AdmissibilityReportsToolStripMenuItem, Me.ToolStripSeparator6, Me.Menu_Rpt_ReportData})
		Me.Menu_Reports.Name = "Menu_Reports"
		Me.Menu_Reports.Size = New System.Drawing.Size(52, 20)
		Me.Menu_Reports.Text = "&Report"
		'
		'Menu_Report_StaticReports
		'
		Me.Menu_Report_StaticReports.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Menu_Rpt_Static_Groups, Me.Menu_Rpt_Static_Entity, Me.Menu_Rpt_Static_Items})
		Me.Menu_Report_StaticReports.Name = "Menu_Report_StaticReports"
		Me.Menu_Report_StaticReports.Size = New System.Drawing.Size(197, 22)
		Me.Menu_Report_StaticReports.Text = "Static Data Reports"
		'
		'Menu_Rpt_Static_Groups
		'
		Me.Menu_Rpt_Static_Groups.Name = "Menu_Rpt_Static_Groups"
		Me.Menu_Rpt_Static_Groups.Size = New System.Drawing.Size(155, 22)
		Me.Menu_Rpt_Static_Groups.Tag = "rptFlorenceGroups"
		Me.Menu_Rpt_Static_Groups.Text = "Groups Report"
		'
		'Menu_Rpt_Static_Entity
		'
		Me.Menu_Rpt_Static_Entity.Name = "Menu_Rpt_Static_Entity"
		Me.Menu_Rpt_Static_Entity.Size = New System.Drawing.Size(155, 22)
		Me.Menu_Rpt_Static_Entity.Tag = "rptFlorenceEntitys"
		Me.Menu_Rpt_Static_Entity.Text = "Entity Report"
		'
		'Menu_Rpt_Static_Items
		'
		Me.Menu_Rpt_Static_Items.Name = "Menu_Rpt_Static_Items"
		Me.Menu_Rpt_Static_Items.Size = New System.Drawing.Size(155, 22)
		Me.Menu_Rpt_Static_Items.Tag = "rptFlorenceItems"
		Me.Menu_Rpt_Static_Items.Text = "Items Report"
		'
		'Menu_Rpt_StatusChecklist
		'
		Me.Menu_Rpt_StatusChecklist.Name = "Menu_Rpt_StatusChecklist"
		Me.Menu_Rpt_StatusChecklist.Size = New System.Drawing.Size(197, 22)
		Me.Menu_Rpt_StatusChecklist.Tag = "frmFlorenceChecklistRpt"
		Me.Menu_Rpt_StatusChecklist.Text = "Status Checklist"
		'
		'Menu_Rpt_StatusReport
		'
		Me.Menu_Rpt_StatusReport.Name = "Menu_Rpt_StatusReport"
		Me.Menu_Rpt_StatusReport.Size = New System.Drawing.Size(197, 22)
		Me.Menu_Rpt_StatusReport.Tag = "frmFlorenceStatusRpt"
		Me.Menu_Rpt_StatusReport.Text = "Compact Status Report"
		'
		'ToolStripSeparator4
		'
		Me.ToolStripSeparator4.Name = "ToolStripSeparator4"
		Me.ToolStripSeparator4.Size = New System.Drawing.Size(194, 6)
		'
		'Menu_Rpt_DueDilligenceFileReports
		'
		Me.Menu_Rpt_DueDilligenceFileReports.Name = "Menu_Rpt_DueDilligenceFileReports"
		Me.Menu_Rpt_DueDilligenceFileReports.Size = New System.Drawing.Size(197, 22)
		Me.Menu_Rpt_DueDilligenceFileReports.Tag = "frmFlorenceFileReports"
		Me.Menu_Rpt_DueDilligenceFileReports.Text = "D/D File Reports"
		'
		'ToolStripSeparator5
		'
		Me.ToolStripSeparator5.Name = "ToolStripSeparator5"
		Me.ToolStripSeparator5.Size = New System.Drawing.Size(194, 6)
		'
		'AdmissibilityReportsToolStripMenuItem
		'
		Me.AdmissibilityReportsToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Menu_Rpt_FlorenceAdmissibilityStatusRpt, Me.Menu_Rpt_FlorenceRiskRpt})
		Me.AdmissibilityReportsToolStripMenuItem.Name = "AdmissibilityReportsToolStripMenuItem"
		Me.AdmissibilityReportsToolStripMenuItem.Size = New System.Drawing.Size(197, 22)
		Me.AdmissibilityReportsToolStripMenuItem.Text = "Admissibility Reports"
		'
		'Menu_Rpt_FlorenceAdmissibilityStatusRpt
		'
		Me.Menu_Rpt_FlorenceAdmissibilityStatusRpt.Name = "Menu_Rpt_FlorenceAdmissibilityStatusRpt"
		Me.Menu_Rpt_FlorenceAdmissibilityStatusRpt.Size = New System.Drawing.Size(152, 22)
		Me.Menu_Rpt_FlorenceAdmissibilityStatusRpt.Tag = "frmFlorenceAdmissibilityStatusRpt"
		Me.Menu_Rpt_FlorenceAdmissibilityStatusRpt.Text = "&Status Report"
		'
		'Menu_Rpt_FlorenceRiskRpt
		'
		Me.Menu_Rpt_FlorenceRiskRpt.Name = "Menu_Rpt_FlorenceRiskRpt"
		Me.Menu_Rpt_FlorenceRiskRpt.Size = New System.Drawing.Size(152, 22)
		Me.Menu_Rpt_FlorenceRiskRpt.Tag = "frmFlorenceRiskRpt"
		Me.Menu_Rpt_FlorenceRiskRpt.Text = "&Risk Report"
		'
		'ToolStripSeparator6
		'
		Me.ToolStripSeparator6.Name = "ToolStripSeparator6"
		Me.ToolStripSeparator6.Size = New System.Drawing.Size(194, 6)
		'
		'Menu_Rpt_ReportData
		'
		Me.Menu_Rpt_ReportData.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Menu_Rpt_ReportData_SaveData})
		Me.Menu_Rpt_ReportData.Name = "Menu_Rpt_ReportData"
		Me.Menu_Rpt_ReportData.Size = New System.Drawing.Size(197, 22)
		Me.Menu_Rpt_ReportData.Text = "Report Data"
		'
		'Menu_Rpt_ReportData_SaveData
		'
		Me.Menu_Rpt_ReportData_SaveData.Name = "Menu_Rpt_ReportData_SaveData"
		Me.Menu_Rpt_ReportData_SaveData.Size = New System.Drawing.Size(187, 22)
		Me.Menu_Rpt_ReportData_SaveData.Text = "Save Datasets to File"
		'
		'Menu_KnowledgeDate
		'
		Me.Menu_KnowledgeDate.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Menu_KD_SetKD, Me.ToolStripSeparator2, Me.Menu_KD_Refresh, Me.ToolStripSeparator3, Me.Menu_KD_Display})
		Me.Menu_KnowledgeDate.Name = "Menu_KnowledgeDate"
		Me.Menu_KnowledgeDate.Size = New System.Drawing.Size(94, 20)
		Me.Menu_KnowledgeDate.Text = "&KnowledgeDate"
		'
		'Menu_KD_SetKD
		'
		Me.Menu_KD_SetKD.Name = "Menu_KD_SetKD"
		Me.Menu_KD_SetKD.Size = New System.Drawing.Size(179, 22)
		Me.Menu_KD_SetKD.Tag = "frmSetKnowledgeDate"
		Me.Menu_KD_SetKD.Text = "&Set KnowledgeDate"
		'
		'ToolStripSeparator2
		'
		Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
		Me.ToolStripSeparator2.Size = New System.Drawing.Size(176, 6)
		'
		'Menu_KD_Refresh
		'
		Me.Menu_KD_Refresh.Name = "Menu_KD_Refresh"
		Me.Menu_KD_Refresh.Size = New System.Drawing.Size(179, 22)
		Me.Menu_KD_Refresh.Text = "&Refresh All Tables"
		'
		'ToolStripSeparator3
		'
		Me.ToolStripSeparator3.Name = "ToolStripSeparator3"
		Me.ToolStripSeparator3.Size = New System.Drawing.Size(176, 6)
		'
		'Menu_KD_Display
		'
		Me.Menu_KD_Display.Name = "Menu_KD_Display"
		Me.Menu_KD_Display.Size = New System.Drawing.Size(179, 22)
		Me.Menu_KD_Display.Text = " "
		'
		'Menu_Windows
		'
		Me.Menu_Windows.Name = "Menu_Windows"
		Me.Menu_Windows.Size = New System.Drawing.Size(62, 20)
		Me.Menu_Windows.Text = "&Windows"
		'
		'Menu_System
		'
		Me.Menu_System.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Menu_System_UserPermissions})
		Me.Menu_System.Name = "Menu_System"
		Me.Menu_System.Size = New System.Drawing.Size(54, 20)
		Me.Menu_System.Text = "&System"
		'
		'Menu_System_UserPermissions
		'
		Me.Menu_System_UserPermissions.Name = "Menu_System_UserPermissions"
		Me.Menu_System_UserPermissions.Size = New System.Drawing.Size(165, 22)
		Me.Menu_System_UserPermissions.Tag = "frmUserPermissions"
		Me.Menu_System_UserPermissions.Text = "User &Permissions"
		'
		'FlorenceStatusStrip
		'
		Me.FlorenceStatusStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.MainProgressBar, Me.FlorenceStatusLabel})
		Me.FlorenceStatusStrip.Location = New System.Drawing.Point(0, 96)
		Me.FlorenceStatusStrip.Name = "FlorenceStatusStrip"
		Me.FlorenceStatusStrip.Size = New System.Drawing.Size(891, 22)
		Me.FlorenceStatusStrip.TabIndex = 1
		Me.FlorenceStatusStrip.Text = "FlorenceStatusStrip"
		'
		'MainProgressBar
		'
		Me.MainProgressBar.Maximum = 20
		Me.MainProgressBar.Name = "MainProgressBar"
		Me.MainProgressBar.Size = New System.Drawing.Size(150, 16)
		Me.MainProgressBar.Step = 1
		Me.MainProgressBar.Visible = False
		'
		'FlorenceStatusLabel
		'
		Me.FlorenceStatusLabel.Name = "FlorenceStatusLabel"
		Me.FlorenceStatusLabel.Size = New System.Drawing.Size(0, 17)
		'
		'FlorenceMain
		'
		Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.ClientSize = New System.Drawing.Size(891, 118)
		Me.Controls.Add(Me.FlorenceStatusStrip)
		Me.Controls.Add(Me.FlorenceMenu)
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
		Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
		Me.Name = "FlorenceMain"
		Me.Text = "Florence"
		Me.FlorenceMenu.ResumeLayout(False)
		Me.FlorenceMenu.PerformLayout()
		Me.FlorenceStatusStrip.ResumeLayout(False)
		Me.FlorenceStatusStrip.PerformLayout()
		Me.ResumeLayout(False)
		Me.PerformLayout()

	End Sub
  Friend WithEvents FlorenceMenu As System.Windows.Forms.MenuStrip
  Friend WithEvents Menu_File As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_File_ToggleMDI As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
  Friend WithEvents Menu_File_Close As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents FlorenceStatusStrip As System.Windows.Forms.StatusStrip
  Friend WithEvents FlorenceStatusLabel As System.Windows.Forms.ToolStripStatusLabel
  Friend WithEvents Menu_KnowledgeDate As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_KD_SetKD As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
  Friend WithEvents Menu_KD_Refresh As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_About As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripSeparator3 As System.Windows.Forms.ToolStripSeparator
  Friend WithEvents Menu_KD_Display As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_Reports As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_StaticData As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_Edit_Groups As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_System As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_System_UserPermissions As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_Edit_Items As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_Edit_Entity As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_Update As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_Update_WorkItems As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_Report_StaticReports As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_Rpt_Static_Groups As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_Rpt_Static_Entity As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_Rpt_Static_Items As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_Rpt_StatusChecklist As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_Rpt_StatusReport As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents MainProgressBar As System.Windows.Forms.ToolStripProgressBar
  Friend WithEvents Menu_Rpt_DueDilligenceFileReports As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripSeparator4 As System.Windows.Forms.ToolStripSeparator
  Friend WithEvents Menu_Windows As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripSeparator5 As System.Windows.Forms.ToolStripSeparator
	Friend WithEvents AdmissibilityReportsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
	Friend WithEvents Menu_Rpt_FlorenceAdmissibilityStatusRpt As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_Rpt_FlorenceRiskRpt As System.Windows.Forms.ToolStripMenuItem
	Friend WithEvents Menu_Update_UpdateDataValues As System.Windows.Forms.ToolStripMenuItem
	Friend WithEvents ToolStripSeparator6 As System.Windows.Forms.ToolStripSeparator
	Friend WithEvents Menu_Rpt_ReportData As System.Windows.Forms.ToolStripMenuItem
	Friend WithEvents Menu_Rpt_ReportData_SaveData As System.Windows.Forms.ToolStripMenuItem

End Class
