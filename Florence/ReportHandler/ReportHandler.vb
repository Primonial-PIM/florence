' ***********************************************************************
' Assembly         : Florence
' Author           : Nicholas Pennington
' Created          : 11-28-2012
'
' Last Modified By : acullig
' Last Modified On : 08-07-2013
' ***********************************************************************
' <copyright file="ReportHandler.vb" company="">
'     Copyright (c) . All rights reserved.
' </copyright>
' <summary></summary>
' ***********************************************************************
Option Explicit On

Imports System.IO
Imports System.Reflection
Imports System.Data.SqlClient

Imports C1.C1Report

Imports RenaissanceGlobals
Imports RenaissanceGlobals.Globals


''' <summary>
''' Class classReportFieldValue
''' </summary>
Public Class classReportFieldValue
    ''' <summary>
    ''' The _ system string
    ''' </summary>
  Private _SystemString As String
    ''' <summary>
    ''' The _ report field name
    ''' </summary>
  Private _ReportFieldName As String
    ''' <summary>
    ''' The _ report property name
    ''' </summary>
  Private _ReportPropertyName As String
    ''' <summary>
    ''' The _ report field value
    ''' </summary>
  Private _ReportFieldValue As String

    ''' <summary>
    ''' Prevents a default instance of the <see cref="classReportFieldValue"/> class from being created.
    ''' </summary>
  Private Sub New()
  End Sub

    ''' <summary>
    ''' Initializes a new instance of the <see cref="classReportFieldValue"/> class.
    ''' </summary>
    ''' <param name="pFieldName">Name of the p field.</param>
    ''' <param name="PFieldValue">The P field value.</param>
  Public Sub New(ByVal pFieldName As String, ByVal PFieldValue As String)
    Me._SystemString = ""
    Me._ReportFieldName = pFieldName
    Me._ReportFieldValue = PFieldValue
    Me._ReportPropertyName = "Text"
  End Sub

    ''' <summary>
    ''' Initializes a new instance of the <see cref="classReportFieldValue"/> class.
    ''' </summary>
    ''' <param name="pFieldName">Name of the p field.</param>
    ''' <param name="PFieldValue">The P field value.</param>
    ''' <param name="pPropertyValue">The p property value.</param>
  Public Sub New(ByVal pFieldName As String, ByVal PFieldValue As String, ByVal pPropertyValue As String)
    Me._SystemString = ""
    Me._ReportFieldName = pFieldName
    Me._ReportFieldValue = PFieldValue
    Me._ReportPropertyName = pPropertyValue
  End Sub

    ''' <summary>
    ''' Initializes a new instance of the <see cref="classReportFieldValue"/> class.
    ''' </summary>
    ''' <param name="pSystemString">The p system string.</param>
    ''' <param name="pFieldName">Name of the p field.</param>
    ''' <param name="PFieldValue">The P field value.</param>
    ''' <param name="pPropertyValue">The p property value.</param>
  Public Sub New(ByVal pSystemString As String, ByVal pFieldName As String, ByVal PFieldValue As String, ByVal pPropertyValue As String)
    Me._SystemString = pSystemString
    Me._ReportFieldName = pFieldName
    Me._ReportFieldValue = PFieldValue
    Me._ReportPropertyName = pPropertyValue
  End Sub

    ''' <summary>
    ''' Gets the system string.
    ''' </summary>
    ''' <value>The system string.</value>
  Public ReadOnly Property SystemString() As String
    Get
      Return _SystemString
    End Get
  End Property

    ''' <summary>
    ''' Gets the name of the report field.
    ''' </summary>
    ''' <value>The name of the report field.</value>
  Public ReadOnly Property ReportFieldName() As String
    Get
      Return _ReportFieldName
    End Get
  End Property

    ''' <summary>
    ''' Gets the report field value.
    ''' </summary>
    ''' <value>The report field value.</value>
  Public ReadOnly Property ReportFieldValue() As String
    Get
      Return _ReportFieldValue
    End Get
  End Property

    ''' <summary>
    ''' Gets the name of the report property.
    ''' </summary>
    ''' <value>The name of the report property.</value>
  Public ReadOnly Property ReportPropertyName() As String
    Get
      Return _ReportPropertyName
    End Get
  End Property
End Class


''' <summary>
''' Class ReportHandler
''' </summary>
Public Class ReportHandler
    ''' <summary>
    ''' The REPOR t_ knowledge date_ field name
    ''' </summary>
  Public Const REPORT_KnowledgeDate_FieldName As String = "Text_KnowledgeDate"
    ''' <summary>
    ''' The REPOR t_ value date_ field name
    ''' </summary>
  Public Const REPORT_ValueDate_FieldName As String = "Text_ValueDate"

  ' Specialist table from which to run the 'Table of contents' report

    ''' <summary>
    ''' The TBL table of contents
    ''' </summary>
  Private tblTableOfContents As DataTable = Nothing

#Region " Local Variable Declaration"

    ''' <summary>
    ''' The _ main form
    ''' </summary>
  Private WithEvents _MainForm As FlorenceMain

#End Region

#Region " Properties"

    ''' <summary>
    ''' Gets the main form.
    ''' </summary>
    ''' <value>The main form.</value>
  Public ReadOnly Property MainForm() As FlorenceMain
    Get
      Return _MainForm
    End Get
  End Property


#End Region

#Region " Constructors"

    ''' <summary>
    ''' Prevents a default instance of the <see cref="ReportHandler"/> class from being created.
    ''' </summary>
  Private Sub New()

  End Sub

    ''' <summary>
    ''' Initializes a new instance of the <see cref="ReportHandler"/> class.
    ''' </summary>
    ''' <param name="pMainForm">The p main form.</param>
  Public Sub New(ByRef pMainForm As FlorenceMain)

    _MainForm = pMainForm

  End Sub

#End Region

#Region " Display Report / Get Report Definition functions"

    ''' <summary>
    ''' Displays the report.
    ''' </summary>
    ''' <param name="pFundID">The p fund ID.</param>
    ''' <param name="pReportName">Name of the p report.</param>
    ''' <param name="pRecordset">The p recordset.</param>
    ''' <param name="pStatusBar">The p status bar.</param>
  Sub DisplayReport(ByVal pFundID As Integer, ByVal pReportName As String, ByVal pRecordset As Object, ByRef pStatusBar As ToolStripProgressBar)
    ' ************************************************************************
    ' Display the given C1Report in using the given data source.
    '
    ' ************************************************************************

    Try
      DisplayReport(pReportName, pReportName, pFundID, pRecordset, pStatusBar)
    Catch ex As Exception
    End Try

  End Sub

    ''' <summary>
    ''' Displays the report.
    ''' </summary>
    ''' <param name="pReportName">Name of the p report.</param>
    ''' <param name="pRecordset">The p recordset.</param>
    ''' <param name="pFundID">The p fund ID.</param>
    ''' <param name="pValueDate">The p value date.</param>
    ''' <param name="pKnowledgeDate">The p knowledge date.</param>
    ''' <param name="pStatusBar">The p status bar.</param>
  Sub DisplayReport(ByVal pReportName As String, ByVal pRecordset As Object, ByVal pFundID As Integer, ByVal pValueDate As Date, ByVal pKnowledgeDate As Date, ByRef pStatusBar As ToolStripProgressBar)
    ' ************************************************************************
    ' Display the given C1Report in using the given data source.
    ' Use the supplied Value Date and knowledgeDate
    ' ************************************************************************

    Try
      DisplayReport(pReportName, pReportName, pRecordset, pFundID, pValueDate, pKnowledgeDate, pStatusBar)
    Catch ex As Exception
    End Try

  End Sub

    ''' <summary>
    ''' Displays the report.
    ''' </summary>
    ''' <param name="pReportFile">The p report file.</param>
    ''' <param name="pReportName">Name of the p report.</param>
    ''' <param name="pFundID">The p fund ID.</param>
    ''' <param name="pRecordset">The p recordset.</param>
    ''' <param name="pStatusBar">The p status bar.</param>
  Sub DisplayReport(ByVal pReportFile As String, ByVal pReportName As String, ByVal pFundID As Integer, ByRef pRecordset As Object, ByRef pStatusBar As ToolStripProgressBar)
    ' ************************************************************************
    ' Display the given C1Report in using the given data source.
    '
    ' ************************************************************************

    DisplayReport(pReportName, pReportName, pRecordset, pFundID, Now(), MainForm.Main_Knowledgedate, pStatusBar)
  End Sub

    ''' <summary>
    ''' Displays the report.
    ''' </summary>
    ''' <param name="pReportFile">The p report file.</param>
    ''' <param name="pReportName">Name of the p report.</param>
    ''' <param name="pRecordset">The p recordset.</param>
    ''' <param name="pFundID">The p fund ID.</param>
    ''' <param name="pValueDate">The p value date.</param>
    ''' <param name="pKnowledgeDate">The p knowledge date.</param>
    ''' <param name="pStatusBar">The p status bar.</param>
  Sub DisplayReport(ByVal pReportFile As String, ByVal pReportName As String, ByRef pRecordset As Object, ByVal pFundID As Integer, ByVal pValueDate As Date, ByVal pKnowledgeDate As Date, ByRef pStatusBar As ToolStripProgressBar)
    ' ************************************************************************
    ' Display the given C1Report in a frmViewReport form.
    '
    ' First, Get the report definition,
    ' Second, Apply the given data source to it.
    ' Third, Display in a preview window.
    '
    ' ************************************************************************

    Dim thisReport As C1Report

    ' Get report definition.

    thisReport = GetReportDefinition(pFundID, pReportFile, pReportName)

    If (thisReport Is Nothing) Then
      MainForm.LogError("ReportHandler", LOG_LEVELS.Error, "", "Failed to get definition for report '" & pReportName & "'", "", True)
      Exit Sub
    End If

    Call DisplayReport(thisReport, pRecordset, pValueDate, pKnowledgeDate, pStatusBar)

  End Sub

    ''' <summary>
    ''' Displays the report.
    ''' </summary>
    ''' <param name="pReportName">Name of the p report.</param>
    ''' <param name="pRecordset">The p recordset.</param>
    ''' <param name="pFundID">The p fund ID.</param>
    ''' <param name="pFieldValues">The p field values.</param>
    ''' <param name="pStatusBar">The p status bar.</param>
  Sub DisplayReport(ByVal pReportName As String, ByVal pRecordset As Object, ByVal pFundID As Integer, ByRef pFieldValues() As classReportFieldValue, ByRef pStatusBar As ToolStripProgressBar)
    ' ************************************************************************
    ' Display the given C1Report in a frmViewReport form.
    '
    ' First, Get the report definition,
    ' Second, Apply the given data source to it.
    ' Third, Display in a preview window.
    '
    ' ************************************************************************

    Dim thisReport As C1Report

    ' Get report definition.

    thisReport = GetReportDefinition(pFundID, pReportName, pReportName)

    If (thisReport Is Nothing) Then
      Exit Sub
    End If

    Call DisplayReport(thisReport, pRecordset, pFieldValues, pStatusBar)

  End Sub

    ''' <summary>
    ''' Displays the report.
    ''' </summary>
    ''' <param name="thisReport">The this report.</param>
    ''' <param name="pRecordset">The p recordset.</param>
    ''' <param name="pValueDate">The p value date.</param>
    ''' <param name="pKnowledgeDate">The p knowledge date.</param>
    ''' <param name="pStatusBar">The p status bar.</param>
  Public Sub DisplayReport(ByRef thisReport As C1Report, ByVal pRecordset As Object, ByVal pValueDate As Date, ByVal pKnowledgeDate As Date, ByRef pStatusBar As ToolStripProgressBar)
    ' ************************************************************************
    ' Display the given C1Report in a frmViewReport form.
    '
    ' First, Set ValueDate and KnowledgeDate fields.
    ' Second, Display in a preview window.
    '
    ' ************************************************************************


    If (thisReport Is Nothing) Then Exit Sub
    If (pRecordset Is Nothing) Then Exit Sub

    Dim KDString As String = ""
    Try
      If pKnowledgeDate.CompareTo(KNOWLEDGEDATE_NOW) <= 0 Then
        KDString = "Live"
      ElseIf pKnowledgeDate.TimeOfDay.TotalSeconds >= LAST_SECOND Then
        KDString = pKnowledgeDate.ToString(REPORT_DATEFORMAT)
      Else
        KDString = pKnowledgeDate.ToString(REPORT_LONGDATEFORMAT)
      End If
    Catch ex As Exception
    End Try

    Call DisplayReport(thisReport, pRecordset, New classReportFieldValue() {New classReportFieldValue(REPORT_ValueDate_FieldName, pValueDate.ToString(REPORT_DATEFORMAT)), New classReportFieldValue(REPORT_KnowledgeDate_FieldName, KDString)}, pStatusBar)

  End Sub

    ''' <summary>
    ''' Displays the report.
    ''' </summary>
    ''' <param name="thisReport">The this report.</param>
    ''' <param name="pRecordset">The p recordset.</param>
    ''' <param name="pFieldValues">The p field values.</param>
    ''' <param name="pStatusBar">The p status bar.</param>
  Public Sub DisplayReport(ByRef thisReport As C1Report, ByRef pRecordset As Object, ByRef pFieldValues() As classReportFieldValue, ByRef pStatusBar As ToolStripProgressBar)
    ' ************************************************************************
    '
    '
    ' ************************************************************************

    Dim thisFieldValue As classReportFieldValue

    If (Not (pFieldValues Is Nothing)) AndAlso (pFieldValues.Length > 0) Then
      For Each thisFieldValue In pFieldValues
        Call SetFieldValue(thisReport, thisFieldValue)
      Next
    End If

    Try
      thisReport.DataSource.Recordset = pRecordset
    Catch ex As Exception
      MainForm.LogError("ReportHandler", LOG_LEVELS.Error, ex.Message, "Failed to set report datasource.", ex.StackTrace, True)
      Exit Sub
    End Try

    DisplayReport(thisReport, pStatusBar)

		SaveReportData(pRecordset, thisReport.ReportName)

  End Sub

    ''' <summary>
    ''' Displays the report.
    ''' </summary>
    ''' <param name="pC1Report">The p c1 report.</param>
    ''' <param name="pStatusBar">The p status bar.</param>
	Public Sub DisplayReport(ByRef pC1Report As C1Report, ByRef pStatusBar As ToolStripProgressBar)
		' ************************************************************************
		' Display the given C1Report in a frmViewReport form.
		'
		' ************************************************************************

		Dim thisViewReportForm As frmViewReport

		' Open new ViewForm object

		Try
			thisViewReportForm = CType(MainForm.New_FlorenceForm(FlorenceFormID.frmViewReport).Form, frmViewReport)
			thisViewReportForm.Opacity = 0
			thisViewReportForm.Text = pC1Report.ReportName
			MainForm.BuildWindowsMenu()
		Catch ex As Exception
			Exit Sub
		End Try

		If (thisViewReportForm Is Nothing) Then
			Exit Sub
		End If

		' Render Report to Preview object

		Try
			Dim thisCursor As Cursor
			Dim OrgTitle As String

			OrgTitle = thisViewReportForm.Text
			thisViewReportForm.Text = "Rendering " & OrgTitle
			thisCursor = thisViewReportForm.Cursor
			thisViewReportForm.Cursor = Cursors.WaitCursor

			If (Not (pStatusBar Is Nothing)) Then
				AddHandler pC1Report.StartSection, AddressOf IncrementStatusBar
				pC1Report.Tag = pStatusBar
			End If

			thisViewReportForm.ReportPreview.Document = pC1Report.Document

			If (Not (pStatusBar Is Nothing)) Then
				pStatusBar.Visible = False
				pC1Report.Tag = Nothing
				RemoveHandler pC1Report.StartSection, AddressOf IncrementStatusBar
			End If

			thisViewReportForm.Cursor = thisCursor
			thisViewReportForm.Text = OrgTitle
		Catch ex As Exception
		End Try

		' Show form (probably unnecessary as it is shown by default by the New_VeniceForm() routine)

		Try
			If Not (thisViewReportForm Is Nothing) Then
				thisViewReportForm.Show()
				'thisViewReportForm.Visible = True

				Dim OCount As Integer

				For OCount = 0 To 100 Step 5
					thisViewReportForm.Opacity = OCount / 100
					Threading.Thread.Sleep(10)
				Next

			End If
		Catch ex As Exception
		End Try

	End Sub

    ''' <summary>
    ''' Saves the report data.
    ''' </summary>
    ''' <param name="pDataSource">The p data source.</param>
    ''' <param name="pReportName">Name of the p report.</param>
	Public Sub SaveReportData(ByRef pDataSource As Object, ByVal pReportName As String)

		Dim FP As Integer = FreeFile()
		Dim ColCount As Integer
		Dim RowCount As Integer
		Dim ThisDataRow As DataRowView
		Dim ThisRowString As String
		Dim pFileName As String

		Try

			If MainForm.GetMenuChecked(MainForm.Menu_Rpt_ReportData_SaveData) Then
				Dim thisFilePath As String = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)
				Dim thisFileName As String = pReportName & "_" & Now.ToString(FILENAME_DATEFORMAT) & ".txt"

				pFileName = Path.Combine(thisFilePath, thisFileName)
			Else
				Exit Sub
			End If

		Catch ex As Exception
			Exit Sub
		End Try

		Try
			Dim ThisView As DataView

			If (TypeOf pDataSource Is DataTable) Then
				ThisView = CType(pDataSource, DataTable).DefaultView
			ElseIf (TypeOf pDataSource Is DataView) Then
				ThisView = CType(pDataSource, DataView)
			Else
				Exit Sub
			End If

			FileOpen(FP, pFileName, OpenMode.Output)

			For RowCount = (-1) To (ThisView.Count - 1)
				ThisRowString = ""

				For ColCount = 0 To (ThisView.Table.Columns.Count - 1)
					If (RowCount < 0) Then
						If (ColCount > 0) Then
							ThisRowString &= Chr(9)
						End If
						ThisRowString &= ThisView.Table.Columns(ColCount).ColumnName
					Else
						ThisDataRow = ThisView.Item(RowCount)
						If (ColCount > 0) Then
							ThisRowString &= Chr(9)
						End If
						If (TypeOf ThisDataRow.Item(ColCount) Is Date) Then
							ThisRowString &= CType(ThisDataRow.Item(ColCount), Date).ToString(QUERY_LONGDATEFORMAT)
						Else
							ThisRowString &= ThisDataRow.Item(ColCount).ToString
						End If
					End If
				Next

				PrintLine(FP, New Object() {ThisRowString})
			Next

		Catch ex As Exception
		Finally
			Try
				FileClose(FP)
			Catch ex As Exception
			End Try
		End Try
	End Sub

    ''' <summary>
    ''' Increments the status bar.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="ReportEventArgs"/> instance containing the event data.</param>
  Private Sub IncrementStatusBar(ByVal sender As Object, ByVal e As ReportEventArgs)
    ' ****************************************************************************************
    '
    '
    '
    ' ****************************************************************************************

    Static LastUpdateTime As Date = #1/1/1900#

    ' Validate Sender.
    If (Not (TypeOf sender Is C1Report)) Then
      Exit Sub
    End If

    Try
      ' Cast Sender to C1Report in order to get at the 'Tag' property
      Dim thisReport As C1Report = CType(sender, C1Report)

      ' If the 'Tag' is a Progress Bar.
      If (Not (thisReport.Tag Is Nothing)) AndAlso (TypeOf thisReport.Tag Is ToolStripProgressBar) Then
        Dim thisProgressbar As ToolStripProgressBar = CType(thisReport.Tag, ToolStripProgressBar)

        If (thisProgressbar.IsDisposed) Then
          Exit Sub
        End If

        ' Make sure the bar is Visible.
        If (thisProgressbar.Visible = False) Then
          thisProgressbar.Visible = True
        End If

        ' Impose a minimum time between status bar updates.
        If (Now() - LastUpdateTime).TotalSeconds >= 0.2 Then
          LastUpdateTime = Now()

          ' Progress the Status Bar....
          If (thisProgressbar.Value < thisProgressbar.Maximum) Then
            thisProgressbar.Value += thisProgressbar.Step
          Else
            thisProgressbar.Value = thisProgressbar.Minimum
          End If
        End If
      End If
    Catch ex As Exception
    End Try

  End Sub

    ''' <summary>
    ''' Gets the report definition.
    ''' </summary>
    ''' <param name="pFundID">The p fund ID.</param>
    ''' <param name="pReportName">Name of the p report.</param>
    ''' <returns>C1Report.</returns>
  Private Function GetReportDefinition(ByVal pFundID As Integer, ByVal pReportName As String) As C1Report
    ' ***********************************************************************************
    '
    ' ***********************************************************************************

    Return GetReportDefinition(pFundID, pReportName, pReportName)
  End Function

    ''' <summary>
    ''' Gets the report definition.
    ''' </summary>
    ''' <param name="pFundID">The p fund ID.</param>
    ''' <param name="pReportFile">The p report file.</param>
    ''' <param name="pReportName">Name of the p report.</param>
    ''' <returns>C1Report.</returns>
  Private Function GetReportDefinition(ByVal pFundID As Integer, ByVal pReportFile As String, ByVal pReportName As String) As C1Report
    ' ******************************************************************************
    ' Return an instance of the given report name from the given report file.
    '
    ' This routine is designed to search upwards from the current executable directory
    ' for the given report file and the given report within it.
    ' In addition to the executable path, it will check for the 'ReportDefinitions'
    ' subdirectory on it's way up the directory tree.
    ' 
    ' ******************************************************************************

    Return SetCustomReportDetails(MainForm, pFundID, pReportFile, pReportName)

  End Function

    ''' <summary>
    ''' Sets the field value.
    ''' </summary>
    ''' <param name="pReport">The p report.</param>
    ''' <param name="pFieldValue">The p field value.</param>
  Private Sub SetFieldValue(ByRef pReport As C1Report, ByRef pFieldValue As classReportFieldValue)
    ' ******************************************************************************
    '
    '
    ' ******************************************************************************
    Try
      If (pFieldValue Is Nothing) OrElse (pReport Is Nothing) Then
        Exit Sub
			End If

			If (pReport.Fields.Contains(pFieldValue.ReportFieldName) = False) Then
				Exit Sub
			End If

		Catch ex As Exception
			Exit Sub
		End Try

    Try
      Dim FieldValue As String
      Dim SystemStringValue As String

      ' Establish  Field Value.
      ' By Default this is the class 'FieldValue' unless a System String name is given.

      FieldValue = pFieldValue.ReportFieldValue

      If pFieldValue.SystemString.Length > 0 Then
        SystemStringValue = MainForm.Get_SystemString(pFieldValue.SystemString)
        If SystemStringValue.Length > 0 Then
          FieldValue = SystemStringValue
        End If
      End If

      ' Set Report Field

      Dim TypeInfo As Type
      Dim thisProperty As PropertyInfo

			If (pReport.Fields.Contains(pFieldValue.ReportFieldName)) Then

				TypeInfo = pReport.Fields(pFieldValue.ReportFieldName).GetType
				thisProperty = TypeInfo.GetProperty(pFieldValue.ReportPropertyName)

				If thisProperty.PropertyType.UnderlyingSystemType Is GetType(System.String) Then
					thisProperty.SetValue(pReport.Fields(pFieldValue.ReportFieldName), FieldValue, Nothing)
				ElseIf thisProperty.PropertyType.UnderlyingSystemType Is GetType(System.DateTime) Then
					thisProperty.SetValue(pReport.Fields(pFieldValue.ReportFieldName), CDate(FieldValue), Nothing)
				ElseIf thisProperty.PropertyType.UnderlyingSystemType Is GetType(System.Boolean) Then
					thisProperty.SetValue(pReport.Fields(pFieldValue.ReportFieldName), CBool(FieldValue), Nothing)
				ElseIf thisProperty.PropertyType.UnderlyingSystemType Is GetType(System.Single) Then
					thisProperty.SetValue(pReport.Fields(pFieldValue.ReportFieldName), CSng(FieldValue), Nothing)
				ElseIf thisProperty.PropertyType.UnderlyingSystemType Is GetType(System.Double) Then
					thisProperty.SetValue(pReport.Fields(pFieldValue.ReportFieldName), CDbl(FieldValue), Nothing)
				ElseIf thisProperty.PropertyType.UnderlyingSystemType Is GetType(System.Int16) Then
					thisProperty.SetValue(pReport.Fields(pFieldValue.ReportFieldName), CShort(FieldValue), Nothing)
				ElseIf thisProperty.PropertyType.UnderlyingSystemType Is GetType(System.Int32) Then
					thisProperty.SetValue(pReport.Fields(pFieldValue.ReportFieldName), CInt(FieldValue), Nothing)
				ElseIf thisProperty.PropertyType.UnderlyingSystemType Is GetType(System.Int64) Then
					thisProperty.SetValue(pReport.Fields(pFieldValue.ReportFieldName), CLng(FieldValue), Nothing)
				ElseIf thisProperty.PropertyType.UnderlyingSystemType Is GetType(System.Drawing.Color) Then
					If IsNumeric(FieldValue) Then
						Dim IntValue As Integer
						IntValue = CInt(FieldValue)
						thisProperty.SetValue(pReport.Fields(pFieldValue.ReportFieldName), System.Drawing.Color.FromArgb(CInt((IntValue And 16777215) / 65536), CInt((IntValue And 65535) / 256), CInt(IntValue And 255)), Nothing)
					Else
						thisProperty.SetValue(pReport.Fields(pFieldValue.ReportFieldName), System.Drawing.Color.FromName(CStr(FieldValue)), Nothing)
					End If
				ElseIf thisProperty.PropertyType.UnderlyingSystemType.IsEnum Then
					Dim EnumObject As Object

					If IsNumeric(FieldValue) Then
						EnumObject = System.Enum.ToObject(thisProperty.PropertyType.UnderlyingSystemType, FieldValue)
					Else
						EnumObject = System.Enum.Parse(thisProperty.PropertyType.UnderlyingSystemType, FieldValue, True)
					End If

					thisProperty.SetValue(pReport.Fields(pFieldValue.ReportFieldName), EnumObject, Nothing)
				Else
					thisProperty.SetValue(pReport.Fields(pFieldValue.ReportFieldName), CDbl(FieldValue), Nothing)
				End If
			End If

    Catch ex As Exception
    End Try

  End Sub

#End Region

#Region " Specific Report Routines"

    ''' <summary>
    ''' RPTs the table audit report.
    ''' </summary>
    ''' <param name="thisReportTable">The this report table.</param>
    ''' <param name="pProgressBar">The p progress bar.</param>
    ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
  Public Function rptTableAuditReport(ByRef thisReportTable As DataTable, Optional ByRef pProgressBar As ToolStripProgressBar = Nothing) As Boolean
    ' ************************************************************************
    '
    ' ************************************************************************

    Try
      DisplayReport(0, "rptTableAuditReport", thisReportTable, pProgressBar)
    Catch ex As Exception
    End Try

  End Function

    ''' <summary>
    ''' Groupses the report.
    ''' </summary>
    ''' <param name="pProgressBar">The p progress bar.</param>
  Public Sub GroupsReport(ByRef pProgressBar As ToolStripProgressBar)
    ' **************************************************************************
    '
    '
    ' **************************************************************************

		Dim GroupDS As RenaissanceDataClass.DSFlorenceGroup
		GroupDS = MainForm.Load_Table(RenaissanceStandardDatasets.tblFlorenceGroup)

		DisplayReport("rptFlorenceGroups", GroupDS.tblFlorenceGroup, 0, Now.Date, MainForm.Main_Knowledgedate, pProgressBar)

	End Sub

    ''' <summary>
    ''' Entityses the report.
    ''' </summary>
    ''' <param name="pProgressBar">The p progress bar.</param>
	Public Sub EntitysReport(ByRef pProgressBar As ToolStripProgressBar)
		' **************************************************************************
		'
		'
		' **************************************************************************

		Dim RptCommand As New SqlCommand
		Dim tblEntitysReport As New DataTable("tblEntitysReport")

		Try
			RptCommand.Connection = New SqlConnection(MainForm.SQLConnectString)
			RptCommand.CommandType = CommandType.StoredProcedure
			RptCommand.CommandText = "[rpt_Florence_EntitysReport]"
			RptCommand.Parameters.Clear()
			RptCommand.Parameters.Add(New SqlParameter("@KnowledgeDate", SqlDbType.DateTime, 8)).Value = MainForm.Main_Knowledgedate
			RptCommand.CommandTimeout = DEFAULT_SQLCOMMAND_TIMEOUT

			RptCommand.Connection.Open()

			Dim RptReader As SqlDataReader = RptCommand.ExecuteReader()
			tblEntitysReport.Load(RptReader)
		Catch ex As Exception
		Finally
			Try
				RptCommand.Connection.Close()
			Catch ex As Exception
			End Try
		End Try
		RptCommand.Connection = Nothing
		RptCommand = Nothing

		DisplayReport("rptFlorenceEntitys", tblEntitysReport, 0, Now.Date, MainForm.Main_Knowledgedate, pProgressBar)

	End Sub

    ''' <summary>
    ''' Itemses the report.
    ''' </summary>
    ''' <param name="pProgressBar">The p progress bar.</param>
	Public Sub ItemsReport(ByRef pProgressBar As ToolStripProgressBar)
		' **************************************************************************
		'
		'
		' **************************************************************************

		Dim RptCommand As New SqlCommand
		Dim tblFlorenceItems As New DataTable("tblFlorenceItems")

		Try
			RptCommand.Connection = New SqlConnection(MainForm.SQLConnectString)
			RptCommand.CommandType = CommandType.StoredProcedure
			RptCommand.CommandText = "[rpt_Florence_ItemsReport]"
			RptCommand.Parameters.Clear()
			RptCommand.Parameters.Add(New SqlParameter("@KnowledgeDate", SqlDbType.DateTime, 8)).Value = MainForm.Main_Knowledgedate
			RptCommand.CommandTimeout = DEFAULT_SQLCOMMAND_TIMEOUT

			RptCommand.Connection.Open()

			Dim RptReader As SqlDataReader = RptCommand.ExecuteReader()
			tblFlorenceItems.Load(RptReader)
		Catch ex As Exception
		Finally
			RptCommand.Connection.Close()
		End Try

		RptCommand.Connection = Nothing
		RptCommand = Nothing

		DisplayReport("rptFlorenceItems", tblFlorenceItems, 0, Now.Date, MainForm.Main_Knowledgedate, pProgressBar)

	End Sub

    ''' <summary>
    ''' Checklists the report.
    ''' </summary>
    ''' <param name="pGroupID">The p group ID.</param>
    ''' <param name="pEntityID">The p entity ID.</param>
    ''' <param name="pRunBriefReport">if set to <c>true</c> [p run brief report].</param>
    ''' <param name="pStatusFilter">The p status filter.</param>
    ''' <param name="pProgressBar">The p progress bar.</param>
  Public Sub ChecklistReport(ByVal pGroupID As Int32, ByVal pEntityID() As Integer, ByRef pRunBriefReport As Boolean, ByVal pStatusFilter As Integer, ByRef pProgressBar As ToolStripProgressBar)
    ' **************************************************************************
    '
    '
    ' **************************************************************************

    Dim RptCommand As New SqlCommand
    Dim tblFlorenceChecklist As New DataTable("tblFlorenceChecklist")
    Dim ReportName As String = "rptFlorenceChecklist"
    Dim StatusFilter As RenaissanceGlobals.FlorenceDueDilligenceStatusIDs
    Dim SelectedRows As DataView
    Dim SelectCommand As String
    Dim Counter As Integer

    If (pRunBriefReport) Then
      ReportName = "rptFlorenceChecklistBrief"
    End If

    Select Case pStatusFilter
      Case 1
        StatusFilter = FlorenceDueDilligenceStatusIDs.InProgress

      Case 2
        StatusFilter = FlorenceDueDilligenceStatusIDs.Declined

      Case 3
        StatusFilter = FlorenceDueDilligenceStatusIDs.Approved

      Case Else
        pStatusFilter = 0

    End Select

    Try
      Dim SingleEntityID As Integer = 0

      If (pEntityID.Length = 1) Then
        SingleEntityID = pEntityID(0)
      End If

      If (pEntityID Is Nothing) OrElse (pEntityID.Length <= 1) Then

        ' Single Entity.

        Try
          RptCommand.Connection = New SqlConnection(MainForm.SQLConnectString)
          RptCommand.CommandType = CommandType.StoredProcedure
          RptCommand.CommandText = "[rpt_Florence_ChecklistReport]"
          RptCommand.Parameters.Clear()
          RptCommand.Parameters.Add(New SqlParameter("@ItemGroupID", SqlDbType.Int, 4)).Value = pGroupID
          RptCommand.Parameters.Add(New SqlParameter("@EntityID", SqlDbType.Int, 4)).Value = SingleEntityID
          RptCommand.Parameters.Add(New SqlParameter("@KnowledgeDate", SqlDbType.DateTime, 8)).Value = MainForm.Main_Knowledgedate
          RptCommand.CommandTimeout = DEFAULT_SQLCOMMAND_TIMEOUT

          RptCommand.Connection.Open()

          Dim RptReader As SqlDataReader = RptCommand.ExecuteReader()
          tblFlorenceChecklist.Load(RptReader)
        Catch ex As Exception
        Finally
          RptCommand.Connection.Close()
        End Try

        If (pStatusFilter > 0) Then

          SelectCommand = "DDStatus = " & CInt(StatusFilter).ToString()
          SelectedRows = New DataView(tblFlorenceChecklist, SelectCommand, "", DataViewRowState.CurrentRows)

          DisplayReport(ReportName, SelectedRows, 0, Now.Date, MainForm.Main_Knowledgedate, pProgressBar)
        Else
          DisplayReport(ReportName, tblFlorenceChecklist, 0, Now.Date, MainForm.Main_Knowledgedate, pProgressBar)
        End If


      Else

        ' Multiple Entities

        Try
          RptCommand.Connection = New SqlConnection(MainForm.SQLConnectString)
          RptCommand.CommandType = CommandType.StoredProcedure
          RptCommand.CommandText = "[rpt_Florence_ChecklistReport]"
          RptCommand.Parameters.Clear()
          RptCommand.Parameters.Add(New SqlParameter("@ItemGroupID", SqlDbType.Int, 4)).Value = pGroupID
          RptCommand.Parameters.Add(New SqlParameter("@EntityID", SqlDbType.Int, 4)).Value = 0
          RptCommand.Parameters.Add(New SqlParameter("@KnowledgeDate", SqlDbType.DateTime, 8)).Value = MainForm.Main_Knowledgedate
          RptCommand.CommandTimeout = DEFAULT_SQLCOMMAND_TIMEOUT

          RptCommand.Connection.Open()

          Dim RptReader As SqlDataReader = RptCommand.ExecuteReader()
          tblFlorenceChecklist.Load(RptReader)
        Catch ex As Exception
        Finally
          RptCommand.Connection.Close()
        End Try

        SelectCommand = "EntityID IN ("
        For Counter = 0 To (pEntityID.Length - 1)
          If (Counter > 0) Then
            SelectCommand &= ","
          End If

          SelectCommand &= pEntityID(Counter).ToString
        Next
        SelectCommand &= ")"

        If (pStatusFilter > 0) Then
          SelectCommand = "(" & SelectCommand & ") AND (DDStatus = " & CInt(StatusFilter).ToString() & ")"
        End If

        SelectedRows = New DataView(tblFlorenceChecklist, SelectCommand, "", DataViewRowState.CurrentRows)

        DisplayReport(ReportName, SelectedRows, 0, Now.Date, MainForm.Main_Knowledgedate, pProgressBar)
      End If

    Catch ex As Exception
    Finally
      RptCommand.Connection = Nothing
      RptCommand = Nothing
    End Try



  End Sub

    ''' <summary>
    ''' Statuses the report.
    ''' </summary>
    ''' <param name="pReportName">Name of the p report.</param>
    ''' <param name="pGroupID">The p group ID.</param>
    ''' <param name="pEntityID">The p entity ID.</param>
	Public Sub StatusReport(ByVal pReportName As String, ByVal pGroupID As Int32, ByVal pEntityID As Int32)
		StatusReport(pReportName, pGroupID, pEntityID, "True", Nothing)
	End Sub

    ''' <summary>
    ''' Statuses the report.
    ''' </summary>
    ''' <param name="pReportName">Name of the p report.</param>
    ''' <param name="pGroupID">The p group ID.</param>
    ''' <param name="pEntityID">The p entity ID.</param>
    ''' <param name="pSelectString">The p select string.</param>
	Public Sub StatusReport(ByVal pReportName As String, ByVal pGroupID As Int32, ByVal pEntityID As Int32, ByVal pSelectString As String)
		StatusReport(pReportName, pGroupID, pEntityID, pSelectString, Nothing)
	End Sub

    ''' <summary>
    ''' Statuses the report.
    ''' </summary>
    ''' <param name="pReportName">Name of the p report.</param>
    ''' <param name="pGroupID">The p group ID.</param>
    ''' <param name="pEntityID">The p entity ID.</param>
    ''' <param name="pStatusBar">The p status bar.</param>
	Public Sub StatusReport(ByVal pReportName As String, ByVal pGroupID As Int32, ByVal pEntityID As Int32, ByRef pStatusBar As ToolStripProgressBar)
		StatusReport(pReportName, pGroupID, pEntityID, "True", pStatusBar)
	End Sub

    ''' <summary>
    ''' Statuses the report.
    ''' </summary>
    ''' <param name="pReportName">Name of the p report.</param>
    ''' <param name="pGroupID">The p group ID.</param>
    ''' <param name="pEntityID">The p entity ID.</param>
    ''' <param name="pSelectString">The p select string.</param>
    ''' <param name="pStatusBar">The p status bar.</param>
	Public Sub StatusReport(ByVal pReportName As String, ByVal pGroupID As Int32, ByVal pEntityID As Int32, ByVal pSelectString As String, ByRef pStatusBar As ToolStripProgressBar)
		' **************************************************************************
		'
		'
		' **************************************************************************
		Static ReportIsProcessing As Boolean = False

		Dim RptCommand As New SqlCommand
		Dim tblFlorenceStatusReport As New DataTable("tblFlorenceStatusReport")

		If (ReportIsProcessing) Then
			MessageBox.Show("A Status Report is currently processing.", "", MessageBoxButtons.OK, MessageBoxIcon.Hand, MessageBoxDefaultButton.Button1)
			Exit Sub
		End If

		Try
			' Clear existing 'tblTableOfContents'
			If (Not (tblTableOfContents Is Nothing)) Then
				tblTableOfContents.Rows.Clear()
			End If
			tblTableOfContents = Nothing

			' Start Report
			ReportIsProcessing = True

			RptCommand.Connection = New SqlConnection(MainForm.SQLConnectString)
			RptCommand.CommandType = CommandType.StoredProcedure
			RptCommand.CommandText = "[rpt_Florence_StatusReportWithData]" ' "[rpt_Florence_StatusReport]"
			RptCommand.Parameters.Clear()
			RptCommand.Parameters.Add(New SqlParameter("@ItemGroupID", SqlDbType.Int, 4)).Value = pGroupID
			RptCommand.Parameters.Add(New SqlParameter("@EntityID", SqlDbType.Int, 4)).Value = pEntityID
			RptCommand.Parameters.Add(New SqlParameter("@KnowledgeDate", SqlDbType.DateTime, 8)).Value = MainForm.Main_Knowledgedate
			RptCommand.CommandTimeout = DEFAULT_SQLCOMMAND_TIMEOUT

			RptCommand.Connection.Open()

			Dim RptReader As SqlDataReader = RptCommand.ExecuteReader()
			tblFlorenceStatusReport.Load(RptReader)
			RptCommand.Connection.Close()
			RptCommand.Connection = Nothing
			RptCommand = Nothing

			' Get Report Definition
			Dim thisReport As C1Report

			' Get report definition.

			thisReport = GetReportDefinition(0, pReportName, pReportName)

			If (thisReport Is Nothing) Then
				MainForm.LogError("ReportHandler", LOG_LEVELS.Error, "", "Failed to get definition for report '" & pReportName & "'", "", True)
				Exit Sub
			End If

			' 
			If (pReportName = "rptFileStatusReport") Then
				LogTableOfContents(Nothing, Nothing) ' Clear Static variables.
				AddHandler thisReport.PrintSection, AddressOf LogTableOfContents
			End If

			If (pSelectString = "True") Then
				Call DisplayReport(thisReport, tblFlorenceStatusReport, Now.Date, MainForm.Main_Knowledgedate, pStatusBar)
			Else
				Dim rptDataView As New DataView(tblFlorenceStatusReport, pSelectString, "EntityID", DataViewRowState.CurrentRows)
				Call DisplayReport(thisReport, rptDataView, Now.Date, MainForm.Main_Knowledgedate, pStatusBar)
				rptDataView = Nothing
			End If

			tblFlorenceStatusReport = Nothing

			' Clear report object.
			If (pReportName = "rptFileStatusReport") Then
				RemoveHandler thisReport.PrintSection, AddressOf LogTableOfContents
			End If

		Catch ex As Exception
			MainForm.LogError("ReportHandler", LOG_LEVELS.Error, ex.Message, "Error running the Status report", ex.StackTrace, True)
		Finally
			ReportIsProcessing = False
		End Try

	End Sub

    ''' <summary>
    ''' Admissiblilities the status report.
    ''' </summary>
    ''' <param name="pReportName">Name of the p report.</param>
    ''' <param name="pFundID">The p fund ID.</param>
    ''' <param name="pGroupID">The p group ID.</param>
    ''' <param name="pEntityID">The p entity ID.</param>
    ''' <param name="pSelectString">The p select string.</param>
    ''' <param name="pStatusBar">The p status bar.</param>
	Public Sub AdmissiblilityStatusReport(ByVal pReportName As String, ByVal pFundID As Int32, ByVal pGroupID As Int32, ByVal pEntityID As Int32, ByVal pSelectString As String, ByRef pStatusBar As ToolStripProgressBar)
		' **************************************************************************
		'
		'
		' **************************************************************************
		Static ReportIsProcessing As Boolean = False

		Dim RptCommand As New SqlCommand
		Dim tblFlorenceStatusReport As New DataTable("tblFlorenceStatusReport")
		Dim thisReportRow As DataRow
		Dim thisDataType As Integer

		If (ReportIsProcessing) Then
			MessageBox.Show("An Admissibility Status Report is currently processing.", "", MessageBoxButtons.OK, MessageBoxIcon.Hand, MessageBoxDefaultButton.Button1)
			Exit Sub
		End If

		Try
			' Start Report
			ReportIsProcessing = True

			RptCommand.Connection = New SqlConnection(MainForm.SQLConnectString)
			RptCommand.CommandType = CommandType.StoredProcedure
			RptCommand.CommandText = "[rpt_Florence_AdmissibilityStatusReport]"
			RptCommand.Parameters.Clear()
			RptCommand.Parameters.Add(New SqlParameter("@FundID", SqlDbType.Int, 4)).Value = pFundID
			RptCommand.Parameters.Add(New SqlParameter("@ValueDate", SqlDbType.DateTime, 8)).Value = Now.Date
			RptCommand.Parameters.Add(New SqlParameter("@ItemGroupID", SqlDbType.Int, 4)).Value = pGroupID
			RptCommand.Parameters.Add(New SqlParameter("@EntityID", SqlDbType.Int, 4)).Value = pEntityID
			RptCommand.Parameters.Add(New SqlParameter("@KnowledgeDate", SqlDbType.DateTime, 8)).Value = MainForm.Main_Knowledgedate
			RptCommand.CommandTimeout = DEFAULT_SQLCOMMAND_TIMEOUT

			RptCommand.Connection.Open()

			Dim RptReader As SqlDataReader = RptCommand.ExecuteReader()
			tblFlorenceStatusReport.Load(RptReader)
			RptCommand.Connection.Close()
			RptCommand.Connection = Nothing
			RptCommand = Nothing

			' Set 'LatestValue' according to 'DataValueType'
			tblFlorenceStatusReport.Columns.Add(New DataColumn("LatestValue", GetType(String)))

			For Each thisReportRow In tblFlorenceStatusReport.Rows
				Try
					thisDataType = CInt(thisReportRow("DataValueType"))

					If (thisDataType = 0) Then
						thisReportRow("LatestValue") = "N/A"
					ElseIf (thisDataType And RenaissanceDataType.NumericType) Then
						thisReportRow("LatestValue") = CDbl(thisReportRow("NumericData")).ToString(DISPLAYMEMBER_DOUBLEFORMAT)
					ElseIf (thisDataType And RenaissanceDataType.PercentageType) Then
						thisReportRow("LatestValue") = CDbl(thisReportRow("PercentageData")).ToString(DISPLAYMEMBER_PERCENTAGEFORMAT)
					ElseIf (thisDataType And RenaissanceDataType.TextType) Then
						thisReportRow("LatestValue") = CStr(thisReportRow("TextData"))
					ElseIf (thisDataType And RenaissanceDataType.BooleanType) Then
						If CBool(thisReportRow("BooleanData")) Then
							thisReportRow("LatestValue") = "True"
						Else
							thisReportRow("LatestValue") = "False"
						End If
					ElseIf (thisDataType And RenaissanceDataType.DateType) Then
						thisReportRow("LatestValue") = CDate(thisReportRow("DateData")).ToString(DISPLAYMEMBER_DATEFORMAT)
					Else
						thisReportRow("LatestValue") = "<Unknown Data Type>"
					End If
				Catch ex As Exception
				End Try

			Next

			' Get Report Definition
			Dim thisReport As C1Report

			' Get report definition.

			Try
				thisReport = GetReportDefinition(0, pReportName, pReportName)
			Catch ex As Exception
				thisReport = Nothing
			End Try

			If (thisReport Is Nothing) Then
				MainForm.LogError("ReportHandler", LOG_LEVELS.Error, "", "Failed to get definition for report '" & pReportName & "'", "", True)
				Exit Sub
			End If

			' 
			If (pSelectString = "True") Then
				Call DisplayReport(thisReport, tblFlorenceStatusReport, Now.Date, MainForm.Main_Knowledgedate, pStatusBar)
			Else
				Dim rptDataView As New DataView(tblFlorenceStatusReport, pSelectString, "EntityID", DataViewRowState.CurrentRows)
				Call DisplayReport(thisReport, rptDataView, Now.Date, MainForm.Main_Knowledgedate, pStatusBar)
				rptDataView = Nothing
			End If

			tblFlorenceStatusReport = Nothing

		Catch ex As Exception
			MainForm.LogError("ReportHandler", LOG_LEVELS.Error, ex.Message, "Error running the Status report", ex.StackTrace, True)
		Finally
			ReportIsProcessing = False
		End Try

	End Sub


    ''' <summary>
    ''' Risks the report.
    ''' </summary>
    ''' <param name="pReportName">Name of the p report.</param>
    ''' <param name="pFundID">The p fund ID.</param>
    ''' <param name="pGroupID">The p group ID.</param>
    ''' <param name="pEntityID">The p entity ID.</param>
    ''' <param name="pSelectString">The p select string.</param>
    ''' <param name="pStatusBar">The p status bar.</param>
	Public Sub RiskReport(ByVal pReportName As String, ByVal pFundID As Int32, ByVal pGroupID As Int32, ByVal pEntityID As Int32, ByVal pSelectString As String, ByRef pStatusBar As ToolStripProgressBar)
		' **************************************************************************
		'
		'
		' **************************************************************************

		Static ReportIsProcessing As Boolean = False

		Dim RptCommand As New SqlCommand
		Dim tblFlorenceStatusReportData As New DataTable("tblFlorenceRiskReportData")
		Dim DataRows() As DataRow
		Dim ThisDataRow As DataRow = Nothing
		Dim NewReportRow As DataRow = Nothing
		Dim tblFlorenceStatusReport As New DataTable("tblFlorenceRiskReport")

		If (ReportIsProcessing) Then
			MessageBox.Show("A Risk Report is currently processing.", "", MessageBoxButtons.OK, MessageBoxIcon.Hand, MessageBoxDefaultButton.Button1)
			Exit Sub
		End If

		Try
			' Start Report
			ReportIsProcessing = True

			RptCommand.CommandType = CommandType.StoredProcedure
			RptCommand.CommandText = "[rpt_Florence_AdmissibilityRiskReport_Data]"
			RptCommand.Parameters.Clear()
			RptCommand.Parameters.Add(New SqlParameter("@FundID", SqlDbType.Int, 4)).Value = pFundID
			RptCommand.Parameters.Add(New SqlParameter("@ValueDate", SqlDbType.DateTime, 8)).Value = Now.Date
			RptCommand.Parameters.Add(New SqlParameter("@ItemGroupID", SqlDbType.Int, 4)).Value = pGroupID
			RptCommand.Parameters.Add(New SqlParameter("@EntityID", SqlDbType.Int, 4)).Value = pEntityID
			RptCommand.Parameters.Add(New SqlParameter("@KnowledgeDate", SqlDbType.DateTime, 8)).Value = MainForm.Main_Knowledgedate
			RptCommand.CommandTimeout = DEFAULT_SQLCOMMAND_TIMEOUT

			Try
				RptCommand.Connection = New SqlConnection(MainForm.SQLConnectString)
				RptCommand.Connection.Open()

				Dim RptReader As SqlDataReader = RptCommand.ExecuteReader()
				tblFlorenceStatusReportData.Load(RptReader)

			Catch ex As Exception
			Finally
				If (RptCommand.Connection IsNot Nothing) Then
					Try
						RptCommand.Connection.Close()
					Catch ex As Exception
					End Try
				End If

				RptCommand.Connection = Nothing
				RptCommand = Nothing
			End Try

			' Calculate Risk

			DataRows = tblFlorenceStatusReportData.Select("True", "Fund, ItemGroup, ItemID")

			tblFlorenceStatusReport.Columns.Add("Fund", GetType(Integer))
			tblFlorenceStatusReport.Columns.Add("FundCode", GetType(String))
			tblFlorenceStatusReport.Columns.Add("FundName", GetType(String))
			tblFlorenceStatusReport.Columns.Add("ItemGroup", GetType(Integer))
			tblFlorenceStatusReport.Columns.Add("ItemGroupDescription", GetType(String))
			tblFlorenceStatusReport.Columns.Add("ItemID", GetType(Integer))
			tblFlorenceStatusReport.Columns.Add("ItemDescription", GetType(String))
			tblFlorenceStatusReport.Columns.Add("ItemCategory", GetType(String))

			tblFlorenceStatusReport.Columns.Add("FundCount", GetType(Integer))
			tblFlorenceStatusReport.Columns.Add("FundPassCount", GetType(Integer))
			tblFlorenceStatusReport.Columns.Add("FundFailCount", GetType(Integer))
			tblFlorenceStatusReport.Columns.Add("FundPassOverdueCount", GetType(Integer))
			tblFlorenceStatusReport.Columns.Add("FundFailOverdueCount", GetType(Integer))
			tblFlorenceStatusReport.Columns.Add("USDValuePass", GetType(Double))
			tblFlorenceStatusReport.Columns.Add("USDValueFail", GetType(Double))

			tblFlorenceStatusReport.Columns.Add("LimitText1", GetType(String))
			tblFlorenceStatusReport.Columns.Add("LimitText2", GetType(String))
			tblFlorenceStatusReport.Columns.Add("LimitText3", GetType(String))
			tblFlorenceStatusReport.Columns.Add("LimitData1", GetType(Integer))
			tblFlorenceStatusReport.Columns.Add("LimitData2", GetType(Integer))
			tblFlorenceStatusReport.Columns.Add("LimitData3", GetType(Integer))
			tblFlorenceStatusReport.Columns.Add("LimitThreshold", GetType(Double))
			tblFlorenceStatusReport.Columns.Add("PassRate", GetType(Double))
			tblFlorenceStatusReport.Columns.Add("Pass1", GetType(String))
			tblFlorenceStatusReport.Columns.Add("Pass2", GetType(String))

			Dim ThisFund As Integer = 0
			Dim LastFund As Integer = 0
			Dim ThisItemGroup As Integer = 0
			Dim LastItemGroup As Integer = 0
			Dim ThisItemID As Integer = 0
			Dim LastItemID As Integer = 0

			Dim DataCounter As Integer = 0

			If (DataRows.Length > 0) Then
				ThisDataRow = DataRows(0)

				NewReportRow = Nothing

				For DataCounter = 0 To (DataRows.Length - 1)
					ThisDataRow = DataRows(DataCounter)

					LastFund = ThisFund
					LastItemGroup = ThisItemGroup
					LastItemID = ThisItemID

					ThisFund = CInt(ThisDataRow("Fund"))
					ThisItemGroup = CInt(ThisDataRow("ItemGroup"))
					ThisItemID = CInt(ThisDataRow("ItemID"))

					If (DataCounter > 0) Then
						If (ThisFund <> LastFund) OrElse (ThisItemGroup <> LastItemGroup) OrElse (ThisItemID <> LastItemID) Then
							' Add existing Row

							NewReportRow("Pass1") = CType(CInt(NewReportRow("FundPassCount")) / CInt(NewReportRow("FundCount")), Double).ToString("#,##0.0%")
							NewReportRow("Pass2") = CType(CDbl(NewReportRow("USDValuePass")) / (CDbl(NewReportRow("USDValuePass")) + CDbl(NewReportRow("USDValueFail"))), Double).ToString("#,##0.0%")
							NewReportRow("PassRate") = CDbl(NewReportRow("USDValuePass")) / (CDbl(NewReportRow("USDValuePass")) + CDbl(NewReportRow("USDValueFail")))

							tblFlorenceStatusReport.Rows.Add(NewReportRow)
							NewReportRow = Nothing
						End If
					End If

					If (NewReportRow Is Nothing) Then
						NewReportRow = tblFlorenceStatusReport.NewRow

						NewReportRow("Fund") = ThisFund
						NewReportRow("FundCode") = ThisDataRow("FundCode")
						NewReportRow("FundName") = ThisDataRow("FundName")
						NewReportRow("ItemGroup") = ThisItemGroup
						NewReportRow("ItemGroupDescription") = ThisDataRow("ItemGroupDescription")
						NewReportRow("ItemID") = ThisItemID
						NewReportRow("ItemDescription") = ThisDataRow("ItemDescription")
						NewReportRow("ItemCategory") = ThisDataRow("Category")
						NewReportRow("FundCount") = 0
						NewReportRow("FundPassCount") = 0
						NewReportRow("FundFailCount") = 0
						NewReportRow("FundPassOverdueCount") = 0
						NewReportRow("FundFailOverdueCount") = 0
						NewReportRow("USDValuePass") = 0
						NewReportRow("USDValueFail") = 0
						NewReportRow("LimitText1") = ""
						NewReportRow("LimitText2") = ""
						NewReportRow("LimitText3") = ""
						NewReportRow("LimitData1") = 0
						NewReportRow("LimitData2") = 0
						NewReportRow("LimitData3") = 0
						NewReportRow("LimitThreshold") = CDbl(ThisDataRow("LimitThreshold"))
						NewReportRow("PassRate") = 0
						NewReportRow("Pass1") = ""
						NewReportRow("Pass2") = ""

						Dim LimitFormat As String
						If (CBool(ThisDataRow("LimitIsPercent")) = False) Then
							LimitFormat = "#,##0.00"
						Else
							LimitFormat = "#,##0.00%"
						End If

						Select Case CInt(ThisDataRow("LimitType"))
							Case RenaissanceGlobals.FlorenceLimitType.Between
								NewReportRow("LimitText1") = "< " & CDbl(ThisDataRow("FromLimit")).ToString(LimitFormat)
								NewReportRow("LimitText2") = "Admissible"
								NewReportRow("LimitText3") = "> " & CDbl(ThisDataRow("ToLimit")).ToString(LimitFormat)

							Case RenaissanceGlobals.FlorenceLimitType.GreaterThan
								NewReportRow("LimitText1") = "< " & CDbl(ThisDataRow("FromLimit")).ToString(LimitFormat)
								NewReportRow("LimitText2") = "Admissible"
								NewReportRow("LimitText3") = ""

							Case RenaissanceGlobals.FlorenceLimitType.LessThan
								NewReportRow("LimitText1") = ""
								NewReportRow("LimitText2") = "Admissible"
								NewReportRow("LimitText3") = "> " & CDbl(ThisDataRow("ToLimit")).ToString(LimitFormat)

							Case RenaissanceGlobals.FlorenceLimitType.Outside
								NewReportRow("LimitText1") = "< " & CDbl(ThisDataRow("FromLimit")).ToString(LimitFormat)
								NewReportRow("LimitText2") = "Not-Admissible"
								NewReportRow("LimitText3") = "> " & CDbl(ThisDataRow("ToLimit")).ToString(LimitFormat)

							Case RenaissanceGlobals.FlorenceLimitType.Boolean
								NewReportRow("LimitText1") = "In-Admissible"
								NewReportRow("LimitText2") = "Admissible"
								NewReportRow("LimitText3") = ""

							Case Else

						End Select
					End If

					' Calculate Risk figures
					NewReportRow("FundCount") = (CInt(NewReportRow("FundCount")) + 1)

					Dim FromLimit As Double
					Dim ToLimit As Double
					Dim ThisValue As Double
					Dim ThisValueType As Integer
					Dim IsOverdue As Integer

					FromLimit = CDbl(ThisDataRow("FromLimit"))
					ToLimit = CDbl(ThisDataRow("ToLimit"))
					ThisValueType = CInt(ThisDataRow("DataValueType"))
					ThisValue = 0
					IsOverdue = CInt(ThisDataRow("IsDataOverdue"))

					If (ThisValueType And RenaissanceGlobals.RenaissanceDataType.NumericType) > 0 Then
						ThisValue = CDbl(ThisDataRow("NumericData"))

					ElseIf (ThisValueType And RenaissanceGlobals.RenaissanceDataType.PercentageType) > 0 Then
						ThisValue = CDbl(ThisDataRow("PercentageData"))

					ElseIf (ThisValueType And RenaissanceGlobals.RenaissanceDataType.BooleanType) > 0 Then
						If (CBool(ThisDataRow("BooleanData")) = False) Then
							ThisValue = 0
						Else
							ThisValue = (-1)
						End If

					ElseIf (ThisValueType And RenaissanceGlobals.RenaissanceDataType.DateType) > 0 Then
						ThisValue = CDate(ThisDataRow("PercentageData")).ToOADate

					End If

					Select Case CInt(ThisDataRow("LimitType"))
						Case RenaissanceGlobals.FlorenceLimitType.Between

							If (ThisValue < FromLimit) Then
								NewReportRow("LimitData1") = CInt(NewReportRow("LimitData1")) + 1
								NewReportRow("USDValueFail") = CDbl(NewReportRow("USDValueFail")) + CDbl(ThisDataRow("USDValue"))
								NewReportRow("FundFailCount") = CInt(NewReportRow("FundFailCount")) + 1

								If (IsOverdue <> 0) Then
									NewReportRow("FundFailOverdueCount") = CInt(NewReportRow("FundFailOverdueCount")) + 1
								End If

							ElseIf (ThisValue > ToLimit) Then
								NewReportRow("LimitData3") = CInt(NewReportRow("LimitData3")) + 1
								NewReportRow("USDValueFail") = CDbl(NewReportRow("USDValueFail")) + CDbl(ThisDataRow("USDValue"))
								NewReportRow("FundFailCount") = CInt(NewReportRow("FundFailCount")) + 1

								If (IsOverdue <> 0) Then
									NewReportRow("FundFailOverdueCount") = CInt(NewReportRow("FundFailOverdueCount")) + 1
								End If

							Else
								NewReportRow("LimitData2") = CInt(NewReportRow("LimitData2")) + 1
								NewReportRow("USDValuePass") = CDbl(NewReportRow("USDValuePass")) + CDbl(ThisDataRow("USDValue"))
								NewReportRow("FundPassCount") = CInt(NewReportRow("FundPassCount")) + 1

								If (IsOverdue <> 0) Then
									NewReportRow("FundPassOverdueCount") = CInt(NewReportRow("FundPassOverdueCount")) + 1
								End If
							End If

						Case RenaissanceGlobals.FlorenceLimitType.GreaterThan
							If (ThisValue < FromLimit) Then
								NewReportRow("LimitData1") = CInt(NewReportRow("LimitData1")) + 1
								NewReportRow("USDValueFail") = CDbl(NewReportRow("USDValueFail")) + CDbl(ThisDataRow("USDValue"))
								NewReportRow("FundFailCount") = CInt(NewReportRow("FundFailCount")) + 1

								If (IsOverdue <> 0) Then
									NewReportRow("FundFailOverdueCount") = CInt(NewReportRow("FundFailOverdueCount")) + 1
								End If

							Else
								NewReportRow("LimitData2") = CInt(NewReportRow("LimitData2")) + 1
								NewReportRow("USDValuePass") = CDbl(NewReportRow("USDValuePass")) + CDbl(ThisDataRow("USDValue"))
								NewReportRow("FundPassCount") = CInt(NewReportRow("FundPassCount")) + 1

								If (IsOverdue <> 0) Then
									NewReportRow("FundPassOverdueCount") = CInt(NewReportRow("FundPassOverdueCount")) + 1
								End If
							End If

						Case RenaissanceGlobals.FlorenceLimitType.LessThan
							If (ThisValue > FromLimit) Then
								NewReportRow("LimitData3") = CInt(NewReportRow("LimitData3")) + 1
								NewReportRow("USDValueFail") = CDbl(NewReportRow("USDValueFail")) + CDbl(ThisDataRow("USDValue"))
								NewReportRow("FundFailCount") = CInt(NewReportRow("FundFailCount")) + 1

								If (IsOverdue <> 0) Then
									NewReportRow("FundFailOverdueCount") = CInt(NewReportRow("FundFailOverdueCount")) + 1
								End If

							Else
								NewReportRow("LimitData2") = CInt(NewReportRow("LimitData2")) + 1
								NewReportRow("USDValuePass") = CDbl(NewReportRow("USDValuePass")) + CDbl(ThisDataRow("USDValue"))
								NewReportRow("FundPassCount") = CInt(NewReportRow("FundPassCount")) + 1

								If (IsOverdue <> 0) Then
									NewReportRow("FundPassOverdueCount") = CInt(NewReportRow("FundPassOverdueCount")) + 1
								End If
							End If

						Case RenaissanceGlobals.FlorenceLimitType.Outside
							If (ThisValue < FromLimit) Then
								NewReportRow("LimitData1") = CInt(NewReportRow("LimitData1")) + 1
								NewReportRow("USDValuePass") = CDbl(NewReportRow("USDValuePass")) + CDbl(ThisDataRow("USDValue"))
								NewReportRow("FundPassCount") = CInt(NewReportRow("FundPassCount")) + 1

								If (IsOverdue <> 0) Then
									NewReportRow("FundPassOverdueCount") = CInt(NewReportRow("FundPassOverdueCount")) + 1
								End If

							ElseIf (ThisValue > ToLimit) Then
								NewReportRow("LimitData3") = CInt(NewReportRow("LimitData3")) + 1
								NewReportRow("USDValuePass") = CDbl(NewReportRow("USDValuePass")) + CDbl(ThisDataRow("USDValue"))
								NewReportRow("FundPassCount") = CInt(NewReportRow("FundPassCount")) + 1

								If (IsOverdue <> 0) Then
									NewReportRow("FundPassOverdueCount") = CInt(NewReportRow("FundPassOverdueCount")) + 1
								End If

							Else
								NewReportRow("LimitData2") = CInt(NewReportRow("LimitData2")) + 1
								NewReportRow("USDValueFail") = CDbl(NewReportRow("USDValueFail")) + CDbl(ThisDataRow("USDValue"))
								NewReportRow("FundFailCount") = CInt(NewReportRow("FundFailCount")) + 1

								If (IsOverdue <> 0) Then
									NewReportRow("FundFailOverdueCount") = CInt(NewReportRow("FundFailOverdueCount")) + 1
								End If
							End If

						Case RenaissanceGlobals.FlorenceLimitType.Boolean

							If (CBool(ThisValue) = CBool(FromLimit)) Then
								NewReportRow("LimitData2") = CInt(NewReportRow("LimitData2")) + 1
								NewReportRow("USDValuePass") = CDbl(NewReportRow("USDValuePass")) + CDbl(ThisDataRow("USDValue"))
								NewReportRow("FundPassCount") = CInt(NewReportRow("FundPassCount")) + 1

								If (IsOverdue <> 0) Then
									NewReportRow("FundPassOverdueCount") = CInt(NewReportRow("FundPassOverdueCount")) + 1
								End If

							Else
								NewReportRow("LimitData1") = CInt(NewReportRow("LimitData1")) + 1
								NewReportRow("USDValueFail") = CDbl(NewReportRow("USDValueFail")) + CDbl(ThisDataRow("USDValue"))
								NewReportRow("FundFailCount") = CInt(NewReportRow("FundFailCount")) + 1

								If (IsOverdue <> 0) Then
									NewReportRow("FundFailOverdueCount") = CInt(NewReportRow("FundFailOverdueCount")) + 1
								End If
							End If

						Case Else

					End Select

				Next

				' Add final row to results table

				NewReportRow("Pass1") = CType(CInt(NewReportRow("FundPassCount")) / CInt(NewReportRow("FundCount")), Double).ToString("#,##0.0%")
				NewReportRow("Pass2") = CType(CDbl(NewReportRow("USDValuePass")) / (CDbl(NewReportRow("USDValuePass")) + CDbl(NewReportRow("USDValueFail"))), Double).ToString("#,##0.0%")
				NewReportRow("PassRate") = CDbl(NewReportRow("USDValuePass")) / (CDbl(NewReportRow("USDValuePass")) + CDbl(NewReportRow("USDValueFail")))

				tblFlorenceStatusReport.Rows.Add(NewReportRow)

			End If


			' Get Report Definition
			Dim thisReport As C1Report

			' Get report definition.

			thisReport = GetReportDefinition(0, pReportName, pReportName)

			If (thisReport Is Nothing) Then
				MainForm.LogError("ReportHandler", LOG_LEVELS.Error, "", "Failed to get definition for report '" & pReportName & "'", "", True)
				Exit Sub
			End If

			' 
			If (pSelectString = "True") Then
				Call DisplayReport(thisReport, tblFlorenceStatusReport, Now.Date, MainForm.Main_Knowledgedate, pStatusBar)
			Else
				Dim rptDataView As New DataView(tblFlorenceStatusReport, pSelectString, Nothing, DataViewRowState.CurrentRows)
				Call DisplayReport(thisReport, rptDataView, Now.Date, MainForm.Main_Knowledgedate, pStatusBar)
				rptDataView = Nothing
			End If

			tblFlorenceStatusReport = Nothing

		Catch ex As Exception
			MainForm.LogError("ReportHandler", LOG_LEVELS.Error, ex.Message, "Error running the Status report", ex.StackTrace, True)
		Finally
			ReportIsProcessing = False
		End Try

	End Sub

    ''' <summary>
    ''' Logs the table of contents.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="ReportEventArgs"/> instance containing the event data.</param>
	Private Sub LogTableOfContents(ByVal sender As Object, ByVal e As ReportEventArgs)
		' **************************************************************************
		'
		'
		' **************************************************************************
		Static LastEntity As String
		Static LastCategory As String
		Static LastItem As String

		Try
			' Clear Static variables ?
			If (sender Is Nothing) Then
				LastEntity = ""
				LastCategory = ""
				LastItem = ""
				Exit Sub
			End If

			' Validate Sender.
			If (Not (TypeOf sender Is C1Report)) Then
				Exit Sub
			End If
		Catch ex As Exception
			Exit Sub
		End Try

		Try
			' Cast Sender to C1Report in order to get at the 'Tag' property
			Dim thisReport As C1Report = CType(sender, C1Report)

			Dim thisEntity As String
			Dim thisEntityID As Integer
			Dim thisCategory As String
			Dim thisItem As String
			Dim thisPageNumber As Integer

			Dim thisRow As DataRow

			' Establish Table ?
			If (tblTableOfContents Is Nothing) Then
				tblTableOfContents = New DataTable("tblTableOfContents")
				tblTableOfContents.Columns.Add("EntityName", GetType(String))
				tblTableOfContents.Columns.Add("EntityID", GetType(Integer))
				tblTableOfContents.Columns.Add("Category", GetType(String))
				tblTableOfContents.Columns.Add("ItemDescription", GetType(String))
				tblTableOfContents.Columns.Add("PageNumber", GetType(Integer))
			End If

			If (Not IsNumeric(thisReport.Fields("Text_EntityID").Value)) Then
				Exit Sub
			End If

			' thisLastEntity = thisReport.Evaluate
			thisEntity = thisReport.Fields("Text_EntityName").Value.ToString
			thisEntityID = CInt(thisReport.Fields("Text_EntityID").Value)
			thisCategory = thisReport.Fields("Text_Category").Value.ToString
			thisItem = thisReport.Fields("Text_ItemDescription").Value.ToString
			thisPageNumber = thisReport.Page


			If (Not (e.Section = SectionTypeEnum.Detail)) OrElse (thisEntity.CompareTo(LastEntity) = 0) AndAlso (thisCategory.CompareTo(LastCategory) = 0) AndAlso (thisItem.CompareTo(LastItem) = 0) Then
				Exit Sub
			End If

			thisRow = tblTableOfContents.NewRow
			thisRow("EntityName") = thisEntity
			thisRow("EntityID") = thisEntityID
			thisRow("Category") = thisCategory
			thisRow("ItemDescription") = thisItem
			thisRow("PageNumber") = thisPageNumber

			tblTableOfContents.Rows.Add(thisRow)
			tblTableOfContents.AcceptChanges()

			LastEntity = thisEntity
			LastCategory = thisCategory
			LastItem = thisItem

		Catch ex As Exception
		End Try


	End Sub

    ''' <summary>
    ''' Statuses the index report.
    ''' </summary>
    ''' <param name="pEntityID">The p entity ID.</param>
    ''' <param name="pStatusBar">The p status bar.</param>
	Public Sub StatusIndexReport(ByVal pEntityID As Int32, ByRef pStatusBar As ToolStripProgressBar)
		' **************************************************************************
		'
		'
		' **************************************************************************
		Static ReportIsProcessing As Boolean = False


		If (ReportIsProcessing) Then
			MessageBox.Show("A Cover Status Report is currently processing.", "", MessageBoxButtons.OK, MessageBoxIcon.Hand, MessageBoxDefaultButton.Button1)
			Exit Sub
		End If

		Try
			ReportIsProcessing = True

			'  Dim SelectedRows(-1) As DataRow
			Dim thisDataView As DataView = Nothing

			' Generate Index Information ? 

			If Not (tblTableOfContents Is Nothing) Then
				If (pEntityID = 0) Then
					thisDataView = New DataView(tblTableOfContents, "True", "EntityName, Category, ItemDescription", DataViewRowState.CurrentRows)
				Else
					thisDataView = New DataView(tblTableOfContents, "EntityID=" & pEntityID.ToString, "EntityName, Category, ItemDescription", DataViewRowState.CurrentRows)
				End If
			End If

			If (thisDataView Is Nothing) OrElse (thisDataView.Count <= 0) Then
				If Not (MessageBox.Show("The D/D File Status report must be run in order to generate the Index information for this report." & vbCrLf & "Do you want to run the Status Report now ?", "", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = DialogResult.Yes) Then
					Exit Sub
				End If

				Call Me.StatusReport("rptFileStatusReport", 0, pEntityID, "True", pStatusBar)

				If (pEntityID = 0) Then
					thisDataView = New DataView(tblTableOfContents, "True", "EntityName, Category, ItemDescription", DataViewRowState.CurrentRows)
				Else
					thisDataView = New DataView(tblTableOfContents, "EntityID=" & pEntityID.ToString, "EntityName, Category, ItemDescription", DataViewRowState.CurrentRows)
				End If
			End If

			' Run Report

			DisplayReport("rptTableOfContents", thisDataView, 0, Now.Date, MainForm.Main_Knowledgedate, pStatusBar)

		Catch ex As Exception
			MainForm.LogError("ReportHandler", LOG_LEVELS.Error, ex.Message, "Error running the File Status report", ex.StackTrace, True)
		Finally
			ReportIsProcessing = False
		End Try

	End Sub


    ''' <summary>
    ''' Covers the insert report.
    ''' </summary>
    ''' <param name="pEntityID">The p entity ID.</param>
    ''' <param name="pStatusBar">The p status bar.</param>
	Public Sub CoverInsertReport(ByVal pEntityID As Int32, ByRef pStatusBar As ToolStripProgressBar)
		' **************************************************************************
		'
		'
		' **************************************************************************
		Static ReportIsProcessing As Boolean = False


		If (ReportIsProcessing) Then
			MessageBox.Show("A Cover Insert Report is currently processing.", "", MessageBoxButtons.OK, MessageBoxIcon.Hand, MessageBoxDefaultButton.Button1)
			Exit Sub
		End If

		Try
			ReportIsProcessing = True

			Dim thisEntityDS As RenaissanceDataClass.DSFlorenceEntity
			thisEntityDS = MainForm.Load_Table(RenaissanceStandardDatasets.tblFlorenceEntity)

			If (thisEntityDS Is Nothing) Then
				MainForm.LogError("ReportHandler", LOG_LEVELS.Warning, "", "Error Loading tblFlorenceEntity", "", True)
				Exit Sub
			End If

			If (pEntityID = 0) Then
				DisplayReport("rptCoverInsert", thisEntityDS.tblFlorenceEntity, 0, Now.Date, MainForm.Main_Knowledgedate, pStatusBar)
			Else
				Dim rptDataView As New DataView(thisEntityDS.tblFlorenceEntity, "EntityID=" & pEntityID.ToString, "EntityID", DataViewRowState.CurrentRows)
				DisplayReport("rptCoverInsert", rptDataView, 0, Now.Date, MainForm.Main_Knowledgedate, pStatusBar)
			End If

		Catch ex As Exception
			MainForm.LogError("ReportHandler", LOG_LEVELS.Error, ex.Message, "Error running the Cover Insert report", ex.StackTrace, True)
		Finally
			ReportIsProcessing = False
		End Try

	End Sub


    ''' <summary>
    ''' Spines the insert report.
    ''' </summary>
    ''' <param name="pEntityID">The p entity ID.</param>
    ''' <param name="pStatusBar">The p status bar.</param>
  Public Sub SpineInsertReport(ByVal pEntityID As Int32, ByRef pStatusBar As ToolStripProgressBar)
    ' **************************************************************************
    '
    '
    ' **************************************************************************
    Static ReportIsProcessing As Boolean = False


    If (ReportIsProcessing) Then
      MessageBox.Show("A Spine Insert Report is currently processing.", "", MessageBoxButtons.OK, MessageBoxIcon.Hand, MessageBoxDefaultButton.Button1)
      Exit Sub
    End If

    Try
      ReportIsProcessing = True

			Dim thisEntityDS As RenaissanceDataClass.DSFlorenceEntity
			thisEntityDS = MainForm.Load_Table(RenaissanceStandardDatasets.tblFlorenceEntity)

			If (thisEntityDS Is Nothing) Then
				MainForm.LogError("ReportHandler", LOG_LEVELS.Warning, "", "Error Loading tblFlorenceEntity", "", True)
				Exit Sub
			End If

			If (pEntityID = 0) Then
				DisplayReport("rptSpineInsert", thisEntityDS.tblFlorenceEntity, 0, Now.Date, MainForm.Main_Knowledgedate, pStatusBar)
			Else
				Dim rptDataView As New DataView(thisEntityDS.tblFlorenceEntity, "EntityID=" & pEntityID.ToString, "EntityID", DataViewRowState.CurrentRows)
				DisplayReport("rptSpineInsert", rptDataView, 0, Now.Date, MainForm.Main_Knowledgedate, pStatusBar)
			End If

		Catch ex As Exception
			MainForm.LogError("ReportHandler", LOG_LEVELS.Error, ex.Message, "Error running the Spine Insert report", ex.StackTrace, True)
		Finally
			ReportIsProcessing = False
		End Try

	End Sub

    ''' <summary>
    ''' Signs the off sheet report.
    ''' </summary>
    ''' <param name="pEntityID">The p entity ID.</param>
    ''' <param name="pStatusBar">The p status bar.</param>
	Public Sub SignOffSheetReport(ByVal pEntityID As Int32, ByRef pStatusBar As ToolStripProgressBar)
		' **************************************************************************
		' SignOffSheetReport
		'
		'
		' **************************************************************************
		Static ReportIsProcessing As Boolean = False


		If (ReportIsProcessing) Then
			MessageBox.Show("A Sign-Off Sheet Report is currently processing.", "", MessageBoxButtons.OK, MessageBoxIcon.Hand, MessageBoxDefaultButton.Button1)
			Exit Sub
		End If

		Try
			ReportIsProcessing = True

			Dim thisEntityDS As RenaissanceDataClass.DSFlorenceEntity
			thisEntityDS = MainForm.Load_Table(RenaissanceStandardDatasets.tblFlorenceEntity)

			If (thisEntityDS Is Nothing) Then
				MainForm.LogError("ReportHandler", LOG_LEVELS.Warning, "", "Error Loading tblFlorenceEntity", "", True)
				Exit Sub
			End If

			If (pEntityID = 0) Then
				DisplayReport("rptSignOffSheet", thisEntityDS.tblFlorenceEntity, 0, Now.Date, MainForm.Main_Knowledgedate, pStatusBar)
			Else
				Dim rptDataView As New DataView(thisEntityDS.tblFlorenceEntity, "EntityID=" & pEntityID.ToString, "EntityID", DataViewRowState.CurrentRows)
				DisplayReport("rptSignOffSheet", rptDataView, 0, Now.Date, MainForm.Main_Knowledgedate, pStatusBar)
			End If

		Catch ex As Exception
			MainForm.LogError("ReportHandler", LOG_LEVELS.Error, ex.Message, "Error running the SignOffSheet report", ex.StackTrace, True)
		Finally
			ReportIsProcessing = False
		End Try

	End Sub

    ''' <summary>
    ''' Keys the details report.
    ''' </summary>
    ''' <param name="pEntityID">The p entity ID.</param>
    ''' <param name="pSelectString">The p select string.</param>
    ''' <param name="pStatusBar">The p status bar.</param>
  Public Sub KeyDetailsReport(ByVal pEntityID As Int32, ByVal pSelectString As String, ByRef pStatusBar As ToolStripProgressBar)
    ' **************************************************************************
    ' KeyDetailsReport
    '
    '
    ' **************************************************************************
    Static ReportIsProcessing As Boolean = False

    Dim RptCommand As New SqlCommand
    Dim tblFlorenceKeyDetailsReport As New DataTable("tblFlorenceKeyDetailsReport")

    If (ReportIsProcessing) Then
      MessageBox.Show("A Key Details Report is currently processing.", "", MessageBoxButtons.OK, MessageBoxIcon.Hand, MessageBoxDefaultButton.Button1)
      Exit Sub
    End If

    Try
      ReportIsProcessing = True

      RptCommand.Connection = New SqlConnection(MainForm.SQLConnectString)
      RptCommand.CommandType = CommandType.StoredProcedure
      RptCommand.CommandText = "[rpt_Florence_StatusReport]"
      RptCommand.Parameters.Clear()
      RptCommand.Parameters.Add(New SqlParameter("@ItemGroupID", SqlDbType.Int, 4)).Value = 0
      RptCommand.Parameters.Add(New SqlParameter("@EntityID", SqlDbType.Int, 4)).Value = pEntityID
      RptCommand.Parameters.Add(New SqlParameter("@KnowledgeDate", SqlDbType.DateTime, 8)).Value = MainForm.Main_Knowledgedate
      RptCommand.CommandTimeout = DEFAULT_SQLCOMMAND_TIMEOUT

      RptCommand.Connection.Open()

      Dim RptReader As SqlDataReader = RptCommand.ExecuteReader()
      tblFlorenceKeyDetailsReport.Load(RptReader)
      RptCommand.Connection.Close()
      RptCommand.Connection = Nothing
      RptCommand = Nothing

      Dim rptDataView As New DataView(tblFlorenceKeyDetailsReport, pSelectString, "EntityName, ItemDescription", DataViewRowState.CurrentRows)
      DisplayReport("rptKeyDetails", rptDataView, 0, Now.Date, MainForm.Main_Knowledgedate, pStatusBar)

    Catch ex As Exception
      MainForm.LogError("ReportHandler", LOG_LEVELS.Error, ex.Message, "Error running the Key Details report", ex.StackTrace, True)
    Finally
      ReportIsProcessing = False
    End Try

  End Sub

#End Region



End Class
