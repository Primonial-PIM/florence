' ***********************************************************************
' Assembly         : Florence
' Author           : Nicholas Pennington
' Created          : 11-28-2012
'
' Last Modified By : acullig
' Last Modified On : 03-01-2013
' ***********************************************************************
' <copyright file="Globals.vb" company="">
'     Copyright (c) . All rights reserved.
' </copyright>
' <summary></summary>
' ***********************************************************************
''' <summary>
''' Class Globals
''' </summary>
Module Globals

#Region " Global Declarations"

    ''' <summary>
    ''' The SPLAS h_ DISPLAY
    ''' </summary>
  Public Const SPLASH_DISPLAY As Integer = 3 ' Minimum Duration of Splash Display

  ' Base registry key for program settings

    ''' <summary>
    ''' The REGISTR y_ BASE
    ''' </summary>
	Public Const REGISTRY_BASE As String = "Software\Primonial\Florence"

  ' Standard Connection Name, as used with the DataHandler object.

    ''' <summary>
    ''' The FLORENC e_ CONNECTION
    ''' </summary>
  Public Const FLORENCE_CONNECTION As String = "cnnFlorence"

  ' Standard Form Caching Counters

    ''' <summary>
    ''' The STANDAR d_ entry form_ CACH e_ COUNT
    ''' </summary>
  Public Const STANDARD_EntryForm_CACHE_COUNT As Integer = 2
    ''' <summary>
    ''' The STANDAR d_ report form_ CACH e_ COUNT
    ''' </summary>
  Public Const STANDARD_ReportForm_CACHE_COUNT As Integer = 1
    ''' <summary>
    ''' The STANDAR d_ single form_ CACH e_ COUNT
    ''' </summary>
  Public Const STANDARD_SingleForm_CACHE_COUNT As Integer = 1


  ' Standard reference Dates & timings.
  ' LAST_Second is used to convert KDs to a precise 'Whole Day' value, also used to
  ' evaluate the 'Whole Day' status of a given Date.

	' Public Const KNOWLEDGEDATE_NOW As Date = #1/1/1900#
	' Public Const Renaissance_BaseDate As Date = #1/1/1900#
    ''' <summary>
    ''' The renaissance_ base date DATA
    ''' </summary>
	Public Const Renaissance_BaseDateDATA As Date = #1/1/2000#
	' Public Const LAST_SECOND As Integer = 86399


  ' Standard SQL Command Timeouts

    ''' <summary>
    ''' The DEFAUL t_ SQLCOMMAN d_ TIMEOUT
    ''' </summary>
  Public Const DEFAULT_SQLCOMMAND_TIMEOUT As Integer = 600


  ' Standard Date Formats

	'Public Const DISPLAYMEMBER_DATEFORMAT As String = "dd MMM yyyy"
	'Public Const DISPLAYMEMBER_LONGDATEFORMAT As String = "dd MMM yyyy HH:mm:ss"
	'Public Const DISPLAYMEMBER_DOUBLEFORMAT As String = "#,##0.00"
	'Public Const DISPLAYMEMBER_PERCENTAGEFORMAT As String = "#,##0.00%"

	'Public Const REPORT_DATEFORMAT As String = "dd MMMM yyyy"
	'Public Const REPORT_LONGDATEFORMAT As String = "dd MMM yyyy HH:mm:ss"
	'Public Const REPORT_MonthAndYear_DATEFORMAT As String = "MMMM yyyy"

	'Public Const QUERY_SHORTDATEFORMAT As String = "dd MMM yyyy"
	'Public Const QUERY_LONGDATEFORMAT As String = "dd MMM yyyy HH:mm:ss"

	'Public Const TIMEFORMAT As String = "HH:mm:ss"

  ' Standard Florence form interface.
  ' Must be implemented by any form that is cached.

    ''' <summary>
    ''' Interface StandardFlorenceForm
    ''' </summary>
  Interface StandardFlorenceForm
        ''' <summary>
        ''' Gets or sets a value indicating whether this instance is over cancel button.
        ''' </summary>
        ''' <value><c>true</c> if this instance is over cancel button; otherwise, <c>false</c>.</value>
    Property IsOverCancelButton() As Boolean
        ''' <summary>
        ''' Gets a value indicating whether this instance is in paint.
        ''' </summary>
        ''' <value><c>true</c> if this instance is in paint; otherwise, <c>false</c>.</value>
    ReadOnly Property IsInPaint() As Boolean
        ''' <summary>
        ''' Gets a value indicating whether [in use].
        ''' </summary>
        ''' <value><c>true</c> if [in use]; otherwise, <c>false</c>.</value>
    ReadOnly Property InUse() As Boolean
        ''' <summary>
        ''' Gets a value indicating whether [form open failed].
        ''' </summary>
        ''' <value><c>true</c> if [form open failed]; otherwise, <c>false</c>.</value>
    ReadOnly Property FormOpenFailed() As Boolean
        ''' <summary>
        ''' Gets the main form.
        ''' </summary>
        ''' <value>The main form.</value>
    ReadOnly Property MainForm() As FlorenceMain
        ''' <summary>
        ''' Resets the form.
        ''' </summary>
    Sub ResetForm()
        ''' <summary>
        ''' Closes the form.
        ''' </summary>
    Sub CloseForm()
  End Interface

#End Region

  ' Form ID Enumeration. Intended to uniquely identify each Form Type

    ''' <summary>
    ''' Enum FlorenceFormID
    ''' </summary>
  Public Enum FlorenceFormID
    ' **************************************************************************************
    '
    ' Enumeration member name MUST be the same as the associated form object.
    ' this is so that the generic 'New_FlorenceForm()' function in the Florence Main form will 
    ' be able to instantiate the correct object.
    '
    ' **************************************************************************************

        ''' <summary>
        ''' The none
        ''' </summary>
    None = 0
        ''' <summary>
        ''' The FRM florence main
        ''' </summary>
    frmFlorenceMain

        ''' <summary>
        ''' The FRM about
        ''' </summary>
    frmAbout

        ''' <summary>
        ''' The FRM entity
        ''' </summary>
    frmEntity
        ''' <summary>
        ''' The FRM group
        ''' </summary>
    frmGroup
        ''' <summary>
        ''' The FRM items
        ''' </summary>
    frmItems
        ''' <summary>
        ''' The FRM select status list
        ''' </summary>
    frmSelectStatusList
        ''' <summary>
        ''' The FRM set knowledge date
        ''' </summary>
    frmSetKnowledgeDate
        ''' <summary>
        ''' The FRM status
        ''' </summary>
    frmStatus
        ''' <summary>
        ''' The FRM update data items
        ''' </summary>
    frmUpdateDataItems

        ''' <summary>
        ''' The FRM user permissions
        ''' </summary>
    frmUserPermissions

    ' Reports :-

        ''' <summary>
        ''' The FRM view report
        ''' </summary>
    frmViewReport

        ''' <summary>
        ''' The FRM florence admissibility status RPT
        ''' </summary>
		frmFlorenceAdmissibilityStatusRpt
        ''' <summary>
        ''' The FRM florence checklist RPT
        ''' </summary>
    frmFlorenceChecklistRpt
        ''' <summary>
        ''' The FRM florence file reports
        ''' </summary>
    frmFlorenceFileReports
        ''' <summary>
        ''' The FRM florence risk RPT
        ''' </summary>
    frmFlorenceRiskRpt
        ''' <summary>
        ''' The FRM florence status RPT
        ''' </summary>
    frmFlorenceStatusRpt

        ''' <summary>
        ''' The max value
        ''' </summary>
    MaxValue = 100
  End Enum

    ''' <summary>
    ''' Enum FlorenceReportID
    ''' </summary>
  Public Enum FlorenceReportID
    ' **************************************************************************************
    '
    ' Enumeration member name MUST be the same as the associated report.
    ' this is so that report permissioning can be performed.
    '
    ' **************************************************************************************

        ''' <summary>
        ''' The none
        ''' </summary>
    None = 0

        ''' <summary>
        ''' The RPT cover insert
        ''' </summary>
    rptCoverInsert

        ''' <summary>
        ''' The RPT florence admissibility status report
        ''' </summary>
    rptFlorenceAdmissibilityStatusReport
        ''' <summary>
        ''' The RPT florence checklist
        ''' </summary>
		rptFlorenceChecklist
        ''' <summary>
        ''' The RPT florence checklist brief
        ''' </summary>
		rptFlorenceChecklistBrief
        ''' <summary>
        ''' The RPT florence entitys
        ''' </summary>
		rptFlorenceEntitys
        ''' <summary>
        ''' The RPT florence groups
        ''' </summary>
    rptFlorenceGroups
        ''' <summary>
        ''' The RPT florence items
        ''' </summary>
    rptFlorenceItems
        ''' <summary>
        ''' The RPT key details
        ''' </summary>
    rptKeyDetails
        ''' <summary>
        ''' The RPT sign off sheet
        ''' </summary>
    rptSignOffSheet
        ''' <summary>
        ''' The RPT file status report
        ''' </summary>
    rptFileStatusReport
        ''' <summary>
        ''' The RPT florence risk report
        ''' </summary>
    rptFlorenceRiskReport
        ''' <summary>
        ''' The RPT florence status report
        ''' </summary>
    rptFlorenceStatusReport
        ''' <summary>
        ''' The RPT spine insert
        ''' </summary>
    rptSpineInsert
        ''' <summary>
        ''' The RPT table of contents
        ''' </summary>
    rptTableOfContents

    ' End Stop

        ''' <summary>
        ''' The max value
        ''' </summary>
    MaxValue
  End Enum

End Module
