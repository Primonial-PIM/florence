' ***********************************************************************
' Assembly         : Florence
' Author           : Nicholas Pennington
' Created          : 11-28-2012
'
' Last Modified By : acullig
' Last Modified On : 11-05-2012
' ***********************************************************************
' <copyright file="UtilityFunctions.vb" company="">
'     Copyright (c) . All rights reserved.
' </copyright>
' <summary></summary>
' ***********************************************************************
Imports System.Data.SqlClient

''' <summary>
''' Class UtilityFunctions
''' </summary>
Module UtilityFunctions

    ''' <summary>
    ''' Nzs the specified first object.
    ''' </summary>
    ''' <param name="FirstObject">The first object.</param>
    ''' <param name="SecondObject">The second object.</param>
    ''' <returns>System.Object.</returns>
  Friend Function Nz(ByVal FirstObject As Object, ByVal SecondObject As Object) As Object
    If (FirstObject Is Nothing) OrElse (FirstObject Is DBNull.Value) Then
      Return SecondObject
    Else
      Return FirstObject
    End If
  End Function

    ''' <summary>
    ''' Compares the value.
    ''' </summary>
    ''' <param name="val1">The val1.</param>
    ''' <param name="val2">The val2.</param>
    ''' <returns>System.Int32.</returns>
  Public Function CompareValue(ByVal val1 As IComparable, _
  ByVal val2 As IComparable) As Integer

    Try
      Return val1.CompareTo(val2)
    Catch ex As Exception
    End Try

    If IsNumeric(val1) AndAlso IsNumeric(val2) Then
      Try
        Return CDbl(val1).CompareTo(CDbl(val2))
      Catch ex As Exception
      End Try
    End If

    If IsDate(val1) AndAlso IsDate(val2) Then
      Try
        Return CDate(val1).CompareTo(CDate(val2))
      Catch ex As Exception
      End Try
    End If

    Return CStr(val1).ToUpper.CompareTo(CStr(val2).ToUpper)

  End Function



End Module
