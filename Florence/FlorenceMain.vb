' ***********************************************************************
' Assembly         : Florence
' Author           : Nicholas Pennington
' Created          : 11-28-2012
'
' Last Modified By : acullig
' Last Modified On : 08-07-2013
' ***********************************************************************
' <copyright file="FlorenceMain.vb" company="">
'     Copyright (c) . All rights reserved.
' </copyright>
' <summary></summary>
' ***********************************************************************
Imports Microsoft.Win32
Imports System.IO
Imports System.Data.SqlClient
Imports System.Net
Imports System.Net.Sockets
Imports TCPServer_Common
Imports System.Threading

Imports RenaissanceGlobals
Imports RenaissanceGlobals.Globals
Imports RenaissanceDataClass

''' <summary>
''' Class FlorenceMain
''' </summary>
Public Class FlorenceMain
	Implements RenaissanceGlobals.StandardRenaissanceMainForm

#Region " Class Globals and Declarations"

	' Declaration of the AutoUpdate Event.
	' Used to notify forms of changes.
    ''' <summary>
    ''' Occurs when [florence auto update].
    ''' </summary>
	Public Event FlorenceAutoUpdate(ByVal sender As Object, ByVal e As RenaissanceUpdateEventArgs) Implements RenaissanceGlobals.StandardRenaissanceMainForm.RenaissanceAutoUpdate

	' Collection of Florence Form Handles. One item for the Main-form and one item for each sub-Form.
    ''' <summary>
    ''' The _ florence forms
    ''' </summary>
	Private _FlorenceForms As New RenaissanceFormCollection

	' Central Data Structures.
    ''' <summary>
    ''' The florence TCP client
    ''' </summary>
  Private WithEvents FlorenceTCPClient As TCPServer_Common.GenericTCPClient
    ''' <summary>
    ''' The update event control
    ''' </summary>
	Private UpdateEventControl As UpdateEventControlClass

	' Application Knowledgedate
    ''' <summary>
    ''' The _ main_ knowledgedate
    ''' </summary>
	Private _Main_Knowledgedate As Date

	' Central Data and Report handlers
    ''' <summary>
    ''' The _ main data handler
    ''' </summary>
	Private _MainDataHandler As New RenaissanceDataClass.DataHandler
    ''' <summary>
    ''' The _ main adaptor handler
    ''' </summary>
	Private _MainAdaptorHandler As New RenaissanceDataClass.AdaptorHandler
    ''' <summary>
    ''' The _ main report handler
    ''' </summary>
	Private _MainReportHandler As New ReportHandler(Me)
    ''' <summary>
    ''' The _ pertrac data
    ''' </summary>
	Private _PertracData As New RenaissancePertracDataClass.PertracDataClass(Me)

	' Standard Message Server
    ''' <summary>
    ''' The _ message server name
    ''' </summary>
	Private _MessageServerName As String = ""
    ''' <summary>
    ''' The message server address
    ''' </summary>
	Private MessageServerAddress As IPAddress = IPAddress.None

	' SQL Server
    ''' <summary>
    ''' The SQL server name
    ''' </summary>
	Private SQLServerName As String = "<Unknown>"
	' SQL Server
	''' <summary>
	''' The SQL server instance name. Empty string for Default instance.
	''' </summary>
	Private SQLServerInstance As String = ""
	''' <summary>
	''' The _ SQL connection string
	''' </summary>
	Private _SQLConnectionString As String
    ''' <summary>
    ''' The _ SQL trusted connection
    ''' </summary>
	Private _SQLTrustedConnection As Boolean = True
    ''' <summary>
    ''' The _ SQL user name
    ''' </summary>
	Private _SQLUserName As String = ""
    ''' <summary>
    ''' The _ SQL password
    ''' </summary>
	Private _SQLPassword As String = ""

    ''' <summary>
    ''' The _ entry form_ cache count
    ''' </summary>
	Private _EntryForm_CacheCount As Integer = 0 ' STANDARD_EntryForm_CACHE_COUNT
    ''' <summary>
    ''' The _ report form_ cache count
    ''' </summary>
	Private _ReportForm_CacheCount As Integer = 0	' STANDARD_ReportForm_CACHE_COUNT
    ''' <summary>
    ''' The _ single cache_ cache count
    ''' </summary>
	Private _SingleCache_CacheCount As Integer = 0 ' STANDARD_SingleForm_CACHE_COUNT
    ''' <summary>
    ''' The _ no cache_ cache count
    ''' </summary>
	Private _NoCache_CacheCount As Integer = 0

    ''' <summary>
    ''' The STANDAR d_ report form_ CACH e_ COUNT
    ''' </summary>
	Public Const STANDARD_ReportForm_CACHE_COUNT As Integer = 1

    ''' <summary>
    ''' The splash
    ''' </summary>
	Dim Splash As New FlorenceSplash
    ''' <summary>
    ''' The splash time
    ''' </summary>
	Dim SplashTime As Date

    ''' <summary>
    ''' The _ mastername dictionary
    ''' </summary>
	Friend _MasternameDictionary As LookupCollection(Of Integer, String) = Nothing ' Dictionary(Of Integer, String)
    ''' <summary>
    ''' The _ locations dictionary
    ''' </summary>
	Friend _LocationsDictionary As LookupCollection(Of Integer, String) = Nothing	 ' Dictionary(Of Integer, String)

#End Region

#Region " Form Properties"

    ''' <summary>
    ''' Gets or sets the main_ knowledgedate.
    ''' </summary>
    ''' <value>The main_ knowledgedate.</value>
	Public Property Main_Knowledgedate() As Date Implements RenaissanceGlobals.StandardRenaissanceMainForm.Main_Knowledgedate
		' *******************************************************************************
		' Public property to Set the system Knowledgedate
		'
		' The update event is automatically triggered.
		'
		' *******************************************************************************
		Get
			Return _Main_Knowledgedate
		End Get
		Set(ByVal Value As Date)
			If Value <> _Main_Knowledgedate Then
				_Main_Knowledgedate = Value

				If Value.CompareTo(KNOWLEDGEDATE_NOW) <= 0 Then
					Me.FlorenceStatusLabel.Text = "KnowledgeDate is Live."
				Else
					If Value.TimeOfDay.TotalSeconds = 0 Then
						Me.FlorenceStatusLabel.Text = "KnowledgeDate is " & _Main_Knowledgedate.ToString(DISPLAYMEMBER_DATEFORMAT)
						Me.Menu_KD_Display.Text = "KD is " & _Main_Knowledgedate.ToString(DISPLAYMEMBER_DATEFORMAT)
					Else
						Me.FlorenceStatusLabel.Text = "KnowledgeDate is " & _Main_Knowledgedate.ToString(DISPLAYMEMBER_LONGDATEFORMAT)
						Me.Menu_KD_Display.Text = "KD is " & _Main_Knowledgedate.ToString(DISPLAYMEMBER_LONGDATEFORMAT)
					End If
				End If

				Call Main_RaiseEvent(New RenaissanceGlobals.RenaissanceUpdateEventArgs(RenaissanceChangeID.KnowledgeDate))

			End If
		End Set
	End Property

    ''' <summary>
    ''' Gets the main data handler.
    ''' </summary>
    ''' <value>The main data handler.</value>
	Public ReadOnly Property MainDataHandler() As RenaissanceDataClass.DataHandler Implements RenaissanceGlobals.StandardRenaissanceMainForm.MainDataHandler
		Get
			Return _MainDataHandler
		End Get
	End Property

    ''' <summary>
    ''' Gets the main adaptor handler.
    ''' </summary>
    ''' <value>The main adaptor handler.</value>
	Public ReadOnly Property MainAdaptorHandler() As RenaissanceDataClass.AdaptorHandler Implements RenaissanceGlobals.StandardRenaissanceMainForm.MainAdaptorHandler
		Get
			Return _MainAdaptorHandler
		End Get
	End Property

    ''' <summary>
    ''' Gets the main report handler.
    ''' </summary>
    ''' <value>The main report handler.</value>
	Public ReadOnly Property MainReportHandler() As ReportHandler
		Get
			Return _MainReportHandler
		End Get
	End Property

    ''' <summary>
    ''' Gets the florence forms.
    ''' </summary>
    ''' <value>The florence forms.</value>
	Public ReadOnly Property FlorenceForms() As RenaissanceFormCollection
		Get
			Return _FlorenceForms
		End Get
	End Property

    ''' <summary>
    ''' Gets the entry form_ cache count.
    ''' </summary>
    ''' <value>The entry form_ cache count.</value>
	Public ReadOnly Property EntryForm_CacheCount() As Integer
		Get
			Return _EntryForm_CacheCount
		End Get
	End Property

    ''' <summary>
    ''' Gets the no cache_ cache count.
    ''' </summary>
    ''' <value>The no cache_ cache count.</value>
	Public ReadOnly Property NoCache_CacheCount() As Integer
		Get
			Return _NoCache_CacheCount
		End Get
	End Property

    ''' <summary>
    ''' Gets the report form_ cache count.
    ''' </summary>
    ''' <value>The report form_ cache count.</value>
	Public ReadOnly Property ReportForm_CacheCount() As Integer
		Get
			Return _ReportForm_CacheCount
		End Get
	End Property

	' _SingleCache_CacheCount
    ''' <summary>
    ''' Gets the single cache_ cache count.
    ''' </summary>
    ''' <value>The single cache_ cache count.</value>
	Public ReadOnly Property SingleCache_CacheCount() As Integer
		Get
			Return _SingleCache_CacheCount
		End Get
	End Property

    ''' <summary>
    ''' Gets the name of the message server.
    ''' </summary>
    ''' <value>The name of the message server.</value>
	Public ReadOnly Property MessageServerName() As String
		Get
			Return _MessageServerName
		End Get
	End Property

    ''' <summary>
    ''' Gets a value indicating whether [SQL trusted connection].
    ''' </summary>
    ''' <value><c>true</c> if [SQL trusted connection]; otherwise, <c>false</c>.</value>
	Private ReadOnly Property SQLTrustedConnection() As Boolean
		Get
			Return _SQLTrustedConnection
		End Get
	End Property

    ''' <summary>
    ''' Gets the name of the SQL user.
    ''' </summary>
    ''' <value>The name of the SQL user.</value>
	Private ReadOnly Property SQLUserName() As String
		Get
			Return _SQLUserName
		End Get
	End Property

    ''' <summary>
    ''' Gets the SQL password.
    ''' </summary>
    ''' <value>The SQL password.</value>
	Private ReadOnly Property SQLPassword() As String
		Get
			Return _SQLPassword
		End Get
	End Property

    ''' <summary>
    ''' Gets the SQL connect string.
    ''' </summary>
    ''' <value>The SQL connect string.</value>
	Public ReadOnly Property SQLConnectString() As String
		Get
			Return Me._SQLConnectionString
		End Get
	End Property

    ''' <summary>
    ''' Gets the pertrac data.
    ''' </summary>
    ''' <value>The pertrac data.</value>
	<CLSCompliant(False)> Public ReadOnly Property PertracData() As RenaissancePertracDataClass.PertracDataClass
		Get
			Return _PertracData
		End Get
	End Property

    ''' <summary>
    ''' Gets the mastername dictionary.
    ''' </summary>
    ''' <value>The mastername dictionary.</value>
	Public ReadOnly Property MasternameDictionary() As LookupCollection(Of Integer, String) Implements StandardRenaissanceMainForm.MasternameDictionary
		Get
			Return _MasternameDictionary
		End Get
	End Property

    ''' <summary>
    ''' Gets the locations dictionary.
    ''' </summary>
    ''' <value>The locations dictionary.</value>
	Public ReadOnly Property LocationsDictionary() As LookupCollection(Of Integer, String) Implements StandardRenaissanceMainForm.LocationsDictionary
		Get
			Return _LocationsDictionary
		End Get
	End Property
#End Region

#Region " Main, New, Load() and Closing() routines"

    ''' <summary>
    ''' Defines the entry point of the application.
    ''' </summary>
    ''' <param name="CmdArgs">The CMD args.</param>
	Shared Sub Main(ByVal CmdArgs() As String)
		' *******************************************************************************
		' Start Florence
		' 
		' *******************************************************************************

		Application.EnableVisualStyles()

		Application.Run(New FlorenceMain(CmdArgs))

	End Sub

    ''' <summary>
    ''' Prevents a default instance of the <see cref="FlorenceMain"/> class from being created.
    ''' </summary>
	Private Sub New()
		' **********************************************************************
		'
		'
		' **********************************************************************


		' This call is required by the Windows Form Designer.
		InitializeComponent()

		' Add any initialization after the InitializeComponent() call.

	End Sub

    ''' <summary>
    ''' Initializes a new instance of the <see cref="FlorenceMain"/> class.
    ''' </summary>
    ''' <param name="CmdArgs">The CMD args.</param>
	Public Sub New(ByVal CmdArgs() As String)
		' **********************************************************************
		'
		'
		' **********************************************************************

		Me.New()

		If (Not (Splash Is Nothing)) Then
			SplashTime = Now
			Splash.Show()
		End If

		' Get Computername

		Dim ComputerName As String
		Try
			ComputerName = Environment.GetEnvironmentVariable("COMPUTERNAME")
		Catch ex As Exception
			ComputerName = ""
		End Try

		' Startup Parameters
		'
		' MessageServer=<ServerName>

		_MessageServerName = ""
		_SQLTrustedConnection = True
		_SQLUserName = ""
		_SQLPassword = ""

		MessageServerAddress = IPAddress.None

		If Not (CmdArgs Is Nothing) Then
			If (CmdArgs.Length > 0) Then
				Dim ParameterString As String
				Dim ParameterName As String

				For Each ParameterString In CmdArgs
					If InStr(ParameterString, "=") > 0 Then
						ParameterName = ParameterString.Substring(0, InStr(ParameterString, "=") - 1)
						ParameterString = ParameterString.Substring(InStr(ParameterString, "="), (ParameterString.Length - InStr(ParameterString, "=")))

						Select Case ParameterName.ToUpper

							Case "MESSAGESERVER"
								_MessageServerName = ParameterString.Trim
								Try
                  MessageServerAddress = NetworkFunctions.GetServerAddress(_MessageServerName)
								Catch ex As Exception
									_MessageServerName = ""
									MessageServerAddress = IPAddress.None
								End Try

							Case "SQLSERVER"
								If ParameterString.Trim.Length > 0 Then
									If (ParameterString.Contains("\")) Then
										Dim Parts As String() = ParameterString.Split(New Char() {"\"}, StringSplitOptions.RemoveEmptyEntries)

										If (Parts.Length > 0) Then SQLServerName = Parts(0)
										If (Parts.Length > 1) Then SQLServerInstance = Parts(1)
									Else
										SQLServerName = ParameterString.Trim
										SQLServerInstance = ""
									End If
								End If

							Case "SQLINSTANCE"
								If ParameterString.Trim.Length > 0 Then
									SQLServerInstance = ParameterString.Trim
								End If

							Case "SQLUSERNAME"
								If ParameterString.Trim.Length > 0 Then
									_SQLUserName = ParameterString.Trim
									_SQLTrustedConnection = False
								End If

							Case "SQLPASSWORD"
								If ParameterString.Trim.Length > 0 Then
									_SQLPassword = ParameterString.Trim
								End If

							Case "MDIFORM"
								If ParameterString.Trim.Length > 0 Then
									If ParameterString.Trim.ToUpper = "TRUE" Then
										Me.Menu_File_ToggleMDI.Checked = True
										Me.FormBorderStyle = Windows.Forms.FormBorderStyle.Sizable
										Me.MaximizeBox = True
										Me.IsMdiContainer = True
									ElseIf IsNumeric(ParameterString.Trim) Then
										If CInt(ParameterString.Trim) <> 0 Then
											Me.Menu_File_ToggleMDI.Checked = False
											Me.FormBorderStyle = Windows.Forms.FormBorderStyle.Sizable
											Me.MaximizeBox = True
											Me.IsMdiContainer = True
										End If
									End If
								End If

							Case "FORMCACHE"
								If ParameterString.Trim.Length > 0 Then
									Dim Flag As Boolean

									Flag = False
									Try
										Flag = CBool(ParameterString.Trim)
									Catch ex As Exception
										Flag = True
									End Try

									If (Flag = False) Then
										_EntryForm_CacheCount = 0
										_ReportForm_CacheCount = 0
										_SingleCache_CacheCount = 0
										_NoCache_CacheCount = 0
									Else
										_EntryForm_CacheCount = STANDARD_EntryForm_CACHE_COUNT
										_ReportForm_CacheCount = STANDARD_ReportForm_CACHE_COUNT
										_SingleCache_CacheCount = STANDARD_SingleForm_CACHE_COUNT
										_NoCache_CacheCount = 0
									End If
								End If

						End Select
					End If
				Next
			End If
		End If

		' Check Registry if IPAddress = None

		If (IPAddress.None.Equals(MessageServerAddress)) Then
			' Try to resolve an address from the Registry

			_MessageServerName = CType(Get_RegistryItem(Registry.CurrentUser, REGISTRY_BASE & "\MessageServer", "ServerName", ""), String)

			If (_MessageServerName = "") Then
				_MessageServerName = CType(Get_RegistryItem(Registry.LocalMachine, REGISTRY_BASE & "\MessageServer", "ServerName", System.Environment.MachineName), String)
			End If

			Try
				If (_MessageServerName.Length <= 0) Then
					_MessageServerName = System.Environment.MachineName
				End If
        MessageServerAddress = NetworkFunctions.GetServerAddress(_MessageServerName)
			Catch ex As Exception
				MessageServerAddress = IPAddress.None
				_MessageServerName = ""
			End Try
		End If

		' Initiate TCP Client
		Try
			UpdateEventControl = New UpdateEventControlClass(Me)
			UpdateEventControl.Visible = False
			Me.Controls.Add(Me.UpdateEventControl)

      FlorenceTCPClient = New TCPServer_Common.GenericTCPClient(Me, _MessageServerName, MessageServerAddress, SQLServerInstance, FCP_Application.Florence, New FCP_Application() {FCP_Application.Florence, FCP_Application.Renaissance})

			' Add TCP Client Message Handler
			AddHandler FlorenceTCPClient.MessageReceivedEvent, AddressOf Me.TCPMessageReceived

			FlorenceTCPClient.Start()
		Catch ex As Exception
		End Try

		' Process Florence Update requests
		AddHandler Me.FlorenceAutoUpdate, AddressOf Me.AutoUpdate

		' Set up Menu Callbacks

		' Add / Edit
		AddHandler Me.Menu_Edit_Groups.Click, AddressOf Generic_FormMenu_Click
		AddHandler Me.Menu_Edit_Items.Click, AddressOf Generic_FormMenu_Click
		AddHandler Me.Menu_Edit_Entity.Click, AddressOf Generic_FormMenu_Click

		' Update
		AddHandler Me.Menu_Update_WorkItems.Click, AddressOf Generic_FormMenu_Click
		AddHandler Me.Menu_Update_UpdateDataValues.Click, AddressOf Generic_FormMenu_Click

		' Reports
		AddHandler Me.Menu_Rpt_StatusChecklist.Click, AddressOf Generic_FormMenu_Click
		AddHandler Me.Menu_Rpt_StatusReport.Click, AddressOf Generic_FormMenu_Click
		AddHandler Me.Menu_Rpt_DueDilligenceFileReports.Click, AddressOf Generic_FormMenu_Click
		AddHandler Me.Menu_Rpt_FlorenceAdmissibilityStatusRpt.Click, AddressOf Generic_FormMenu_Click
		AddHandler Me.Menu_Rpt_FlorenceRiskRpt.Click, AddressOf Generic_FormMenu_Click

		' KnowledgeDate
		AddHandler Me.Menu_KD_SetKD.Click, AddressOf Generic_FormMenu_Click

		' System
		AddHandler Me.Menu_System_UserPermissions.Click, AddressOf Generic_FormMenu_Click

		'About
		AddHandler Me.Menu_About.Click, AddressOf Generic_FormMenu_Click

	End Sub

    ''' <summary>
    ''' Handles the Load event of the FlorenceMain control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub FlorenceMain_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
		' *******************************************************************************
		' Initialise Florence
		' 
		' *******************************************************************************

		Dim myConnection As SqlConnection

		Dim ComputerName As String
		Dim ConnectString As String
		Dim ConnectionCount As Integer
		Dim CurrentConnection As Integer
		Dim ItemName As String

		' Add the Main form to the Florence Forms Collection.
		FlorenceForms.Add(New RenaissanceGlobals.RenaissanceFormHandle(Me, FlorenceFormID.frmFlorenceMain, True))


		' **********************************************************************
		' Initialise Databse Connection.
		' 
		' 1) Construct a reasonable default, then query the registry for a saved value.
		' 2) Create a managed connection object 'cnnFlorence'
		'
		' **********************************************************************

		Me.Visible = True
		Application.DoEvents()
		Me.FlorenceStatusLabel.Text = "Initailising."
		Me.FlorenceStatusStrip.Refresh()

		' Get initial connection string(s)
		' Default value will be determined by what machine this is running on.
		' Is it at home, my development machine or AN Other ?

		' Establish default connection string.
		Me.FlorenceStatusLabel.Text = "Initailising, SQL Connection."
		Me.FlorenceStatusStrip.Refresh()

		If SQLServerName.StartsWith("<") Then
			ComputerName = Environment.GetEnvironmentVariable("COMPUTERNAME")

			If ComputerName Is Nothing Then
				ComputerName = "<Unknown>"
			End If

			If (SQLServerInstance.Length > 0) Then
				ComputerName &= "\" & SQLServerInstance
			End If

			If ComputerName.StartsWith("UKPRWO") Then
				If ComputerName.StartsWith("UKPRWO000457") Then
					ConnectString = "SERVER=UKPRWO000457;DATABASE=InvestMaster_Test1;Trusted_Connection=Yes;Application Name=" & Application.ProductName
					SQLServerName = "UKPRWO000457"
				Else
					ConnectString = "SERVER=EXMSDB53;DATABASE=Renaissance;Trusted_Connection=Yes;Application Name=" & Application.ProductName
					SQLServerName = "EXMSDB53"
				End If
			Else
				ConnectString = "SERVER=" & ComputerName & ";DATABASE=InvestMaster_Test1;Trusted_Connection=Yes;Application Name=" & Application.ProductName
			End If
		Else
			If (SQLTrustedConnection) Then
				If (SQLServerInstance.Length > 0) Then
					ConnectString = "SERVER=" & SQLServerName & "\" & SQLServerInstance & ";DATABASE=Renaissance;Trusted_Connection=Yes;Application Name=" & Application.ProductName
				Else
					ConnectString = "SERVER=" & SQLServerName & ";DATABASE=Renaissance;Trusted_Connection=Yes;Application Name=" & Application.ProductName
				End If
			Else
				If (SQLServerInstance.Length > 0) Then
					ConnectString = "SERVER=" & SQLServerName & "\" & SQLServerInstance & ";DATABASE=Renaissance;Trusted_Connection=No;User ID=" & SQLUserName & ";Password=" & SQLPassword & ";Application Name=" & Application.ProductName
				Else
					ConnectString = "SERVER=" & SQLServerName & ";DATABASE=Renaissance;Trusted_Connection=No;User ID=" & SQLUserName & ";Password=" & SQLPassword & ";Application Name=" & Application.ProductName
				End If
			End If
		End If


		' Retrieve existing connection strings from the registry.

		ConnectionCount = CType(Get_RegistryItem(REGISTRY_BASE & "\Connections", "ConnectionCount", (-1)), Integer)
		If ConnectionCount > 0 Then
			CurrentConnection = CType(Get_RegistryItem(REGISTRY_BASE & "\Connections", "CurrentConnection", (-1)), Integer)

			If (CurrentConnection < ConnectionCount) And (CurrentConnection >= 0) Then
				ItemName = "C" & CurrentConnection.ToString

				ConnectString = CType(Get_RegistryItem(REGISTRY_BASE & "\Connections", ItemName, ConnectString), String)
			End If

		End If

		' create connection object in the DataHandler object.

		myConnection = MainDataHandler.Add_Connection(FLORENCE_CONNECTION, ConnectString)
		_SQLConnectionString = ConnectString

		If (myConnection Is Nothing) Then
			If MainDataHandler.ErrorMessage.Length > 0 Then
				Me.LogError(Me.Name, LOG_LEVELS.Error, MainDataHandler.ErrorMessage, "Failed to open Connection.", "", True)
				Exit Sub
			Else
				Me.LogError(Me.Name, LOG_LEVELS.Error, "", "Failed to open Connection.", "", True)
				Exit Sub
			End If
		End If

		' Ensure that it is opened.

		Try
			If myConnection.State = ConnectionState.Closed Then
				myConnection.Open()
			End If
		Catch ex As Exception
			Me.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Failed to open Connection.", ex.StackTrace, True)
			Exit Sub
		End Try

		' **********************************************************************

		Main_Knowledgedate = KNOWLEDGEDATE_NOW

		Me.FlorenceStatusLabel.Text = ""
		Me.FlorenceStatusStrip.Refresh()

		Call SetMenuBarEnabled()

		' Preload a couple of tables

		' Call RefreshMasternameDictionary()
		' Call RefreshLocationsDictionary()

		If (Not (Splash Is Nothing)) Then
			Try
				If (Now - SplashTime).TotalSeconds < SPLASH_DISPLAY Then
					Splash.StartTimer(SplashTime.AddSeconds(SPLASH_DISPLAY))
					Splash.Hide()
					Splash.ShowDialog()
				End If

				Splash.Hide()
				Splash.Close()
			Catch ex As Exception
			Finally
				If (Not (Splash Is Nothing)) Then
					If Splash.Visible Then Splash.Hide()
				End If
				Splash = Nothing
			End Try
		End If

	End Sub

    ''' <summary>
    ''' Handles the Closing event of the FlorenceMain control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.ComponentModel.CancelEventArgs"/> instance containing the event data.</param>
	Private Sub FlorenceMain_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
		' **********************************************************************
		' Close the application neatly.
		' **********************************************************************

		Dim TimeCounter As Integer

		' Tidy Up

		' ShutDown FlorenceTCPClient

		Try
			If (Not (FlorenceTCPClient Is Nothing)) Then
				FlorenceTCPClient.CloseDown = True
				TimeCounter = 20
				While (TimeCounter > 0) And (FlorenceTCPClient.IsCompleted = False)
					Threading.Thread.Sleep(10)
					TimeCounter -= 1
				End While

				If (FlorenceTCPClient.IsCompleted = False) Then
					FlorenceTCPClient.Stop()
				End If
			End If
		Catch ex As Exception
		End Try

		' **********************************************************************
		' Clear Data Handler
		' **********************************************************************

		Try
			SyncLock MainDataHandler

				If (MainDataHandler IsNot Nothing) Then
					If (MainDataHandler.Adaptors.Count > 0) Then
						MainDataHandler.Adaptors.Clear()
					End If

					If (MainDataHandler.Datasets.Count > 0) Then
						MainDataHandler.Datasets.Clear()
					End If

					If (MainDataHandler.DBConnections.Count > 0) Then
						Dim ThisConnection As SqlConnection

						For Each ThisConnection In MainDataHandler.DBConnections.Values
							If (ThisConnection IsNot Nothing) Then
								If (ThisConnection.State And ConnectionState.Open) Then
									ThisConnection.Close()
								End If
							End If
						Next

						MainDataHandler.DBConnections.Clear()
					End If
				End If

			End SyncLock

		Catch ex As Exception
		End Try


	End Sub

    ''' <summary>
    ''' Delegate GetMenuCheckedDelegate
    ''' </summary>
    ''' <param name="thisMenu">The this menu.</param>
    ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
	Private Delegate Function GetMenuCheckedDelegate(ByVal thisMenu As ToolStripMenuItem) As Boolean

    ''' <summary>
    ''' Gets the menu checked.
    ''' </summary>
    ''' <param name="thisMenu">The this menu.</param>
    ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
	Friend Function GetMenuChecked(ByVal thisMenu As ToolStripMenuItem) As Boolean
		If Me.InvokeRequired Then
			Dim d As New GetMenuCheckedDelegate(AddressOf GetMenuChecked)
			Return Me.Invoke(d, New Object() {thisMenu})
		Else
			Return thisMenu.Checked
		End If
	End Function


#End Region

#Region " Display Report "

    ''' <summary>
    ''' Delegate SetToolStripTextDelegate
    ''' </summary>
    ''' <param name="thisStripLabel">The this strip label.</param>
    ''' <param name="StripText">The strip text.</param>
	Private Delegate Sub SetToolStripTextDelegate(ByVal thisStripLabel As ToolStripLabel, ByVal StripText As String)

    ''' <summary>
    ''' Sets the tool strip text.
    ''' </summary>
    ''' <param name="thisStripLabel">The this strip label.</param>
    ''' <param name="StripText">The strip text.</param>
	Friend Sub SetToolStripText(ByVal thisStripLabel As ToolStripLabel, ByVal StripText As String)
		' ********************************************************************
		' InvokeRequired compares the thread ID of the
		' calling thread to the thread ID of the creating thread.
		' If these threads are different, it returns true.
		' ********************************************************************

		If thisStripLabel.Owner.InvokeRequired Then
			Dim d As New SetToolStripTextDelegate(AddressOf SetToolStripText)
			thisStripLabel.Owner.Invoke(d, New Object() {thisStripLabel, StripText})
		Else
			thisStripLabel.Text = StripText
			thisStripLabel.Owner.Refresh()
		End If

	End Sub

#End Region

#Region " Update Event processes and Event Thread synchronisation classe"

	' **************************************************************************************
	' When a Florence Update is received from the TCPServer, the message should be de-coded and 
	' appropriate Update events should be triggered and processed by the appropriate threads.
	' And this is what I thought was happening, but I was wrong !
	'
	'
	' When a MessageReceivedEvent is triggered by the TCPClient object, it is the TCPClient thread that
	' executes the associated handlers.
	' Thus the 'TCPMessageReceived' process is (always?) executed by the TCPclient thread.
	' That being the case, the subsequent FlorenceUpdateEvents are also run within the TCPClient thread, which
	' can cause problems when controls are updated on Florence forms, control update methods and properties not being 
	' thread safe. 
	' dotNet 1 did not seem to complain about this, though it is not thread safe and may have been the root cause
	' of some strange bugs (Brian's mysterious hangings ?). Dot Net 2.0 does seem to complain vociferously
	' about this which is what has brought it to my attention.
	'
	' The simplest way of marshaling the update execution onto the main Florence UI thread seems to be to
	' leverage the 'Invoke' method of the 'Control' class, which is designed to marshal execution onto the 
	' Control's parent thread.
	' 
	' Thus the 'UpdateEventControlClass' exists for this sole purpose. It exposes a simple delegate which take parameters 
	' identical to the ProcessTCPMessage() process. 
	'
	' AddHandler FlorenceTCPClient.MessageReceivedEvent, AddressOf Me.TCPMessageReceived
	' **************************************************************************************

    ''' <summary>
    ''' Class UpdateEventControlClass
    ''' </summary>
	Friend Class UpdateEventControlClass
		' **************************************************************************************
		' Simple Control Class designed to Marshal Update Event processing back to the main Florence
		' UserInterface thread so that subsequent control updates are conducted in a thread safe fashion.
		' **************************************************************************************

		Inherits Control

        ''' <summary>
        ''' Delegate MessageReceivedDelegate
        ''' </summary>
        ''' <param name="sender">The sender.</param>
        ''' <param name="e">The <see cref="MessageReceivedEventArgs"/> instance containing the event data.</param>
		Delegate Sub MessageReceivedDelegate(ByVal sender As System.Object, ByVal e As MessageReceivedEventArgs)

        ''' <summary>
        ''' The _ message received delegate
        ''' </summary>
		Private _MessageReceivedDelegate As MessageReceivedDelegate
        ''' <summary>
        ''' The main form
        ''' </summary>
		Private MainForm As FlorenceMain

        ''' <summary>
        ''' Gets the TCP message delegate.
        ''' </summary>
        ''' <value>The TCP message delegate.</value>
		Public ReadOnly Property TCPMessageDelegate() As MessageReceivedDelegate
			Get
				Return _MessageReceivedDelegate
			End Get
		End Property

        ''' <summary>
        ''' Prevents a default instance of the <see cref="UpdateEventControlClass"/> class from being created.
        ''' </summary>
		Private Sub New()
			MyBase.New()
		End Sub

        ''' <summary>
        ''' Initializes a new instance of the <see cref="UpdateEventControlClass"/> class.
        ''' </summary>
        ''' <param name="pMainForm">The p main form.</param>
		Public Sub New(ByVal pMainForm As FlorenceMain)
			MyBase.New()

			MainForm = pMainForm
			_MessageReceivedDelegate = New MessageReceivedDelegate(AddressOf Me.TCPMessageReceived)
		End Sub

        ''' <summary>
        ''' TCPs the message received.
        ''' </summary>
        ''' <param name="sender">The sender.</param>
        ''' <param name="e">The <see cref="MessageReceivedEventArgs"/> instance containing the event data.</param>
		Private Sub TCPMessageReceived(ByVal sender As System.Object, ByVal e As MessageReceivedEventArgs)
			MainForm.ProcessTCPMessage(sender, e)
		End Sub

	End Class

    ''' <summary>
    ''' Main_s the raise event.
    ''' </summary>
    ''' <param name="pEventArgs">The <see cref="RenaissanceGlobals.RenaissanceUpdateEventArgs"/> instance containing the event data.</param>
	Public Sub Main_RaiseEvent(ByVal pEventArgs As RenaissanceGlobals.RenaissanceUpdateEventArgs) Implements StandardRenaissanceMainForm.Main_RaiseEvent
		' **********************************************************************
		' Propagate the Changed Event, Globally.
		' **********************************************************************
		Call Main_RaiseEvent(pEventArgs, True)
	End Sub

    ''' <summary>
    ''' Main_s the raise event.
    ''' </summary>
    ''' <param name="pEventArgs">The <see cref="RenaissanceGlobals.RenaissanceUpdateEventArgs"/> instance containing the event data.</param>
    ''' <param name="pGlobalUpdate">if set to <c>true</c> [p global update].</param>
    ''' <param name="ApplicationName">Name of the application.</param>
	Public Sub Main_RaiseEvent(ByVal pEventArgs As RenaissanceGlobals.RenaissanceUpdateEventArgs, ByVal pGlobalUpdate As Boolean, Optional ByVal ApplicationName As FCP_Application = (FCP_Application.Florence Or FCP_Application.Renaissance))
		' **********************************************************************
		' Propagate the local Changed Event
		'
		' **********************************************************************
		Dim thisChangeID As RenaissanceGlobals.RenaissanceChangeID
		Dim ThisDataset As StandardDataset
		Dim thisUpdateDetail As String

		' If this event includes the KnowledgeDate ID or the DB Connection ID, then re-load all the tables.
		' If this is only a KnowledgeDate Update, then don't reload non-Knowledgedated tables.
		' If this is a connection update, then re-load all standard tables.

		Dim IDsToReload As New ArrayList
		Dim UpdateID As RenaissanceChangeID

		Try
			Dim UpdateIsKnowledgeDate As Boolean = False
			Dim UpdateIsConnection As Boolean = False

			For Each thisChangeID In pEventArgs.TablesChanged
				thisUpdateDetail = pEventArgs.UpdateDetail(thisChangeID)

				If (thisChangeID = RenaissanceChangeID.KnowledgeDate) Then
					UpdateIsKnowledgeDate = True
				End If
				If (thisChangeID = RenaissanceChangeID.Connection) Then
					UpdateIsConnection = True
				End If

				' Cycle through each standard dataset (via ChangeID enumeration)
				' checking whether there should be a dependency based re-load.
				' Do not bother re-loading the dataset if this is a Connection or KD Update
				' as the DS will be re-loaded in the next step.

				For Each UpdateID In System.Enum.GetValues(GetType(RenaissanceChangeID))
					ThisDataset = RenaissanceStandardDatasets.GetStandardDataset(UpdateID)

					If (Not (ThisDataset Is Nothing)) Then
						ThisDataset.ISChangeIDToNote(thisChangeID)

						If Not (UpdateIsConnection Or (UpdateIsKnowledgeDate And ThisDataset.IsKnowledgeDated)) Then
							If ThisDataset.IsChangeIDToTriggerUpdate(thisChangeID) Then
								If Not IDsToReload.Contains(UpdateID) Then
									IDsToReload.Add(UpdateID)
									ReloadTable(UpdateID, False)
								End If
							End If
						End If
					End If
				Next

				' Post Table Refresh Actions

				Select Case thisChangeID

					Case RenaissanceChangeID.tblLocations

						Call RefreshLocationsDictionary()

					Case RenaissanceChangeID.Mastername

						Call RefreshMasternameDictionary()

					Case RenaissanceChangeID.Information

						Try
							' The PertracData object caches Instrument Information Data and is shared between forms, thus
							' it makes sense to clear the cache at this central point.

							SyncLock PertracData
								PertracData.ClearInformationCache()
							End SyncLock
						Catch ex As Exception
						End Try

					Case RenaissanceChangeID.Performance

						Try
							' The PertracData object caches Instrument Returns Data and is shared between forms, thus
							' it makes sense to clear the cache at this central point.

							SyncLock PertracData
								PertracData.ClearDataCache()
							End SyncLock
						Catch ex As Exception
						End Try
				End Select

			Next

			' Re-load Datasets as appropriate to a connection or KnowledgeDate change.

			For Each UpdateID In System.Enum.GetValues(GetType(RenaissanceChangeID))
				ThisDataset = RenaissanceStandardDatasets.GetStandardDataset(UpdateID)

				If (Not (ThisDataset Is Nothing)) Then
					If (UpdateIsConnection Or (UpdateIsKnowledgeDate And ThisDataset.IsKnowledgeDated)) Then
						ReloadTable(ThisDataset.ChangeID, False)
					End If
				End If
			Next

		Catch ex As Exception
			Call LogError(Me.Name & ", Main_RaiseEvent", LOG_LEVELS.Error, ex.Message, "Error checking KnowledgeDate or Connection Update.", ex.StackTrace, True)
		End Try

		Try
			RaiseEvent FlorenceAutoUpdate(Me, pEventArgs)
		Catch ex As Exception
			Call LogError(Me.Name & ", Main_RaiseEvent", LOG_LEVELS.Error, ex.Message, "Error raising AutoUpdate event.", ex.StackTrace, True)
		End Try

		' Post 'Cascaded' Update messages
		' This is structured so that Cascaded updates are broadcast after the primary Table update.

		If (Not (IDsToReload Is Nothing)) AndAlso (IDsToReload.Count > 0) Then
			For Each UpdateID In IDsToReload
				Try
					RaiseEvent FlorenceAutoUpdate(Me, New RenaissanceGlobals.RenaissanceUpdateEventArgs(UpdateID))
				Catch ex As Exception
					Call LogError(Me.Name & ", Main_RaiseEvent", LOG_LEVELS.Error, ex.Message, "Error raising AutoUpdate event.", ex.StackTrace, True)
					Exit For
				End Try
			Next
			IDsToReload.Clear()
		End If

		' Pass Updates to the message server, if required

		If (pGlobalUpdate = True) Then
			If (Not (Me.FlorenceTCPClient Is Nothing)) Then

				If pEventArgs.TablesChanged.GetLength(0) >= 1 Then
					' Multiple Updates, 
					' Construct a Renaissance Update object 

					Dim ChangeIDArray() As RenaissanceChangeID

					ChangeIDArray = pEventArgs.TablesChanged

					Dim thisRenaissanceUpdateClass As New RenaissanceUpdateClass(ChangeIDArray)
					Dim ThisMessageHeader As New TCPServerHeader(TCP_ServerMessageType.Client_ApplicationUpdateMessage, ApplicationName, SQLServerInstance, "", 1, 0)

					' Post Header and Update object to Queue for sending.

					Me.FlorenceTCPClient.PostNewMessage(New MessageFolder(ThisMessageHeader, thisRenaissanceUpdateClass))

				Else
					' Only a single Update Event.
					' Post it as a simple Header based Update
					Dim UpdateDetail As String

					For Each thisChangeID In pEventArgs.TablesChanged
						If (Not (thisChangeID = RenaissanceGlobals.RenaissanceChangeID.None)) Then

							UpdateDetail = pEventArgs.UpdateDetail(thisChangeID)

							If (UpdateDetail IsNot Nothing) AndAlso (UpdateDetail.Length > 0) Then
								Me.FlorenceTCPClient.PostNewMessage(New MessageFolder(New TCPServerHeader(TCP_ServerMessageType.Client_ApplicationUpdateMessage, ApplicationName, SQLServerInstance, thisChangeID.ToString & "|" & UpdateDetail, 0, 0)))
							Else
								Me.FlorenceTCPClient.PostNewMessage(New MessageFolder(New TCPServerHeader(TCP_ServerMessageType.Client_ApplicationUpdateMessage, ApplicationName, SQLServerInstance, thisChangeID.ToString, 0, 0)))
							End If

						End If
					Next
				End If
			End If

		End If
	End Sub

    ''' <summary>
    ''' TCPs the message received.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="MessageReceivedEventArgs"/> instance containing the event data.</param>
	Private Sub TCPMessageReceived(ByVal sender As System.Object, ByVal e As MessageReceivedEventArgs)
		' **************************************************************************************
		' 
		' Event handler for the TCP Client MessageReceivedEvent Event.
		' This Routine should use the UpdateEventControl to marshal the update execution back to 
		' the Main Florence User Interface thread.
		'
		' **************************************************************************************

		Try
			UpdateEventControl.Invoke(UpdateEventControl.TCPMessageDelegate, New Object() {sender, e})
		Catch ex As Exception
		End Try
	End Sub

    ''' <summary>
    ''' Processes the TCP message.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="MessageReceivedEventArgs"/> instance containing the event data.</param>
	Friend Sub ProcessTCPMessage(ByVal sender As System.Object, ByVal e As MessageReceivedEventArgs)
		' **********************************************************************
		'
		' Process MessageReceived events from the TCP Client.
		' Usually this event is only trggered for Update Messages.
		'
		' **********************************************************************
		Dim TerminateFlag As Integer = 0

		Select Case e.HeaderMessage.MessageType

			Case TCP_ServerMessageType.Server_ApplicationUpdateMessage, TCP_ServerMessageType.Client_ApplicationUpdateMessage

				' **********************************************************************
				' If this is an Update Message....
				' **********************************************************************

				If (e.HeaderMessage.Application And (FCP_Application.Florence Or FCP_Application.Renaissance)) <> FCP_Application.None Then
					' **********************************************************************
					' And it is for Florence...
					' **********************************************************************

					If (e.HeaderMessage.FollowingMessageCount > 0) AndAlso _
					(e.FollowingObjects.Count > 0) AndAlso _
					(e.FollowingObjects(0).GetType Is GetType(RenaissanceUpdateClass)) Then

						' **********************************************************************
						' Multiple Updates have been sent as in a 'RenaissanceUpdateClass' class
						' **********************************************************************

						Dim thisRenaissanceUpdateEventArgs As New RenaissanceUpdateEventArgs
						Dim thisRenaissanceUpdateClass As New RenaissanceUpdateClass
						Dim thisChangeID As RenaissanceChangeID

						' Construct the local UpdateEvent Argument class

						thisRenaissanceUpdateClass = CType(e.FollowingObjects(0), RenaissanceUpdateClass)

						For Each thisChangeID In thisRenaissanceUpdateClass.ChangeIDs

							' Don't Propagate Knowledgedate or DBConnection changes from other applications.

							If (thisChangeID <> RenaissanceChangeID.KnowledgeDate) And (thisChangeID <> RenaissanceChangeID.Connection) Then

								' Add this ChangeID to the Update Class Object
								thisRenaissanceUpdateEventArgs.TableChanged(thisChangeID) = True
								thisRenaissanceUpdateEventArgs.UpdateDetail(thisChangeID) = thisRenaissanceUpdateClass.UpdateDetail(thisChangeID)

								' ReLoad the related dataset
								Call ReloadTable(thisChangeID, False)

							End If

							If thisChangeID = RenaissanceChangeID.Terminate1 Then
								TerminateFlag = (TerminateFlag Or 1)
							End If
							If thisChangeID = RenaissanceChangeID.Terminate2 Then
								TerminateFlag = (TerminateFlag Or 2)
							End If
						Next

						' **********************************************************************
						' Handle the Shutdown Message.
						' **********************************************************************
						If TerminateFlag = 3 Then
							Call Menu_File_Close_Click(Me, New System.EventArgs)

							Exit Sub
						End If

						' **********************************************************************
						' Trigger the local Changed Event
						' **********************************************************************

						Main_RaiseEvent(thisRenaissanceUpdateEventArgs, False)

					Else

						' **********************************************************************
						' Only a single Update has been set, encoded in the MessageHeader String
						' The Message string may either be an Integer relating to the RenaissanceChangeID Enum,
						' OR the text representation of the Enum value, e.g. 'tblFund'.
						' **********************************************************************

						Dim ChangeID As Integer		 ' RenaissanceGlobals.RenaissanceChangeID
						Dim UpdateDetail As String
						UpdateDetail = e.UpdateDetail(FCP_Application.Florence Or FCP_Application.Renaissance)
						ChangeID = e.UpdateMessage(FCP_Application.Florence Or FCP_Application.Renaissance)

						If (ChangeID > 0) And (ChangeID <> RenaissanceChangeID.KnowledgeDate) Then
							Try
								' **********************************************************************
								' ReLoad the related dataset
								' Trigger the local Changed Event
								'
								' **********************************************************************

								Call ReloadTable(ChangeID, False)

								' Raise Event.

								Main_RaiseEvent(New RenaissanceUpdateEventArgs(ChangeID), False)

							Catch ex As Exception
							End Try
						End If
					End If
				End If

			Case TCP_ServerMessageType.Server_NaplesDataAnswer, TCP_ServerMessageType.Client_NaplesDataAnswer





			Case Else

		End Select

	End Sub

    ''' <summary>
    ''' Autoes the update.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="RenaissanceGlobals.RenaissanceUpdateEventArgs"/> instance containing the event data.</param>
	Private Sub AutoUpdate(ByVal sender As System.Object, ByVal e As RenaissanceGlobals.RenaissanceUpdateEventArgs)
		' **************************************************************************************
		' Eable / Disable menu items according to any changes made in the User Permissions table.
		'
		'
		' **************************************************************************************

		If (Me.IsDisposed) Then Exit Sub

		' Changes to the tblUserPermissions table :-
		Try
			If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblUserPermissions) = True) Then
				Call SetMenuBarEnabled()
			End If
		Catch ex As Exception
		End Try

	End Sub

#End Region

#Region " Florence Forms Code"

    ''' <summary>
    ''' New_s the florence form.
    ''' </summary>
    ''' <param name="pFormID">The p form ID.</param>
    ''' <returns>RenaissanceGlobals.RenaissanceFormHandle.</returns>
	Friend Function New_FlorenceForm(ByVal pFormID As FlorenceFormID) As RenaissanceGlobals.RenaissanceFormHandle
		' **********************************************************************
		' Spawn new Form.
		' **********************************************************************

		Dim newFormHandle As RenaissanceGlobals.RenaissanceFormHandle
		Dim StartCursor As Cursor
		Dim IsNewForm As Boolean = False

		StartCursor = Me.Cursor
		Me.Cursor = Cursors.WaitCursor

		newFormHandle = FlorenceForms.UnusedItem(pFormID)

		' Parannoia check for disposed forms. Should never occur. (ha !)
		Try
			If (Not (newFormHandle Is Nothing)) AndAlso (Not (newFormHandle.Form Is Nothing)) Then
				If (newFormHandle.Form.IsDisposed) Then
					FlorenceForms.Remove(newFormHandle)
					newFormHandle = Nothing
				End If
			End If
		Catch ex As Exception
			newFormHandle = Nothing
		End Try

		' Create a New form if an existing one was not returned.
		If (newFormHandle Is Nothing) Then
			Try
				Dim ThisAssembly As System.Reflection.Assembly = System.Reflection.Assembly.GetExecutingAssembly
				Dim myForm As Form
				myForm = CType(ThisAssembly.CreateInstance("Florence." & System.Enum.GetName(GetType(FlorenceFormID), pFormID), True, Reflection.BindingFlags.Default, Nothing, New Object() {Me}, Nothing, Nothing), Form)
				myForm.Icon = Me.Icon.Clone

				newFormHandle = New RenaissanceGlobals.RenaissanceFormHandle(myForm, pFormID)
				Application.DoEvents()
				If Not (newFormHandle.Form Is Nothing) Then
					FlorenceForms.Add(newFormHandle)
					IsNewForm = True
				End If

			Catch ex As Exception
				newFormHandle = Nothing
				Call LogError(Me.Name & ", New_FlorenceForm", LOG_LEVELS.Error, ex.Message, "Error Loading Form " & System.Enum.GetName(GetType(FlorenceFormID), pFormID), ex.StackTrace, True)
			End Try

		Else
			' Refurbish an existing form.

			' newFormHandle.Form.Visible = True
			Try
				Dim StdForm As StandardFlorenceForm
				StdForm = newFormHandle.Form
				StdForm.ResetForm()
			Catch ex As Exception
				newFormHandle = Nothing
				Call LogError(Me.Name & ", New_FlorenceForm", LOG_LEVELS.Error, ex.Message, "Error Re-Setting Form " & System.Enum.GetName(GetType(FlorenceFormID), pFormID), ex.StackTrace, True)
			End Try
		End If

		If Me.IsMdiContainer Then
			Try
				newFormHandle.Form.MdiParent = Me
			Catch ex As Exception
			End Try
		End If

		Try
			If Not (newFormHandle Is Nothing) Then
				Dim StdForm As StandardFlorenceForm

				newFormHandle.InUse = True
				newFormHandle.Form.ShowInTaskbar = False

				' Don't immediately show the Report form.
				' Show all other forms.

				If Not (pFormID = FlorenceFormID.frmViewReport) Then

					If newFormHandle.Form.Visible = False Then
						newFormHandle.Form.Show()
					Else
						newFormHandle.Form.WindowState = FormWindowState.Normal
					End If

					Application.DoEvents()

					StdForm = newFormHandle.Form
					If StdForm.FormOpenFailed Then
						newFormHandle.Form.Hide()
						RemoveFromFormsCollection(newFormHandle.Form)
					End If
				End If

				SetComboSelectionLengths(newFormHandle.Form)

			End If
		Catch ex As Exception
			Call LogError(Me.Name & ", New_FlorenceForm", LOG_LEVELS.Error, ex.Message, "Error Showing Form " & System.Enum.GetName(GetType(FlorenceFormID), pFormID), ex.StackTrace, True)
		End Try

		Me.FlorenceStatusLabel.Text = ""
		Me.Cursor = StartCursor
		Call BuildWindowsMenu()

		Return newFormHandle
	End Function

    ''' <summary>
    ''' Removes from forms collection.
    ''' </summary>
    ''' <param name="pForm">The p form.</param>
	Public Sub RemoveFromFormsCollection(ByRef pForm As Form)
		FlorenceForms.Remove(pForm)
		Call BuildWindowsMenu()
	End Sub

    ''' <summary>
    ''' Removes from forms collection.
    ''' </summary>
    ''' <param name="pRenaissanceFormHandle">The p renaissance form handle.</param>
	Public Sub RemoveFromFormsCollection(ByRef pRenaissanceFormHandle As RenaissanceGlobals.RenaissanceFormHandle)
		FlorenceForms.Remove(pRenaissanceFormHandle)
		Call BuildWindowsMenu()
	End Sub

    ''' <summary>
    ''' Hides the in forms collection.
    ''' </summary>
    ''' <param name="pForm">The p form.</param>
	Public Sub HideInFormsCollection(ByRef pForm As Form)
		FlorenceForms.Hide(pForm)
		Call BuildWindowsMenu()
	End Sub

    ''' <summary>
    ''' Hides the in forms collection.
    ''' </summary>
    ''' <param name="pRenaissanceFormHandle">The p renaissance form handle.</param>
	Public Sub HideInFormsCollection(ByRef pRenaissanceFormHandle As RenaissanceGlobals.RenaissanceFormHandle)
		FlorenceForms.Hide(pRenaissanceFormHandle)
		Call BuildWindowsMenu()
	End Sub

    ''' <summary>
    ''' Builds the windows menu.
    ''' </summary>
	Friend Sub BuildWindowsMenu()
		' ************************************************************************
		'
		'
		' ************************************************************************

		Dim thisWindowsMenu As ToolStripMenuItem
		Dim ActiveWindows As ArrayList
		Dim WindowCount As Integer
		Dim ThisFormHandle As RenaissanceFormHandle

		thisWindowsMenu = Menu_Windows
		If (thisWindowsMenu.DropDownItems.Count > 0) Then
			thisWindowsMenu.DropDownItems.Clear()
		End If

		ActiveWindows = Me.FlorenceForms.AllActiveItems
		If (Not (ActiveWindows Is Nothing)) AndAlso (ActiveWindows.Count > 1) Then
			' Index 0 should always be the Main Form, so skip it.
			Dim thisMenuItem As ToolStripMenuItem

			For WindowCount = 1 To (ActiveWindows.Count - 1)
				ThisFormHandle = ActiveWindows(WindowCount)

				thisMenuItem = New ToolStripMenuItem(ThisFormHandle.Form.Text, Nothing, AddressOf Me.WindowsMenuEvent)
				thisMenuItem.Tag = (WindowCount)

				thisWindowsMenu.DropDownItems.Add(thisMenuItem)
			Next
		End If

	End Sub


    ''' <summary>
    ''' Windowses the menu event.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
	Private Sub WindowsMenuEvent(ByVal sender As Object, ByVal e As EventArgs)
		' ************************************************************************
		' Action Event for the Windows Menu.
		'
		' ************************************************************************

		Dim SelectedMenuItem As ToolStripMenuItem
		Dim ActiveWindows As ArrayList
		Dim ThisFormHandle As RenaissanceFormHandle

		Try
			SelectedMenuItem = CType(sender, ToolStripMenuItem)
			ActiveWindows = Me.FlorenceForms.AllActiveItems

			ThisFormHandle = ActiveWindows(CInt(SelectedMenuItem.Tag))
			ThisFormHandle.Form.Activate()

		Catch ex As Exception
		End Try

	End Sub

    ''' <summary>
    ''' Sets the combo selection lengths.
    ''' </summary>
    ''' <param name="pForm">The p form.</param>
	Public Sub SetComboSelectionLengths(ByRef pForm As Form)
		' ************************************************************************
		' I have come accross an issue such that if an editable Combo box is used
		' on a form, then when the form is drawn and GetFormData() is called, the
		' text contents of the combo box appear as highlighted text. Most annoying.
		' My initial solution involved setting the SelectionLength property to Zero
		' which succeeded in clearing the text highlight, however it was found in
		' operation that randomly and occasionally it would take several seconds for
		' the "SelectionLength = 0" command to execute. Even More Annoying !
		' Thus I have re-written this function to set focus to any appropriate Combos
		' as this seems to be equally effective at clearing the highlighted text
		' but will, I hope, avoid the aforementioned side affects.
		'
		' ************************************************************************
		Dim thisControl As Control
		Dim thisCombo As ComboBox

		If (pForm Is Nothing) Then
			Exit Sub
		End If

		Try
			For Each thisControl In pForm.Controls
				If TypeOf thisControl Is ComboBox Then
					thisCombo = CType(thisControl, ComboBox)
					If thisCombo.DropDownStyle <> ComboBoxStyle.DropDownList Then
						thisCombo.Focus()
					End If
				End If
			Next
		Catch ex As Exception
		End Try

	End Sub

    ''' <summary>
    ''' Generics the form resize handler.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Public Sub GenericFormResizeHandler(ByVal sender As Object, ByVal e As System.EventArgs)
		Try
			SetComboSelectionLengths(sender)
		Catch ex As Exception
		End Try
	End Sub

    ''' <summary>
    ''' Sets the menu bar enabled.
    ''' </summary>
	Private Sub SetMenuBarEnabled()
		' **********************************************************************
		' 
		' **********************************************************************

		Try
			If Me.FlorenceMenu.Items.Count > 0 Then
				SetMenuItemEnabled(FlorenceMenu.Items)
			End If
		Catch ex As Exception
		End Try

	End Sub

    ''' <summary>
    ''' Sets the menu item enabled.
    ''' </summary>
    ''' <param name="ItemArray">The item array.</param>
	Private Sub SetMenuItemEnabled(ByRef ItemArray As ToolStripItemCollection)
		' **********************************************************************
		' 
		' **********************************************************************

		Dim thisItem As ToolStripItem
		Dim thisMenuItem As ToolStripMenuItem
		Dim ThisFeatureName As String
		Dim ThisFeatureType As PermissionFeatureType

		For Each thisItem In ItemArray
			Try
				If (Not (thisItem Is Nothing)) Then
					If (TypeOf thisItem Is ToolStripMenuItem) Then
						thisMenuItem = CType(thisItem, ToolStripMenuItem)

						If (thisMenuItem.DropDownItems.Count > 0) Then
							SetMenuItemEnabled(thisMenuItem.DropDownItems)
						ElseIf (thisItem.Tag Is Nothing) Then
							thisItem.Enabled = True
						Else
							ThisFeatureName = thisItem.Tag.ToString
							If ThisFeatureName.StartsWith("rpt") Then
								ThisFeatureType = PermissionFeatureType.TypeReport
							Else
								ThisFeatureType = PermissionFeatureType.TypeForm
							End If

							If (CheckPermissions(ThisFeatureName, ThisFeatureType) > 0) Then
								thisItem.Enabled = True
							Else
								thisItem.Enabled = False
							End If
						End If

					End If

				End If

			Catch ex As Exception
			End Try
        Next

        Me.Menu_About.Enabled = True

	End Sub

    ''' <summary>
    ''' Builds the standard form menu.
    ''' </summary>
    ''' <param name="RootMenu">The root menu.</param>
    ''' <param name="pThisTable">The p this table.</param>
    ''' <param name="SelectClick">The select click.</param>
    ''' <param name="SortClick">The sort click.</param>
    ''' <param name="AuditReportClick">The audit report click.</param>
    ''' <returns>MenuStrip.</returns>
	Public Function BuildStandardFormMenu( _
	ByRef RootMenu As MenuStrip, _
	ByVal pThisTable As DataTable, _
	ByRef SelectClick As System.EventHandler, _
	ByRef SortClick As System.EventHandler, _
	ByRef AuditReportClick As System.EventHandler) As MenuStrip

		' **********************************************************************
		' Standard routine to build the Standard Form menus - Select & Order
		' **********************************************************************

		Dim ThisColumn As System.Data.DataColumn
		Dim SelectByMenuItem As New ToolStripMenuItem("&Select By")
		Dim OrderByItem As New ToolStripMenuItem("&Order By")
		Dim AuditReportItem As New ToolStripMenuItem("&Audit Reports")
		Dim newMenuItem As ToolStripMenuItem
		Dim Counter As Integer

		'SelectByMenuItem.Text = "&Select By"
		'OrderByItem.Text = "&Order By"
		'AuditReportItem.Text = "&Audit Reports"

		Counter = 0
		For Each ThisColumn In pThisTable.Columns

			If Not (SelectClick Is Nothing) Then
				'newMenuItem = New ToolStripMenuItem(ThisColumn.ColumnName)
				'AddHandler newMenuItem.Click, SelectClick
				newMenuItem = SelectByMenuItem.DropDownItems.Add(ThisColumn.ColumnName, Nothing, SelectClick)
				newMenuItem.Tag = Counter
			End If

			If Not (SortClick Is Nothing) Then
				newMenuItem = OrderByItem.DropDownItems.Add(ThisColumn.ColumnName, Nothing, SortClick)
				newMenuItem.Tag = Counter
			End If

			Counter += 1
		Next

		If Not (SelectClick Is Nothing) Then
			RootMenu.Items.Add(SelectByMenuItem)
		End If
		If Not (SortClick Is Nothing) Then
			RootMenu.Items.Add(OrderByItem)
		End If

		If Not (AuditReportClick Is Nothing) Then
			newMenuItem = AuditReportItem.DropDownItems.Add("This Record", Nothing, AuditReportClick)
			newMenuItem.Tag = 0

			newMenuItem = AuditReportItem.DropDownItems.Add("All Records", Nothing, AuditReportClick)
			newMenuItem.Tag = 1

			RootMenu.Items.Add(AuditReportItem)
		End If

		Return RootMenu

	End Function

#End Region

#Region " Florence Menu Event Code"

    ''' <summary>
    ''' Handles the Click event of the Menu_File_ToggleMDI control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Menu_File_ToggleMDI_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Menu_File_ToggleMDI.Click
		' **********************************************************************
		'
		' **********************************************************************

		Dim HandlesToDelete As New ArrayList

		If Menu_File_ToggleMDI.Checked = False Then
			Try
				Menu_File_ToggleMDI.Checked = True

				Me.FormBorderStyle = Windows.Forms.FormBorderStyle.Sizable
				Me.MaximizeBox = True
				Me.IsMdiContainer = True

				Dim thisFormHandle As RenaissanceGlobals.RenaissanceFormHandle
				For Each thisFormHandle In FlorenceForms
					Try
						If (thisFormHandle.Form Is Nothing) Then
							HandlesToDelete.Add(thisFormHandle)
						Else
							thisFormHandle.Form.Hide()

							Try
								thisFormHandle.Form.MdiParent = Me
							Catch ex As Exception
							End Try

							If thisFormHandle.InUse Then
								thisFormHandle.Form.Show()
							End If
						End If
					Catch ex As Exception
					End Try
				Next
			Catch ex As Exception
			End Try
		Else
			Try
				Menu_File_ToggleMDI.Checked = False

				Me.FormBorderStyle = Windows.Forms.FormBorderStyle.FixedSingle
				Me.MaximizeBox = False

				Dim thisFormHandle As RenaissanceGlobals.RenaissanceFormHandle
				For Each thisFormHandle In FlorenceForms
					Try
						If (thisFormHandle.Form Is Nothing) Then
							HandlesToDelete.Add(thisFormHandle)

						Else

							thisFormHandle.Form.Hide()

							Try
								thisFormHandle.Form.MdiParent = Nothing
							Catch ex As Exception
							End Try

							If thisFormHandle.InUse Then
								thisFormHandle.Form.Show()
							End If

						End If

					Catch ex As Exception
					End Try
				Next

				Me.IsMdiContainer = False
				Me.Height = 196
				Me.Width = 884

			Catch ex As Exception
			End Try

		End If

		If HandlesToDelete.Count > 0 Then
			Dim DeleteCounter As Integer

			For DeleteCounter = 0 To (HandlesToDelete.Count - 1)
				Try
					FlorenceForms.Remove(HandlesToDelete(DeleteCounter))
				Catch ex As Exception
				End Try
			Next

		End If

	End Sub

    ''' <summary>
    ''' Handles the Click event of the Menu_File_Close control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Menu_File_Close_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Menu_File_Close.Click
		' **********************************************************************
		'
		' **********************************************************************
		Dim FormHandle As RenaissanceGlobals.RenaissanceFormHandle
		Dim StdFormHandle As StandardFlorenceForm

		While FlorenceForms.Count > 0
			FormHandle = FlorenceForms.Item(FlorenceForms.Count - 1)
			FlorenceForms.Remove(FormHandle)

			If TypeOf (FormHandle.Form) Is StandardFlorenceForm Then
				StdFormHandle = FormHandle.Form
				StdFormHandle.CloseForm()
			Else
				FormHandle.Form.Close()
			End If
		End While

		Me.Close()
	End Sub

	' **********************************************************************
	' **********************************************************************
	' 
	'  Menu.
	'
	' **********************************************************************
	' **********************************************************************

    ''' <summary>
    ''' Handles the Click event of the Generic_FormMenu control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Generic_FormMenu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

		' **********************************************************************
		'
		' Open New form of type relating to Menu Item's tag
		'
		' **********************************************************************

		Dim thisSender As ToolStripMenuItem
		Dim thisFormID As FlorenceFormID

		If (TypeOf sender Is ToolStripMenuItem) Then
			thisSender = CType(sender, ToolStripMenuItem)
		Else
			Exit Sub
		End If

		If (thisSender.Tag Is Nothing) Then
			Exit Sub
		End If

		thisFormID = System.Enum.Parse(GetType(FlorenceFormID), thisSender.Tag.ToString)

		Call New_FlorenceForm(thisFormID)

	End Sub

    ''' <summary>
    ''' Handles the Click event of the Menu_KD_Refresh control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Menu_KD_Refresh_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Menu_KD_Refresh.Click
		' **********************************************************************
		' Use the Connection Update Event to refresh all tables
		' **********************************************************************

		Call Main_RaiseEvent(New RenaissanceGlobals.RenaissanceUpdateEventArgs(RenaissanceChangeID.Connection))

	End Sub

    ''' <summary>
    ''' Handles the Click event of the Menu_Rpt_Static_Entity control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Menu_Rpt_Static_Entity_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Menu_Rpt_Static_Entity.Click
		' **********************************************************************
		' 
		' **********************************************************************
		Static InReport As Boolean = False

		If (InReport) Then
			MessageBox.Show("An Entity Report is already being processed", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1)
			Exit Sub
		End If

		Try
			InReport = True
			Me.MainReportHandler.EntitysReport(MainProgressBar)
		Catch ex As Exception
		Finally
			InReport = False
		End Try

	End Sub

    ''' <summary>
    ''' Handles the Click event of the Menu_Rpt_Static_Groups control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Menu_Rpt_Static_Groups_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Menu_Rpt_Static_Groups.Click
		' **********************************************************************
		' 
		' **********************************************************************
		Static InReport As Boolean = False

		If (InReport) Then
			MessageBox.Show("A Groups Report is already being processed", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1)
			Exit Sub
		End If

		Try
			InReport = True
			Me.MainReportHandler.GroupsReport(MainProgressBar)
		Catch ex As Exception
		Finally
			InReport = False
		End Try

	End Sub

    ''' <summary>
    ''' Handles the Click event of the Menu_Rpt_Static_Items control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Menu_Rpt_Static_Items_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Menu_Rpt_Static_Items.Click
		' **********************************************************************
		' 
		' **********************************************************************
		Static InReport As Boolean = False

		If (InReport) Then
			MessageBox.Show("An Items Report is already being processed", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1)
			Exit Sub
		End If

		Try
			InReport = True
			Me.MainReportHandler.ItemsReport(MainProgressBar)
		Catch ex As Exception
		Finally
			InReport = False
		End Try

	End Sub

    ''' <summary>
    ''' Handles the Click event of the Menu_Rpt_ReportData_SaveData control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Private Sub Menu_Rpt_ReportData_SaveData_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Menu_Rpt_ReportData_SaveData.Click
		Try
			Menu_Rpt_ReportData_SaveData.Checked = Not Menu_Rpt_ReportData_SaveData.Checked
		Catch ex As Exception
		End Try
	End Sub

#End Region

#Region " CheckPermissions()"

    ''' <summary>
    ''' Checks the permissions.
    ''' </summary>
    ''' <param name="PermissionArea">The permission area.</param>
    ''' <param name="PermissionType">Type of the permission.</param>
    ''' <returns>System.Int32.</returns>
	Public Function CheckPermissions(ByVal PermissionArea As String, ByVal PermissionType As RenaissanceGlobals.PermissionFeatureType) As Integer
		' **********************************************************************
		' Central Permissions checking Function.
		'
		' Simply invokes the underlying SQL Strored procedure to check the available
		' permissons of the current user.
		' **********************************************************************

		Dim PermissionsCommand As New SqlCommand

		PermissionsCommand.Connection = Me.GetFlorenceConnection

		PermissionsCommand.CommandType = CommandType.StoredProcedure
		PermissionsCommand.CommandText = "[fn_Check_Permission]"
		PermissionsCommand.CommandTimeout = DEFAULT_SQLCOMMAND_TIMEOUT

    PermissionsCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@UserName", System.Data.SqlDbType.NVarChar, 100))
    PermissionsCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@FunctionName", System.Data.SqlDbType.NVarChar, 100))
		PermissionsCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@FunctionType", System.Data.SqlDbType.Int, 4))
		PermissionsCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))

		PermissionsCommand.Parameters("@UserName").Value = DBNull.Value
		PermissionsCommand.Parameters("@FunctionName").Value = PermissionArea
		PermissionsCommand.Parameters("@FunctionType").Value = PermissionType

		Try

			If (Not (PermissionsCommand.Connection Is Nothing)) Then
				SyncLock PermissionsCommand.Connection
					PermissionsCommand.ExecuteNonQuery()
				End SyncLock
			End If

		Catch ex As Exception
			PermissionsCommand.Parameters("@RETURN_VALUE").Value = 0

		Finally

			If (Not (PermissionsCommand.Connection Is Nothing)) Then
				Try
					PermissionsCommand.Connection.Close()
				Catch ex As Exception
				End Try
			End If
		End Try


		CheckPermissions = PermissionsCommand.Parameters("@RETURN_VALUE").Value

	End Function

#End Region

#Region " Standard Format checking handlers "

    ''' <summary>
    ''' Handles the NotNull event of the Text control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	Public Sub Text_NotNull(ByVal sender As System.Object, ByVal e As System.EventArgs)
		' **********************************************************************
		' Standard File Format checking function.
		' **********************************************************************

		Dim thisControl As System.Windows.Forms.Control
		Dim Parentform As StandardFlorenceForm
		Dim ParentObject As Object

		'ParentObject = sender.TopLevelControl
		ParentObject = sender
		thisControl = Nothing

		While Not (TypeOf (ParentObject) Is StandardFlorenceForm)
			Try
				thisControl = ParentObject
				ParentObject = thisControl.Parent
			Catch ex As Exception
				Exit Sub
			End Try
		End While

		Parentform = ParentObject

		If (Parentform.IsOverCancelButton = False) And (Parentform.IsInPaint = False) Then
			If thisControl.Text.Length = 0 Then
				MessageBox.Show("This field may not be left empty.", "Field Validation", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1)
				thisControl.Focus()
			End If
		End If
	End Sub





#End Region

#Region " Establish Standard table datasets "

    ''' <summary>
    ''' Load_s the table.
    ''' </summary>
    ''' <param name="pDatasetDetails">The p dataset details.</param>
    ''' <returns>DataSet.</returns>
	Public Function Load_Table(ByVal pDatasetDetails As RenaissanceGlobals.StandardDataset) As DataSet Implements RenaissanceGlobals.StandardRenaissanceMainForm.Load_Table
		' Get Dataset, forcing a refresh if Noted IDs have changed.

    Return Load_Table(pDatasetDetails, "", pDatasetDetails.NotedIDsHaveChanged, True)
	End Function

    ''' <summary>
    ''' Load_s the table.
    ''' </summary>
    ''' <param name="pDatasetDetails">The p dataset details.</param>
    ''' <param name="pForceRefresh">if set to <c>true</c> [p force refresh].</param>
    ''' <returns>DataSet.</returns>
	Public Function Load_Table(ByVal pDatasetDetails As RenaissanceGlobals.StandardDataset, ByVal pForceRefresh As Boolean) As DataSet Implements RenaissanceGlobals.StandardRenaissanceMainForm.Load_Table
    Return Load_Table(pDatasetDetails, "", pForceRefresh, True)
	End Function

    ''' <summary>
    ''' Load_s the table.
    ''' </summary>
    ''' <param name="pDatasetDetails">The p dataset details.</param>
    ''' <param name="pForceRefresh">if set to <c>true</c> [p force refresh].</param>
    ''' <param name="pRaiseEvent">if set to <c>true</c> [p raise event].</param>
    ''' <returns>DataSet.</returns>
  Public Function Load_Table(ByVal pDatasetDetails As RenaissanceGlobals.StandardDataset, ByVal pForceRefresh As Boolean, ByVal pRaiseEvent As Boolean) As DataSet Implements RenaissanceGlobals.StandardRenaissanceMainForm.Load_Table
    Return Load_Table(pDatasetDetails, "", pForceRefresh, pRaiseEvent)
  End Function

    ''' <summary>
    ''' Load_s the table.
    ''' </summary>
    ''' <param name="pDatasetDetails">The p dataset details.</param>
    ''' <param name="pUpdateString">The p update string.</param>
    ''' <param name="pForceRefresh">if set to <c>true</c> [p force refresh].</param>
    ''' <param name="pRaiseEvent">if set to <c>true</c> [p raise event].</param>
    ''' <returns>DataSet.</returns>
  Public Function Load_Table(ByVal pDatasetDetails As RenaissanceGlobals.StandardDataset, ByVal pUpdateString As String, ByVal pForceRefresh As Boolean, ByVal pRaiseEvent As Boolean) As DataSet Implements RenaissanceGlobals.StandardRenaissanceMainForm.Load_Table
    ' **********************************************************************
    ' Routine to Load a standard dataset or retrieve an existing one if it already
    ' exists.
    ' **********************************************************************

    Dim myDataset As DataSet
    Dim myConnection As SqlConnection
    Dim myAdaptor As SqlDataAdapter
    Dim UpdateDetail As String = ""

    If (pUpdateString IsNot Nothing) Then
      UpdateDetail = pUpdateString
    End If

    If (pDatasetDetails Is Nothing) Then
      Return Nothing
    End If

    Dim THIS_TABLENAME As String = pDatasetDetails.TableName
    Dim THIS_ADAPTORNAME As String = pDatasetDetails.Adaptorname
    Dim THIS_DATASETNAME As String = pDatasetDetails.DatasetName

    ' StandardDatasetNames

    myConnection = Me.MainDataHandler.Get_Connection(FLORENCE_CONNECTION)
    If myConnection Is Nothing Then
      Return Nothing
    End If

    myAdaptor = Me.MainDataHandler.Get_Adaptor(THIS_ADAPTORNAME, FLORENCE_CONNECTION, THIS_TABLENAME)
    myDataset = Me.MainDataHandler.Get_Dataset(THIS_DATASETNAME)

    'AddHandler myAdaptor.RowUpdating, AddressOf OnRowUpdating
    'AddHandler myAdaptor.RowUpdated, AddressOf OnRowUpdated
    'AddHandler myAdaptor.FillError, AddressOf OnRowFillError

    If myDataset Is Nothing Then

      myDataset = Me.MainDataHandler.Get_Dataset(THIS_DATASETNAME, True)

      SyncLock myAdaptor.SelectCommand.Connection
        SyncLock myDataset

          Try
            If (myDataset.Tables.Count > 0) Then
              myDataset.Tables(0).Clear()
            End If
          Catch ex As Exception
          End Try
          Try
            myAdaptor.SelectCommand.Parameters("@Knowledgedate").Value = Me.Main_Knowledgedate
          Catch ex As Exception
          End Try

          Try
            Dim OrgText As String
            OrgText = Me.FlorenceStatusLabel.Text

            If (myDataset.Tables.Count > 0) Then
              Me.FlorenceStatusLabel.Text = "Loading " & myDataset.Tables(0).TableName
              Me.FlorenceStatusStrip.Refresh()
              Application.DoEvents()

              myDataset.EnforceConstraints = False
              myAdaptor.Fill(myDataset.Tables(0))
              myDataset.EnforceConstraints = True

              Me.FlorenceStatusLabel.Text = OrgText
            Else
              Me.FlorenceStatusLabel.Text = "Loading Table " & pDatasetDetails.TableName
              Me.FlorenceStatusStrip.Refresh()
              Application.DoEvents()

              myDataset.EnforceConstraints = False
              myAdaptor.Fill(myDataset, pDatasetDetails.TableName)
              myDataset.EnforceConstraints = True

              Me.FlorenceStatusLabel.Text = OrgText
            End If

            Me.FlorenceStatusStrip.Refresh()
          Catch ex As Exception
            If (myDataset.Tables.Count > 0) Then
              Call LogError(Me.Name & ", Load_Table", LOG_LEVELS.Error, ex.Message, "Error Filling Table " & myDataset.Tables(0).TableName, ex.StackTrace, True)
            Else
              Call LogError(Me.Name & ", Load_Table", LOG_LEVELS.Error, ex.Message, "Error Filling Table ", ex.StackTrace, True)
            End If
          End Try

        End SyncLock
      End SyncLock


      ' Reset the IDs have Changed Flag, as the table has been re-loaded.
      pDatasetDetails.NotedIDsHaveChanged = False

    ElseIf (pForceRefresh = True) Or (pDatasetDetails.NotedIDsHaveChanged = True) Then

      SyncLock myAdaptor.SelectCommand.Connection
        SyncLock myDataset

          Try
            Dim OrgText As String
            OrgText = Me.FlorenceStatusLabel.Text

            Try
              If (myDataset.Tables.Count > 0) Then
                myDataset.Tables(0).Clear()
              End If
            Catch ex As Exception
            End Try

            Try
              myAdaptor.SelectCommand.Parameters("@Knowledgedate").Value = Me.Main_Knowledgedate
            Catch ex As Exception
            End Try

            Me.FlorenceStatusLabel.Text = "Loading Table " & pDatasetDetails.TableName

            myDataset.EnforceConstraints = False
            If (myDataset.Tables.Count > 0) Then
              myAdaptor.Fill(myDataset.Tables(0))
            Else
              myAdaptor.Fill(myDataset, pDatasetDetails.TableName)
            End If
            myDataset.EnforceConstraints = True

            If (pRaiseEvent = True) Then
              Call Me.Main_RaiseEvent(New RenaissanceGlobals.RenaissanceUpdateEventArgs(pDatasetDetails.ChangeID, UpdateDetail), False)
            End If

            Me.FlorenceStatusLabel.Text = OrgText

          Catch ex As Exception
            Me.FlorenceStatusLabel.Text = "Error loading table " & pDatasetDetails.TableName
            Call LogError(Me.Name & ", Load_Table", LOG_LEVELS.Error, ex.Message, "Error Filling (Refresh) Table " & myDataset.Tables(0).TableName, ex.StackTrace, True)
          End Try

        End SyncLock
      End SyncLock

      ' Reset the IDs have Changed Flag, as the table has been re-loaded.
      pDatasetDetails.NotedIDsHaveChanged = False
    End If

    Return myDataset
  End Function

    ''' <summary>
    ''' Reloads the table.
    ''' </summary>
    ''' <param name="pTableID">The p table ID.</param>
    ''' <param name="pRaiseEvent">if set to <c>true</c> [p raise event].</param>
    ''' <returns><c>true</c> if XXXX, <c>false</c> otherwise</returns>
  Public Function ReloadTable(ByVal pTableID As RenaissanceChangeID, ByVal pRaiseEvent As Boolean) As Boolean
    ' **********************************************************************
    ' Function to Re-Load a standard dataset if it is already loaded.
    '
    ' If the dataset is not already loaded, then the call to 'Get_Dataset' will return
    ' Nothing since insufficient of the constructor parameters are given.
    ' If a dataset IS returned, then the Load_Dataset funcion is used to refresh it.
    ' **********************************************************************
    Dim ThisStandardDataset As RenaissanceGlobals.StandardDataset = RenaissanceStandardDatasets.GetStandardDataset(pTableID)
    Dim myDataset As DataSet

    Try
      If Not (ThisStandardDataset Is Nothing) Then
        myDataset = Me.MainDataHandler.Get_Dataset(ThisStandardDataset.DatasetName)
        If (Not (myDataset Is Nothing)) Then
          If (Load_Table(ThisStandardDataset, "", True, pRaiseEvent) Is Nothing) Then
            Return False
          Else
            Return True
          End If
        End If
      End If
    Catch ex As Exception
    End Try

    Return False
  End Function

    ''' <summary>
    ''' Delegate Load_Table_Delegate
    ''' </summary>
    ''' <param name="pDatasetDetails">The p dataset details.</param>
    ''' <param name="pForceRefresh">if set to <c>true</c> [p force refresh].</param>
    ''' <param name="pRaiseEvent">if set to <c>true</c> [p raise event].</param>
    ''' <returns>DataSet.</returns>
  Public Delegate Function Load_Table_Delegate(ByVal pDatasetDetails As RenaissanceGlobals.StandardDataset, ByVal pForceRefresh As Boolean, ByVal pRaiseEvent As Boolean) As DataSet

#End Region

#Region " Standard 'Set' Functions : Combos, Tooltips etc. "


    ''' <summary>
    ''' Sets the TBL generic combo.
    ''' </summary>
    ''' <param name="pCombo">The p combo.</param>
    ''' <param name="pDataRows">The p data rows.</param>
    ''' <param name="pDisplayMember">The p display member.</param>
    ''' <param name="pValueMember">The p value member.</param>
    ''' <param name="pSelectString">The p select string.</param>
    ''' <param name="pSELECTDISTINCT">if set to <c>true</c> [p SELECTDISTINCT].</param>
    ''' <param name="pOrderAscending">if set to <c>true</c> [p order ascending].</param>
    ''' <param name="pLeadingBlank">if set to <c>true</c> [p leading blank].</param>
    ''' <param name="pLeadingBlankValue">The p leading blank value.</param>
    ''' <param name="pLeadingBlankText">The p leading blank text.</param>
  Public Sub SetTblGenericCombo(ByVal pCombo As System.Windows.Forms.ComboBox, ByVal pDataRows As DataRow(), ByVal pDisplayMember As String, ByVal pValueMember As String, ByVal pSelectString As String, Optional ByVal pSELECTDISTINCT As Boolean = False, Optional ByVal pOrderAscending As Boolean = True, Optional ByVal pLeadingBlank As Boolean = False, Optional ByVal pLeadingBlankValue As Object = "<DBNull>", Optional ByVal pLeadingBlankText As String = "")
    ' ************************************************************************************
    ' The process for setting standard combos has been split into two parts.
    ' The first part (the other function) takes a Florence 'StandardDataset' parameter
    ' and then creates a DataRow array in accordance with the other parameters.
    ' This DataRow array is then passed to this procedure and the Combo is populated.
    '
    ' It has been done this way so that the Selection Combos on Add/Edit forms can be
    ' populated using just this procedure and the standard form selectedrows array.
    '
    ' ************************************************************************************

    Dim thisSortedRows() As DataRow
    Dim thisRow As DataRow
    Dim lastRow As DataRow
    Dim newRow As DataRow
    Dim currentSelectedValue As Object
    Dim OrgText As String
    Dim CurrentValueIsIndex As Boolean

    Dim DisplayFieldNames As String()
    Dim FormatStrings As String()
    Dim DCount As Integer

    Try
      If (pCombo Is Nothing) Then Exit Sub
      If (pDataRows Is Nothing) Then Exit Sub

      OrgText = Me.FlorenceStatusLabel.Text
      If (pDataRows.Length > 0) Then
        SetToolStripText(FlorenceStatusLabel, "Building " & pDataRows(0).Table.TableName & " Combo")
      Else
        SetToolStripText(FlorenceStatusLabel, "Building Combo")
      End If
      If Not (Me.InvokeRequired) Then
        Me.FlorenceStatusStrip.Refresh()
        Application.DoEvents()
      End If

      ' Preserve Existing Combo Value

      CurrentValueIsIndex = True

      If pCombo.SelectedIndex >= 0 Then
        currentSelectedValue = pCombo.SelectedValue
      ElseIf pCombo.DropDownStyle = ComboBoxStyle.DropDown Then
        CurrentValueIsIndex = False
        currentSelectedValue = pCombo.Text
      Else
        currentSelectedValue = Nothing
      End If

      ' Resolve Display Field names

      DisplayFieldNames = pDisplayMember.Split(New Char() {",", "|"})
      ReDim FormatStrings(DisplayFieldNames.Length - 1)

      Dim SelectSortString As String = ""
      Try

        For DCount = 0 To (DisplayFieldNames.Length - 1)
          DisplayFieldNames(DCount) = DisplayFieldNames(DCount).Trim(New Char() {" "})

          If (DisplayFieldNames(DCount).Length > 0) Then
            If (DCount > 0) AndAlso (SelectSortString.Length > 0) Then
              SelectSortString &= ", "
            End If

            If (DisplayFieldNames(DCount).ToUpper.EndsWith(" DESC")) Then
              DisplayFieldNames(DCount) = DisplayFieldNames(DCount).Substring(0, DisplayFieldNames(DCount).Length - 5)
              SelectSortString &= DisplayFieldNames(DCount)
            ElseIf (DisplayFieldNames(DCount).ToUpper.EndsWith(" ASC")) Then
              DisplayFieldNames(DCount) = DisplayFieldNames(DCount).Substring(0, DisplayFieldNames(DCount).Length - 4)
              SelectSortString &= DisplayFieldNames(DCount)
            ElseIf pOrderAscending Then
              SelectSortString &= DisplayFieldNames(DCount)
            Else
              SelectSortString &= (DisplayFieldNames(DCount) & " DESC")
            End If
          End If
        Next
      Catch ex As Exception
      End Try

      ' Get Source Dataset
      ' pDataRows
      If (pDataRows Is Nothing) Then
        Try
          ' Clear Existing Binding
          pCombo.DataSource = Nothing
          pCombo.DisplayMember = ""
          pCombo.ValueMember = ""
          pCombo.Items.Clear()
        Catch ex As Exception
        End Try

        Exit Sub
      End If

      thisSortedRows = pDataRows

      ' Clear Existing Binding
      pCombo.DataSource = Nothing
      pCombo.DisplayMember = ""
      pCombo.ValueMember = ""

      ' Build new Source table
      ' Linking directly to the source had odd side affects.
      Dim newTable As New DataTable

      newTable.Clear()

      Try
        newTable.Columns.Add(New DataColumn("DM", GetType(System.String)))
        If (thisSortedRows.Length <= 0) Then
          newTable.Columns.Add(New DataColumn("VM", pLeadingBlankValue.GetType))
        Else
          newTable.Columns.Add(New DataColumn("VM", thisSortedRows(0).Table.Columns(pValueMember).DataType))
        End If
      Catch ex As Exception
        Exit Sub
      End Try

      ' Set Date Format strings for Display fields as necessary

      For DCount = 0 To (DisplayFieldNames.Length - 1)
        FormatStrings(DCount) = ""
        If (thisSortedRows IsNot Nothing) AndAlso (thisSortedRows.Length > 0) Then
          If (DisplayFieldNames(DCount).Length > 0) Then
            If thisSortedRows(0).Table.Columns(DisplayFieldNames(DCount)).DataType Is GetType(System.DateTime) Then
              FormatStrings(DCount) = DISPLAYMEMBER_DATEFORMAT
            End If
          End If
        End If
      Next DCount

      ' Leading Blank.

      If (pLeadingBlank = True) Then
        Try
          newRow = newTable.NewRow

          newRow("DM") = pLeadingBlankText
          If (TypeOf pLeadingBlankValue Is String) AndAlso (CType(pLeadingBlankValue, String) = "<DBNull>") Then
            newRow("VM") = DBNull.Value
          Else
            newRow("VM") = pLeadingBlankValue
          End If

          newTable.Rows.Add(newRow)
        Catch ex As Exception
        End Try
      End If

      lastRow = Nothing
      If (Not (thisSortedRows Is Nothing)) AndAlso (thisSortedRows.Length > 0) Then
        Dim DisplayString As String

        For Each thisRow In thisSortedRows
          If (pSELECTDISTINCT) Then
            If (Not (lastRow Is Nothing)) AndAlso (CompareValue(thisRow(pValueMember), lastRow(pValueMember)) <> 0) Then
              lastRow = Nothing
            End If
          End If

          If (lastRow Is Nothing) Then
            newRow = newTable.NewRow

            Try
              DisplayString = ""
              For DCount = 0 To (DisplayFieldNames.Length - 1)
                If (DisplayFieldNames(DCount).Length > 0) Then
                  If DisplayString.Length > 0 Then
                    DisplayString &= ", "
                  End If

                  If thisRow.IsNull(DisplayFieldNames(DCount)) Then
                    DisplayString &= "<Null>"
                  Else
                    DisplayString &= Format(thisRow(DisplayFieldNames(DCount)), FormatStrings(DCount))
                  End If
                End If
              Next DCount

              newRow("DM") = DisplayString

              If thisRow.IsNull(pValueMember) Then
                newRow("VM") = 0
              Else
                newRow("VM") = thisRow(pValueMember)
              End If

              newTable.Rows.Add(newRow)

            Catch ex As Exception
            End Try
          End If

          If pSELECTDISTINCT Then
            lastRow = thisRow
          End If
        Next
      End If

      ' Link to new table
      Try
        pCombo.DisplayMember = "DM"
        pCombo.ValueMember = "VM"
        pCombo.DataSource = newTable
      Catch ex As Exception
      End Try

      ' Re-Size
      SetComboDropDownWidth(pCombo)

      ' Re-set existing value
      If Not (currentSelectedValue Is Nothing) Then
        Try
          If CurrentValueIsIndex Then
            pCombo.SelectedValue = currentSelectedValue
          Else
            pCombo.SelectedText = currentSelectedValue.ToString
            pCombo.Text = currentSelectedValue.ToString
          End If
        Catch ex As Exception
        End Try
      End If

      SetToolStripText(FlorenceStatusLabel, OrgText)

    Catch ex As Exception
    End Try

  End Sub

    ''' <summary>
    ''' Sets the TBL generic combo.
    ''' </summary>
    ''' <param name="pCombo">The p combo.</param>
    ''' <param name="pDataRows">The p data rows.</param>
    ''' <param name="pDisplayMember">The p display member.</param>
    ''' <param name="pValueMember">The p value member.</param>
    ''' <param name="pSelectString">The p select string.</param>
    ''' <param name="pSELECTDISTINCT">if set to <c>true</c> [p SELECTDISTINCT].</param>
    ''' <param name="pOrderAscending">if set to <c>true</c> [p order ascending].</param>
    ''' <param name="pLeadingBlank">if set to <c>true</c> [p leading blank].</param>
    ''' <param name="pLeadingBlankValue">The p leading blank value.</param>
    ''' <param name="pLeadingBlankText">The p leading blank text.</param>
  Public Sub SetTblGenericCombo(ByVal pCombo As Telerik.WinControls.UI.RadComboBox, ByVal pDataRows As DataRow(), ByVal pDisplayMember As String, ByVal pValueMember As String, ByVal pSelectString As String, Optional ByVal pSELECTDISTINCT As Boolean = False, Optional ByVal pOrderAscending As Boolean = True, Optional ByVal pLeadingBlank As Boolean = False, Optional ByVal pLeadingBlankValue As Object = "<DBNull>", Optional ByVal pLeadingBlankText As String = "")
    ' ************************************************************************************
    ' The process for setting standard combos has been split into two parts.
    ' The first part (the other function) takes a Florence 'StandardDataset' parameter
    ' and then creates a DataRow array in accordance with the other parameters.
    ' This DataRow array is then passed to this procedure and the Combo is populated.
    '
    ' It has been done this way so that the Selection Combos on Add/Edit forms can be
    ' populated using just this procedure and the standard form selectedrows array.
    '
    ' ************************************************************************************

    Dim thisSortedRows() As DataRow
    Dim thisRow As DataRow
    Dim currentSelectedValue As Object
    Dim OrgText As String = ""
    Dim CurrentValueIsIndex As Boolean

    Dim DisplayFieldNames As String()
    Dim FormatStrings As String()
    Dim DCount As Integer

    Try
      If (pCombo Is Nothing) Then Exit Sub
      If (pDataRows Is Nothing) Then Exit Sub

      OrgText = Me.FlorenceStatusLabel.Text
      If (pDataRows.Length > 0) Then
        SetToolStripText(FlorenceStatusLabel, "Building " & pDataRows(0).Table.TableName & " Combo")
      Else
        SetToolStripText(FlorenceStatusLabel, "Building Combo")
      End If
      If Not (Me.InvokeRequired) Then
        Me.FlorenceStatusStrip.Refresh()
        Application.DoEvents()
      End If

      ' Preserve Existing Combo Value

      CurrentValueIsIndex = True

      If pCombo.SelectedIndex >= 0 Then
        currentSelectedValue = pCombo.SelectedValue
      ElseIf pCombo.DropDownStyle = ComboBoxStyle.DropDown Then
        CurrentValueIsIndex = False
        currentSelectedValue = pCombo.Text
      Else
        currentSelectedValue = Nothing
      End If

      ' Resolve Display Field names

      DisplayFieldNames = pDisplayMember.Split(New Char() {",", "|"})
      ReDim FormatStrings(DisplayFieldNames.Length - 1)

      Dim SelectSortString As String = ""
      Try

        For DCount = 0 To (DisplayFieldNames.Length - 1)
          DisplayFieldNames(DCount) = DisplayFieldNames(DCount).Trim(New Char() {" "})

          If (DisplayFieldNames(DCount).Length > 0) Then
            If (DCount > 0) AndAlso (SelectSortString.Length > 0) Then
              SelectSortString &= ", "
            End If

            If (DisplayFieldNames(DCount).ToUpper.EndsWith(" DESC")) Then
              DisplayFieldNames(DCount) = DisplayFieldNames(DCount).Substring(0, DisplayFieldNames(DCount).Length - 5)
              SelectSortString &= DisplayFieldNames(DCount)
            ElseIf (DisplayFieldNames(DCount).ToUpper.EndsWith(" ASC")) Then
              DisplayFieldNames(DCount) = DisplayFieldNames(DCount).Substring(0, DisplayFieldNames(DCount).Length - 4)
              SelectSortString &= DisplayFieldNames(DCount)
            ElseIf pOrderAscending Then
              SelectSortString &= DisplayFieldNames(DCount)
            Else
              SelectSortString &= (DisplayFieldNames(DCount) & " DESC")
            End If
          End If
        Next
      Catch ex As Exception
      End Try

      ' Clear Existing Binding
      pCombo.DataSource = Nothing
      pCombo.DisplayMember = ""
      pCombo.ValueMember = ""
      pCombo.Items.Clear()

      ' Get Source Dataset
      ' pDataRows
      If (pDataRows Is Nothing) Then
        Exit Sub
      End If

      thisSortedRows = pDataRows

      ' Build new Source 
      ' Linking directly to the source had odd side affects.

      ' Set Date Format strings for Display fields as necessary

      For DCount = 0 To (DisplayFieldNames.Length - 1)
        FormatStrings(DCount) = ""
        If (thisSortedRows IsNot Nothing) AndAlso (thisSortedRows.Length > 0) Then
          If (DisplayFieldNames(DCount).Length > 0) Then
            If thisSortedRows(0).Table.Columns(DisplayFieldNames(DCount)).DataType Is GetType(System.DateTime) Then
              FormatStrings(DCount) = DISPLAYMEMBER_DATEFORMAT
            End If
          End If
        End If
      Next DCount

      ' Leading Blank.

      If (pLeadingBlank = True) Then
        Try
          If (TypeOf pLeadingBlankValue Is String) AndAlso (CType(pLeadingBlankValue, String) = "<DBNull>") Then
            pCombo.Items.Add(New Telerik.WinControls.UI.RadComboBoxItem(pLeadingBlankText, DBNull.Value))
          Else
            pCombo.Items.Add(New Telerik.WinControls.UI.RadComboBoxItem(pLeadingBlankText, pLeadingBlankValue))
          End If

        Catch ex As Exception
        End Try
      End If

      If (Not (thisSortedRows Is Nothing)) AndAlso (thisSortedRows.Length > 0) Then
        Dim DisplayString As String
        Dim LastValueMember As Object = Nothing

        Try
          pCombo.BeginUpdate()

          For Each thisRow In thisSortedRows
            If (pSELECTDISTINCT) Then
              If (Not (LastValueMember IsNot Nothing)) AndAlso (CompareValue(thisRow(pValueMember), LastValueMember) <> 0) Then
                LastValueMember = Nothing
              End If
            End If

            If (LastValueMember Is Nothing) Then

              Try
                DisplayString = ""
                For DCount = 0 To (DisplayFieldNames.Length - 1)
                  If (DisplayFieldNames(DCount).Length > 0) Then
                    If DisplayString.Length > 0 Then
                      DisplayString &= ", "
                    End If

                    If thisRow.IsNull(DisplayFieldNames(DCount)) Then
                      DisplayString &= "<Null>"
                    Else
                      DisplayString &= Format(thisRow(DisplayFieldNames(DCount)), FormatStrings(DCount))
                    End If
                  End If
                Next DCount

                If thisRow.IsNull(pValueMember) Then
                  pCombo.Items.Add(New Telerik.WinControls.UI.RadComboBoxItem(DisplayString, 0))
                  If pSELECTDISTINCT Then
                    LastValueMember = 0
                  End If
                Else
                  pCombo.Items.Add(New Telerik.WinControls.UI.RadComboBoxItem(DisplayString, thisRow(pValueMember)))
                  If pSELECTDISTINCT Then
                    LastValueMember = thisRow(pValueMember)
                  End If
                End If

              Catch ex As Exception
              End Try
            End If
          Next

        Catch ex As Exception
        Finally
          pCombo.EndUpdate()
        End Try
      End If

      ' Re-set existing value
      If Not (currentSelectedValue Is Nothing) Then
        Try
          If CurrentValueIsIndex Then
            pCombo.SelectedValue = currentSelectedValue
          Else
            pCombo.SelectedText = currentSelectedValue.ToString
            pCombo.Text = currentSelectedValue.ToString
          End If
        Catch ex As Exception
        End Try
      End If


    Catch ex As Exception
    Finally

      SetToolStripText(FlorenceStatusLabel, OrgText)

    End Try

  End Sub

    ''' <summary>
    ''' Sets the TBL generic combo.
    ''' </summary>
    ''' <param name="pCombo">The p combo.</param>
    ''' <param name="pDataRows">The p data rows.</param>
    ''' <param name="DisplayFieldNames">The display field names.</param>
    ''' <param name="pValueMember">The p value member.</param>
    ''' <param name="DisplayFieldNames_DependencyTable">The display field names_ dependency table.</param>
    ''' <param name="DisplayFieldNames_DependencyField">The display field names_ dependency field.</param>
    ''' <param name="pSelectString">The p select string.</param>
    ''' <param name="pSELECTDISTINCT">if set to <c>true</c> [p SELECTDISTINCT].</param>
    ''' <param name="pOrderAscending">if set to <c>true</c> [p order ascending].</param>
    ''' <param name="pLeadingBlank">if set to <c>true</c> [p leading blank].</param>
    ''' <param name="pLeadingBlankValue">The p leading blank value.</param>
    ''' <param name="pLeadingBlankText">The p leading blank text.</param>
  Private Sub SetTblGenericCombo(ByVal pCombo As System.Windows.Forms.ComboBox, ByVal pDataRows As DataRow(), ByVal DisplayFieldNames As String(), ByVal pValueMember As String, ByVal DisplayFieldNames_DependencyTable As RenaissanceChangeID(), ByVal DisplayFieldNames_DependencyField As String(), ByVal pSelectString As String, Optional ByVal pSELECTDISTINCT As Boolean = False, Optional ByVal pOrderAscending As Boolean = True, Optional ByVal pLeadingBlank As Boolean = False, Optional ByVal pLeadingBlankValue As Object = "<DBNull>", Optional ByVal pLeadingBlankText As String = "")
    ' ************************************************************************************
    ' The process for setting standard combos has been split into two parts.
    ' The first part (the other function) takes a Renaissance 'StandardDataset' parameter
    ' and then creates a DataRow array in accordance with the other parameters.
    ' This DataRow array is then passed to this procedure and the Combo is populated.
    '
    ' It has been done this way so that the Selection Combos on Add/Edit forms can be
    ' populated using just this procedure and the standard form selectedrows() array.
    '
    ' ************************************************************************************

    Dim thisSortedRows() As DataRow
    Dim thisRow As DataRow
    Dim lastRow As DataRow
    Dim newRow As DataRow
    Dim currentSelectedValue As Object
    Dim OrgText As String = ""
    Dim CurrentValueIsIndex As Boolean

    'Dim DisplayFieldNames_DependencyTable As RenaissanceChangeID()
    'Dim DisplayFieldNames_DependencyField As String()
    Dim FormatStrings As String()
    Dim DCount As Integer

    Try
      If (pCombo Is Nothing) OrElse (pCombo.IsDisposed) Then Exit Sub
      If (pDataRows Is Nothing) Then Exit Sub

      OrgText = FlorenceStatusLabel.Text

      If (pDataRows.Length > 0) Then
        Me.FlorenceStatusLabel.Text = "Building " & pDataRows(0).Table.TableName & " Combo"
      Else
        Me.FlorenceStatusLabel.Text = "Building Combo"
      End If

      Me.FlorenceStatusStrip.Refresh()
      Application.DoEvents()

      ' Preserve Existing Combo Value

      CurrentValueIsIndex = True
      If pCombo.DropDownStyle = ComboBoxStyle.DropDownList Then
        If pCombo.SelectedIndex >= 0 Then
          currentSelectedValue = pCombo.SelectedValue
        ElseIf pCombo.DropDownStyle = ComboBoxStyle.DropDown Then
          currentSelectedValue = pCombo.Text
        Else
          currentSelectedValue = Nothing
        End If
      ElseIf pCombo.DropDownStyle = ComboBoxStyle.DropDown Then
        If pCombo.SelectedIndex >= 0 Then
          currentSelectedValue = pCombo.SelectedValue
        Else
          CurrentValueIsIndex = False
          currentSelectedValue = pCombo.SelectedText
        End If
      End If

      If pCombo.SelectedIndex >= 0 Then
        currentSelectedValue = pCombo.SelectedValue
      ElseIf pCombo.DropDownStyle = ComboBoxStyle.DropDown Then
        currentSelectedValue = pCombo.Text
      Else
        currentSelectedValue = Nothing
      End If

      ' Resolve Display Field names

      ReDim FormatStrings(DisplayFieldNames.Length - 1)

      ' Get Source Dataset
      ' pDataRows
      If (pDataRows Is Nothing) Then
        Try
          ' Clear Existing Binding
          pCombo.DataSource = Nothing
          pCombo.DisplayMember = ""
          pCombo.ValueMember = ""
          pCombo.Items.Clear()
        Catch ex As Exception
        End Try

        Exit Sub
      End If

      thisSortedRows = pDataRows

      ' Clear Existing Binding
      pCombo.DataSource = Nothing
      pCombo.DisplayMember = ""
      pCombo.ValueMember = ""

      ' Build new Source table
      ' Linking directly to the source had odd side affects.
      Dim newTable As New DataTable

      newTable.Clear()

      Try
        newTable.Columns.Add(New DataColumn("DM", GetType(System.String)))
        If (thisSortedRows.Length <= 0) Then
          newTable.Columns.Add(New DataColumn("VM", pLeadingBlankValue.GetType))
        Else
          newTable.Columns.Add(New DataColumn("VM", thisSortedRows(0).Table.Columns(pValueMember).DataType))
        End If
      Catch ex As Exception
        Me.FlorenceStatusLabel.Text = OrgText
        Exit Sub
      End Try

      ' Set Date Format strings for Display fields as necessary

      For DCount = 0 To (DisplayFieldNames.Length - 1)
        FormatStrings(DCount) = ""

        If (thisSortedRows IsNot Nothing) AndAlso (thisSortedRows.Length > 0) Then
          If (DisplayFieldNames(DCount).Length > 0) Then
            If thisSortedRows(0).Table.Columns(DisplayFieldNames(DCount)).DataType Is GetType(System.DateTime) Then
              FormatStrings(DCount) = DISPLAYMEMBER_DATEFORMAT
            End If
          End If
        End If
      Next DCount

      ' Leading Blank.

      If (pLeadingBlank = True) Then
        Try
          newRow = newTable.NewRow

          newRow("DM") = pLeadingBlankText
          If (TypeOf pLeadingBlankValue Is String) AndAlso (pLeadingBlankValue.ToString = "<DBNull>") Then
            newRow("VM") = DBNull.Value
          Else
            newRow("VM") = pLeadingBlankValue
          End If

          newTable.Rows.Add(newRow)
        Catch ex As Exception
        End Try
      End If

      lastRow = Nothing
      If (Not (thisSortedRows Is Nothing)) AndAlso (thisSortedRows.Length > 0) Then
        Dim DisplayString As String

        For Each thisRow In thisSortedRows
          If (pSELECTDISTINCT) Then
            If (Not (lastRow Is Nothing)) AndAlso (Not (thisRow(pValueMember).ToString = lastRow(pValueMember).ToString)) Then
              lastRow = Nothing
            End If
          End If

          If (lastRow Is Nothing) Then
            newRow = newTable.NewRow

            Try
              DisplayString = ""
              For DCount = 0 To (DisplayFieldNames.Length - 1)
                If (DisplayFieldNames(DCount).Length > 0) Then
                  If DisplayString.Length > 0 Then
                    DisplayString &= ", "
                  End If

                  If (DisplayFieldNames_DependencyTable(DCount) = RenaissanceChangeID.None) Then
                    If thisRow.IsNull(DisplayFieldNames(DCount)) Then
                      DisplayString &= "<Null>"
                    Else
                      DisplayString &= Format(thisRow(DisplayFieldNames(DCount)), FormatStrings(DCount))
                    End If
                  Else
                    If thisRow.IsNull(DisplayFieldNames(DCount)) Then
                      DisplayString &= "<Null>"
                    Else
                      Try
                        DisplayString &= LookupTableValue(Me, RenaissanceGlobals.RenaissanceStandardDatasets.GetStandardDataset(DisplayFieldNames_DependencyTable(DCount)), CInt(thisRow(DisplayFieldNames(DCount))), DisplayFieldNames_DependencyField(DCount)).ToString
                      Catch ex As Exception
                      End Try
                    End If
                  End If
                End If
              Next DCount

              newRow("DM") = DisplayString

              If thisRow.IsNull(pValueMember) Then
                newRow("VM") = 0
              Else
                newRow("VM") = thisRow(pValueMember)
              End If

              newTable.Rows.Add(newRow)

            Catch ex As Exception
            End Try
          End If

          If pSELECTDISTINCT Then
            lastRow = thisRow
          End If
        Next
      End If

      ' Link to new table
      pCombo.DataSource = newTable
      pCombo.DisplayMember = "DM"
      pCombo.ValueMember = "VM"

      ' Re-Size
      SetComboDropDownWidth(pCombo)

      ' Re-set existing value
      If Not (currentSelectedValue Is Nothing) Then
        Try
          If CurrentValueIsIndex Then
            pCombo.SelectedValue = currentSelectedValue
          Else
            pCombo.SelectedText = currentSelectedValue.ToString
            pCombo.Text = currentSelectedValue.ToString
          End If
        Catch ex As Exception
        End Try
      End If

    Catch ex As Exception
    End Try

    Me.FlorenceStatusLabel.Text = OrgText

  End Sub

    ''' <summary>
    ''' Sets the TBL generic combo.
    ''' </summary>
    ''' <param name="pCombo">The p combo.</param>
    ''' <param name="pDataRows">The p data rows.</param>
    ''' <param name="DisplayFieldNames">The display field names.</param>
    ''' <param name="pValueMember">The p value member.</param>
    ''' <param name="DisplayFieldNames_DependencyTable">The display field names_ dependency table.</param>
    ''' <param name="DisplayFieldNames_DependencyField">The display field names_ dependency field.</param>
    ''' <param name="pSelectString">The p select string.</param>
    ''' <param name="pSELECTDISTINCT">if set to <c>true</c> [p SELECTDISTINCT].</param>
    ''' <param name="pOrderAscending">if set to <c>true</c> [p order ascending].</param>
    ''' <param name="pLeadingBlank">if set to <c>true</c> [p leading blank].</param>
    ''' <param name="pLeadingBlankValue">The p leading blank value.</param>
    ''' <param name="pLeadingBlankText">The p leading blank text.</param>
  Private Sub SetTblGenericCombo(ByVal pCombo As Telerik.WinControls.UI.RadComboBox, ByVal pDataRows As DataRow(), ByVal DisplayFieldNames As String(), ByVal pValueMember As String, ByVal DisplayFieldNames_DependencyTable As RenaissanceChangeID(), ByVal DisplayFieldNames_DependencyField As String(), ByVal pSelectString As String, Optional ByVal pSELECTDISTINCT As Boolean = False, Optional ByVal pOrderAscending As Boolean = True, Optional ByVal pLeadingBlank As Boolean = False, Optional ByVal pLeadingBlankValue As Object = "<DBNull>", Optional ByVal pLeadingBlankText As String = "")
    ' ************************************************************************************
    ' The process for setting standard combos has been split into two parts.
    ' The first part (the other function) takes a Renaissance 'StandardDataset' parameter
    ' and then creates a DataRow array in accordance with the other parameters.
    ' This DataRow array is then passed to this procedure and the Combo is populated.
    '
    ' It has been done this way so that the Selection Combos on Add/Edit forms can be
    ' populated using just this procedure and the standard form selectedrows() array.
    '
    ' ************************************************************************************

    Dim thisSortedRows() As DataRow
    Dim thisRow As DataRow
    Dim currentSelectedValue As Object
    Dim OrgText As String = ""
    Dim CurrentValueIsIndex As Boolean

    'Dim DisplayFieldNames_DependencyTable As RenaissanceChangeID()
    'Dim DisplayFieldNames_DependencyField As String()
    Dim FormatStrings As String()
    Dim DCount As Integer

    Try
      If (pCombo Is Nothing) OrElse (pCombo.IsDisposed) Then Exit Sub
      If (pDataRows Is Nothing) Then Exit Sub

      OrgText = FlorenceStatusLabel.Text

      If (pDataRows.Length > 0) Then
        Me.FlorenceStatusLabel.Text = "Building " & pDataRows(0).Table.TableName & " Combo"
      Else
        Me.FlorenceStatusLabel.Text = "Building Combo"
      End If

      Me.FlorenceStatusStrip.Refresh()
      Application.DoEvents()

      ' Preserve Existing Combo Value

      CurrentValueIsIndex = True
      If pCombo.DropDownStyle = ComboBoxStyle.DropDownList Then
        If pCombo.SelectedIndex >= 0 Then
          currentSelectedValue = pCombo.SelectedValue
        ElseIf pCombo.DropDownStyle = ComboBoxStyle.DropDown Then
          currentSelectedValue = pCombo.Text
        Else
          currentSelectedValue = Nothing
        End If
      ElseIf pCombo.DropDownStyle = ComboBoxStyle.DropDown Then
        If pCombo.SelectedIndex >= 0 Then
          currentSelectedValue = pCombo.SelectedValue
        Else
          CurrentValueIsIndex = False
          currentSelectedValue = pCombo.SelectedText
        End If
      End If

      If pCombo.SelectedIndex >= 0 Then
        currentSelectedValue = pCombo.SelectedValue
      ElseIf pCombo.DropDownStyle = ComboBoxStyle.DropDown Then
        currentSelectedValue = pCombo.Text
      Else
        currentSelectedValue = Nothing
      End If

      ' Resolve Display Field names

      ReDim FormatStrings(DisplayFieldNames.Length - 1)

      pCombo.DataSource = Nothing
      pCombo.DisplayMember = ""
      pCombo.ValueMember = ""
      pCombo.Items.Clear()

      ' Get Source Dataset
      ' pDataRows
      If (pDataRows Is Nothing) Then
        Exit Sub
      End If

      thisSortedRows = pDataRows

      ' Build new Source 
      ' Linking directly to the source had odd side affects.

      ' Set Date Format strings for Display fields as necessary

      For DCount = 0 To (DisplayFieldNames.Length - 1)
        FormatStrings(DCount) = ""

        If (thisSortedRows IsNot Nothing) AndAlso (thisSortedRows.Length > 0) Then
          If (DisplayFieldNames(DCount).Length > 0) Then
            If thisSortedRows(0).Table.Columns(DisplayFieldNames(DCount)).DataType Is GetType(System.DateTime) Then
              FormatStrings(DCount) = DISPLAYMEMBER_DATEFORMAT
            End If
          End If
        End If
      Next DCount

      ' Leading Blank.

      If (pLeadingBlank = True) Then
        Try
          If (TypeOf pLeadingBlankValue Is String) AndAlso (CType(pLeadingBlankValue, String) = "<DBNull>") Then
            pCombo.Items.Add(New Telerik.WinControls.UI.RadComboBoxItem(pLeadingBlankText, DBNull.Value))
          Else
            pCombo.Items.Add(New Telerik.WinControls.UI.RadComboBoxItem(pLeadingBlankText, pLeadingBlankValue))
          End If

        Catch ex As Exception
        End Try
      End If

      If (Not (thisSortedRows Is Nothing)) AndAlso (thisSortedRows.Length > 0) Then
        Dim DisplayString As String
        Dim LastValueMember As Object = Nothing

        Try
          pCombo.BeginUpdate()

          For Each thisRow In thisSortedRows
            If (pSELECTDISTINCT) Then
              If (Not (LastValueMember IsNot Nothing)) AndAlso (CompareValue(thisRow(pValueMember), LastValueMember) <> 0) Then
                LastValueMember = Nothing
              End If
            End If

            If (LastValueMember Is Nothing) Then

              Try
                DisplayString = ""
                For DCount = 0 To (DisplayFieldNames.Length - 1)
                  If (DisplayFieldNames(DCount).Length > 0) Then
                    If DisplayString.Length > 0 Then
                      DisplayString &= ", "
                    End If

                    If (DisplayFieldNames_DependencyTable(DCount) = RenaissanceChangeID.None) Then
                      If thisRow.IsNull(DisplayFieldNames(DCount)) Then
                        DisplayString &= "<Null>"
                      Else
                        DisplayString &= Format(thisRow(DisplayFieldNames(DCount)), FormatStrings(DCount))
                      End If
                    Else
                      If thisRow.IsNull(DisplayFieldNames(DCount)) Then
                        DisplayString &= "<Null>"
                      Else
                        Try
                          DisplayString &= LookupTableValue(Me, RenaissanceGlobals.RenaissanceStandardDatasets.GetStandardDataset(DisplayFieldNames_DependencyTable(DCount)), CInt(thisRow(DisplayFieldNames(DCount))), DisplayFieldNames_DependencyField(DCount)).ToString
                        Catch ex As Exception
                        End Try
                      End If
                    End If
                  End If
                Next DCount

                If thisRow.IsNull(pValueMember) Then
                  pCombo.Items.Add(New Telerik.WinControls.UI.RadComboBoxItem(DisplayString, 0))
                  If pSELECTDISTINCT Then
                    LastValueMember = 0
                  End If
                Else
                  pCombo.Items.Add(New Telerik.WinControls.UI.RadComboBoxItem(DisplayString, thisRow(pValueMember)))
                  If pSELECTDISTINCT Then
                    LastValueMember = thisRow(pValueMember)
                  End If
                End If

              Catch ex As Exception
              End Try
            End If

          Next

        Catch ex As Exception
        Finally
          pCombo.EndUpdate()
        End Try

      End If

      ' Re-set existing value
      If Not (currentSelectedValue Is Nothing) Then
        Try
          If CurrentValueIsIndex Then
            pCombo.SelectedValue = currentSelectedValue
          Else
            pCombo.SelectedText = currentSelectedValue.ToString
            pCombo.Text = currentSelectedValue.ToString
          End If
        Catch ex As Exception
        End Try
      End If

    Catch ex As Exception
    End Try

    Me.FlorenceStatusLabel.Text = OrgText

  End Sub

    ''' <summary>
    ''' Sets the TBL generic combo.
    ''' </summary>
    ''' <param name="pCombo">The p combo.</param>
    ''' <param name="pTableID">The p table ID.</param>
    ''' <param name="pDisplayMember">The p display member.</param>
    ''' <param name="pValueMember">The p value member.</param>
    ''' <param name="pSelectString">The p select string.</param>
    ''' <param name="pSELECTDISTINCT">if set to <c>true</c> [p SELECTDISTINCT].</param>
    ''' <param name="pOrderAscending">if set to <c>true</c> [p order ascending].</param>
    ''' <param name="pLeadingBlank">if set to <c>true</c> [p leading blank].</param>
    ''' <param name="pLeadingBlankValue">The p leading blank value.</param>
    ''' <param name="pLeadingBlankText">The p leading blank text.</param>
  Public Sub SetTblGenericCombo(ByVal pCombo As System.Windows.Forms.ComboBox, ByVal pTableID As RenaissanceGlobals.StandardDataset, ByVal pDisplayMember As String, ByVal pValueMember As String, ByVal pSelectString As String, Optional ByVal pSELECTDISTINCT As Boolean = False, Optional ByVal pOrderAscending As Boolean = True, Optional ByVal pLeadingBlank As Boolean = False, Optional ByVal pLeadingBlankValue As Object = "<DBNull>", Optional ByVal pLeadingBlankText As String = "")
    ' ************************************************************************************
    ' Wrapper for the  SetTblGenericCombo() Routine.
    ' ************************************************************************************

    Dim thisDataset As DataSet
    Dim thisSortedRows() As DataRow
    Dim SelectString As String
    Dim OrgText As String

    Try
      ' SetMasterNameCombo / LocationsCombo

      If (pTableID.ChangeID = RenaissanceGlobals.RenaissanceStandardDatasets.Mastername.ChangeID) AndAlso ((pSelectString.Length = 0) OrElse (pSelectString.ToUpper = "TRUE")) AndAlso (pValueMember = "ID") Then
        If (_MasternameDictionary Is Nothing) Then
          _MasternameDictionary = New LookupCollection(Of Integer, String)
        End If

        Call SetMasterNameCombo(pCombo, pSelectString, pLeadingBlank, pLeadingBlankValue, pLeadingBlankText)
        Exit Sub
      End If

      If (pTableID.ChangeID = RenaissanceGlobals.RenaissanceStandardDatasets.tblLocations.ChangeID) AndAlso ((pSelectString.Length = 0) OrElse (pSelectString.ToUpper = "TRUE")) AndAlso (pValueMember = "CityID") Then
        If (_LocationsDictionary Is Nothing) Then
          ' This prevents the Locations collection being maintained unless a Locations Combo is actually requested.

          _LocationsDictionary = New LookupCollection(Of Integer, String)
        End If

        Call SetLocationsCombo(pCombo)
        Exit Sub
      End If
    Catch ex As Exception
    End Try

    Dim DisplayFieldNames As String()
    Dim DCount As Integer
    Dim ReferentialDataset As RenaissanceDataClass.DSReferentialIntegrity = Nothing
    Dim ReferentialTable As RenaissanceDataClass.DSReferentialIntegrity.tblReferentialIntegrityDataTable = Nothing
    Dim IntegrityChecks() As RenaissanceDataClass.DSReferentialIntegrity.tblReferentialIntegrityRow
    Dim DisplayFieldNames_DependencyTable As RenaissanceChangeID()
    Dim DisplayFieldNames_DependencyField As String()

    Try
      If (pCombo Is Nothing) Then Exit Sub
      If (pTableID Is Nothing) Then Exit Sub

      OrgText = FlorenceStatusLabel.Text
      Me.FlorenceStatusLabel.Text = "Building " & pTableID.TableName & " Combo"
      Me.FlorenceStatusStrip.Refresh()
      Application.DoEvents()

      ' Resolve Display Field names
      DisplayFieldNames = pDisplayMember.Split(New Char() {",", "|"})
      ' ReDim FormatStrings(DisplayFieldNames.Length - 1)
      ReDim DisplayFieldNames_DependencyTable(DisplayFieldNames.Length - 1)
      ReDim DisplayFieldNames_DependencyField(DisplayFieldNames.Length - 1)

      Dim SelectSortString As String = ""
      Try

        For DCount = 0 To (DisplayFieldNames.Length - 1)
          DisplayFieldNames(DCount) = DisplayFieldNames(DCount).Trim(New Char() {" "})
          DisplayFieldNames_DependencyTable(DCount) = RenaissanceChangeID.None
          DisplayFieldNames_DependencyField(DCount) = ""

          If (DisplayFieldNames(DCount).Length > 0) Then
            If (DCount > 0) AndAlso (SelectSortString.Length > 0) Then
              SelectSortString &= ", "
            End If

            If (DisplayFieldNames(DCount).ToUpper.EndsWith(" DESC")) Then
              DisplayFieldNames(DCount) = DisplayFieldNames(DCount).Substring(0, DisplayFieldNames(DCount).Length - 5)
              SelectSortString &= DisplayFieldNames(DCount)
            ElseIf (DisplayFieldNames(DCount).ToUpper.EndsWith(" ASC")) Then
              DisplayFieldNames(DCount) = DisplayFieldNames(DCount).Substring(0, DisplayFieldNames(DCount).Length - 4)
              SelectSortString &= DisplayFieldNames(DCount)
            ElseIf pOrderAscending Then
              SelectSortString &= DisplayFieldNames(DCount)
            Else
              SelectSortString &= (DisplayFieldNames(DCount) & " DESC")
            End If

            ' Check Referential Dependency
            If (ReferentialTable Is Nothing) Then
              ReferentialDataset = Load_Table(RenaissanceStandardDatasets.tblReferentialIntegrity, False)
              ReferentialTable = ReferentialDataset.tblReferentialIntegrity
            End If

            Try
              IntegrityChecks = ReferentialTable.Select("(FeedsTable = '" & pTableID.TableName & "') AND (FeedsField = '" & DisplayFieldNames(DCount) & "')")
              If (IntegrityChecks IsNot Nothing) AndAlso (IntegrityChecks.Length > 0) Then

                DisplayFieldNames_DependencyTable(DCount) = System.Enum.Parse(GetType(RenaissanceChangeID), IntegrityChecks(0).TableName)
                DisplayFieldNames_DependencyField(DCount) = IntegrityChecks(0).DescriptionField
              End If
            Catch ex As Exception
              DisplayFieldNames_DependencyTable(DCount) = RenaissanceChangeID.None
              DisplayFieldNames_DependencyField(DCount) = ""
            End Try

          End If
        Next
      Catch ex As Exception
      End Try

      ' Get Source Dataset
      thisDataset = Me.Load_Table(pTableID, False)
      If Not (thisDataset Is Nothing) Then
        If pSelectString Is Nothing Then
          SelectString = "True"
        ElseIf pSelectString.Length <= 0 Then
          SelectString = "True"
        Else
          SelectString = pSelectString
        End If

        Try
          thisSortedRows = thisDataset.Tables(0).Select(SelectString, SelectSortString)
        Catch ex As Exception
          thisSortedRows = Nothing
        End Try
      Else
        Try
          ' Clear Existing Binding
          pCombo.DataSource = Nothing
          pCombo.DisplayMember = ""
          pCombo.ValueMember = ""
          pCombo.Items.Clear()
        Catch ex As Exception
        End Try

        Exit Sub
      End If

      Call SetTblGenericCombo(pCombo, thisSortedRows, DisplayFieldNames, pValueMember, DisplayFieldNames_DependencyTable, DisplayFieldNames_DependencyField, pSelectString, pSELECTDISTINCT, pOrderAscending, pLeadingBlank, pLeadingBlankValue, pLeadingBlankText)

    Catch ex As Exception
    End Try

  End Sub

    ''' <summary>
    ''' Sets the TBL generic combo.
    ''' </summary>
    ''' <param name="pCombo">The p combo.</param>
    ''' <param name="pTableID">The p table ID.</param>
    ''' <param name="pDisplayMember">The p display member.</param>
    ''' <param name="pValueMember">The p value member.</param>
    ''' <param name="pSelectString">The p select string.</param>
    ''' <param name="pSELECTDISTINCT">if set to <c>true</c> [p SELECTDISTINCT].</param>
    ''' <param name="pOrderAscending">if set to <c>true</c> [p order ascending].</param>
    ''' <param name="pLeadingBlank">if set to <c>true</c> [p leading blank].</param>
    ''' <param name="pLeadingBlankValue">The p leading blank value.</param>
    ''' <param name="pLeadingBlankText">The p leading blank text.</param>
  Public Sub SetTblGenericCombo(ByVal pCombo As Telerik.WinControls.UI.RadComboBox, ByVal pTableID As RenaissanceGlobals.StandardDataset, ByVal pDisplayMember As String, ByVal pValueMember As String, ByVal pSelectString As String, Optional ByVal pSELECTDISTINCT As Boolean = False, Optional ByVal pOrderAscending As Boolean = True, Optional ByVal pLeadingBlank As Boolean = False, Optional ByVal pLeadingBlankValue As Object = "<DBNull>", Optional ByVal pLeadingBlankText As String = "")
    ' ************************************************************************************
    ' Wrapper for the  SetTblGenericCombo() Routine.
    ' ************************************************************************************

    Dim thisDataset As DataSet
    Dim thisSortedRows() As DataRow
    Dim SelectString As String
    Dim OrgText As String

    Dim DisplayFieldNames As String()
    Dim DCount As Integer
    Dim ReferentialDataset As RenaissanceDataClass.DSReferentialIntegrity = Nothing
    Dim ReferentialTable As RenaissanceDataClass.DSReferentialIntegrity.tblReferentialIntegrityDataTable = Nothing
    Dim IntegrityChecks() As RenaissanceDataClass.DSReferentialIntegrity.tblReferentialIntegrityRow
    Dim DisplayFieldNames_DependencyTable As RenaissanceChangeID()
    Dim DisplayFieldNames_DependencyField As String()

    Try
      If (pCombo Is Nothing) Then Exit Sub
      If (pTableID Is Nothing) Then Exit Sub

      OrgText = FlorenceStatusLabel.Text
      Me.FlorenceStatusLabel.Text = "Building " & pTableID.TableName & " Combo"
      Me.FlorenceStatusStrip.Refresh()
      Application.DoEvents()

      ' Resolve Display Field names
      DisplayFieldNames = pDisplayMember.Split(New Char() {",", "|"})
      ' ReDim FormatStrings(DisplayFieldNames.Length - 1)
      ReDim DisplayFieldNames_DependencyTable(DisplayFieldNames.Length - 1)
      ReDim DisplayFieldNames_DependencyField(DisplayFieldNames.Length - 1)

      Dim SelectSortString As String = ""
      Try

        For DCount = 0 To (DisplayFieldNames.Length - 1)
          DisplayFieldNames(DCount) = DisplayFieldNames(DCount).Trim(New Char() {" "})
          DisplayFieldNames_DependencyTable(DCount) = RenaissanceChangeID.None
          DisplayFieldNames_DependencyField(DCount) = ""

          If (DisplayFieldNames(DCount).Length > 0) Then
            If (DCount > 0) AndAlso (SelectSortString.Length > 0) Then
              SelectSortString &= ", "
            End If

            If (DisplayFieldNames(DCount).ToUpper.EndsWith(" DESC")) Then
              DisplayFieldNames(DCount) = DisplayFieldNames(DCount).Substring(0, DisplayFieldNames(DCount).Length - 5)
              SelectSortString &= DisplayFieldNames(DCount)
            ElseIf (DisplayFieldNames(DCount).ToUpper.EndsWith(" ASC")) Then
              DisplayFieldNames(DCount) = DisplayFieldNames(DCount).Substring(0, DisplayFieldNames(DCount).Length - 4)
              SelectSortString &= DisplayFieldNames(DCount)
            ElseIf pOrderAscending Then
              SelectSortString &= DisplayFieldNames(DCount)
            Else
              SelectSortString &= (DisplayFieldNames(DCount) & " DESC")
            End If

            ' Check Referential Dependency
            If (ReferentialTable Is Nothing) Then
              ReferentialDataset = Load_Table(RenaissanceStandardDatasets.tblReferentialIntegrity, False)
              ReferentialTable = ReferentialDataset.tblReferentialIntegrity
            End If

            Try
              IntegrityChecks = ReferentialTable.Select("(FeedsTable = '" & pTableID.TableName & "') AND (FeedsField = '" & DisplayFieldNames(DCount) & "')")
              If (IntegrityChecks IsNot Nothing) AndAlso (IntegrityChecks.Length > 0) Then

                DisplayFieldNames_DependencyTable(DCount) = System.Enum.Parse(GetType(RenaissanceChangeID), IntegrityChecks(0).TableName)
                DisplayFieldNames_DependencyField(DCount) = IntegrityChecks(0).DescriptionField
              End If
            Catch ex As Exception
              DisplayFieldNames_DependencyTable(DCount) = RenaissanceChangeID.None
              DisplayFieldNames_DependencyField(DCount) = ""
            End Try

          End If
        Next
      Catch ex As Exception
      End Try

      ' Get Source Dataset
      thisDataset = Me.Load_Table(pTableID, False)
      If Not (thisDataset Is Nothing) Then
        If pSelectString Is Nothing Then
          SelectString = "True"
        ElseIf pSelectString.Length <= 0 Then
          SelectString = "True"
        Else
          SelectString = pSelectString
        End If

        Try
          thisSortedRows = thisDataset.Tables(0).Select(SelectString, SelectSortString)
        Catch ex As Exception
          thisSortedRows = Nothing
        End Try
      Else
        Try
          ' Clear Existing Binding
          pCombo.DataSource = Nothing
          pCombo.DisplayMember = ""
          pCombo.ValueMember = ""
          pCombo.Items.Clear()
        Catch ex As Exception
        End Try

        Exit Sub
      End If

      Call SetTblGenericCombo(pCombo, thisSortedRows, DisplayFieldNames, pValueMember, DisplayFieldNames_DependencyTable, DisplayFieldNames_DependencyField, pSelectString, pSELECTDISTINCT, pOrderAscending, pLeadingBlank, pLeadingBlankValue, pLeadingBlankText)

    Catch ex As Exception
    End Try

  End Sub

    ''' <summary>
    ''' Sets the TBL generic combo.
    ''' </summary>
    ''' <param name="pCombo">The p combo.</param>
    ''' <param name="pEnumeration">The p enumeration.</param>
    ''' <param name="pLeadingBlank">if set to <c>true</c> [p leading blank].</param>
    ''' <param name="pLeadingBlankLabel">The p leading blank label.</param>
    ''' <param name="pLeadingBlankValue">The p leading blank value.</param>
  Public Sub SetTblGenericCombo(ByRef pCombo As System.Windows.Forms.ComboBox, ByRef pEnumeration As Type, Optional ByVal pLeadingBlank As Boolean = False, Optional ByVal pLeadingBlankLabel As String = "", Optional ByVal pLeadingBlankValue As Object = CInt(0))

    Dim newRow As DataRow
    Dim currentSelectedValue As Object
    Dim OrgText As String

    Dim EnumNames As String()
    Dim EnumCounter As Integer
    Dim CharCounter As Integer
    Dim thischar As Char

    Dim ThisName As String
    Dim NewNameBuilder As System.Text.StringBuilder

    Try

      OrgText = Me.FlorenceStatusLabel.Text
      SetToolStripText(FlorenceStatusLabel, "Building " & pEnumeration.Name & " Combo")
      If Not (Me.InvokeRequired) Then
        Me.FlorenceStatusStrip.Refresh()
        Application.DoEvents()
      End If

      ' Preserve Existing Combo Value

      If pCombo.SelectedIndex >= 0 Then
        currentSelectedValue = pCombo.SelectedValue
      Else
        currentSelectedValue = Nothing
      End If

      EnumNames = System.Enum.GetNames(pEnumeration)

      ' Clear Existing Binding
      pCombo.DataSource = Nothing
      pCombo.DisplayMember = ""
      pCombo.ValueMember = ""

      ' Build new Source table
      ' Linking directly to the source had odd side affects.
      Dim newTable As New DataTable

      newTable.Clear()

      Try
        newTable.Columns.Add(New DataColumn("DM", GetType(System.String)))
        newTable.Columns.Add(New DataColumn("VM", GetType(System.Int32)))
      Catch ex As Exception
        Exit Sub
      End Try

      If (pLeadingBlank = True) Then
        Try
          newRow = newTable.NewRow

          newRow("DM") = pLeadingBlankLabel
          newRow("VM") = pLeadingBlankValue

          newTable.Rows.Add(newRow)
        Catch ex As Exception
        End Try
      End If

      If EnumNames.Length > 0 Then
        For EnumCounter = 0 To (EnumNames.Length - 1)
          newRow = newTable.NewRow

          NewNameBuilder = New System.Text.StringBuilder
          ThisName = EnumNames(EnumCounter)
          For CharCounter = 0 To (ThisName.Length - 1)
            thischar = ThisName.Substring(CharCounter, 1)
            If (CharCounter > 0) AndAlso (Char.IsUpper(thischar)) Then
              NewNameBuilder.Append(" ")
            End If
            NewNameBuilder.Append(thischar)
          Next

          newRow("DM") = NewNameBuilder.ToString
          newRow("VM") = CInt(System.Enum.Parse(pEnumeration, EnumNames(EnumCounter)))

          newTable.Rows.Add(newRow)
        Next

      End If

      ' Link to new table
      Try
        pCombo.DisplayMember = "DM"
        pCombo.ValueMember = "VM"
      Catch ex As Exception
      End Try
      pCombo.DataSource = newTable
      Try
        pCombo.DisplayMember = "DM"
        pCombo.ValueMember = "VM"
      Catch ex As Exception
      End Try

      ' Re-Size
      SetComboDropDownWidth(pCombo)

      ' Re-set existing value
      If Not (currentSelectedValue Is Nothing) Then
        Try
          pCombo.SelectedValue = currentSelectedValue
        Catch ex As Exception
        End Try
      End If

      SetToolStripText(FlorenceStatusLabel, OrgText)

    Catch ex As Exception
    End Try

  End Sub

    ''' <summary>
    ''' Sets the TBL generic combo.
    ''' </summary>
    ''' <param name="pCombo">The p combo.</param>
    ''' <param name="pEnumeration">The p enumeration.</param>
    ''' <param name="pLeadingBlank">if set to <c>true</c> [p leading blank].</param>
    ''' <param name="pLeadingBlankLabel">The p leading blank label.</param>
    ''' <param name="pLeadingBlankValue">The p leading blank value.</param>
  Public Sub SetTblGenericCombo(ByRef pCombo As Telerik.WinControls.UI.RadComboBox, ByRef pEnumeration As Type, Optional ByVal pLeadingBlank As Boolean = False, Optional ByVal pLeadingBlankLabel As String = "", Optional ByVal pLeadingBlankValue As Object = CInt(0))

    Dim currentSelectedValue As Object
    Dim OrgText As String = ""

    Dim EnumNames As String()
    Dim EnumCounter As Integer
    Dim CharCounter As Integer
    Dim thischar As Char

    Dim ThisName As String
    Dim NewNameBuilder As System.Text.StringBuilder

    Try

      OrgText = Me.FlorenceStatusLabel.Text
      SetToolStripText(FlorenceStatusLabel, "Building " & pEnumeration.Name & " Combo")
      If Not (Me.InvokeRequired) Then
        Me.FlorenceStatusStrip.Refresh()
        Application.DoEvents()
      End If

      ' Preserve Existing Combo Value

      If pCombo.SelectedIndex >= 0 Then
        currentSelectedValue = pCombo.SelectedValue
      Else
        currentSelectedValue = Nothing
      End If

      EnumNames = System.Enum.GetNames(pEnumeration)

      ' Clear Existing Binding
      pCombo.DataSource = Nothing
      pCombo.DisplayMember = ""
      pCombo.ValueMember = ""
      pCombo.Items.Clear()

      ' Build new Source table
      ' Linking directly to the source had odd side affects.

      If (pLeadingBlank = True) Then
        Try
          pCombo.Items.Add(New Telerik.WinControls.UI.RadComboBoxItem(pLeadingBlankLabel, pLeadingBlankValue))

        Catch ex As Exception
        End Try
      End If

      If EnumNames.Length > 0 Then
        For EnumCounter = 0 To (EnumNames.Length - 1)

          NewNameBuilder = New System.Text.StringBuilder
          ThisName = EnumNames(EnumCounter)
          For CharCounter = 0 To (ThisName.Length - 1)
            thischar = ThisName.Substring(CharCounter, 1)
            If (CharCounter > 0) AndAlso (Char.IsUpper(thischar)) Then
              NewNameBuilder.Append(" ")
            End If
            NewNameBuilder.Append(thischar)
          Next

          pCombo.Items.Add(New Telerik.WinControls.UI.RadComboBoxItem(NewNameBuilder.ToString, CInt(System.Enum.Parse(pEnumeration, EnumNames(EnumCounter)))))
        Next

      End If

      ' Re-set existing value
      If Not (currentSelectedValue Is Nothing) Then
        Try
          pCombo.SelectedValue = currentSelectedValue
        Catch ex As Exception
        End Try
      End If

    Catch ex As Exception
    Finally

      SetToolStripText(FlorenceStatusLabel, OrgText)

    End Try

  End Sub

    ''' <summary>
    ''' Sets the master name combo.
    ''' </summary>
    ''' <param name="pCombo">The p combo.</param>
    ''' <param name="pSelectString">The p select string.</param>
    ''' <param name="pLeadingBlank">if set to <c>true</c> [p leading blank].</param>
    ''' <param name="pLeadingBlankValue">The p leading blank value.</param>
    ''' <param name="pLeadingBlankText">The p leading blank text.</param>
  Public Sub SetMasterNameCombo(ByVal pCombo As System.Windows.Forms.ComboBox, ByVal pSelectString As String, Optional ByVal pLeadingBlank As Boolean = False, Optional ByVal pLeadingBlankValue As Object = "<DBNull>", Optional ByVal pLeadingBlankText As String = "")
    ' ************************************************************************************
    '
    ' ************************************************************************************

    Dim currentSelectedValue As Object = Nothing
    Dim IsCurrentValue As Boolean = False
    Dim currentText As String = ""

    Dim OrgText As String = ""

    Try
      If (pCombo Is Nothing) Then Exit Sub
      If (_MasternameDictionary Is Nothing) Then Exit Sub

      OrgText = FlorenceStatusLabel.Text
      FlorenceStatusLabel.Text = "Building Mastername Combo"

      Try
        If (pCombo.SelectedIndex >= 0) Then
          currentSelectedValue = pCombo.SelectedValue
          IsCurrentValue = True
        ElseIf (pCombo.Text.Length > 0) Then
          currentText = pCombo.Text
        End If
      Catch ex As Exception
        currentSelectedValue = Nothing
      End Try

      Dim bs As BindingSource

      ' Clear existing BS ?

      If (pCombo.DataSource IsNot Nothing) Then
        If (GetType(BindingSource).IsInstanceOfType(pCombo.DataSource)) Then
          Try
            bs = CType(pCombo.DataSource, BindingSource)
            bs.Sort = ""
            bs.DataSource = Nothing
            bs.Clear()
            bs = Nothing

            pCombo.DisplayMember = ""
            pCombo.ValueMember = ""
            pCombo.DataSource = Nothing

          Catch ex As Exception
          End Try
        Else
          Try
            pCombo.DisplayMember = ""
            pCombo.ValueMember = ""
            pCombo.DataSource = Nothing
          Catch ex As Exception
          End Try
        End If
      End If

      ' Build Combo Bs.

      bs = New BindingSource

      bs.DataSource = _MasternameDictionary
      pCombo.DisplayMember = "Value"
      pCombo.ValueMember = "Key"
      pCombo.DataSource = bs

      If (IsCurrentValue) AndAlso (currentSelectedValue IsNot Nothing) Then
        pCombo.SelectedValue = currentSelectedValue
      ElseIf (currentText.Length > 0) Then
        pCombo.Text = currentText
      ElseIf (pCombo.Items.Count > 0) Then
        pCombo.SelectedIndex = 0
      End If

    Catch ex As Exception
      LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error Setting Mastername Combo", ex.StackTrace, True)
    Finally
      FlorenceStatusLabel.Text = OrgText
    End Try


  End Sub

    ''' <summary>
    ''' Sets the locations combo.
    ''' </summary>
    ''' <param name="pCombo">The p combo.</param>
    ''' <param name="pLeadingBlank">if set to <c>true</c> [p leading blank].</param>
  Public Sub SetLocationsCombo(ByVal pCombo As System.Windows.Forms.ComboBox, Optional ByVal pLeadingBlank As Boolean = False)
    ' ************************************************************************************
    '
    ' ************************************************************************************

    Dim currentSelectedValue As Object = Nothing
    Dim IsCurrentValue As Boolean = False
    Dim currentText As String = ""

    Dim OrgText As String = ""

    Try
      If (pCombo Is Nothing) Then Exit Sub
      If (_LocationsDictionary Is Nothing) Then Exit Sub

      OrgText = FlorenceStatusLabel.Text
      FlorenceStatusLabel.Text = "Building Locations Combo"

      If (pCombo.SelectedIndex >= 0) Then
        currentSelectedValue = pCombo.SelectedValue
        IsCurrentValue = True
      ElseIf (pCombo.Text.Length > 0) Then
        currentText = pCombo.Text
      End If

      Dim bs As BindingSource

      ' Clear existing BS ?

      If (pCombo.DataSource IsNot Nothing) AndAlso (GetType(BindingSource).IsInstanceOfType(pCombo.DataSource)) Then
        Try
          bs = CType(pCombo.DataSource, BindingSource)
          bs.Sort = ""
          bs.DataSource = Nothing
          bs.Clear()
          bs = Nothing
        Catch ex As Exception
        End Try
      End If

      ' Build Combo Bs.

      bs = New BindingSource

      bs.DataSource = _LocationsDictionary
      pCombo.DisplayMember = "Value"
      pCombo.ValueMember = "Key"
      pCombo.DataSource = bs

      If (IsCurrentValue) AndAlso (currentSelectedValue IsNot Nothing) Then
        pCombo.SelectedValue = currentSelectedValue
      ElseIf (currentText.Length > 0) Then
        pCombo.Text = currentText
      ElseIf (pCombo.Items.Count > 0) Then
        pCombo.SelectedIndex = 0
      End If

    Catch ex As Exception
      LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error Setting Locations Combo", ex.StackTrace, True)
    Finally
      FlorenceStatusLabel.Text = OrgText
    End Try


  End Sub

    ''' <summary>
    ''' Refreshes the mastername dictionary.
    ''' </summary>
  Private Sub RefreshMasternameDictionary()
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************

    If (_MasternameDictionary Is Nothing) Then Exit Sub

    Try

      Dim MasternameDataView As DataView = PertracData.GetPertracInstruments
      Dim ExistingKeys() As Integer
      Dim KeyCounter As Integer = 0
      Dim TableCounter As Integer = 0
      Dim ValueOrdinal As Integer = MasternameDataView.Table.Columns("PertracCode").Ordinal
      Dim DisplayOrdinal As Integer = MasternameDataView.Table.Columns("ListDescription").Ordinal

      ' Add Default Row to the Dictionary
      If (_MasternameDictionary.Count = 0) Then
        _MasternameDictionary.Add(0, "")
      End If

      MasternameDataView.Sort = "PertracCode"

      ExistingKeys = CType(Me._MasternameDictionary.KeyArray, Integer())
      Array.Sort(ExistingKeys)

      While (KeyCounter < ExistingKeys.Length) OrElse (TableCounter < MasternameDataView.Count)
        If (KeyCounter < ExistingKeys.Length) AndAlso (TableCounter < MasternameDataView.Count) Then
          Select Case (ExistingKeys(KeyCounter)).CompareTo(MasternameDataView(TableCounter)(ValueOrdinal))
            Case Is > 0
              ' Add Entry to Dictionary

              _MasternameDictionary.Add(CInt(MasternameDataView(TableCounter)(ValueOrdinal)), MasternameDataView(TableCounter)(DisplayOrdinal).ToString)
              TableCounter += 1

            Case 0
              ' Keys are the Same

              If ExistingKeys(KeyCounter) > 0 Then
                If CStr(_MasternameDictionary(ExistingKeys(KeyCounter))) <> (MasternameDataView(TableCounter)(DisplayOrdinal).ToString) Then
                  _MasternameDictionary(ExistingKeys(KeyCounter)) = (MasternameDataView(TableCounter)(DisplayOrdinal).ToString)
                End If
              End If

              TableCounter += 1
              KeyCounter += 1

            Case Is < 0
              ' Delete Dictionary Entry

              _MasternameDictionary.Remove(ExistingKeys(KeyCounter))
              KeyCounter += 1

          End Select

        ElseIf (KeyCounter < ExistingKeys.Length) Then
          ' Delete Dictionary Entry

          _MasternameDictionary.Remove(ExistingKeys(KeyCounter))
          KeyCounter += 1

        ElseIf (TableCounter < MasternameDataView.Count) Then
          ' Add Entry to Dictionary

          _MasternameDictionary.Add(CInt(MasternameDataView(TableCounter)(ValueOrdinal)), MasternameDataView(TableCounter)(DisplayOrdinal).ToString)
          TableCounter += 1

        Else
          Exit While
        End If
      End While

    Catch ex As Exception
    Finally
      _MasternameDictionary.Sort()
    End Try

  End Sub

    ''' <summary>
    ''' Refreshes the locations dictionary.
    ''' </summary>
  Private Sub RefreshLocationsDictionary()
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************

    If (_LocationsDictionary Is Nothing) Then Exit Sub

    Try

      Dim tblLocations As DSLocations.tblLocationsDataTable = CType(Load_Table(RenaissanceGlobals.RenaissanceStandardDatasets.tblLocations).Tables(0), DSLocations.tblLocationsDataTable)
      Dim SelectedRows() As DSLocations.tblLocationsRow
      Dim ExistingKeys() As Integer
      Dim KeyCounter As Integer = 0
      Dim TableCounter As Integer = 0
      Dim ValueString As String

      ' Add Default Row to the Dictionary
      If (_LocationsDictionary.Count = 0) Then
        _LocationsDictionary.Add(0, "")
      End If

      SelectedRows = CType(tblLocations.Select("True", "CityID"), DSLocations.tblLocationsRow())
      ExistingKeys = CType(Me._LocationsDictionary.KeyArray, Integer())
      Array.Sort(ExistingKeys)

      While (KeyCounter < ExistingKeys.Length) OrElse (TableCounter < SelectedRows.Length)
        If (KeyCounter < ExistingKeys.Length) AndAlso (TableCounter < SelectedRows.Length) Then
          Select Case (ExistingKeys(KeyCounter)).CompareTo(SelectedRows(TableCounter).CityID)
            Case Is > 0
              ' Add Entry to Dictionary
              ValueString = SelectedRows(TableCounter).City & ", " & SelectedRows(TableCounter).RegionName

              _LocationsDictionary.Add(SelectedRows(TableCounter).CityID, ValueString)
              TableCounter += 1

            Case 0
              ' Keys are the Same
              ValueString = SelectedRows(TableCounter).City & ", " & SelectedRows(TableCounter).RegionName

              If CStr(_LocationsDictionary(ExistingKeys(KeyCounter))) <> ValueString Then
                _LocationsDictionary(ExistingKeys(KeyCounter)) = ValueString
              End If

              TableCounter += 1
              KeyCounter += 1

            Case Is < 0
              ' Delete Dictionary Entry

              If ExistingKeys(KeyCounter) > 0 Then
                _LocationsDictionary.Remove(ExistingKeys(KeyCounter))
              End If

              KeyCounter += 1

          End Select

        ElseIf (KeyCounter < ExistingKeys.Length) Then
          ' Delete Dictionary Entry

          _LocationsDictionary.Remove(ExistingKeys(KeyCounter))
          KeyCounter += 1

        ElseIf (TableCounter < SelectedRows.Length) Then
          ' Add Entry to Dictionary
          ValueString = SelectedRows(TableCounter).City & ", " & SelectedRows(TableCounter).RegionName

          _LocationsDictionary.Add(SelectedRows(TableCounter).CityID, ValueString)
          TableCounter += 1

        Else
          Exit While
        End If
      End While

    Catch ex As Exception
    Finally
      _LocationsDictionary.Sort()
    End Try

  End Sub

    ''' <summary>
    ''' Sets the width of the combo drop down.
    ''' </summary>
    ''' <param name="pCombo">The p combo.</param>
  Public Sub SetComboDropDownWidth(ByRef pCombo As System.Windows.Forms.ComboBox)
    ' ************************************************************************************
    ' 
    ' ************************************************************************************

    Dim thisDrowDownWidth As Integer
    Dim SizingBitmap As Bitmap
    Dim SizingGraphics As Graphics
    Dim stringSize As SizeF

    Dim ItemString As String
    Dim ItemObject As Object
    Dim ItemCount As Integer

    If (pCombo Is Nothing) Then
      Exit Sub
    End If

    If (pCombo.Items.Count <= 0) Then
      Exit Sub
    End If

    Try

      thisDrowDownWidth = pCombo.DropDownWidth
      SizingBitmap = New Bitmap(1, 1, System.Drawing.Imaging.PixelFormat.Format32bppArgb)
      SizingGraphics = Graphics.FromImage(SizingBitmap)

      For ItemCount = 0 To (pCombo.Items.Count - 1)
        ItemObject = pCombo.Items(ItemCount)
        ItemString = ""

        If (pCombo.DataSource Is Nothing) Then
          ItemString = CStr(ItemObject)
        ElseIf GetType(DataRowView).IsInstanceOfType(ItemObject) Then
          ItemString = CType(ItemObject, DataRowView).Row(pCombo.DisplayMember).ToString()
        ElseIf GetType(DataRow).IsInstanceOfType(ItemObject) Then
          ItemString = CType(ItemObject, DataRow)(pCombo.DisplayMember).ToString()
        ElseIf GetType(KeyValuePair(Of Integer, String)).IsInstanceOfType(ItemObject) Then
          ItemString = CType(ItemObject, KeyValuePair(Of Integer, String)).Value
        ElseIf TypeOf ItemObject Is ILookup Then
          ItemString = CType(ItemObject, ILookup).Value.ToString.ToUpper
        End If

        If (ItemString.Length > 0) Then
          stringSize = SizingGraphics.MeasureString(ItemString, pCombo.Font)
          If (stringSize.Width) > thisDrowDownWidth Then
            thisDrowDownWidth = CInt(stringSize.Width)
          End If
        End If
      Next

      If (pCombo.DropDownWidth < thisDrowDownWidth) Then
        pCombo.DropDownWidth = thisDrowDownWidth
      End If

    Catch ex As Exception
    End Try

  End Sub

    ''' <summary>
    ''' Clears the combo selection.
    ''' </summary>
    ''' <param name="pCombo">The p combo.</param>
  Public Sub ClearComboSelection(ByRef pCombo As Object)

    If (TypeOf pCombo Is ComboBox) Then
      Dim ThisCombo As ComboBox = CType(pCombo, ComboBox)

      Try
        If (TypeOf ThisCombo.FindForm Is StandardFlorenceForm) Then
          If CType(ThisCombo.FindForm, StandardFlorenceForm).InUse = False Then
            Exit Sub
          End If
        End If
      Catch ex As Exception
      End Try

      Try
        If ThisCombo.Items.Count > 0 Then
          ThisCombo.SelectedIndex = 0
          ThisCombo.SelectedIndex = -1
        End If
      Catch ex As Exception
      End Try

    ElseIf (TypeOf pCombo Is Telerik.WinControls.UI.RadComboBox) Then
      Try
        CType(pCombo, Telerik.WinControls.UI.RadComboBox).SelectedValue = Nothing
      Catch ex As Exception
      End Try
    End If

  End Sub

    ''' <summary>
    ''' Sets the form tool tip.
    ''' </summary>
    ''' <param name="thisForm">The this form.</param>
    ''' <param name="FormTooltip">The form tooltip.</param>
  Public Sub SetFormToolTip(ByRef thisForm As Form, ByRef FormTooltip As ToolTip)
    Dim ToolTipDS As DSVeniceFormTooltips
    Dim SelectedToolTipRows() As DSVeniceFormTooltips.tblVeniceFormTooltipsRow
    Dim ToolTipRow As DSVeniceFormTooltips.tblVeniceFormTooltipsRow
    Dim thisControl As Object

    Try
      FormTooltip.RemoveAll()

      ToolTipDS = Me.Load_Table(RenaissanceStandardDatasets.tblVeniceFormTooltips)

      If (Not (ToolTipDS Is Nothing)) AndAlso (Not (ToolTipDS.tblVeniceFormTooltips Is Nothing)) Then
        SelectedToolTipRows = ToolTipDS.tblVeniceFormTooltips.Select("FormName='" & thisForm.Name & "'")

        If (Not (SelectedToolTipRows Is Nothing)) AndAlso (SelectedToolTipRows.Length > 0) Then
          For Each ToolTipRow In SelectedToolTipRows
            thisControl = Nothing
            Try
              thisControl = thisForm.Controls(ToolTipRow.ControlName)

              If (Not (thisControl Is Nothing)) Then
                FormTooltip.SetToolTip(thisControl, ToolTipRow.ToolTip)
              End If
            Catch ex As Exception
            End Try
          Next

        End If
      End If

    Catch ex As Exception
    End Try

  End Sub

#End Region

#Region " Geneic Combo Select Code"

  ' There Is an issue with the ComboBox control whereby if you select the combobox and type 
  ' One or more characters to select an item and then leave the combo by pressing the TAB key,
  ' Then the SelectedIndex (and other) properties revert to their starting values.
  ' This seems to be a generic .NET problem and is not specific to any of the coding in Florence.
  ' After EXTENSIVE research, I found that the SelectedIndex property reverts somehow between
  ' the 'Leave' and 'LostFocus' events. Furthermore I found that you can reset the SelectedIndex 
  ' in the 'LostFocus' event and it sticks.
  ' So this is what I have done :- Use the 'Tag' property to persist the SelectedIndex value.
  ' The Tag is initialised in the 'GotFocus' Event, Updated in the 'SelectedIndexChanged'
  ' Event and Re-set in the 'LostFocus' Event.
  '

    ''' <summary>
    ''' Handles the GotFocus event of the GenericCombo control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Public Sub GenericCombo_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs)
    Try
      If TypeOf sender Is ComboBox Then
        Dim thisCombo As ComboBox
        thisCombo = CType(sender, ComboBox)
        thisCombo.Tag = CType(sender, ComboBox).SelectedIndex
      End If
    Catch ex As Exception
    End Try
  End Sub

    ''' <summary>
    ''' Handles the SelectedIndexChanged event of the GenericCombo control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Public Sub GenericCombo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    Try
      If TypeOf sender Is ComboBox Then
        Dim thisCombo As ComboBox
        thisCombo = CType(sender, ComboBox)
        thisCombo.Tag = CType(sender, ComboBox).SelectedIndex
      End If
    Catch ex As Exception
    End Try
  End Sub

    ''' <summary>
    ''' Handles the LostFocus event of the GenericCombo control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
  Public Sub GenericCombo_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs)
    Try
      If TypeOf sender Is ComboBox Then
        Dim thisCombo As ComboBox
        thisCombo = CType(sender, ComboBox)

        If (Not (thisCombo.Tag Is Nothing)) AndAlso IsNumeric(thisCombo.Tag) AndAlso (thisCombo.Items.Count > 0) Then
          If thisCombo.SelectedIndex <> CInt(thisCombo.Tag) Then
            thisCombo.SelectedIndex = thisCombo.Tag
          End If
        End If
      End If
    Catch ex As Exception
    End Try
  End Sub

    ''' <summary>
    ''' Comboes the type of the select as you.
    ''' </summary>
    ''' <param name="sender">The sender.</param>
    ''' <param name="e">The <see cref="System.Windows.Forms.KeyEventArgs"/> instance containing the event data.</param>
  Public Sub ComboSelectAsYouType(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)
    ' *******************************************************************************
    ' This routine provides the 'Select-as-you-type' functionallity on some of the combo boxes.
    ' 
    ' It does not allow you to type an item that is not in the combo items.
    '
    ' Note that it will only work for Combo Boxes derived from a Data Table or View.
    ' *******************************************************************************

    Dim SelectString As String
    'Dim selectRow As DataRow
    'Dim selectRowView As DataRowView
    'Dim IndexCounter As Integer
    Dim ThisComboBox As ComboBox
    Dim StartingIndex As Integer = 0

    Try
      SyncLock sender

        ' Cast the sender to type Combo Box.
        ' In the event of an error, exit silently.
        Try
          ThisComboBox = CType(sender, ComboBox)
        Catch ex As Exception
          Exit Sub
        End Try

        ' Check for <Enter>

        If e.KeyCode = System.Windows.Forms.Keys.Enter Then
          ThisComboBox.SelectionStart = 0
          ThisComboBox.SelectionLength = ThisComboBox.Text.Length
          Exit Sub
        End If

        SelectString = ThisComboBox.Text

        If (ThisComboBox.Tag IsNot Nothing) AndAlso IsNumeric(ThisComboBox.Tag) AndAlso (SelectString.Length > 1) Then
          StartingIndex = CInt(ThisComboBox.Tag)
        End If

        If e.KeyCode = System.Windows.Forms.Keys.Back Then
          If SelectString.Length > 0 Then
            SelectString = SelectString.Substring(0, SelectString.Length - 1)
          End If
        End If

        If e.KeyCode = System.Windows.Forms.Keys.Down Then
          Exit Sub
        End If

        If e.KeyCode = System.Windows.Forms.Keys.Down Then
          Exit Sub
        End If

startSearch:

        ' Search the Combo Data Items for an item starting with the typed text.

        If SelectString.Length <= 0 Then
          If ThisComboBox.Items.Count > 0 Then
            ThisComboBox.SelectedIndex = 0
            ThisComboBox.SelectionStart = 0
            ThisComboBox.SelectionLength = ThisComboBox.Text.Length
          Else
            ThisComboBox.SelectedIndex = -1
          End If

          Exit Sub
        End If

        Dim IndexString As String

        Try
          Dim RIndex As Integer

          If (StartingIndex <= 0) AndAlso (ThisComboBox.Items.Count > 1000) Then
            StartingIndex = BestGuessStartingIndex(ThisComboBox, SelectString, StartingIndex)
          End If

          RIndex = ThisComboBox.FindString(SelectString, Math.Max(0, StartingIndex - 1))
          If (RIndex >= 0) Then
            If (ThisComboBox.SelectedIndex <> RIndex) Then
              IndexString = GetComboItemStringValue(ThisComboBox.Items(RIndex), ThisComboBox.DisplayMember)

              'If GetType(DataRowView).IsInstanceOfType(ThisComboBox.Items(RIndex)) Then
              '	IndexString = CType(ThisComboBox.Items(RIndex), DataRowView)(ThisComboBox.DisplayMember).ToString.ToUpper
              'ElseIf GetType(DataRow).IsInstanceOfType(ThisComboBox.Items(RIndex)) Then
              '	IndexString = CType(ThisComboBox.Items(RIndex), DataRow)(ThisComboBox.DisplayMember).ToString.ToUpper
              'ElseIf GetType(KeyValuePair(Of Integer, String)).IsInstanceOfType(ThisComboBox.Items(RIndex)) Then
              '	IndexString = CType(ThisComboBox.Items(RIndex), KeyValuePair(Of Integer, String)).Value
              'ElseIf TypeOf ThisComboBox.Items(RIndex) Is ILookup Then
              '	IndexString = CType(ThisComboBox.Items(RIndex), ILookup).Value.ToString.ToUpper
              'Else
              '	Try
              '		IndexString = CStr(ThisComboBox.Items(RIndex)).ToUpper
              '	Catch ex As Exception
              '		Exit Sub
              '	End Try
              'End If

              ThisComboBox.SelectedIndex = RIndex
              ThisComboBox.SelectionStart = SelectString.Length
              ThisComboBox.SelectionLength = IndexString.Length - SelectString.Length
            End If
            Exit Sub
          End If
        Catch ex As Exception
        End Try

        ' String not found
        SelectString = SelectString.Substring(0, SelectString.Length - 1)
        GoTo startSearch

      End SyncLock

    Catch ex As Exception
    End Try


  End Sub

    ''' <summary>
    ''' Handles the NoMatch event of the ComboSelectAsYouType control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.Windows.Forms.KeyEventArgs"/> instance containing the event data.</param>
  Public Sub ComboSelectAsYouType_NoMatch(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)
    ' *******************************************************************************
    ' This routine provides the 'Select-as-you-type' functionallity on some of the combo boxes.
    ' 
    ' It does allow you to type an item that is not in the combo items.
    '
    ' Note that it will only work for Combo Boxes derived from a Data Table or View.
    ' *******************************************************************************

    Dim SelectString As String
    'Dim selectRow As DataRow
    'Dim selectRowView As DataRowView
    'Dim IndexCounter As Integer
    Dim ThisComboBox As ComboBox
    Dim StartingIndex As Integer = 0

    Try

      ' Cast the sender to type Combo Box.
      ' In the event of an error, exit silently.
      Try
        ThisComboBox = CType(sender, ComboBox)
      Catch ex As Exception
        Exit Sub
      End Try

      ' Check for <Enter>

      If e.KeyCode = System.Windows.Forms.Keys.Enter Then
        ThisComboBox.SelectionStart = 0
        ThisComboBox.SelectionLength = ThisComboBox.Text.Length
        Exit Sub
      End If

      SelectString = ThisComboBox.Text

      If (ThisComboBox.Tag IsNot Nothing) AndAlso IsNumeric(ThisComboBox.Tag) AndAlso (SelectString.Length > 1) Then
        StartingIndex = CInt(ThisComboBox.Tag)
      End If

      If e.KeyCode = System.Windows.Forms.Keys.Back Then
        If SelectString.Length > 0 Then
          SelectString = SelectString.Substring(0, SelectString.Length - 1)
        End If
      End If

      If e.KeyCode = System.Windows.Forms.Keys.Down Then
        Exit Sub
      End If

      If e.KeyCode = System.Windows.Forms.Keys.Down Then
        Exit Sub
      End If

startSearch:

      ' Search the Combo Data Items for an item starting with the typed text.

      If SelectString.Length <= 0 Then
        If ThisComboBox.Items.Count > 0 Then
          ThisComboBox.SelectedIndex = 0
          ThisComboBox.SelectionStart = 0
          ThisComboBox.SelectionLength = ThisComboBox.Text.Length
        Else
          ThisComboBox.SelectedIndex = -1
        End If

        Exit Sub
      End If

      Dim IndexString As String

      Try
        Dim RIndex As Integer

        If (StartingIndex <= 0) AndAlso (ThisComboBox.Items.Count > 1000) Then
          StartingIndex = BestGuessStartingIndex(ThisComboBox, SelectString, StartingIndex)
        End If

        RIndex = ThisComboBox.FindString(SelectString, Math.Max(0, StartingIndex - 1))
        If (RIndex >= 0) Then
          If (ThisComboBox.SelectedIndex <> RIndex) Then
            IndexString = GetComboItemStringValue(ThisComboBox.Items(RIndex), ThisComboBox.DisplayMember)

            'If GetType(DataRowView).IsInstanceOfType(ThisComboBox.Items(RIndex)) Then
            '	IndexString = CType(ThisComboBox.Items(RIndex), DataRowView)(ThisComboBox.DisplayMember).ToString.ToUpper
            'ElseIf GetType(DataRow).IsInstanceOfType(ThisComboBox.Items(RIndex)) Then
            '	IndexString = CType(ThisComboBox.Items(RIndex), DataRow)(ThisComboBox.DisplayMember).ToString.ToUpper
            'ElseIf GetType(KeyValuePair(Of Integer, String)).IsInstanceOfType(ThisComboBox.Items(RIndex)) Then
            '	IndexString = CType(ThisComboBox.Items(RIndex), KeyValuePair(Of Integer, String)).Value
            'ElseIf TypeOf ThisComboBox.Items(RIndex) Is ILookup Then
            '	IndexString = CType(ThisComboBox.Items(RIndex), ILookup).Value.ToString.ToUpper
            'Else
            '	Try
            '		IndexString = CStr(ThisComboBox.Items(RIndex)).ToUpper
            '	Catch ex As Exception
            '		Exit Sub
            '	End Try
            'End If

            ThisComboBox.SelectedIndex = RIndex
            ThisComboBox.SelectionStart = SelectString.Length
            ThisComboBox.SelectionLength = IndexString.Length - SelectString.Length
          End If
          Exit Sub
        End If
      Catch ex As Exception
      End Try

      ' String not found
      ThisComboBox.Tag = Nothing ' Stop LotFocus Event setting the SelectedIndex
      Exit Sub

    Catch ex As Exception
    End Try

  End Sub

    ''' <summary>
    ''' Bests the index of the guess starting.
    ''' </summary>
    ''' <param name="ThisComboBox">The this combo box.</param>
    ''' <param name="SelectString">The select string.</param>
    ''' <param name="OriginalStartingIndex">Index of the original starting.</param>
    ''' <returns>System.Int32.</returns>
  Private Function BestGuessStartingIndex(ByVal ThisComboBox As ComboBox, ByVal SelectString As String, ByVal OriginalStartingIndex As Integer) As Integer

    Dim RVal As Integer = OriginalStartingIndex

    Try
      Dim TopIndex As Integer = 0
      Dim BottomIndex As Integer = ThisComboBox.Items.Count - 1
      Dim MidIndex As Integer
      Dim ThisString As String
      Dim CVal As Double

      While (BottomIndex - TopIndex) > 5
        MidIndex = CInt((TopIndex + BottomIndex) / 2)

        ThisString = GetComboItemStringValue(ThisComboBox.Items(MidIndex), ThisComboBox.DisplayMember)
        CVal = ThisString.CompareTo(SelectString)

        If (CVal = 0) Then
          RVal = MidIndex
          Exit While
        ElseIf (CVal > 0) Then
          BottomIndex = MidIndex
        Else
          TopIndex = MidIndex
        End If

        RVal = TopIndex
      End While

    Catch ex As Exception
    End Try

    Return RVal

  End Function

    ''' <summary>
    ''' Gets the combo item string value.
    ''' </summary>
    ''' <param name="pComboItem">The p combo item.</param>
    ''' <param name="DisplayMember">The display member.</param>
    ''' <returns>System.String.</returns>
  Private Function GetComboItemStringValue(ByVal pComboItem As Object, ByVal DisplayMember As String) As String

    If TypeOf pComboItem Is DataRowView Then ' GetType(DataRowView).IsInstanceOfType(ThisComboBox.Items(IndexCounter)) Then
      Return CType(pComboItem, DataRowView).Row(DisplayMember).ToString.ToUpper
    ElseIf TypeOf pComboItem Is DataRow Then ' GetType(DataRow).IsInstanceOfType(ThisComboBox.Items(IndexCounter)) Then
      Return CType(pComboItem, DataRow)(DisplayMember).ToString.ToUpper
    ElseIf TypeOf pComboItem Is KeyValuePair(Of Integer, String) Then ' GetType(KeyValuePair(Of Integer, String)).IsInstanceOfType(ThisComboBox.Items(IndexCounter)) Then
      Return CType(pComboItem, KeyValuePair(Of Integer, String)).Value.ToUpper
    ElseIf TypeOf pComboItem Is ILookup Then   'TypeOf ThisComboBox.Items(IndexCounter) Is ILookup Then
      Return CType(pComboItem, ILookup).Value.ToString.ToUpper
    Else
      Try
        Return CStr(pComboItem).ToUpper
      Catch ex As Exception
      End Try
    End If

    Return ""

  End Function

#End Region

#Region " tblSystemString Functions "

    ''' <summary>
    ''' Get_s the system string.
    ''' </summary>
    ''' <param name="pStringName">Name of the p string.</param>
    ''' <returns>System.String.</returns>
  Friend Function Get_SystemString(ByVal pStringName As String) As String

    Dim adpSystemStrings As SqlDataAdapter
    Dim tblSystemstrings As New DataTable

    adpSystemStrings = Me.MainDataHandler.Get_Adaptor("adp_GetSystemString", FLORENCE_CONNECTION, "tblSystemStrings")

    If (adpSystemStrings Is Nothing) Then
      Return ""
    End If

    Try
      SyncLock adpSystemStrings.SelectCommand.Connection
        SyncLock adpSystemStrings
          adpSystemStrings.SelectCommand.Parameters("@StringDescription").Value = pStringName
          adpSystemStrings.Fill(tblSystemstrings)
        End SyncLock
      End SyncLock
    Catch ex As Exception
      Me.LogError(Me.Name & ", Get_SystemString()", LOG_LEVELS.Error, ex.Message, "Failed to Get System String.", ex.StackTrace, True)

      Return ""
    End Try

    If (tblSystemstrings Is Nothing) OrElse (tblSystemstrings.Rows.Count <= 0) Then
      Return ""
    End If

    Try
      Return CStr(tblSystemstrings.Rows(0)("StringValue"))
    Catch ex As Exception
      Me.LogError(Me.Name & ", Get_SystemString()", LOG_LEVELS.Error, ex.Message, "Failed to Get System String.", ex.StackTrace, True)
      Return ""
    End Try

    Return ""

  End Function

    ''' <summary>
    ''' Set_s the system string.
    ''' </summary>
    ''' <param name="pStringName">Name of the p string.</param>
    ''' <param name="pStringValue">The p string value.</param>
    ''' <returns>System.String.</returns>
  Friend Function Set_SystemString(ByVal pStringName As String, ByVal pStringValue As String) As String

    Dim adpSystemStrings As SqlDataAdapter
    Dim tblSystemstrings As New DataTable

    adpSystemStrings = Me.MainDataHandler.Get_Adaptor("adp_GetSystemString", FLORENCE_CONNECTION, "tblSystemStrings")

    If (adpSystemStrings Is Nothing) Then
      Return ""
    End If

    Try
      SyncLock adpSystemStrings.InsertCommand.Connection
        SyncLock adpSystemStrings
          adpSystemStrings.InsertCommand.Parameters("@StringDescription").Value = pStringName
          adpSystemStrings.InsertCommand.Parameters("@StringValue").Value = pStringValue

          Call adpSystemStrings.InsertCommand.ExecuteNonQuery()

          Return pStringValue

        End SyncLock
      End SyncLock
    Catch ex As Exception
      Me.LogError(Me.Name & ", Set_SystemString()", LOG_LEVELS.Error, ex.Message, "Failed to Set System String.", ex.StackTrace, True)

      Return ""
    End Try

    Return ""

  End Function


#End Region

#Region " AdaptorUpdate, SQL Connection Synchlock functions."

    ''' <summary>
    ''' Adaptors the update.
    ''' </summary>
    ''' <param name="FormName">Name of the form.</param>
    ''' <param name="pAdaptor">The p adaptor.</param>
    ''' <param name="pTable">The p table.</param>
    ''' <returns>System.Int32.</returns>
  Public Function AdaptorUpdate(ByVal FormName As String, ByVal pAdaptor As SqlDataAdapter, ByVal pTable As DataTable) As Integer

    Dim thisDataRow As DataRow
    Dim LogString As String
    Dim EditState As String

    Try
      For Each thisDataRow In pTable.Rows
        If (thisDataRow.RowState = DataRowState.Added) Or (thisDataRow.RowState = DataRowState.Deleted) Or (thisDataRow.RowState = DataRowState.Modified) Then
          EditState = thisDataRow.RowState.ToString & " Record"
          LogString = Me.GetStandardLogString(thisDataRow)
          Me.LogError(FormName, LOG_LEVELS.Changes, EditState, LogString, "", False)
        End If
      Next
    Catch ex As Exception
    End Try

    Return MainDataHandler.AdaptorUpdate(pAdaptor, pTable)
  End Function

    ''' <summary>
    ''' Adaptors the update.
    ''' </summary>
    ''' <param name="FormName">Name of the form.</param>
    ''' <param name="pAdaptor">The p adaptor.</param>
    ''' <param name="pDataSet">The p data set.</param>
    ''' <returns>System.Int32.</returns>
  Public Function AdaptorUpdate(ByVal FormName As String, ByRef pAdaptor As SqlDataAdapter, ByRef pDataSet As DataSet) As Integer

    Dim thisTable As DataTable
    Dim thisDataRow As DataRow
    Dim LogString As String
    Dim EditState As String

    Try
      For Each thisTable In pDataSet.Tables
        For Each thisDataRow In thisTable.Rows
          If (thisDataRow.RowState = DataRowState.Added) Or (thisDataRow.RowState = DataRowState.Deleted) Or (thisDataRow.RowState = DataRowState.Modified) Then
            EditState = thisDataRow.RowState.ToString & " Record"
            LogString = Me.GetStandardLogString(thisDataRow)
            Me.LogError(FormName, LOG_LEVELS.Changes, EditState, LogString, "", False)
          End If
        Next
      Next
    Catch ex As Exception
    End Try

    Return MainDataHandler.AdaptorUpdate(pAdaptor, pDataSet)
  End Function

    ''' <summary>
    ''' Adaptors the update.
    ''' </summary>
    ''' <param name="FormName">Name of the form.</param>
    ''' <param name="pAdaptor">The p adaptor.</param>
    ''' <param name="pDataRows">The p data rows.</param>
    ''' <returns>System.Int32.</returns>
  Public Function AdaptorUpdate(ByVal FormName As String, ByVal pAdaptor As SqlDataAdapter, ByVal pDataRows() As DataRow) As Integer

    Dim thisDataRow As DataRow
    Dim LogString As String
    Dim EditState As String

    Try
      For Each thisDataRow In pDataRows
        If (thisDataRow.RowState = DataRowState.Added) Or (thisDataRow.RowState = DataRowState.Deleted) Or (thisDataRow.RowState = DataRowState.Modified) Then
          EditState = thisDataRow.RowState.ToString & " Record"
          LogString = Me.GetStandardLogString(thisDataRow)
          Me.LogError(FormName, LOG_LEVELS.Changes, EditState, LogString, "", False)
        End If
      Next
    Catch ex As Exception
    End Try

    Return MainDataHandler.AdaptorUpdate(pAdaptor, pDataRows)
  End Function

    ''' <summary>
    ''' Adaptors the update.
    ''' </summary>
    ''' <param name="FormName">Name of the form.</param>
    ''' <param name="pAdaptor">The p adaptor.</param>
    ''' <param name="pDataSet">The p data set.</param>
    ''' <param name="pTableName">Name of the p table.</param>
    ''' <returns>System.Int32.</returns>
  Public Function AdaptorUpdate(ByVal FormName As String, ByRef pAdaptor As SqlDataAdapter, ByRef pDataSet As DataSet, ByVal pTableName As String) As Integer

    Dim thisDataRow As DataRow
    Dim LogString As String
    Dim EditState As String

    Try
      For Each thisDataRow In pDataSet.Tables(pTableName).Rows
        If (thisDataRow.RowState = DataRowState.Added) Or (thisDataRow.RowState = DataRowState.Deleted) Or (thisDataRow.RowState = DataRowState.Modified) Then
          EditState = thisDataRow.RowState.ToString & " Record"
          LogString = Me.GetStandardLogString(thisDataRow)
          Me.LogError(FormName, LOG_LEVELS.Changes, EditState, LogString, "", False)
        End If
      Next
    Catch ex As Exception
    End Try

    Return MainDataHandler.AdaptorUpdate(pAdaptor, pDataSet, pTableName)
  End Function

    ''' <summary>
    ''' Gets the florence connection.
    ''' </summary>
    ''' <returns>System.Data.SqlClient.SqlConnection.</returns>
  Public Function GetFlorenceConnection() As System.Data.SqlClient.SqlConnection Implements RenaissanceGlobals.StandardRenaissanceMainForm.GetRenaissanceConnection

    Try
      Dim RVal As New SqlConnection(Me.SQLConnectString)
      RVal.Open()

      Return RVal
    Catch ex As Exception
    End Try

    Return Nothing
  End Function
#End Region

#Region " Error Logging "

    ''' <summary>
    ''' Logs the error1.
    ''' </summary>
    ''' <param name="p_Error_Location">The p_ error_ location.</param>
    ''' <param name="p_System_Error_Number">The p_ system_ error_ number.</param>
    ''' <param name="p_System_Error_Description">The p_ system_ error_ description.</param>
    ''' <param name="p_Error_Message">The p_ error_ message.</param>
    ''' <param name="p_Error_StackTrace">The p_ error_ stack trace.</param>
    ''' <param name="p_Show_Error">if set to <c>true</c> [p_ show_ error].</param>
    ''' <param name="p_MessageHeading">The p_ message heading.</param>
    ''' <param name="p_MessageButtons">The p_ message buttons.</param>
    ''' <param name="p_MessageBoxIcon">The p_ message box icon.</param>
    ''' <param name="p_Log_to">The p_ log_to.</param>
    ''' <returns>System.Int32.</returns>
  Public Function LogError1(ByVal p_Error_Location As String, ByVal p_System_Error_Number As Integer, ByVal p_System_Error_Description As String, Optional ByVal p_Error_Message As String = "", Optional ByVal p_Error_StackTrace As String = "", Optional ByVal p_Show_Error As Boolean = True, Optional ByVal p_MessageHeading As String = "", Optional ByVal p_MessageButtons As System.Windows.Forms.MessageBoxButtons = System.Windows.Forms.MessageBoxButtons.OK, Optional ByVal p_MessageBoxIcon As System.Windows.Forms.MessageBoxIcon = System.Windows.Forms.MessageBoxIcon.Exclamation, Optional ByVal p_Log_to As Integer = 0) As Integer Implements RenaissanceGlobals.StandardRenaissanceMainForm.LogError
    '**************************************************************************************************
    '
    '
    '**************************************************************************************************

    If (p_MessageHeading.Length <= 0) Then
      Return LogError(p_Error_Location, p_System_Error_Number, p_System_Error_Description, p_Error_Message, p_Error_StackTrace, p_Show_Error, "Florence Message", p_MessageButtons, p_MessageBoxIcon, p_Log_to)
    Else
      Return LogError(p_Error_Location, p_System_Error_Number, p_System_Error_Description, p_Error_Message, p_Error_StackTrace, p_Show_Error, p_MessageHeading, p_MessageButtons, p_MessageBoxIcon, p_Log_to)
    End If
  End Function

    ''' <summary>
    ''' Logs the error.
    ''' </summary>
    ''' <param name="p_Error_Location">The p_ error_ location.</param>
    ''' <param name="p_System_Error_Number">The p_ system_ error_ number.</param>
    ''' <param name="p_System_Error_Description">The p_ system_ error_ description.</param>
    ''' <param name="p_Error_Message">The p_ error_ message.</param>
    ''' <param name="p_Error_StackTrace">The p_ error_ stack trace.</param>
    ''' <param name="p_Show_Error">if set to <c>true</c> [p_ show_ error].</param>
    ''' <param name="p_MessageHeading">The p_ message heading.</param>
    ''' <param name="p_MessageButtons">The p_ message buttons.</param>
    ''' <param name="p_MessageBoxIcon">The p_ message box icon.</param>
    ''' <param name="p_Log_to">The p_ log_to.</param>
    ''' <returns>System.Int32.</returns>
  Public Function LogError(ByVal p_Error_Location As String, _
  ByVal p_System_Error_Number As Integer, _
  ByVal p_System_Error_Description As String, _
  Optional ByVal p_Error_Message As String = "", _
  Optional ByVal p_Error_StackTrace As String = "", _
  Optional ByVal p_Show_Error As Boolean = True, _
  Optional ByVal p_MessageHeading As String = "Florence Message", _
  Optional ByVal p_MessageButtons As System.Windows.Forms.MessageBoxButtons = MessageBoxButtons.OK, _
  Optional ByVal p_MessageBoxIcon As System.Windows.Forms.MessageBoxIcon = MessageBoxIcon.Exclamation, _
  Optional ByVal p_Log_to As Integer = 0) As Integer

    '**************************************************************************************************
    ' Public function returning public variables
    '
    ' Purpose:  Display Error messages and log to Database / File if required
    '
    ' Accepts:  p_Error_Location as String            : Location of Error (for debugging and logging purposes)
    '           p_System_Error_Number As Integer      : System 'Err'
    '           p_System_Error_Description As String  : System 'Error'
    '           p_Error_Message as String (Optional)  : Meaningful User Error message
    '           p_Error_StackTrace as String (Optional)  : StackTrace Property of Error message
    '           p_Show_Error As Boolean (Optional)    : If TRUE then a MsgBox is diplayed
    '           p_MessageHeading as String (Optional) : Title for Error message box
    '           p_MessageButtons                      : Button(s) to show on the error message
    '           p_MessageBoxIcon                      : Icon to show on the Error message
    '           p_Log_to As Integer (Optional)        : Write log conditions
    '
    ' Returns:  0 or error number on fail, only returns an error if logged to File / Table and the update fails
    '**************************************************************************************************

    ' Display Error message if required.

    If p_Show_Error = True Then
      MessageBox.Show(p_System_Error_Number.ToString & vbCrLf & _
      p_System_Error_Description & vbCrLf & _
      p_Error_Message, p_MessageHeading, p_MessageButtons, p_MessageBoxIcon, MessageBoxDefaultButton.Button1)
    End If

    ' Log to DB, if possible.

    Dim InsertCommand As New SqlCommand
    Dim myConnection As SqlConnection
    myConnection = MainDataHandler.Get_Connection(FLORENCE_CONNECTION)

    If Not (myConnection Is Nothing) Then
      Try
        InsertCommand.CommandText = "[adp_tblINVEST_ERROR_LOG_InsertCommand]"
        InsertCommand.CommandType = System.Data.CommandType.StoredProcedure
        InsertCommand.CommandTimeout = DEFAULT_SQLCOMMAND_TIMEOUT

        InsertCommand.Connection = Me.GetFlorenceConnection

        InsertCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        InsertCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@WindowsUserName", System.Data.SqlDbType.NVarChar, 100))
        InsertCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ErrorNumber", System.Data.SqlDbType.Int, 4))
        InsertCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ErrorLocation", System.Data.SqlDbType.NVarChar, 250))
        InsertCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SystemErrorDescription", System.Data.SqlDbType.NVarChar, 250))
        InsertCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@UserErrorDescription", System.Data.SqlDbType.NVarChar, 450))
        InsertCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CallStack", System.Data.SqlDbType.Text, 2147483647))

        InsertCommand.Parameters("@WindowsUserName").Value = Environment.UserName
        InsertCommand.Parameters("@ErrorNumber").Value = p_System_Error_Number
        InsertCommand.Parameters("@ErrorLocation").Value = p_Error_Location
        InsertCommand.Parameters("@SystemErrorDescription").Value = p_System_Error_Description
        InsertCommand.Parameters("@UserErrorDescription").Value = p_Error_Message
        InsertCommand.Parameters("@CallStack").Value = p_Error_StackTrace

        If (p_Error_Message.Length > 450) And (p_Error_StackTrace.Length = 0) Then
          InsertCommand.Parameters("@UserErrorDescription").Value = "See StackTrace."
          InsertCommand.Parameters("@CallStack").Value = p_Error_Message
        End If


        Try

          If (Not (InsertCommand.Connection Is Nothing)) Then
            SyncLock InsertCommand.Connection
              InsertCommand.ExecuteNonQuery()
            End SyncLock
          End If

        Catch ex As Exception

        Finally

          If (Not (InsertCommand.Connection Is Nothing)) Then
            Try
              InsertCommand.Connection.Close()
            Catch ex As Exception
            End Try
          End If
        End Try

      Catch ex As Exception
      End Try
    End If

    Return 0

  End Function

    ''' <summary>
    ''' Gets the standard log string.
    ''' </summary>
    ''' <param name="thisDataRow">The this data row.</param>
    ''' <returns>System.String.</returns>
  Public Function GetStandardLogString(ByRef thisDataRow As DataRow) As String

    Dim LogString As String
    LogString = ""

    Try
      Dim ItemCount As Integer
      Dim thisItem As Object
      Dim TableName As String
      Dim ItemName As String

      TableName = ""
      Try
        TableName = thisDataRow.Table.TableName.ToUpper
        If TableName.StartsWith("TBL") Then
          TableName = TableName.Substring(3)
        End If
      Catch ex As Exception
      End Try

      LogString = thisDataRow.RowState.ToString & ", " & thisDataRow.Table.TableName & ", "

      If (thisDataRow.RowState And DataRowState.Deleted) <> DataRowState.Deleted Then
        For ItemCount = 0 To (thisDataRow.ItemArray.Length - 1)
          thisItem = thisDataRow.ItemArray(ItemCount)
          ItemName = thisDataRow.Table.Columns(ItemCount).ColumnName
          If ItemName.ToUpper.StartsWith(TableName) Then
            ItemName = ItemName.Substring(TableName.Length)
          End If

          LogString &= ItemName & "="

          If (thisItem Is Nothing) Then
            LogString &= "Null,"
          Else
            LogString &= "`" & thisItem.ToString & "`,"
          End If
        Next
      End If
    Catch ex As Exception
    End Try

    Return LogString

  End Function

#End Region

#Region " Bug hunting "

  ' AddHandler myAdaptor.RowUpdating, AddressOf OnRowUpdating
  ' AddHandler myAdaptor.RowUpdated, AddressOf OnRowUpdated
  ' AddHandler myAdaptor.FillError, AddressOf OnRowFillError

    ''' <summary>
    ''' Called when [row updating].
    ''' </summary>
    ''' <param name="Sender">The sender.</param>
    ''' <param name="e">The <see cref="SqlRowUpdatingEventArgs"/> instance containing the event data.</param>
  Protected Shared Sub OnRowUpdating(ByVal Sender As Object, ByVal e As SqlRowUpdatingEventArgs)
    Dim a As Integer

    If Not e.Command Is Nothing Then
      a = 1
    End If

    If Not e.Errors Is Nothing Then
      a = 2
    End If
  End Sub

    ''' <summary>
    ''' Called when [row updated].
    ''' </summary>
    ''' <param name="Sender">The sender.</param>
    ''' <param name="e">The <see cref="SqlRowUpdatedEventArgs"/> instance containing the event data.</param>
  Protected Shared Sub OnRowUpdated(ByVal Sender As Object, ByVal e As SqlRowUpdatedEventArgs)
    Dim a As Integer

    If Not e.Command Is Nothing Then
      a = 1
    End If

    If Not e.Errors Is Nothing Then
      a = 2
    End If
  End Sub

    ''' <summary>
    ''' Called when [row fill error].
    ''' </summary>
    ''' <param name="Sender">The sender.</param>
    ''' <param name="e">The <see cref="FillErrorEventArgs"/> instance containing the event data.</param>
  Protected Shared Sub OnRowFillError(ByVal Sender As Object, ByVal e As FillErrorEventArgs)
    Dim a As Integer

    a = 1
  End Sub

#End Region



End Class
