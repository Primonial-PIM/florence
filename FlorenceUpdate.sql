if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[fn_tblFlorenceItems_SelectKD]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[fn_tblFlorenceItems_SelectKD]
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO




CREATE FUNCTION dbo.fn_tblFlorenceItems_SelectKD 
(
@KnowledgeDate datetime
)
RETURNS TABLE
AS
RETURN
(
SELECT 	RN ,
	AuditID ,
	ItemID ,
	Category ,
	ItemDescription ,
	UpdatePeriod ,
	DataUpdatePeriod ,
	ItemMessage ,
	ItemGroup , 
	KeyDetail , 
	ItemDiscontinued , 
	IsDataItem , 
	DataItemType , 
	UpdateStatusOnDataUpdate , 
	IsLimitItem , 
	LimitType ,
	LimitIsPercent , 
	FromLimit , 
	ToLimit , 
	LimitThreshold , 
	DateEntered ,
	DateDeleted
FROM tblFlorenceItems
WHERE RN IN ( SELECT MAX(RN) 
              FROM tblFlorenceItems 
              WHERE (((DateEntered <= @KnowledgeDate) or (DateEntered IS NULL)) OR (ISNULL(@KnowledgeDate, '1 Jan 1900') <= '1 Jan 1900')) AND 
                    (((DateDeleted > @KnowledgeDate) or (DateDeleted IS NULL)) AND (NOT ((ISNULL(@KnowledgeDate, '1 Jan 1900') <= '1 Jan 1900') AND (NOT (DateDeleted is NULL)))))
              GROUP BY ItemID )
)




GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[adp_tblFlorenceItems_DeleteCommand]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[adp_tblFlorenceItems_DeleteCommand]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[adp_tblFlorenceItems_InsertCommand]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[adp_tblFlorenceItems_InsertCommand]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[adp_tblFlorenceItems_SelectCommand]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[adp_tblFlorenceItems_SelectCommand]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[adp_tblFlorenceItems_UpdateCommand]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[adp_tblFlorenceItems_UpdateCommand]
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO




CREATE PROCEDURE dbo.adp_tblFlorenceItems_DeleteCommand 
(
	@ItemID int = 0
)
AS

	SET NOCOUNT OFF;
	UPDATE tblFlorenceItems
	SET DateDeleted = (getdate())
	WHERE ItemID = @ItemID

	IF @@ERROR = 0
	  BEGIN
	    RETURN -1
	  END
	ELSE
	  BEGIN
	    RETURN 0
	  END

	RETURN -1





GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[adp_tblFlorenceItems_DeleteCommand]  TO [Florence_Update]
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO




CREATE PROCEDURE dbo.adp_tblFlorenceItems_InsertCommand 
	(
	@ItemID int,
	@Category varchar(50),
	@ItemDescription varchar(100),
	@UpdatePeriod int, 
	@DataUpdatePeriod int = -1, 
	@ItemMessage varchar(500), 
	@ItemGroup int, 
	@KeyDetail int = 0, 
	@ItemDiscontinued int = 0, 
	@IsDataItem int = 0 , 
	@DataItemType int = 0 , 
	@UpdateStatusOnDataUpdate bit = 0, 
	@IsLimitItem int = 0, 
	@LimitType int = 1,
	@LimitIsPercent bit = 0, 
	@FromLimit float = 0, 
	@ToLimit float = 0, 
	@LimitThreshold float = 0, 
	@KnowledgeDate datetime
	)
--
-- (adp) Adaptor, tblFlorenceItems procedure
--
-- Controlled Insert of a new Florence Due Dilligence Item to the Venice (Renaissance) database
--
-- 
AS
	
	DECLARE @New_ItemID int 

	SET NOCOUNT OFF

	SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
	
	IF (ISNULL(@DataUpdatePeriod, 0) < 0)
	BEGIN
		SELECT @DataUpdatePeriod = ISNULL(@UpdatePeriod, 0)
	END
	
	BEGIN TRANSACTION

	-- Establish FundID to use. Existing or New one.
		
	IF ISNULL(@ItemID, 0) > 0 
	  BEGIN
	  SELECT @New_ItemID = @ItemID
	  END
	ELSE
	  BEGIN
	  SELECT @New_ItemID = (ISNULL(MAX(ItemID), 0)+1) FROM tblFlorenceItems
	  END
		  
	-- Insert new record.
	
	INSERT INTO tblFlorenceItems(
		ItemID ,
		Category ,
		ItemDescription ,
		UpdatePeriod ,
		DataUpdatePeriod , 
		ItemMessage ,
		ItemGroup , 
		KeyDetail , 
		ItemDiscontinued ,
		IsDataItem , 
		DataItemType , 
		UpdateStatusOnDataUpdate , 
		IsLimitItem , 
		LimitType , 
		LimitIsPercent , 
		FromLimit , 
		ToLimit ,
		LimitThreshold
		)
	VALUES (
		@New_ItemID, 
		ISNULL(@Category, '<No Category>') ,
		ISNULL(@ItemDescription, '<No Description>') ,
		ISNULL(@UpdatePeriod, 0) ,
		ISNULL(@DataUpdatePeriod, 0) ,
		ISNULL(@ItemMessage, '') ,
		ISNULL(@ItemGroup, 0) ,
		ISNULL(@KeyDetail, 0) , 
		ISNULL(@ItemDiscontinued, 0) ,
		ISNULL(@IsDataItem, 0) , 
		ISNULL(@DataItemType, 0) , 
		ISNULL(@UpdateStatusOnDataUpdate, 0) , 
		ISNULL(@IsLimitItem, 0) ,
		ISNULL(@LimitType, 1) ,
		ISNULL(@LimitIsPercent, 0) , 
		ISNULL(@FromLimit, 0) ,
		ISNULL(@ToLimit, 0) , 
		ISNULL(@LimitThreshold, 0)
		)
	
	-- Select Newly created record

	SELECT 	RN ,
		AuditID ,
		ItemID ,
		Category ,
		ItemDescription ,
		UpdatePeriod ,
		DataUpdatePeriod , 
		ItemMessage ,
		ItemGroup , 
		KeyDetail , 
		ItemDiscontinued , 
		IsLimitItem , 
		DataItemType , 
		UpdateStatusOnDataUpdate , 
		LimitType , 
		LimitIsPercent , 
		FromLimit , 
		ToLimit , 
		LimitThreshold ,
		DateEntered ,
		DateDeleted
	FROM tblFlorenceItems
	WHERE (RN = SCOPE_IDENTITY())


	IF @@ERROR = 0
	  BEGIN
	    COMMIT TRANSACTION
	    RETURN @New_ItemID
	  END
	ELSE
	  BEGIN
	    ROLLBACK TRANSACTION
	    RETURN 0
	  END

	RETURN 0




GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[adp_tblFlorenceItems_InsertCommand]  TO [Florence_Update]
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO





CREATE PROCEDURE dbo.adp_tblFlorenceItems_SelectCommand 
(
@KnowledgeDate datetime = Null
)
AS

SELECT 	RN ,
	AuditID ,
	ItemID ,
	Category ,
	ItemDescription ,
	UpdatePeriod ,
	DataUpdatePeriod ,
	ItemMessage ,
	ItemGroup , 
	KeyDetail , 
	ItemDiscontinued , 
	IsDataItem , 
	DataItemType , 
	UpdateStatusOnDataUpdate , 
	IsLimitItem , 
	LimitType , 
	LimitIsPercent , 
	FromLimit , 
	ToLimit , 
	LimitThreshold , 
	DateEntered ,
	DateDeleted
FROM fn_tblFlorenceItems_SelectKD(@KnowledgeDate)
	
RETURN -1






GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[adp_tblFlorenceItems_SelectCommand]  TO [Florence_Read]
GO

GRANT  EXECUTE  ON [dbo].[adp_tblFlorenceItems_SelectCommand]  TO [Florence_Update]
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO




CREATE PROCEDURE dbo.adp_tblFlorenceItems_UpdateCommand 
	(
	@ItemID int,
	@Category varchar(50),
	@ItemDescription varchar(100),
	@UpdatePeriod int, 
	@DataUpdatePeriod int = -1, 
	@ItemMessage varchar(500), 
	@ItemGroup int, 
	@KeyDetail int = 0, 
	@ItemDiscontinued int = 0, 
	@IsDataItem int = 0 , 
	@DataItemType int = 0 , 
	@UpdateStatusOnDataUpdate bit = 0, 
	@IsLimitItem int = 0, 
	@LimitType int = 1,
	@LimitIsPercent bit = 0, 
	@FromLimit float = 0, 
	@ToLimit float = 0, 
	@LimitThreshold float = 0, 
	@KnowledgeDate datetime
	)
--
-- (adp) Adaptor, tblFlorenceGroup procedure
--
-- Controlled Update of a FlorenceGroup in the Venice (Renaissance) database
--
-- 
AS
	
	DECLARE @RVal int
	
	EXEC @RVal = adp_tblFlorenceItems_InsertCommand
								@ItemID = @ItemID,
								@Category = @Category,
								@ItemDescription = @ItemDescription,
								@UpdatePeriod = @UpdatePeriod, 
								@DataUpdatePeriod = @DataUpdatePeriod, 
								@ItemMessage = @ItemMessage, 
								@ItemGroup = @ItemGroup, 
								@KeyDetail = @KeyDetail, 
								@ItemDiscontinued = @ItemDiscontinued, 
								@IsDataItem = @IsDataItem, 
								@DataItemType = @DataItemType, 
								@UpdateStatusOnDataUpdate = @UpdateStatusOnDataUpdate, 
								@IsLimitItem = @IsLimitItem, 
								@LimitType = @LimitType,
								@LimitIsPercent = @LimitIsPercent, 
								@FromLimit = @FromLimit, 
								@ToLimit = @ToLimit, 
								@LimitThreshold = @LimitThreshold, 
								@KnowledgeDate = @KnowledgeDate

	RETURN @RVal




GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[adp_tblFlorenceItems_UpdateCommand]  TO [Florence_Update]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[adp_tblFlorenceWorkItemsData_SelectCommand]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[adp_tblFlorenceWorkItemsData_SelectCommand]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[fn_tblFlorenceWorkItemsData_SelectKD]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[fn_tblFlorenceWorkItemsData_SelectKD]
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

CREATE FUNCTION dbo.fn_tblFlorenceWorkItemsData_SelectKD 
(
@KnowledgeDate datetime
)
RETURNS 
@tblFlorenceWorkItems  TABLE 
(
	[RN] [int] IDENTITY (1, 1), 
	[EntityID] [int] ,
	[EntityName] [varchar] (100) ,
	[EntityIsInactive] [bit] , 
	[ItemGroupID] [int] ,
	[InstrumentID] [int] ,
	[ItemID] [int] ,
	[Category] [varchar] (50) ,
	[ItemDescription] [varchar] (100) ,
	[DataUpdatePeriod] [int] ,
	[UpdateStatusOnDataUpdate] [bit] , 
	[ItemDiscontinued] [int] , 
	[IsDataItem] [int] ,
	[DataItemType] [int] , 
	[IsLimitItem] [int] ,
	[DataValueDate] datetime
	PRIMARY KEY ([EntityID], [ItemID])
)

AS
	BEGIN
	
DECLARE @tblFlorenceEntity  TABLE 
(
	[RN] [int] IDENTITY (1, 1), 
	[EntityID] [int] ,
	[EntityName] [varchar] (100) ,
	[EntityIsInactive] [bit], 
	[ItemGroupID] [int] ,
	[InstrumentID] [int] ,
	PRIMARY KEY ([EntityID])
)

DECLARE @tblFlorenceItems  TABLE 
(
	[RN] [int] IDENTITY (1, 1), 
	[ItemID] [int] ,
	[Category] [varchar] (50) ,
	[ItemDescription] [varchar] (100) ,
	[DataUpdatePeriod] [int] ,
	[UpdateStatusOnDataUpdate] [bit] , 
	[ItemGroup] [int] ,
	[ItemDiscontinued] [int] , 
	[IsDataItem] [int] ,
	[DataItemType] [int] , 
	[IsLimitItem] [int] ,
	PRIMARY KEY ([ItemID])
)

DECLARE @tblFlorenceDataValues  TABLE 
(
	[RN] [int] IDENTITY (1, 1), 
	[DataEntityID] [int],
	[DataItemID] [int],
	[DataValueDate] datetime, 
	PRIMARY KEY ([DataEntityID], [DataItemID])
)

DECLARE @tblFlorenceALLWorkItems  TABLE 
(
	[RN] [int] IDENTITY (1, 1), 
	[EntityID] [int] ,
	[EntityName] [varchar] (100) ,
	[EntityIsInactive] [bit] , 
	[ItemGroupID] [int] ,
	[InstrumentID] [int] ,
	[ItemID] [int] ,
	[Category] [varchar] (50) ,
	[ItemDescription] [varchar] (100) ,
	[DataUpdatePeriod] [int] ,
	[UpdateStatusOnDataUpdate] [bit] , 
	[ItemDiscontinued] [int] , 
	[IsDataItem] [int] ,
	[DataItemType] [int] , 
	[IsLimitItem] [int] ,
	PRIMARY KEY ([EntityID], [ItemID])
)


INSERT INTO @tblFlorenceEntity(EntityID, EntityName, EntityIsInactive, ItemGroupID, InstrumentID)
SELECT EntityID, EntityName, EntityIsInactive, ItemGroupID, InstrumentID 
FROM fn_tblFlorenceEntity_SelectKD(@KnowledgeDate)

INSERT INTO @tblFlorenceItems(ItemID, Category, ItemDescription, DataUpdatePeriod, UpdateStatusOnDataUpdate, ItemGroup, ItemDiscontinued, IsDataItem, DataItemType, IsLimitItem)
SELECT ItemID, Category, ItemDescription, DataUpdatePeriod, UpdateStatusOnDataUpdate, ItemGroup, ItemDiscontinued, IsDataItem, DataItemType, IsLimitItem 
FROM fn_tblFlorenceItems_SelectKD(@KnowledgeDate)

INSERT INTO @tblFlorenceDataValues(DataEntityID, DataItemID, DataValueDate)
SELECT DataEntityID, DataItemID, DataValueDate 
FROM fn_tblFlorenceDataValues_LatestValues(Null, @KnowledgeDate)

INSERT INTO @tblFlorenceALLWorkItems(EntityID, EntityName, EntityIsInactive, ItemGroupID, InstrumentID, ItemID, Category, ItemDescription, DataUpdatePeriod, UpdateStatusOnDataUpdate, ItemDiscontinued, IsDataItem, DataItemType, IsLimitItem)
	SELECT
		Entity.EntityID ,
		Entity.EntityName ,
		Entity.EntityIsInactive ,
		Entity.ItemGroupID ,
		Entity.InstrumentID, 
		Items.ItemID ,
		Items.Category ,
		Items.ItemDescription ,
		Items.DataUpdatePeriod , 
		Items.UpdateStatusOnDataUpdate ,
		Items.ItemDiscontinued , 
		Items.IsDataItem , 
		Items.DataItemType , 
		Items.IsLimitItem
	FROM 
		@tblFlorenceEntity Entity INNER JOIN
		@tblFlorenceItems Items on (Entity.ItemGroupID = Items.ItemGroup)

INSERT INTO @tblFlorenceWorkItems(
		EntityID ,
		EntityName , 
		EntityIsInactive , 
		ItemGroupID ,
		InstrumentID ,
		ItemID ,
		Category ,
		ItemDescription ,
		DataUpdatePeriod ,
		UpdateStatusOnDataUpdate , 
		ItemDiscontinued , 
		IsDataItem ,
		DataItemType ,
		IsLimitItem , 
		DataValueDate
		)
	SELECT
		Items.EntityID ,
		Items.EntityName ,
		Items.EntityIsInactive , 
		Items.ItemGroupID ,
		Items.InstrumentID , 
		Items.ItemID ,
		Items.Category ,
		Items.ItemDescription ,
		Items.DataUpdatePeriod ,
		Items.UpdateStatusOnDataUpdate , 
		Items.ItemDiscontinued , 
		Items.IsDataItem , 
		Items.DataItemType , 
		Items.IsLimitItem , 
		ISNULL(DataValue.DataValueDate, '1 Jan 1900')
	FROM 
		@tblFlorenceALLWorkItems Items LEFT JOIN
		@tblFlorenceDataValues DataValue on (Items.EntityID = DataValue.DataEntityID AND Items.ItemID = DataValue.DataItemID)
	WHERE (Items.EntityIsInactive = 0) AND ((Items.ItemDiscontinued = 0) OR (ISNULL(DataValue.DataValueDate, '1 Jan 1900') > '1 Jan 1900'))
	ORDER BY Items.EntityID, Items.ItemID
	

	RETURN
	END


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE PROCEDURE dbo.adp_tblFlorenceWorkItemsData_SelectCommand 
(
@KnowledgeDate datetime = Null
)
AS

SELECT 	RN ,
		EntityID ,
		EntityName , 
		EntityIsInactive , 
		ItemGroupID ,
		InstrumentID ,
		ItemID ,
		Category ,
		ItemDescription ,
		DataUpdatePeriod ,
		UpdateStatusOnDataUpdate , 
		ItemDiscontinued , 
		IsDataItem ,
		DataItemType , 
		IsLimitItem , 
		DataValueDate ,
		IsDateOverdue = CASE ItemDiscontinued
			WHEN 0 THEN dbo.fn_IsDateOverdue(DataValueDate, GETDATE(), DataUpdatePeriod, 0, @KnowledgeDate)
			ELSE 0
			END
FROM fn_tblFlorenceWorkItemsData_SelectKD(@KnowledgeDate)
	
RETURN -1

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

